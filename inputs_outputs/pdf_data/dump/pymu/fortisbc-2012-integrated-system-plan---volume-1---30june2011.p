��-      �helpers.pdf_toolbox��PymuDocument���)��}�(�document_name��vC:\Uqar\repos\emh-uqar-001\inputs_outputs\pdf_sources\fortisbc-2012-integrated-system-plan---volume-1---30june2011.pdf��sub_dir��pymu��extractor_name��PyMu��_PymuDocument__tables�]�(h �CustomTable���)��}�(�_CustomTable__columns_type�N�	bbox_list�]�((K �numpy.core.multiarray��scalar����numpy��dtype����f8�����R�(K�<�NNNJ����J����K t�bC   `��X@���R�G@�      G@��     t�(hhC   ����?���R�hhC   �J|B@���R�G@�      G@��     t�(hhC   �Z�@���R�hhC   �o:@���R�G@�      G@��     t�(hhC   �=�@���R�hhC   ��XA@���R�G@�      G@��     t�(hhC   �+@���R�hhC    Q�@���R�G@�      G@��     t�e�header�]��rows�]�(]�(N�4.1��Custon�K e]�(N�4.2��Provin�K e]�(NN�4.2.1�Ne]�(NN�4.2.2�Ne]�(NN�4.2.3�Ne]�(NN�4.2.4��11�e]�(NN�4.2.5��11�e]�(N�4.3��Feder:�K e]�(NN�4.3.1��12�e]�(NN�4.3.2��13�e]�(N�4.4��First��13�e]�(N�4.5��Healtk��16�e]�(NN�4.5.1��17�e]�(NN�4.5.2��19�e]�(N�4.6��Custon��23�e]�(NN�4.6.1��24�e]�(�5��PUE��BLIC A��28�e]�(N�5.1��Public��28�e]�(K �5.2��First��30�e]�(N�5.3��Consl��30�e]�(�1��1��1�e]�(�2��In its��Un its 2012 Integrated System Plan (2012 ISP), FortisBC Inc. (FortisBC or the Company)�e]�(�3��outline��Vutlines its long-term strategic direction in the areas of capital, resource and energy�e]�(�4��conse��conservation:�e]�(�5��Volum��	Volume 1:�e]�(�6��1��1_�e]�(�7�N�Pprovides an overview of major factors in the external environment that influence�e]�(�8�N�#FortisBC's planning and operations:�e]�(�9�NK e]�(�10�N�Qstakeholders were provided with an opportunity to learn about;, and provide input�e]�(�11�N�0into, the 2012 ISP during its development phase �e]�(�12��2�K e]�(�13�N�Oassets and infrastructure, including Generation, Transmission and Distribution,�e]�(�14�N�HTelecommunications, and General Plant components comprised of Buildings,�e]�(�15�NK e]�(�17��1 .��1_�e]�(�19�N�Lof new power resources in order to ensure that the actions the Company takes�e]�(�20�NK e]�(�21��2_��2�e]�(�22�N�OCompanys plan on how to fulfill provincial policy and enabling regulations that�e]�(�23�N�Fplaces demand side management as the priority resource to meet growing�e]�(�24�N�'electricity demand in British Columbia:�e]�(�25��The 20��QThe 2012 ISP is being filed concurrently with, and provides the long term context�e]�(�26��FortisF��FortisBC's 2012�e]�(�27��Comp=��Company's 2012�e]�(�28��Reven��ORevenue Requirements and the 2012 Integrated System Plan are referred to as the�e]�(�29��Applic��Application:�e]�(�30��As des��OAs described in Tab 8, Approvals Sought and Proposed Regulatory Process, of the�e]�(�31��Applic��SApplication, the Company seeks the Commission's acceptance under section 44.1(6) of�e]�(NN�PAGE�e]�(�1��3��3�e]�(�2��In 200��In 2004,the�e]�(�3��Transr��ETransmission and Distribution System Development Plan (the 2005 SDP):�e]�(�4��Fortisb��CFortisBC has seen significant changes in its operating environment:�e]�(�5��transfc��Utransformations occurred within FortisBC as an organization, the environment in which�e]�(�6��the util��>the utility operates has also changed and continues to change.�e]�(�7��respor��"respond to those external changes.�e]�(�8��In this��KIn this section, the Company will describe the benefits from the Company' s�e]�(�9��organi��Xorganizational changes in recent years, while the external operating environment will be�e]�(�0��detaile��$detailed in the following Section 4.�e]�(�1��In 200��AIn 2004, FortisBC became a wholly-owned subsidiary of Fortis Inc.�e]�(�2��Compz��ZCompany has begun to realize benefits as a result of its relationships with its parent and�e]�(�3��	affiliate�K e]�(�4�N�JStrong Relationship with Customers, the Regulator, First Nations and Other�e]�(NN�Olanciiuiugi 0,�e]�(�6�N� Streamlined Corporate Functions;�e]�(�0�NK e]�(�7�N�Access to Capital;�e]�(�8�N�Customer Satisfaction; and�e]�(�9�N�1Leveraged Purchasing Power of Goods and Services.�e]�(NNK e]�(�0��Repat�� Repatriating Corporate Functions�e]�(�1��One O1��WOne of tne priorities tor tne Company peginning In 2004 was to repatriate tne corporate�e]�(�2��tunctio��3tunctions OT tne pusiness t0 tne service territory.�e]�(�3��comple��complete py Zuub:�e]�(�4��Tunctio��RTunctions incluaing cuslomer senvice, Inormation systeMS, Iiance, numan resources,�e]�(�I��lleIaI��Ldn�e]�(�6��Capite��Capital Investment�e]�(N�duNSlC��oubslallial Capllal mnvesiiieit�e]�(�0��Ogi VCC��Ogi Vicg lmougioul lg yumpany ?�e]�(�9��approx��Wapproximateiy *(uU million In new or upgradea generation, transmissioniaistribution ana�e]�(�J��ycnCI C��ycGal�e]�(�JU��yonicia��y*icai�e]�(NN�PAGE 4�e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�6�K e]�(�7�K e]�(�7�K e]�(NK e]�(�8�K e]�(�9�K e]�(�9�K e]�(�0�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(NK e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�10�K e]�(�11�K e]�(�12�K e]�(�13�K e]�(�14�K e]�(�15�K e]�(�16�K e]�(�17�K e]�(�18�K e]�(�19�K e]�(�It�K e]�(�20�K e]�(�21�K e]�(�22�K e]�(�22�K e]�(�23�K e]�(�23�K e]�(�24�K e]�(�25�K e]�(�26�K e]�(�27�K e]�(�28�K e]�(�29�K e]�(�30�K e]�(�31�K e]�(�32�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011��page_number�K�title��helpers.table_toolbox��
TableTitle���)��}�(j�  �FBCI. ORTISNC�j�  K�bbox�(G@R      G@E
��   G@bZ�@   G@K5��   t�ububh)��}�(hNh]�(G@V��   G@R��    G@�\     G@���   t�ah?]�X  4.5 Health and Safety, Security, and the Environment Health, safety, security and environment requirements are another external factor that FortisBC has to consider and address when delivering electricity to the communities it serves. Health, safety, security and environment requirements are constantly evolving because of new legislation or more recent industry best practices. FortisBC uses a Safety and Environment management System to systematically address both safety and environmental risks associated with the construction, operation and maintenance of infrastructure.. FortisBC places significant emphasis on its employees’ health and safety. In the last decade, FortisBC has made steady gains in health and safety performance, as reflected in Figure 4.5 below. The figure illustrates a history of accident frequency and severity, as defined by the Canadian Electricity Association Standard for Recording and Measuring Occupational Injury / Illness Experience and Transportation Incidents. Figure 4.5 – FortisBC Injury Frequency and Severity�ahA]�(]�X  4.5 Health and Safety, Security, and the Environment
Health, safety, security and environment requirements are another external factor that
FortisBC has to consider and address when delivering electricity to the communities it
serves. Health, safety, security and environment requirements are constantly evolving
because of new legislation or more recent industry best practices. FortisBC uses a
Safety and Environment management System to systematically address both safety and
environmental risks associated with the construction, operation and maintenance of
infrastructure..
FortisBC places significant emphasis on its employees’ health and safety. In the last
decade, FortisBC has made steady gains in health and safety performance, as reflected
in Figure 4.5 below. The figure illustrates a history of accident frequency and severity,
as defined by the Canadian Electricity Association Standard for Recording and
Measuring Occupational Injury / Illness Experience and Transportation Incidents.
Figure 4.5 – FortisBC Injury Frequency and Severity�a]�X'  60.00 7.00
srekrow
6.00 50.00 etaaR
5.00
40.00
001 ycneuqerF
4.00
30.00 rep
3.00
syad
20.00
2.00
yrujnI
10.00 tsoL
1.00
llA-RFIA
0.00 0.00 -
RSI
2002 2003 2004 2005 2006 2007 2008 2009 2010
ISR 22.64 53.49 15.44 2.70 40.17 11.83 23.37 23.43 5.82
AIFR 6.36 6.01 4.77 2.02 1.80 1.71 2.87 1.41 1.72�a]�X�  Note: Performance statistics based on a January to December calendar year.
As part of its continual improvement process, the Company periodically engages a
certified independent auditor to audit the Company’s safety system. These audits and
their results are described in the Company’s 2011 Safety Plan, included as Appendix K
of the 2012 – 2013 Revenue Requirements Application.�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kj�  j�  )��}�(j�  �FBCI. ORTISNC�j�  Kj�  (G@V�     G@E
��   G@d��@   G@K5��   t�ububh)��}�(hNh]�(G@[&f`   G@w�P   G@|���   G@��f`   t�ah?]�(� �j  j  j  j  j  j  j  j  j  ehA]�(]�(Nj  NNNNNNNNe]�(Nj  NNNNNNNNe]�(Nj  NNNNNNNNe]�(Nj  NNNNNNNNe]�(Nj  NNNNNNNNe]�(Nj  NNNNNNNNe]�(N�2002��2003��2004��2005��2006��2007��2008��2009��2010�e]�(�ISR��22.64��53.49��15.44��2.70��40.17��11.83��23.37��23.43��5.82�e]�(�AIFR��6.36��6.01��4.77��2.02��1.80��1.71��2.87��1.41��1.72�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kj�  j�  )��}�(j�  �6Figure 4.5 – FortisBC Injury Frequency and Severity �j�  Kj�  (G@eP��   G@u�J�   G@{���   G@vX�    t�ububh)��}�(hNh]�(hhC   `Vl�?���R�hhC   ��.@���R�G@�      G@��     t�ah?]�hA]�(]�(�1�N�0�e]�(�2�N�Stream;�e]�(�3�N�0�e]�(�4�N�Regulation; and�e]�(�5�N�0�e]�(�6��4.6��Customer Expectations�e]�(N�	The Canac��Hian Electricity Association (CEA) describes the changing environment for�e]�(�8��electrical ir��frastructure development:�e]�(�9��Most��Garts of Canada have not had significant new electricity development for�e]�(�10��20 yea��$S Or more. Infrastructure across the�e]�(�11��useful��Cife and requires refurbishment or replacement: During the same time�e]�(�12��period,��?the environmental assessment procedures and public consultation�e]�(�13��require��Aments have changed significantly. The advent of social media also�e]�(�14��means��Cthat any project opponents are able to communicate more effectively�e]�(�15��and bu��Ild support for their causes:�e]�(�16��that ha��Bve been cancelled due to a lack of public acceptance. All forms of�e]�(K �electric��Iity development have seen significant public protest: wind & solar farms,�e]�(K �coal plz��ants,�e]�(�Ao��Uoui��A7iLd,�e]�(�20��puuny U��L�e]�(�21��Despite the��se challenaes,�e]�(�22��
opinion of��heir electrical utilitv (CEA�e]�(�23��
for custom��er satisfaction include:�e]�(�24��The��;�e]�(�25��The��6perceptions that the company cares about its customers�e]�(�26��The��5perception that the company is efficient and well-run�e]�(�27��The��Fperception that the company listens to and acts upon customer concerns�e]�(N�5��_lectricity Association (2009). Building Tomorrow's Electricity System: Electricity Fundamentals�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kj�  Nubh)��}�(hNh]�(hhC   ���?���R�hhC   ��'@���R�G@�      G@��     t�ah?]�(�1�NK ehA]�(]�(j�  NK e]�(�2��
In additic�K e]�(�3��	has prov �K e]�(�4��	has offer�K e]�(�5��concerns�K e]�(�6��Septemb�K e]�(�7�NK e]�(�8��The mos�K e]�(�9��	collectec�K e]�(�10��depth fe�K e]�(�11��houses�K e]�(�12��Super G�K e]�(�13�NK e]�(�14�NK e]�(N�6�K e]�(�15�NK e]�(�O��tl�K e]�(�17�NK e]�(�18�NK e]�(�19��Conserv�K e]�(�20�NK e]�(�21�NK e]�(�22�NK e]�(�23�NK e]�(�24��Rates�K e]�(�25�NK e]�(�26�NK e]�(�27�NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K j�  Nubh)��}�(hNh]�(hhC    �0T@���R�hhC   ��{@���R�G@�      G@��     t�ah?]�(K �Asse��
Management��ent�K ehA]�(]�(K j  j  j  K e]�(K �1.1.1��!How FortisBC Manages Assets Today��rtisBC Manages Assets Today�K e]�(K �1.1.2��*Asset Management Development and Transitio��&anagement Development and Transition _��4�e]�(K �1.1.3��Recommendations��
nendations��5�e]�(K �Smar��Grid�NK e]�(K �tem D��velopment Planning��ent Planning��9�e]�(K �Load��Forecastina��14�K e]�(K �2.1.1��#Kelowna Area Spatial Load Forecast.��1 Area Spatial Load Forecast.�K e]�(K �Proie��t Estimation Methodoloqv _��on�K e]�(K �2.2.1��Accounting Practices ��ing Practices.�K e]�(K �2.2.2��Costs of Removal_��{�K e]�(K �Fortis��3C Svstem Description��n Description�K e]�(K �2.3.1��North Okanaqan Reaion��kanaqan Reaion�K e]�(K �2.3.2��South Okanaqan Reaion_��kanaqan Reaion_��23�e]�(K �2.3.3��Kootenav / Boundarv Reaions�� / Boundarv Reaions��27�e]�(K �Gene��ation�NK e]�(K �2.41��Overview of FortisBC Generation��w of FortisBC Generation��34�e]�(K �2.4.1��Overview of FortisBC Generation��w of FortisBC Generation��34�e]�(K �2.4.2��Canital Exnenditures��Expenditures�K e]�(K �2.4.2��Capital Expenditures��Expenditures��36�e]�(K �2.4.3��Reliabilitv��tv��37�e]�(K �2.4.3��Reliability _��ty��37�e]�(K �2.44��.Unner Bonninaton Units 1 to Unit 4 (The Old PT��+onninaton Units 1 to Unit 4 (The Old Plant)��38�e]�(K �2.4.4��Upper Bonnington Units��'onnington Units��38�e]�(K �2.4.5��Phvsical Civil Infrastructure��I Civil Infrastructure��39�e]�(K �Gene��Ation Canital Proiects��ital Proiects�K e]�(K �CTO��O��Tic��Se�e]�(K �L rI>1��	TT] #ivui��ANrrceccccT ��57�e]�(K �2 53��Sd��Sd��63�e]�(K �2 54��#AII Plants Minor Sustainina Canital��ts Minor Sustainina Canital�K e]�(K �Netw�NN�68�e]�(K �2 6 1��!Previous Svstem Develonment Plans��3 Svstem Develonment Plans��68�e]�(K �2 6 2��Se��1�K e]�(K �Trans��nission and Stations��nd Stations�K e]�(K �2 7 1��Trancmiccion Planning��iccion Planning��71�e]�(K �2.7.2��-The FortisBC Electricitv Transmission Network��&tisBC Electricitv Transmission Network��71�e]�(K N�E��E�Ne]�(K NN�P��PAGE ii�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K#j�  Nubh)��}�(hNh]�(hhC    �aP@���R�hhC    8@���R�G@�      G@��     t�ah?]�hA]�(]�(N�2.7.3��FERC Order 890��77�e]�(N�2.7.4��+Transmission Planning Reliability Standards�K e]�(N�2.7.5��Transmission Planning Studies.�K e]�(N�2.7.6��Reliability Studies_�K e]�(N�2.7.7��5Resource Planning Interrelationship with Transmission�K e]�(N�2.7.8��$FortisBC Transmission Configurations�K e]�(�2.8��Transn��+ission and Stations Growth Capital Projects�K e]�(K �2.8.1��1Okanagan Transmission Reinforcement Project (OTR)�K e]�(K �2.8.2��$Ellison to Sexsmith Transmission Tie�K e]�(K �2.8.3��;Grand Forks Terminal Transformer Addition and High-Capacity�K e]�(K �2.8.4��*Kelowna Bulk Transformer Capacity Addition�K e]�(K �2.8.5��South Okanagan Area Upgrade.�K e]�(K �2.8.6��Meshing Kelowna Loop_�K e]�(K �2.8.7��)Summerland Substation Transformer Upgrade�K e]�(K �2 8 8��Beaver Vallev South Solution�K e]�(K �2.8.9��,RG Anderson Distribution Transformer Uparade�K e]�(K �2.8.10��DG Bell Terminal Uparades�K e]�(K �2.8.11��(Vaseux Lake Third 500/230 kV Transformer�K e]�(K �L.o��Vas6ux�K e]�(N�2.8.12��Boundary Area Supply�K e]�(N�L.o.i2��
Duullual y�K e]�(N�2.8.13��"Reconductor 31 Line (Creston Area)�K e]�(N�L.o��necui Iuuclui�K e]�(K �2.8.14��"Stoney Creek Transformer Addition_�K e]�(N�L.o.��oluiiey�K e]�(N�2.8.15��4Playmor Substation Distribution Transformer Addition��116�e]�(K �L.o��	r layiiol�K e]�(N�2.8.16�� Reconductor 50 Line (Eight Spans�K e]�(N�L.o��B�K e]�(N�2.8.17��5Reconductor 50 Line (Remaining Recreation to Saucier)��117�e]�(K �2.8.18��9Reconductor 50 Line (Remaining FA Lee to Springfield Tap)�K e]�(K �2.8.19��8Reconductor 51 Line and 60 Line (DG Bell to OK Mission) �K e]�(N�2.8.20��/Reconductor 54 Line (Dg Bell to Black Mountain)�K e]�(N�2.8.21��.Kelowna Area Transformation Capacity Additions�K e]�(K �2.8.22�� New Central Okanagan Substation_�K e]�(K �2.8.23��Creston Area Capacity Increase�K e]�(K �2.8.24��RG Anderson�K e]�(�2.9��Transn��ission Sustainment_�K e]�(N�2.9.1��&Transmission Line Condition Assessment�K e]�(N�2.9.2�� Transmission Line Rehabilitation��129�e]�(N�2.9.3��Transmission Line Urgent Repair��130�e]�(N�2.9.4��Transmission Line Right of��130�e]�(NNN�DAcc#=�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K$j�  Nubh)��}�(hNh]�(hhC   �L�]@���R�hhC   �L�B@���R�G@�      G@��     t�ah?]�(K �2.9.5��Transmission Line Rebuild.�K ehA]�(]�(K jN  jO  K e]�(K �Station��IS Sustainment�K e]�(K �2.10.1��*Environmental Compliance (PCB Mitigation)_�K e]�(K �2.10.2��Station Urgent Repairs_�K e]�(K �2.10.3��/Station Assessment and Minor Planned Projects _��140�e]�(K �2.10.4��Specific Station Projects�K e]�(K �2.10.5��Transformer Replacements�K e]�(K �2.10.6��0Distribution Substation Transformer Replacements�K e]�(K �ributio��n�K e]�(K �ibution��Voltage Conversion�K e]�(K �Distribl��ution Growth�K e]�(K �3.1.1��New Connects System Wide�K e]�(K �3.1.2��Small Growth Projects.�K e]�(K �3.1.3��Distribution Unplanned Growth�K e]�(K �3.1.4��2Glenmerrv Feeder 2 to Glenmerrv Feeder 1 Tie Line_�K e]�(K �3.1.5��%Ellison Feeder 2 to Sexsmith Feeder 1�K e]�(K �3.1.6��Hollvwood Feeder 5 Uparades�K e]�(K �31 7��"Kaleden Feeder 1 Canacitv Unarades��165�e]�(K �3.1.7��"Kaleden Feeder 1 Capacity Upgrades�K e]�(K �3 1 8��$Grand Forks Terminal Feeder Addition��166�e]�(K �3.1.8��$Grand Forks Terminal Feeder Addition�K e]�(K �3 1.9��(Kettle Vallev to Nk'Min Distribution Tie��167�e]�(K �3.1.9��(Kettle Valley to Nk'Mip Distribution Tie��167�e]�(K �3.1.10��DG Bell Feeder Addition��167�e]�(K �3.1.10��DG Bell Feeder Addition��167�e]�(K �Distribl��ution Sustainment��168�e]�(�3.22��Distribl��ution Sustainment��168�e]�(K �321��:41 Line Salvaae and Distribution Underbuild Rehabilitation��168�e]�(N�3.2.1��<41 Line Salvage and Distribution Underbuild Rehabilitation _�K e]�(K �3 9 9��n��169�e]�(K �Oil'L��Diouinulionn��1 0o�e]�(K �O.L.o��Dloli inulioi|��IU�e]�(N�O.l.t��Diouinulioi|��71�e]�(K �0.l.o��Diouiinulioin��E�e]�(N�Oil.o�NK e]�(N�O.L'i��Diouinulioi|��I / O�e]�(�Talc��L.l.0��LinvMnoinnoinuI��17a�e]�(K �3 C+__��Tn5��17a�e]�(K �dyjicii��
Ovgi ViGVV��47 4�e]�(K �Uunioi��IL��1 O�e]�(K �Tl'I��I�� / O�e]�(K �T..c��iVu��1 / 0�e]�(K NN�PAGE iv�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K%j�  Nubh)��}�(hNh]�(hhC    � ^@���R�hhC    L�B@���R�G@�      G@��     t�ah?]�hA]�(]�(K �4.2.3��Kelowna Grid Stability _��178�e]�(K �4.2.4��$Telecommunications Speed Constraints�K e]�(K �4.2.5��Location and Feeder Coverage��179�e]�(K �4.2.6��#Mobile Voice Shared Use with SCADA:��180�e]�(K �4.2.7��(Dial Access to Transmission substations_��180�e]�(K �SCAC��6A, Telecommunications, Protection and Control Projects�K e]�(K �4.3.1��Growth Projects_�K e]�(K �4.3.2��Sustainment Projects�K e]�(K �neral��ant�K e]�(K �Koote��!ay Long Term Facilities Strategy.�K e]�(K �Trail��ffice Lease Purchase�K e]�(K �Okan��gan Long Term Solution�K e]�(K �Centr��1�K e]�(K �Adva��ced Metering Infrastructure�K e]�(K �Inforr��ation Systems Projects.��201�e]�(K �5.6.1��Infrastructure Sustainment_�K e]�(K �5.6.2��#Desktop Infrastructure Sustainment:��203�e]�(K �5.6.3��Application Enhancements��204�e]�(K �5.6.4��Application Sustainment ��205�e]�(N�5.6.5��!PowerSense DSM Reporting Software��205�e]�(K �Vehic��3S��206�e]�(�5.8��Metem��ng Changes.��207�e]�(�5.9��Telec��mmunications��207�e]�(K �)��gs��208�e]�(�5.1��1��Ire and Fixtures��208�e]�(�5.13��2��and Equipment��209�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K&j�  Nubh)��}�(hNh]�((hhC   ���A@���R�hhC   @-[@���R�G@�      G@��     t�(hhC    XE7@���R�hhC    �51@���R�G@�      G@��     t�eh?]�hA]�(]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(�Table 2.5.3��.65�e]�(�Table 2.5.4��.67�e]�(�Table 2.6.1�K e]�(�Table 2.7.22�K e]�(�Table 2.8 (a)�K e]�(�Table 2.8 (a)�K e]�(�Table 2.8.3�K e]�(�Table 2.8.4�K e]�(�Table 2.8.5.4�K e]�(�*Table 2.8.10 - DG Bell Terminal Upgrades _�K e]�(�	Table 2.9�K e]�(�Table 2.9.1�K e]�(�Table 2.9.2�K e]�(�Table 2.9.3�K e]�(�Table 2.9.4��130�e]�(�Table 2.9.5 (a)�K e]�(�Table 2.9.5 (b)�K e]�(�T anig��iOo�e]�(�-Table 2.9.5 (c) - 21-24 Line Expenditure Plan�K e]�(�anig��I Ou�e]�(�?Table 2.9.5 (d) - 6 Line/26 Line River Crossing Reconfiguration�K e]�(�Tanig�K e]�(�3Table 2.9.5 (e) - 19 Line / 29 Line Reconfiguration�K e]�(�anig�K e]�(�6Table 2.9.5 (f) - 30 Line Lake Crossing Rehabilitation�K e]�(�
Table 2.10�K e]�(�Table 2.10.2�K e]�(�Table 2.10.3�K e]�(�Table 2.10.4�K e]�(�Table 2.10.4.4�K e]�(�	Table 3.0�K e]�(�Table 3.1.1�K e]�(�Table 3.1.2�K e]�(�Table 3.1.3�K e]�(�	Table 3.2�K e]�(�Table 3.2.2��170�e]�(�Table 3.2.3��171�e]�(N�PAGE vii�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K'j�  j�  )��}�(j�  �List of Appendices �j�  K'j�  (G@o��    G@T'��   G@wج    G@X�Ѡ   t�ububh)��}�(hNh]�((hhC   @��,@���R�hhC    9uJ@���R�G@�      G@��     t�(hhC   ��R;@���R�hhC   ��U;@���R�G@�      G@��     t�(hhC   ��)�?���R�hhC   ���1@���R�G@�      G@��     t�(hhC   �57�?���R�hhC    hr-@���R�G@�      G@��     t�eh?]�hA]�(]�(K �172�e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K �2209�e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K �84�e]�(K �	PAGE viii�e]�(�Figure 2.7.6 (d)�K e]�(�Figure 2.8.2 (a)�K e]�(�Figure 2.8.2 (b)�K e]�(�Figure 2.8.3 (a)�K e]�(�Figure 2.8.3 (b)�K e]�(�Figure 2.8.3 (c)�K e]�(�Figure 2.8.5�K e]�(�Figure 2.8.7�K e]�(�Figure 2.8.8 (a)�K e]�(�7Figure 2.8.8 (b) - Beaver Valley South Solution (after)�K e]�(�Figure 2.8.16�K e]�(�Figure 2.28.22�K e]�(�Fiqure 2.8.22�K e]�(�Fiqure 2.10.4.5�K e]�(�Figure 2.10.4.6�K e]�(�Figure 2.10.4.7�K e]�(�Figure 2.10.5 (a)�K e]�(�Figure 2.10.5 (a)�K e]�(�Figure 2.10.5 (b)�K e]�(�Figure 2.10.6 (a)�K e]�(�Figure 2.10.6 (b)�K e]�(�Figure 3.1.4�K e]�(�Figure 3.1.5�K e]�(�Figure 3.1.6�K e]�(�Figure 3.1.7�K e]�(�Figure 3.1.8�K e]�(�Figure 4.2.1�K e]�(�Figure 4.2.3�K e]�(�Figure 4.3.1.1��186�e]�(�Figure 4.3.2.5��.194�e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2��Rsubstation CMMS was approved by Commission Order G-52-05. This system tracks basic�e]�(�2��Rsubstation CMMS was approved by Commission Order G-52-05. This system tracks basic�e]�(N�PAGE 3�e]�(�1��Sada. In at least one instance a full system implementation has been endorsed by the�e]�(�2��*IC for another provincial electric utility�e]�(�3��Qcases the consultants advised a staged approach to this initiative. The proposals�e]�(�4��Td significantly and will ultimately depend on the available data and internal effort�e]�(�5��OsBC applies to this initiative. Based on this, FortisBC is proposing a measured�e]�(�6��Uementation timeline in the range of five years. This timeline will provide sufficient�e]�(�7��Rrtunity to develop the processes, tools and technology, and organizational culture�e]�(�8��UIge to transition the organization to a comprehensive Asset Management philosophy. At�e]�(�9��us�e]�(�0��ementation resources.�e]�(�1��Pwide range of implementation costs in the Expressions of Interest can in part be�e]�(�2��Quted to inclusion of software and technology in some proposals as opposed to only�e]�(�3��Tulting services in others. Where softwareltechnology solutions are recommended these�e]�(�4��Kstand-alone solutions and are intended to leverage FortisBC's existing CMMS�e]�(�5��bases and associated system:�e]�(�6��Nregard to inclusion of General Plant assets (Vehicles, Information Technology,�e]�(�7��Zlities, Metering, etc:) the general expert recommendation is to exclude these, at least in�e]�(�8��Knitial implementation stages of FortisBC's Asset Management strategy. It is�e]�(�9��Qmmended to develop and substantially implement the systems and processes for core�e]�(�0��Xness areas such as generation, transmission and distribution first. At a later time when�e]�(�1��SAsset Management strategy is well established the system may be extended to include�e]�(�2��e Other business areas.�e]�(�3��1.1.3�e]�(�4��LsBC is proposing a staged approach to the development of an Asset Management�e]�(�5��Otion: Expenditures of $785,000 in 2012 and 2013 are proposed to accommodate the�e]�(�6��Wlopment of a project team comprising internal and external resources. This project team�e]�(�7��Cxamine FortisBC's existing Asset Management processes and provide a�e]�(�8��Pprehensive report and project cost estimate recommending changes and mapping out�e]�(�9��Rnplementation plan: The project will also investigate and evaluate available Asset�e]�(�0��agement software solutions:�e]�(�1��Uoutcome of this project will be a fully-developed business case with a specific Asset�e]�(K �Xagement implementation strategy. Included in the deliverables is the identification of a�e]�(�2��Xagement implementation strategy. Included in the deliverables is the identification of a�e]�(N�PAGE E�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K)j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K)j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   `��A@���R�hhC   ���C@���R�G@�      G@��     t�ah?]�(�20th Century Grid��21st Century Smart Grid�ehA]�(]�(j  j  e]�(�!Minimal communications capability��)Integrated two-way communications between�e]�(�+Customer costlconsumption feedback provided��+Customer costlconsumption feedback provided�e]�(�+No customer outage detection (customer must��+Automated outage detection and notification�e]�(�-Limited ability to support conservation rates��*Full ability to support multiple types and�e]�(�No tamper detection capability��*Automated meter tamper alarms, support for�e]�(�+Designed for unidirectional power flow from�K e]�(�,Few sensors to provide information on system��'Self-monitoring with sensors throughout�e]�(�Manual restoration�K e]�(�Few consumer choices�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K1j�  Nubh)��}�(hNh]�(G@Vw�    G@X=p�   G@��    G@v     t�ah?]�(�20th Century Grid��21st Century Smart Grid�ehA]�(]�(�20th Century Grid��21st Century Smart Grid�e]�(�!Minimal communications capability��FIntegrated two-way communications between
the customer and the utility�e]�(�MCustomer cost/consumption feedback provided
after-the-fact through bills only��SCustomer cost/consumption feedback provided
near real-time and via multiple choices�e]�(�4No customer outage detection (customer must
call in)��+Automated outage detection and notification�e]�(�-Limited ability to support conservation rates��EFull ability to support multiple types and
complex conservation rates�e]�(�No tamper detection capability��EAutomated meter tamper alarms, support for
theft detection strategies�e]�(�RDesigned for unidirectional power flow from
centralized generation to the customer��@Accommodates distributed generation and
bidirectional power flow�e]�(�3Few sensors to provide information on system
status��'Self-monitoring with sensors throughout�e]�(�Manual restoration��6Semi-automated restoration and eventually
self-healing�e]�(�Few consumer choices��Many consumer choices�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K1j�  j�  )��}�(j�  �/Table 1.2 - Evolution of the Power System Grid �j�  K1j�  (G@h"��   G@S�w    G@{�+@   G@W�f    t�ububh)��}�(hNh]�((hhC   �@���R�hhC   �D�@@���R�G@�      G@��     t�(hhC   ���@���R�hhC   ��8@���R�G@�      G@��     t�eh?]�hA]�(]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�10�K e]�(�11�K e]�(�12�K e]�(�13�K e]�(�14�K e]�(�15�K e]�(�16�K e]�(�17�K e]�(�18�K e]�(�19�K e]�(�20�K e]�(�21�K e]�(�22�K e]�(�23�K e]�(�24�K e]�(�25�K e]�(�1��2_�e]�(�2��STo assist in optimal decision making for capital investments, FortisBC periodically�e]�(�3��Vundertakes the development of long-term capital plans. The planning process is complex�e]�(�4��Xand dynamic; this section describes major inputs to the planning process, including load�e]�(�5��Dforecasts, cost estimation and capital-related accounting practices.�e]�(�6��2.1�e]�(�7��VLoad forecasting is essential for capacity , contingency planning and system upgrades.�e]�(�8��ZPlanning, designing, building transmission and distribution infrastructure can take from a�e]�(�9�K e]�(�10��8necessary to understand how load may grow in the future.�e]�(�11��66[�e]�(K K e]�(�13�K e]�(�14��Vnon-coincidental peak load forecast used to determine the substation, distribution and�e]�(�15�K e]�(�16��.demand periods and adverse weather conditions.�e]�(�17��XDistribution planning involves both capacity and contingency planning: Capacity planning�e]�(�18�K e]�(�19�K e]�(�20�K e]�(�21��0served when a transformer or a substation fails.�e]�(�22��\In preparing the Distribution Load Forecast (found at Appendix B), Load is forecast first at�e]�(�23�K e]�(�24�K e]�(�25��Wavailable through the relevant official community plans and through ongoing discussions�e]�(�26��9with regional or municipal planners and local developers.�e]�(�27��\The forecast regional load growth rate is determined from trends of historical regional load�e]�(�28��Rdata, adjusted to the system load growth: It takes account of highly probable load�e]�(�29��Rdevelopments, such as community developments that have an expected online date and�e]�(�30��defined load,�e]�(�31��or transformer in a certain�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K2j�  Nubh)��}�(hNh]�(hhC   ��,B@���R�hhC    p�O@���R�G@�      G@��     t�ah?]�(�AACE��Project Stage��Description��FortisBC Typical Project�ehA]�(]�(j�  j�  j�  j�  e]�(�Class 5��Identify��!Determine project feasibility and��5 to 20 year�e]�(�Class 4��Evaluate��Select the preferred��3 to 5 year�e]�(�Class 3��Define�� Finalize project scope, cost and��1 to 2 year plan window�e]�(�Class 2��Execute��Safely Produce an operating��Tracking execution�e]�(�Class��Operate (or��Evaluate and Operate asset to��Quality Control or Close�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K7j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K7j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@U$4�I$�G@fQ�   G@�3    G@y���   t�ah?]�(�AACE Classification��Project Stage��Description��%FortisBC Typical Project Plan Windows�ehA]�(]�(�AACE
Classification��Project Stage��Description��%FortisBC Typical Project
Plan Windows�e]�(�Class 5��Identify��CDetermine project feasibility and
alignment with business
strategy.��5 to 20 year plan window�e]�(�Class 4��Evaluate��BSelect the preferred
Development Option(s) and
Execution Strategy.��3 to 5 year plan window�e]�(�Class 3��Define��ZFinalize project scope, cost and
schedule and Sanction Project.
Prepare for Execute Phase.��61 to 2 year plan window
(Capital Plan approval
window)�e]�(�Class 2��Execute��KSafely Produce an operating
asset consistent with scope, cost
and schedule.��Tracking execution�e]�(�Class 1��Operate (or
Audit level)��dEvaluate and Operate asset to
ensure performance to
specifications and maximum
return to the Client.��Quality Control or Close
Out�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K7j�  j�  )��}�(j�  �!Table 2.2 - AACE Classifications �j�  K7j�  (G@l�z�   G@d�    G@y&�@   G@f�   t�ububh)��}�(hNh]�((hhC   ���@���R�hhC   ��8@���R�G@�      G@��     t�(hhC   �˙@���R�K G@�      G@��     t�eh?]�hA]�(]�(�1��Hmpany's regulatory filings through 2011. However beginning in 2012 these�e]�(�2��Ms will be withdrawn by Canadian standard setters and will cease to exist as a�e]�(�3��Oreporting option: This leaves two available options for FortisBC to prepare its�e]�(�4��Ky filings beginning in 2012 and onwards; US Generally Acceptable Accounting�e]�(�5��Bs (US GAAP) or International Financial Reporting Standards (IFRS):�e]�(�6��M BCUC has not yet approved FortisBC to adopt US GAAP for regulatory purposes,�e]�(�7��Mtalization Policy under pre-changeover CGAAP is generally consistent with and�e]�(�8��QJ under US GAAP , particularly if the policy receives regulatory approval: In the�e]�(�9��Mt FortisBC is ordered to implement accounting policies other than US GAAP for�e]�(�0��3�e]�(�1��2ng standards and policies can be found in the 2012�e]�(K �)n_�e]�(K �Rf Order G-195-10, the Commission directed certain expenditures to be classified as�e]�(�4��Mg and Maintenance Expense, and further (Directive 16) required the Company to�e]�(�5�K e]�(�6��Ksion and Distribution Capital Sustainment programs. This report is found at�e]�(�7��)< Mof the 2012-2013 Revenue Requirements:�e]�(�8��Vosts include direct and indirect (overhead) costs. Projects are allocated a proportior�e]�(�9��Nt costs, including corporate overheads, using the principles of activity-based�e]�(�0�K e]�(�1��.Requirements Application (Tab 4, section 4.4):�e]�(�2��2.2.2�e]�(�3��Snt with Canadian rate-regulated utility industry practice, FortisBC incurs costs of�e]�(�4��Usometimes referred to as negative salvage, that are integral to executing its capital�e]�(K �Ture plan and providing safe and reliable electricity to its customers. Since utility�e]�(�6��re�e]�(�7��Xto be incurred at the end of the useful life of a long-lived capital asset that has been�e]�(�8��Pto service. There are also instances when costs of removal must be incurred when�e]�(�9��Nf plant suddenly fails. Costs of removal are prudent and necessary costs to be�e]�(�0��as�e]�(�1��Qusers of the system and infrastructure. The forecast amounts reflect the expected�e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�0�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K8j�  Nubh)��}�(hNh]�(hhC   `߰E@���R�hhC   @�b@���R�G@�      hhC   ���@���R�t�ah?]�(N�Table 2.3 (a)��mer Counts per Region�NehA]�(]�(Nj�  j�  Ne]�(�Region��Direct Customers��Indirect Customers��Total Customers�e]�(�North Okanagan��48,915��14,289��63,204�e]�(�South Okanagan��24,804��22,488��47,292�e]�(�Kootenay��32,567��9,869��42,436�e]�(�Boundary��6,332��2,123��8,455�e]�(�FortisBC��112,618��48,769��161,387�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K:j�  j�  )��}�(j�  �#(3) General Plant costs of removal �j�  K:j�  (G@V�    G@S�w    G@p��    G@W�f    t�ububh)��}�(h]�(�string�j$	  j$	  j$	  eh]�((G@U$4�I$�G@��3@   G@�3    G@��30   t�(hhC   @��Q@���R�hhC   �5�E@���R�G@�      hhC   ��~@���R�t�eh?]�(�Region��Direct Customers��Indirect Customers��Total Customers�ehA]�(]�(�Region��Direct Customers��Indirect Customers��Total Customers�e]�(�North Okanagan��48,915��14,289��63,204�e]�(�South Okanagan��24,804��22,488��47,292�e]�(�Kootenay��32,567��9,869��42,436�e]�(�Boundary��6,332��2,123��8,455�e]�(�FortisBC��112,618��48,769��161,387�e]�(�Region��Transmission��Distribution��Total�e]�(�Region��Transmission��Distribution��Total�e]�(�North Okanagan��230��1,243��1,473�e]�(�South Okanagan��355��1,667��2,022�e]�(�Kootenay��539��1,846��2,385�e]�(�Boundary��266��848��1,114�e]�(�Total��1,390��5,604��6,994�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K:j�  j�  )��}�(j�  �+Table 2.3 (a) - Customer Counts per Region �j�  K:j�  (G@i:�@   G@���    G@z�    G@�x<    t�ububh)��}�(hNh]�(hhC    �\@���R�hhC   ��@@���R�G@�      G@��     t�ah?]�(�1��2.3.1�K ehA]�(]�(j�	  j�	  K e]�(�2��Characteristics�K e]�(�3��FortisBC's North Oka�K e]�(�4��including Winfield to�K e]�(�5��Kelowna is the larges�K e]�(�6��concentration: Compz�K e]�(�7��small geographic are�K e]�(�8��the region contains n�K e]�(�9��directly and indirectly�K e]�(�0��The North Okanagan�K e]�(�1��Kelowna;�K e]�(�2��operates the municip�K e]�(�3��approximately 14,30C�K e]�(�4��approximately 48,90C�K e]�(�5��Okanagan region:�K e]�(�6��
Operations�K e]�(�7��Operations in the Nor�K e]�(�8��service territory beca�K e]�(�9��operations centre to�K e]�(�9��
operalions�K e]�(�0��Big White Ski Resort�K e]�(�1��patterns: Combined�K e]�(�2��load density is much�K e]�(�3��To meet customer gr�K e]�(�4��ana upgraaea Intrastr�K e]�(�5��and distribution feede�K e]�(�6��and there are fewer�K e]�(�7��Operations are also�K e]�(�8��underground distribut�K e]�(�9��infrastructure, along�K e]�(�0��underground and is h�K e]�(�1��capital upgrades.�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K>j�  Nubh)��}�(hNh]�(hhC   �6�7@���R�hhC   @�gX@���R�G@�      G@��     t�ah?]�(K �Project��Purpose��Section�ehA]�(]�(K j�	  j�	  j�	  e]�(K �Ellison to Sexsmith��,Provide a 138 kV loop into the north Kelowna��2.8.2�e]�(K �Ellison Feeder 2 to Sexsmith��-Reconductor line to allow transfer capability��3.1.5�e]�(K �Kelowna Bulk Transformer��(Add additional 230/138 kV transformation��2.8.4�e]�(K �Hollywood Feeder 5��,Upgrade the existing distribution network to��3.1.6�e]�(K �Meshing Kelowna Loop��&Mesh the 138 kV transmission system in��2.8.6�e]�(K �FA Lee Distribution��-Addition of a distribution transformer at the��	2.8.221.1�e]�(K �DG Bell Fourth Feeder��1Build a fourth DG Bell feeder in order to offload��3.1.10�e]�(K �Reconductor 50 Line��,Reconductor Recreation to Saucier section of��2.8.16�e]�(K �Reconductor 51 Line to 60��/Reconductor both lines 51 Line and 60 Line to a��2.8.19�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K@j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K@j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�((G@P��wE�tG@R��    G@�������G@��f`   t�(hhC   �Dz@���R�hhC   `�D@���R�G@�      G@��     t�eh?]�(X,  1 • Kelowna Airport and the University of British Columbia Okanagan expansions which 2 will be completed in 2011; 3 • The addition of a sixth commercial tower near the corner of Highway 97 and Spall 4 Road, expected later this year; 5 • Multiple pumping stations for the Glenmore-Ellison Irrigation District (GEID) in north 6 Kelowna; and 7 • Numerous new residential subdivisions throughout the City of Kelowna and 8 surrounding area. 9 A summary of projects proposed in the area shown below in Table 2.3.1. 0 Table 2.3.1 - North Okanagan Projects�j  j  j  ehA]�(]�(X,  1 • Kelowna Airport and the University of British Columbia Okanagan expansions which
2 will be completed in 2011;
3 • The addition of a sixth commercial tower near the corner of Highway 97 and Spall
4 Road, expected later this year;
5 • Multiple pumping stations for the Glenmore-Ellison Irrigation District (GEID) in north
6 Kelowna; and
7 • Numerous new residential subdivisions throughout the City of Kelowna and
8 surrounding area.
9 A summary of projects proposed in the area shown below in Table 2.3.1.
0 Table 2.3.1 - North Okanagan Projects�NNNe]�(�
Time
Frame��Project��Purpose��Section�e]�(�	2012-2013��$Ellison to Sexsmith
Transmission Tie��IProvide a 138 kV loop into the north Kelowna
area to improve reliability.��2.8.2�e]�(N�)Ellison Feeder 2 to Sexsmith
Feeder 1 Tie���Reconductor line to allow transfer capability
from Sexsmith Feeder 1 and FA Lee Feeder 1
to Ellison Feeder 2. Move normal open point to
transfer load.��3.1.5�e]�(N�*Kelowna Bulk Transformer
Capacity Addition��gAdd additional 230/138 kV transformation
capacity in Kelowna to adequately supply area
load up to 2030.��2.8.4�e]�(�	2014-2016��Hollywood Feeder 5
Upgrades���Upgrade the existing distribution network to
transfer as much load as possible off of the FA
Lee distribution feeder to delay the need for a
distribution source at FA Lee.��3.1.6�e]�(N�Meshing Kelowna Loop��FMesh the 138 kV transmission system in
Kelowna to improve reliability.��2.8.6�e]�(�	2017-2031��(FA Lee Distribution
Transformer addition���Addition of a distribution transformer at the
Kelowna Lee Terminal. This will help reduce the
risk of a major outage to the Kelowna area.��2.8.21.1�e]�(N�DG Bell Fourth Feeder
Addition��~Build a fourth DG Bell feeder in order to offload
the existing overloading DG Bell feeders and
provide some backup capability.��3.1.10�e]�(N�3Reconductor 50 Line
(Recreation-Saucier
substation)��xReconductor Recreation to Saucier section of
line to larger conductor to remove the bottle
neck of capacity on the line.��2.8.16�e]�(N�CReconductor 51 Line to 60
Line (Benvoulin to OK
Mission substation)��qReconductor both lines 51 Line and 60 Line to a
higher rated conductor to provide adequate
transmission capacity.��2.8.19�e]�(�1�NNNe]�(�Time��Project��Purpose��Section�e]�(N�Sexsmith Second��3Install more transformation capacity at Sexsmith to��2.8.21.3�e]�(N�New Enterprise��3Install more transformation capacity at Glenmore to��2.8.21.2�e]�(N�Saucier Second��3Install more transformation capacity at Sexsmith to��2.8.21.4�e]�(N�DG Bell 230 kV Ring��7Providing the ring bus and sectionalizing the line will��2.8.10.2�e]�(N�DG Bell Static VAR��1Install a +150/-50 MVAR Static VAR Compensator at��2.8.10.1�e]�(�	2017-2031��Ellison Second��:Install more transformation capacity at Ellison to offload��	2.8.221.6�e]�(N�Benvoulin Second��4Install more transformation capacity at Benvoulin to��2.8.21.5�e]�(N�DG Bell Distribution��2Install more transformation capacity at DG Bell to��	2.8.21,.7�e]�(N�Reconductor 50 Line��/Reconductor the whole 50 Line to a higher rated��2.8.16�e]�(N�DG Bell Second��4Add a 230/138 kV transformer to DG Bell Terminal and��2.8.10.3�e]�(N�Reconductor 54 Line��2Reconductor 54 Line to a higher rated conductor to��2.8.20�e]�(�2��23.2��TH OKANAGAN REGION�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K@j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K@j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   �J�@���R�hhC   ���&@���R�G@�      G@��     t�ah?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KBj�  Nubh)��}�(hNh]�(hhC    ��?@���R�hhC   �TwD@���R�G@�      G@��     t�ah?]�(�	Timeframe��Project��Purpose��Section�ehA]�(]�(j�
  j�
  j�
  j�
  e]�(�	2012-2013��41 Line Salvage and��+To salvage the 41 Line transmission circuit��3.2.1�e]�(N�42 Line Meshed��-To prevent a voltage collapse during in South��2.8.5.1�e]�(N�Kaleden Feeder 1��+To alleviate voltage problems and regulator��3.1.7�e]�(K �Huth Substation 8 kV��Replacement of Feeder 1�K e]�(�	2014-2016��Pine Street Transformer��,To upgrade the Transformer protection at the�K e]�(N�
Summerland��#Install more capacity at Summerland�K e]�(N�Osoyoos 63 kV Breaker��+Provide proper transformer protection while�K e]�(N�Capacitor Addition to��.Addition of capacitors to the Bentley Terminal��2.8.5.2�e]�(N�RG Anderson��%Meet the Capacity requirements of the�K e]�(N�Reconductor 52 Line��(Reconductoring of 52 Line and 53 Line to�K e]�(�	2017-2031��Central Okanagan��.To construct a 25 kV station to provide & long��2.8.22�e]�(K �Vaseux Lake Terminal��,Install the third transformer at Vaseux Lake�K e]�(N�Tie distribution circuit��,To construct a distribution tie line between�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KDj�  Nubh)��}�(hNh]�(G@U%`J���G@X.    G@�3    G@���   t�ah?]�(�	Timeframe��Project��Purpose��Section�ehA]�(]�(�	Timeframe��Project��Purpose��Section�e]�(�	2012-2013��:41 Line Salvage and
Distribution Underbuild
Rehabilitation�X  To salvage the 41 Line transmission circuit
along the full right of way while keeping the
distribution underbuild poles in place.
Distribution structures will be rehabilitated to
an acceptable level accommodating a future
plan to have double circuit distribution.��3.2.1�e]�(�	2014-2016��042 Line Meshed
Operation Between
Huth and Oliver��oTo prevent a voltage collapse during in South
Okanagan 42 Line must be operated closed
between Oliver and Huth.��2.8.5.1�e]�(N�"Kaleden Feeder 1
Capacity Upgrades���To alleviate voltage problems and regulator
overloading by reconductoring a section of
the feeder and relocating a regulator bank to
an optimum location.��3.1.7�e]�(N�(Huth Substation 8 kV
Breaker Replacement��RReplacement of Feeder 1 and Feeder 2
circuit breakers with a suitable replacement.��2.10.4.2�e]�(N�*Pine Street Transformer
Protection Upgrade��DTo upgrade the Transformer protection at the
Pine Street Substation.�j  e]�(N�"Summerland
Transformer
Replacement��XInstall more capacity at Summerland
substation to avoid breach of contract
demand limit.��2.8.7�e]�(N�Osoyoos 63 kV Breaker
Additions��TProvide proper transformer protection while
achieving safety concerns for operators.��2.10.4.6�e]�(N�&Capacitor Addition to
Bentley Terminal��`Addition of capacitors to the Bentley Terminal
to provide voltage support to the South
Okanagan.��2.8.5.2�e]�(�	2017-2031��,RG Anderson
Distribution Transformer
Upgrade��=Meet the Capacity requirements of the City of
Penticton load.��2.8.9�e]�(N�Reconductor 52 Line
and 53 Line��qReconductoring of 52 Line and 53 Line to
continue to provide reliable service to
customers in the South Okanagan.��2.8.5.3�e]�(N�Central Okanagan
Station�X  To construct a 25 kV station to provide a long
term solution to the entire area and alleviate
distribution capacity problems on some of the
long redials taps by converting to 25 kV. The
goal would also be to salvage the OK Falls,
Kaleden, West Bench, and Trout Creek
substations.��2.8.22�e]�(N�)Vaseux Lake Terminal
Transformer Addition��SInstall the third transformer at Vaseux Lake
Terminal to provide adequate capacity.��2.8.11�e]�(N�;Tie distribution circuit
between Kettle Valley
and Nk’Mip���To construct a distribution tie line between
Kettle Valley and Nk’Mip substations to
improve reliability to customers in between.��3.1.9�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KDj�  j�  )��}�(j�  �&Table 2.3.2 - South Okanagan Projects �j�  KDj�  (G@j�4    G@S�w    G@z {    G@W�f    t�ububh)��}�(hNh]�(hhC    ��?���R�hhC   ���:@���R�G@�      G@��     t�ah?]�(�1��2.3.3��2.3.3�ehA]�(]�(j  j�  j�  e]�(�2��Characteristics��aracteristics�e]�(�3��In general, the Koc��jeneral, the Kootenay�e]�(�4��single , larger area��Tgle, larger area with two smaller sub-regions. The Kootenay region includes Creston,�e]�(�5��Salmo, Fruitvale ,��Wmo, Fruitvale, Trail, Rossland, Castlegar, Nelson; South Slocan, Kaslo and Slocan City.�e]�(�6��The Boundary regi��3�e]�(�7��Rock Creek. The K��Vck Creek. The Kootenay region is the largest geographic region in the FortisBC service�e]�(�8��territory and contai��[itory and contains 540 kilometres of transmission line and 1,846 kilometres of distribution�e]�(�9��line. The Kootenay��OThe Kootenay region also includes FortisBC's four generating stations along the�e]�(�0��Kootenay River,��Qtenay River, the Lower Bonnington, Upper Bonnington; South Slocan, and Corra Linn�e]�(�1��plants. The Boundz��Mnts. The Boundary region contains 265 kilometres of transmission Iine and 848�e]�(�2��kilometres of distrib��Xmetres of distribution line. As seen in Table 2.3 (b) above, when combined, the Kootenay�e]�(�3��and Boundary regi��J�e]�(�4��distribution assets��2ribution assets in the FortisBC service territory.�e]�(�5��FortisBC supplies��NtisBC supplies 9,200 customers indirectly through the City of Nelson and 1,900�e]�(�6��customers through��Ttomers through the City of Grand Forks municipal utilities. FortisBC also serves 650�e]�(�7��customers through��Rtomers through two BC Hydro wholesale interconnections. The total number of direct�e]�(�8��and indirect custon��XI indirect customers in this region is approximately 50,000 and represents approximately�e]�(�9��31 percent of the tc��-percent of the total FortisBC customer count:�e]�(�0��The Kootenay and��3�e]�(�1��industrial accounts��Tustrial accounts in the FortisBC service territory: These customers include Columbia�e]�(�2��Power Cornoration��ANer Cornoration (throuah its oneratina entities) Zellstoff Celaar�e]�(�3��Lumber;��"nber; Kokanee Brewery , and Roxul.�e]�(�4��FortisBC acquired��PtisBC acquired the historic assets of the West Kootenay Power and Light Company;�e]�(�5��which originated in��-ch originated in Rossland over 100 years ago.�e]�(�6��some of the oldest��Zne of the oldest assets in the Company that are still in-service. The FortisBC sustainment�e]�(�7��capital programs h��Rital programs have been more heavily weighted in these regions as opposed t growth�e]�(�8��capital programs in��7ital programs in the North Okanagan and South Okanagan:�e]�(�9��
Operations��erations�e]�(�0��The KootenaylBou��3�e]�(�1��Castlegar and Kase��Tstlegar and Kaslo. The Warfield Complex also includes the Kootenay Warehouse and the�e]�(�2��System Control Ce��Otem Control Centre. The Trail Office accommodates the Contact Centre as well as�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KEj�  Nubh)��}�(hNh]�(hhC   ��C�?���R�hhC   �C14@���R�G@�      G@��     t�ah?]�hA]�(]�(�1��Engin�K e]�(�2��Boun�K e]�(�3��terrair�K e]�(�4��areas�K e]�(�5��Wher�K e]�(�6��sprea�K e]�(�7��Fortis�K e]�(�8��Capit�K e]�(�9��Over�K e]�(�0��susta�K e]�(�1��regior�K e]�(�2�NK e]�(�3�NK e]�(�4�NK e]�(�5�NK e]�(�6�NK e]�(�7�NK e]�(�8�NK e]�(�9�NK e]�(�0�NK e]�(�1�NK e]�(�2�NK e]�(�3�NK e]�(�4�NK e]�(�5�NK e]�(�6�NK e]�(�7�NK e]�(�8�NK e]�(�9�NK e]�(�0�NK e]�(�1�NK e]�(NN�Paae 28�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KFj�  Nubh)��}�(hNh]�(hhC   @K�A@���R�hhC   `�GB@���R�G@�      G@��     t�ah?]�(�	Timeframe��Project��Purpose��Section�ehA]�(]�(jd  je  jf  jg  e]�(N�	6/26 Line��5To create new tap off point for 6 Line and 26 Line on��2.9.5�e]�(K �27 Line Rebuild��1To rehabilitate 27 Line to an acceptable level to��2.9.5�e]�(N�Glenmerry Feeder 2 to��.Construct new feeder tie to offload overloaded��3.1.4�e]�(N�
19/29 Line��1To salvage 19 Line between South Slocan Switching��2.9.5�e]�(N�21-24 Lines River��4To rehabilitate the four 63 kV transmission lines to��2.9.5�e]�(N�20 Line WTS to Salmo��+To rehabilitate the transmission line to an��2.9.5�e]�(K �Grand Forks Terminal��.Add a second terminal transformer to achieve a��2.8.3�e]�(N�AII Plants Concrete��0This program is intended to provide a sustaining��2.5.1.1�e]�(N�Upper Bonnington��0This project was approved by BCUC Order G-195-10��2.5.1.22�e]�(K �Lower Bonnington��0This project was approved by BCUC Order G-195-10��2.5.1.3�e]�(N�Upper Bonnington,��1This project is intended to replace miscellaneous��2.5.1.4�e]�(N�Corra Linn Unit 2 Life��.This project was approved by BCUC Order C-5-09��2.5.22.1�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KHj�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�(G@U%M�$�IG@R��    G@�ܝ�m��G@��f`   t�ah?]�(�(Table 2.3.3 - Kootenay Boundary Projects�j  j  j  ehA]�(]�(�(Table 2.3.3 - Kootenay Boundary Projects�NNNe]�(�	Timeframe��Project��Purpose��Section�e]�(�	2012-2013��6/26 Line
Reconfiguration���To create new tap off point for 6 Line and 26 Line on
the south side of the river crossing and salvage from
that point north to the existing tap off point
eliminating two river crossings.��2.9.5�e]�(N�27 Line Rebuild��wTo rehabilitate 27 Line to an acceptable level to
improve reliability and reduce liability from a safety
point of view.��2.9.5�e]�(N�'Glenmerry Feeder 2 to
Feeder 1 Tie Line���Construct new feeder tie to offload overloaded
Glenmerry Feeder 2 and Beaver Park Feeder 1 and
defer a large station upgrade at Beaver Park.��3.1.4�e]�(N�19/29 Line
Reconfiguration���To salvage 19 Line between South Slocan Switching
Station and the Vernon-Slocan highway crossing to
avoid unnecessary large capital expenditure for
rebuild and potential liability.��2.9.5�e]�(N�21-24 Lines River
Plant���To rehabilitate the four 63 kV transmission lines to
an acceptable level to avoid outages that could have
large potential financial implications and to improve
safety.��2.9.5�e]�(N�20 Line WTS to Salmo���To rehabilitate the transmission line to an acceptable
level to improve reliability and reduce liability from a
safety point of view.��2.9.5�e]�(N�)Grand Forks Terminal
Transformer Addition��tAdd a second terminal transformer to achieve a
reliable 63 kV N-1 contingency configuration at
Grand Forks Terminal.��2.8.3�e]�(N�9All Plants Concrete
and Structural
Rehabilitation
Program�XF  This program is intended to provide a sustaining
level of capital investment to address deterioration of
structural steel and concrete elements at all facilities.
The program will target repairs to deteriorated
elements on a priority basis and has been budgeted
at what FortisBC deems a minimum sustaining level
of investment.��2.5.1.1�e]�(N�)Upper Bonnington
Spillgate
Rehabilitation���This project was approved by BCUC Order G-195-10
as part of the 2011 Capital Plan submission and is
intended to ensure the reliable operation of two spill
gates at the Upper Bonnington facility.��2.5.1.2�e]�(N�#Lower Bonnington
Powerhouse Windows���This project was approved by BCUC Order G-195-10
as part of the 2011 Capital Plan submission and is
the second year of a project to replace 80+ year old
windows at this facility.��2.5.1.3�e]�(N�@Upper Bonnington,
South Slocan and
Corra Linn
Powerhouse Windows���This project is intended to replace miscellaneous
windows at these facilities based on the
recommendations of an engineer’s report completed
in 2010.��2.5.1.4�e]�(N� Corra Linn Unit 2 Life
Extension��.This project was approved by BCUC Order C-5-09��2.5.2.1�e]�(j  NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KHj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KHj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(�any�j$	  j$	  j�  eh]�(hhC    �VF@���R�hhC    �sD@���R�G@�      G@��     t�ah?]�(�	Timeframe��Project��Purpose��Section�ehA]�(]�(j�  j�  j�  j�  e]�(N�Lower Bonnington and��0This project was approved by BCUC Order G-195-10��2.5.2.3�e]�(K �Corra Linn Unit 3��)Corra Linn Unit 3 completion will address��2.5.2.4�e]�(�	2012-2013��Upper Bonnington Old��6This project is intended to ensure ongoing reliability��2.5.2.5�e]�(K �AII Plants Station��0This project was approved by BCUC Order G-147-06�K e]�(N�Upper Bonnington,��9This project involves the installation of a fire panel at��2.5.3.1�e]�(K �AII Plants Safety and��This project is to increase��2.5.3.22�e]�(K �30 Line Lake Crossing��2Assess and rehabilitate the 3.5 kilometre Kootenay��2.9.5�e]�(K �Grand Forks Terminal��2Extending the Grand Forks distribution feeder into��3.1.8�e]�(K �AII Plants Concrete��0This program is intended to provide a sustaining��2.5.1.1�e]�(K �Corra Linn Spillgate��3The Corra Linn spill gates and spillway are used to�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KIj�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�(G@U%�UUUG@R��    G@�ܣ�UUUG@��f`   t�ah?]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�j  j  j  ehA]�(]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�NNNe]�(�	Timeframe��Project��Purpose��Section�e]�(�	2012-2013��=Lower Bonnington and
Upper Bonnington
Plant Totalizer
Upgrade��0This project was approved by BCUC Order G-195-10��2.5.2.3�e]�(N�Corra Linn Unit 3
Completion���Corra Linn Unit 3 completion will address
components not addressed during the Life Extension
project for this unit in 2000, but later deemed
necessary as part of subsequent Life Extension
projects.��2.5.2.4�e]�(N�0Upper Bonnington Old
Plant Various Unit
Upgrades��gThis project is intended to ensure ongoing reliability
and safety of the old units at Upper Bonnington.��2.5.2.5�e]�(N�All Plants Station
Service��0This project was approved by BCUC Order G-147-06��2.5.2.2�e]�(N�=Upper Bonnington,
Lower Bonnington and
Corra Linn Fire Panels���This project involves the installation of a fire panel at
all four plants. This project is also being completed
at South Slocan in 2011 which is approved by BCUC
Order G-195-10.��2.5.3.1�e]�(N�All Plants Safety and
Security���This project is to increase plant safety and security
by installing signage, fencing and road barriers. This
project has spending from 2012-2015.��2.5.3.2�e]�(�	2014-2016��330 Line Lake Crossing
Assessment and
Rehabilitation��aAssess and rehabilitate the 3.5 kilometre Kootenay
Lake crossing span to an acceptable condition.��2.9.5�e]�(N�$Grand Forks Terminal
Feeder Addition���Extending the Grand Forks distribution feeder into
the Christina Lake distribution network to offload the
Christina Lake transformer.��3.1.8�e]�(N�9All Plants Concrete
and Structural
Rehabilitation
Program�XF  This program is intended to provide a sustaining
level of capital investment to address deterioration of
structural steel and concrete elements at all facilities.
The program will target repairs to deteriorated
elements on a priority basis and has been budgeted
at what FortisBC deems a minimum sustaining level
of investment.��2.5.1.1�e]�(N�9Corra Linn Spillgate
and Spillway Concrete
Rehabilitation�X�  The Corra Linn spill gates and spillway are used to
control the levels of the Kootenay Lake. They were
originally installed in 1932, and were not installed
with a method of isolation, making routine
maintenance difficult. This project is intended to
rehabilitate the gates and spillway as required to
ensure continued reliable operation, as well as
address the lack of isolation for these gates.��2.5.1.5�e]�(j  NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KIj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KIj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(�integer�j$	  j$	  j$	  eh]�(hhC   �2�8@���R�hhC    =�E@���R�G@�      G@��     t�ah?]�(K �Project��Purpose��Section�ehA]�(]�(K jh  ji  jj  e]�(N�Upper Bonnington,��-Plant automation is intended to assist in the��2.5.2.6�e]�(N�AII Plants Fire Safety��3This project involves upgrading the fire egress and��2.5.3.3�e]�(K �AII Plants Safety and��5This project is to increase plant safety and security��2.5.3.2�e]�(N�Creston Area Capacity��1To upgrade capacity in the Creston Substation and��2.8.23�e]�(K �Boundary Area Supply��.Upgrade one of the terminal transformers at AS��2.8.12�e]�(N�Beaver Valley Solution��3Upgrade the Beaver Valley to 25 kV to eliminate the��2.8.8�e]�(N�Reconductor 31 Line��2Reconductor 31 Line to a higher rated conductor to��2.8.13�e]�(K �AII Plants Concrete��0This program is intended to provide a sustaining��2.5.1.1�e]�(K �Upper Bonnington��*The Upper Bonnington overflow spillway was��2.5.1.7�e]�(K �South Slocan Spillway��-The South Slocan dam is 80+ years old and the��2.5.1�e]�(K �AIl Plants Headgate��3The headgates and spill gates at all facilities are��2.5.1.8�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KJj�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�(G@U%7�'bvG@R��    G@�ܠ���OG@��f`   t�ah?]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�j  j  j  ehA]�(]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�NNNe]�(�	Timeframe��Project��Purpose��Section�e]�(�	2017-2031��BUpper Bonnington,
Lower Bonnington and
Corra Linn Plant
Automation�X  Plant automation is intended to assist in the
transition from time based to condition based
maintenance. The business case to support this
project will be based on data retrieved from the
South Slocan Plant automation project approved in
2011 under BCUC Order G-195-10��2.5.2.6�e]�(N�All Plants Fire Safety��kThis project involves upgrading the fire egress and
fire stops in the powerhouses at all four river plants.��2.5.3.3�e]�(N�All Plants Safety and
Security���This project is to increase plant safety and security
by installing signage, fencing and road barriers. This
project has spending from 2012-2015.��2.5.3.2�e]�(N�Creston Area Capacity
Increase��[To upgrade capacity in the Creston Substation and
feeders to meet the growing load demands.��2.8.23�e]�(N�Boundary Area Supply
Upgrade��zUpgrade one of the terminal transformers at AS
Mawdsley terminal to a larger unit to continue to
provide reliable service.��2.8.12�e]�(N�Beaver Valley Solution��bUpgrade the Beaver Valley to 25 kV to eliminate the
need for the Fruitvale and Hearns substations.��2.8.8�e]�(N�Reconductor 31 Line��RReconductor 31 Line to a higher rated conductor to
meet growing demand in Creston.��2.8.13�e]�(N�9All Plants Concrete
and Structural
Rehabilitation
Program�XF  This program is intended to provide a sustaining
level of capital investment to address deterioration of
structural steel and concrete elements at all facilities.
The program will target repairs to deteriorated
elements on a priority basis and has been budgeted
at what FortisBC deems a minimum sustaining level
of investment.��2.5.1.1�e]�(N�7Upper Bonnington
Overflow Spillway
Concrete Resurfacing���The Upper Bonnington overflow spillway was
originally constructed in 1916 and has not been
refurbished since construction. The spillway is an
integral part of the dam, and the project will restore
the spillway to design condition.��2.5.1.7�e]�(N�%South Slocan Spillway
Concrete Repair���The South Slocan dam is 80+ years old and the
concrete in the spillway has begun to show signs of
deterioration. The project would be focused on
resurfacing and rehabilitating the spillway as
required to extend the life of the asset.��2.5.1�e]�(N�8All Plants Headgate
and Spillgate
Superstructure
upgrade�Xz  The headgates and spill gates at all facilities are
operated by hoists located above deck level on a
structural steel lattice work superstructure. These
structures have not been rehabilitated since
installation, and recent engineering assessments
have identified the potential need for seismic
upgrades as well as routine rehabilitation work to
ensure their long term viability.��2.5.1.8�e]�(j  NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KJj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KJj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j$	  j�  eh]�(hhC   �e�B@���R�hhC   `y�D@���R�G@�      G@��     t�ah?]�(K �Project��Purpose��Section�ehA]�(]�(K j�  j�  j�  e]�(N�Lower Bonnington��/The spill gates at Lower Bonnington are over 80�Ne]�(K �Upper Bonnington,��A��2.5.1.22�e]�(K �AII Plants Heating and��/This project will install automatic louvers and��2.5.2.7�e]�(K �Upper Bonnington Old��2The four old units located at the Upper Bonnington�K e]�(K �AII Plants Fire Water��0At all four plants the fire water supply must be�K e]�(K �Mechanical Equipment��4This project is to upgrade mechanical equipment that��2.5.2.10�e]�(K �Electronic Equipment��.This project is to replace obsolete electronic��2.5.2.11�e]�(K �Corra Linn Unit 3��.This turbine is original from 1932 and will be��2.5.2.13�e]�(K �Upper Bonnington Unit��.This turbine is original from 1940 and will be�K e]�(K �Corra Linn Unit 3��1This project is to replace the generator windings��2.5.2.12�e]�(K �
Dam Safety��5This project is to install concrete and dam structure��2.5.3.4�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KKj�  Nubh)��}�(hNh]�(G@U%7�'bvG@R��    G@�ܠ���OG@��f`   t�ah?]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�j  j  j  ehA]�(]�(�1Table 2.3.3 - Kootenay Boundary Projects cont’d�NNNe]�(�	Timeframe��Project��Purpose��Section�e]�(�	2017-2031��#Lower Bonnington
Spill Gate Rebuild�X�  The spill gates at Lower Bonnington are over 80
years old. There is currently a risk of the gates
jamming and not opening during maximum flood
conditions, resulting in water over topping the dam
and causing damage to the powerhouse.
Deterioration as a result of age as well as corrosion
can cause the collapse of skin plates (outer steel
plates), which would cause water to spill from the
dam.��      j  e]�(N�@Upper Bonnington,
Corra Linn and South
Slocan Window
Replacement���A proposed capital project in 2013 will address the
worst locations in these powerhouses, but given the
age of the remaining windows it is expected that the
balance of the windows will require replacement
within the next 30 years.��2.5.1.2�e]�(N�"All Plants Heating and
Ventilation���This project will install automatic louvers and
controls to remotely operate and maintain
environmental parameters at the facility��2.5.2.7�e]�(N�%Upper Bonnington Old
Plant Repowering���The four old units located at the Upper Bonnington
facility will be upgraded and rehabilitated as part of
this large capital project.��2.5.2.8�e]�(N�All Plants Fire Water
Supply��eAt all four plants the fire water supply must be
upgraded to comply with Fire Safety
recommendations.��2.5.2.9�e]�(N� Mechanical Equipment
Replacement��This project is to upgrade mechanical equipment that
is expected to be a risk of failure due to age and
condition of equipment.��2.5.2.10�e]�(N� Electronic Equipment
Replacement��9This project is to replace obsolete electronic
equipment.��2.5.2.11�e]�(N�%Corra Linn Unit 3
Turbine Replacement��SThis turbine is original from 1932 and will be
replaced based on condition and age.��2.5.2.13�e]�(N�+Upper Bonnington Unit
6 Turbine
Replacement��SThis turbine is original from 1940 and will be
replaced based on condition and age.��2.5.2.14�e]�(N�"Corra Linn Unit 3
Generator Rewind��EThis project is to replace the generator windings
based on condition.��2.5.2.12�e]�(N�Dam Safety
Instrumentation��~This project is to install concrete and dam structure
monitoring equipment as a result of anticipated Dam
Safety Requirements.��2.5.3.4�e]�(j  NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KKj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KKj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   �!�@���R�hhC   �E�2@���R�G@�      G@��     t�ah?]�(�1��2.4.4��2.4.4�ehA]�(]�(ja  jb  jc  e]�(�2��Although the ULE��VJh the ULE program is coming to an end, there are still four units within the FortisBC�e]�(�3��power system that ha��Qsystem that have not been included within this program: Unit 1 to Unit 4 at Upper�e]�(�4��Bonnington have not��Ngton have not been rehabilitated since their installation, and have been under�e]�(�5��consideration for a re��Oration for a repowering project for several years. While evaluating the need to�e]�(�6��repower the old units��Qr the old units, the Company did invest $5.5 million from 2005 to 2010 to address�e]�(�7��issues which were re��
which were�e]�(�8��Issues addressed OvE��(addressed over this time frame included:�e]�(�9��Replacement��QReplacement of two switchyard 63 kV oil breakers and adding oil spill containment�e]�(�0��throughout the��throughout the plant;�e]�(�1��Upgrading the��TUpgrading the intake area, including concrete refurbishment to the forebay piers and�e]�(�2��deck;��deck;�e]�(�3��The purchase��OThe purchase of new stop logs and seals, replace intake gate gantry and upgrade�e]�(�4��intake gates;��intake gates;�e]�(�5��Upgrades to tL�K e]�(�6��deck, replace��Udeck; replace tailrace gantry , replace seals on the tailrace stoplogs, and corrosion�e]�(�7��control on the��"control on the tailrace gates; and�e]�(�8��In the power��In the power house a�e]�(�9��containment��3containment and removal, and spare exciter rebuild:�e]�(�0��The units themselves��Rits themselves are located in the old section of the Upper Bonnington facility and�e]�(�1��have a total generatir��Qtotal generating capacity of approximately 20 MW. The need to upgrade these units�e]�(�2��has been assessed��Ven assessed on the basis of risk of failure, overall benefit to our customers, and the�e]�(�3��potential rate impact��Ral rate impact as a result of undertaking the upgrade. The assessment demonstrated�e]�(�4��the need to maintain��Vd to maintain this generation resource for the benefit of FortisBC customers, but also�e]�(�5��indicated that since th��Yd that since the units are still operating satisfactorily, the project is not required at�e]�(�6��this time. Recognizing��He. Recognizing the age of the equipment; the Company expects that normal�e]�(�7��operation of this equi��Fon of this equipment could require higher maintenance costs over time.�e]�(�8��must be recognized tL��2�e]�(�9��have had Unit Life Ex��Pad Unit Life Extensions. In order to ensure that the Old Plant units continue to�e]�(�0��operate safely,��9�e]�(�1��2.5.2.8), the conditior��W), the condition and operation of these units will be continually monitored. The timing�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KPj�  Nubh)��}�(hNh]�(hhC   ��(@���R�hhC    9�L@���R�G@�      G@��     t�ah?]�(�1��Generation Projects��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j�  j�  j�  j�  j�  j�  j�  j�  e]�(�2��Physical Infrastructure�NN�(SOO��Os)�NK e]�(�3��"AII Plants Concrete and Structural��570��617��647��665��686�K e]�(�4��'Upper Bonnington Spill Gate Rebuild (G-��1,085�NNNNNe]�(�5��#Lower Bonnington Powerhouse Windows��366��8�NNNNe]�(�6��(Upper Bonnington, South Slocan and Corra�N�430�NNNNe]�(N�+Corra Linn Spillway Concrete and Spill Gate�NN�7,874��865��1,786��17,326�e]�(�8��"Upper Bonnington Overflow Spillway�NNNNN�30,576�e]�(�9��%South Slocan Spillway Concrete Repair�NNNNN�42,370�e]�(�10��!AII Plants Superstructure Upgrade�NNNNN�5,020�e]�(�11��#Lower Bonnington Spill Gate Rebuild�NNNNNK e]�(�12��Remaining Powerhouse Window�NNNNNK e]�(�13��&Total Physical Infrastructure Projects��2,021��1,055��8,521��1,530��2,472�K e]�(�14�NNNNNNK e]�(�15��#Mechanical and Electrical Equipment�NNNNNK e]�(�16��(Corra Linn Unit 2 Life Extension(C-5-09)��3.423�NNNNK e]�(�17��%AII Plants Station Service (G-147-06)��672�NNNNK e]�(�18��%Lower Bonnington and Upper Bonnington��90�NNNNK e]�(�19��Corra Linn Unit 3 Completion��722�NNNNK e]�(�20��'Upper Bonnington Old Plant Various Unit��1,311�NNNNK e]�(K �OpoGGoo�NN�283��291��301�K e]�(K �"AII Plants Heatinq and Ventilation�NNNNNK e]�(�23��$Upper Bonnington Old Unit Repowering�NNNNN�57,080�e]�(�24��AIl Plants Fire Water Supply�NNNNNK e]�(�25�� Mechanical Equipment Replacement�NNNNNK e]�(�26�� Electronic Equipment Replacement�NNNNNK e]�(�27��"Corra Linn Unit 3 Generator Rewind�NNNNNK e]�(�28��%Corra Linn Unit 3 Turbine Replacement�NNNNNK e]�(�29��Upper Bonnington�NNNNNK e]�(�30��Ii Oioa  [im��6,218�N�283��291��301�K e]�(N�fquipiem�NNNNNK e]�(NNNNNN�Page�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KUj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KUj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j$	  j$	  j$	  j$	  j$	  j$	  eh]�((G@I"�`   G@R��    G@��Zֵ�G@��f`   t�(hhC    ��1@���R�hhC   ��RB@���R�G@�      G@��     t�eh?]�(�11 Table 2.5 (a) - Generation Sustainment Projects�j  j  j  j  j  j  j  ehA]�(]�(�11 Table 2.5 (a) - Generation Sustainment Projects�NNNNNNNe]�(ja
  �Generation Projects��2012��2013��2014��2015��2016��2017-31�e]�(�2��Physical Infrastructure��($000s)�NNNNNe]�(�3��1All Plants Concrete and Structural
Rehabilitation��570��617��647��665��686��14,454�e]�(�4��/Upper Bonnington Spill Gate Rebuild (G-
195-10)��1,085��-�j�  j�  j�  j�  e]�(�5��#Lower Bonnington Powerhouse Windows��366��8�j  j�  j�  j�  e]�(�6��@Upper Bonnington, South Slocan and Corra
Linn Powerhouse Windows�j�  �430�j�  j�  j�  j�  e]�(�7��:Corra Linn Spillway Concrete and Spill Gate
Rehabilitation�j�  j�  �7,874��865��1,786��17,326�e]�(j�  �5Upper Bonnington Overflow Spillway
Concrete Resurface�j�  j�  j�  j�  j�  �30,576�e]�(�9��%South Slocan Spillway Concrete Repair�j�  j�  j�  j�  j�  �42,370�e]�(�10��!All Plants Superstructure Upgrade�j�  j�  j�  j�  j�  �5,020�e]�(�11��#Lower Bonnington Spill Gate Rebuild�j�  j�  j�  j�  j�  �1,779�e]�(�12��'Remaining Powerhouse Window
Replacement�j�  j�  j�  j�  j�  �2,949�e]�(�13��&Total Physical Infrastructure Projects��2,021��1,055��8,521��1,530��2,472��114,474�e]�(�14�j  j  j  j  j  j  j  e]�(�15��#Mechanical and Electrical Equipment�j  j  j  j  j  j  e]�(�16��(Corra Linn Unit 2 Life Extension(C-5-09)��3,423�j�  j�  j�  j�  j�  e]�(�17��%All Plants Station Service (G-147-06)��672�j�  j�  j�  j�  j�  e]�(�18��HLower Bonnington and Upper Bonnington
Plant Totalizer Upgrade (G-195-10)��90�j�  j�  j�  j�  j�  e]�(�19��Corra Linn Unit 3 Completion��722�j�  j�  j�  j�  j�  e]�(�20��0Upper Bonnington Old Plant Various Unit
Upgrades��1,311�j�  j�  j�  j�  j�  e]�(�21��CUpper Bonnington, Lower Bonnington and
Corra Linn Plants Automation�j�  j�  �283��291��301�j�  e]�(�22��"All Plants Heating and Ventilation�j�  j�  j�  j�  j�  �8,711�e]�(�23��$Upper Bonnington Old Unit Repowering�j�  j�  j�  j�  j�  �57,080�e]�(�24��All Plants Fire Water Supply�j�  j�  j�  j�  j�  �2,431�e]�(�25�� Mechanical Equipment Replacement�j�  j�  j�  j�  j�  �4,340�e]�(�26�� Electronic Equipment Replacement�j�  j�  j�  j�  j�  �4,340�e]�(�27��"Corra Linn Unit 3 Generator Rewind�j�  j�  j�  j�  j�  �4,344�e]�(�28��%Corra Linn Unit 3 Turbine Replacement�j�  j�  j�  j�  j�  �3,974�e]�(�29��+Upper Bonnington Unit 6 Turbine
Replacement�j�  j�  j�  j�  j�  �3,974�e]�(�30��)Total Mechanical and Electrical
Equipment��6,218�j�  �283��291��301��86,763�e]�(j  NNNNNNNe]�(N�Generation Projects��2012��2013��2014��2015��2016��2017-31�e]�(�31��Dam�NN�(SOO��Os)�NNe]�(�32��&Upper Bonnington; Lower Bonnington and��250�NNNNNe]�(�33��AII Plants Safety and Security��471��259��264�NNNe]�(�34��AII Plants Fire Safety�N�475��424��437�NNe]�(�35��$AII Plants Surveillance and Security�NN�1,001��1,031��1,065��738�e]�(�36��Dam Safety Instrumentation�NNNNN�5,991�e]�(�37��(Total Dam; Public Worker Safety Projects��721�NNNN�2,391�e]�(�38�NN�734��1,689��1,468��1,065��9,120�e]�(�39��AII Plants Minor Sustainment�NNNNNNe]�(�40��$AII Plants Minor Sustainment Capital��1,171�NNNNNe]�(�41��"Total AII Plants Minor Sustainment��1,171��1,158��1,203��1,144��1,182��20,902�e]�(�42�NN�1,158��1,203��1,144��1,182��20,902�e]�(�43��Total Generation Proiects��10.131�NNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KUj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KUj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j$	  j^  j$	  j$	  j$	  j$	  eh]�(hhC   �?i2@���R�hhC   `}A@���R�G@�      G@��     t�ah?]�(NN�2012��2013��2014��2015��2016��2017-31�ehA]�(]�(NNjr  js  jt  ju  jv  jw  e]�(K �rysical Mraslruclune�NN�(SC��JOOs)�NNe]�(K �1AII Plants Concrete and Structural Rehabilitation��570��617��647��665��686��14,454�e]�(�2��.Upper Bonnington Spill Gate Rebuild (G-195-10)��1,085�NNNNNe]�(�3��#Lower Bonnington Powerhouse Windows��366��8�NNNNe]�(�4��-Upper Bonnington, South Slocan and Corra Linn�N�430�NNNNe]�(�5��+Corra Linn Spillway Concrete and Spill Gate�NN�7,874��865��1,786��17,326�e]�(�6��+Upper Bonnington Overflow Spillway Concrete�NNNNN�30,57E�e]�(N�%South Slocan Spillway Concrete Repair�NNNNN�42,37C�e]�(K �!AII Plants Superstructure Upgrade�NNNNN�5,02C�e]�(K �#Lower Bonnington Spill Gate Rebuild�NNNNN�1,77e�e]�(K �'Remaining Powerhouse Window Replacement�NNNNN�2,949�e]�(K �&Total Phvsical Infrastructure Proiects��2.021��1.055��8.521��1.530��2.472��114.474�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KWj�  Nubh)��}�(hNh]�(G@P��$�IG@R��    G@���{m��G@��f`   t�ah?]�(�=1 Table 2.5.1 - Physical Infrastructure Projects and Programs�j  j  j  j  j  j  j  ehA]�(]�(�=1 Table 2.5.1 - Physical Infrastructure Projects and Programs�NNNNNNNe]�(j  �Physical Infrastructure��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �1All Plants Concrete and Structural Rehabilitation��570��617��647��665��686��14,454�e]�(j�  �.Upper Bonnington Spill Gate Rebuild (G-195-10)��1,085�j�  j�  j�  j�  j�  e]�(j�  �#Lower Bonnington Powerhouse Windows��366�j�  j  j�  j�  j�  e]�(j�  �@Upper Bonnington, South Slocan and Corra Linn
Powerhouse Windows�j�  �430�j�  j�  j�  j�  e]�(j�  �:Corra Linn Spillway Concrete and Spill Gate
Rehabilitation�j�  j�  �7,874��865��1,786��17,326�e]�(j�  �5Upper Bonnington Overflow Spillway Concrete
Resurface�j�  j�  j�  j�  j�  �30,576�e]�(j�  �%South Slocan Spillway Concrete Repair�j�  j�  j�  j�  j�  �42,370�e]�(j�  �!All Plants Superstructure Upgrade�j�  j�  j�  j�  j�  �5,020�e]�(j�  �#Lower Bonnington Spill Gate Rebuild�j�  j�  j�  j�  j�  �1,779�e]�(�10��'Remaining Powerhouse Window Replacement�j�  j�  j�  j�  j�  �2,949�e]�(�11��&Total Physical Infrastructure Projects��2,021��1,055��8,521��1,530��2,472��114,474�e]�(X�  2 2.5.1.1 All Plants Concrete and Structural Rehabilitation
3 Program
4 Although FortisBC has undertaken projects to complete seismic upgrades of the dams,
5 continuous sustainment capital investments are required to maintain safe facilities, extend
6 the life of the physical assets and ensure ongoing compliance with dam safety guidelines.
7 With major mechanical and electrical components upgraded, the next phase will be the
8 implementation of an effective concrete and structural rehabilitation program. The primary
9 drivers for such a program are discussed in greater detail below.
0 Concrete Deterioration
1 The four facilities located on the Kootenay River system were constructed between 1897
2 and 1940. These facilities are considered to be gravity dams and were constructed using
3 reinforced concrete. Over the past 70 and more years these structures have been subjected
4 to various forces causing deterioration. There are many environmental factors that can lead
5 to concrete deterioration but for clarity only some of the major factors are discussed below.
6 Some of the major factors involved in the deterioration of concrete structures along the
7 Kootenay River system are as follows:�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  KWj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  KWj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@U!G�UUUG@R��    G@����UUUG@��f`   t�ah?]�(�(Photo 2.5.1.1 (f) - Lower Bonnington Dam�j  j  ehA]�(]�(�(Photo 2.5.1.1 (f) - Lower Bonnington Dam�NNe]�(j  �Freeze Thaw��Hydraulic Erosion�e]�(N�Hydraulic Erosion��Freeze Thaw�e]�(j  NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K^j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K^j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@U!G�UUUG@X.    G@��     G@z��P   t�ah?]�(j  �Concrete Deterioration��Concrete Deterioration�ehA]�(]�(j  �Concrete Deterioration��Concrete Deterioration�e]�(N�Structural Deterioration��Carbonation�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K_j�  j�  )��}�(j�  �)Photo 2.5.1.1 (g) - Upper Bonnington Dam �j�  K_j�  (G@i��    G@S�w    G@z�b�   G@W�f    t�ububh)��}�(hNh]�(hhC    ��I@���R�hhC   ���F@���R�G@�      hhC   ��<l@���R�t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j3  j4  j5  j6  j7  j8  j9  e]�(NNN�(SOC��JOs)�NNe]�(�Cost��570��617��647��665��685��14,454�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K`j�  j�  )��}�(j�  �2012 ISP�j�  K`j�  (G@V�     G@D"4    G@lZ�`   G@K��   t�ububh)��}�(hNh]�(G@c��   G@X.    G@}�    G@b
�   t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNe]�(�Cost��570��617��647��665��685��14,454�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K`j�  j�  )��}�(j�  �ITable 2.5.1.1 - All Plants Concrete and Structural Rehabilitation Costs  �j�  K`j�  (G@`�@   G@S�w    G@i�   G@W�f    t�ububh)��}�(hNh]�(hhC    ^DU@���R�hhC   �x�S@���R�G@�      hhC   ��Op@���R�t�ah?]�(K �2014��2015��2016��5�ehA]�(]�(K j~  j  j�  j�  e]�(K �7,874��865��5��86�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kaj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Kaj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@g4z�   G@k\    G@{�   G@p���   t�ah?]�(�Year��2014��2015��2016��2017-31�ehA]�(]�(�Year��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNe]�(�Cost��7,874��865��1,786��17,326�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kaj�  j�  )��}�(j�  �QTable 2.5.1.5 -Corra Linn Spillway Concrete and Spill Gate Rehabilitation Costs  �j�  Kaj�  (G@[L��   G@h��   G@�u�@   G@j�    t�ububh)��}�(hNh]�(hhC    �%0@���R�hhC    j�C@���R�G@�      G@��     t�ah?]�(N�#Mechanical and Electrical Equipment��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(Nj�  j�  j�  j�  j�  j�  j�  e]�(N�Projects�NN�(SOC��OOs)�NNe]�(�1��(Corra Linn Unit 2 Life Extension(C-5-09)��3,423�NNNNNe]�(�2��%AII Plants Station Service (G-147-06)��672�NNNNNe]�(�3��%Lower Bonnington and Upper Bonnington��90�NNNNNe]�(�4��Corra Linn Unit 3 Completion��722�NNNNNe]�(�5��'Upper Bonnington Old Plant Various Unit��1,311�NNNNNe]�(�6��&Upper Bonnington, Lower Bonnington and�NN�283��291��301�Ne]�(N�#AIlI Plants Heating and Ventilation�NNNNN�8,711�e]�(�8��$Upper Bonnington Old Unit Repowering�NNNNN�57,080�e]�(�9��AIl Plants Fire Water Supply�NNNNN�2,431�e]�(�10�� Mechanical Equipment Replacement�NNNNN�4,340�e]�(�11�� Electronic Equipment Replacement�NNNNN�4,340�e]�(�12��"Corra Linn Unit 3 Generator Rewind�NNNNN�4,344_�e]�(�13��%Corra Linn Unit 3 Turbine Replacement�NNNNN�3,974�e]�(�14��Upper Bonnington�NNNNN�3,974�e]�(�15��)Total Mechanical and Electrical Equipment��6,217�N�283��291��301��89,194�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kej�  Nubh)��}�(hNh]�(G@Q]�ێ8�G@R��    G@��DN8�G@��f`   t�ah?]�(�1Table 2.5.2 - Mechanical and Electrical Equipment�j  j  j  j  j  j  j  ehA]�(]�(�1Table 2.5.2 - Mechanical and Electrical Equipment�NNNNNNNe]�(j  �,Mechanical and Electrical Equipment
Projects��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �(Corra Linn Unit 2 Life Extension(C-5-09)��3,423�j�  j�  j�  j�  j�  e]�(j�  �%All Plants Station Service (G-147-06)��672�j�  j�  j�  j�  j�  e]�(j�  �HLower Bonnington and Upper Bonnington
Plant Totalizer Upgrade (G-195-10)��90�j�  j�  j�  j�  j�  e]�(j�  �Corra Linn Unit 3 Completion��722�j�  j�  j�  j�  j�  e]�(j�  �0Upper Bonnington Old Plant Various Unit
Upgrades��1,311�j�  j�  j�  j�  j�  e]�(j�  �CUpper Bonnington, Lower Bonnington and
Corra Linn Plants Automation�j�  j�  �283��291��301�j�  e]�(j�  �"All Plants Heating and Ventilation�j�  j�  j�  j�  j�  �8,711�e]�(j�  �$Upper Bonnington Old Unit Repowering�j�  j�  j�  j�  j�  �57,080�e]�(j�  �All Plants Fire Water Supply�j�  j�  j�  j�  j�  �2,431�e]�(�10�� Mechanical Equipment Replacement�j�  j�  j�  j�  j�  �4,340�e]�(�11�� Electronic Equipment Replacement�j�  j�  j�  j�  j�  �4,340�e]�(�12��"Corra Linn Unit 3 Generator Rewind�j�  j�  j�  j�  j�  �4,344�e]�(�13��%Corra Linn Unit 3 Turbine Replacement�j�  j�  j�  j�  j�  �3,974�e]�(�14��+Upper Bonnington Unit 6 Turbine
Replacement�j�  j�  j�  j�  j�  �3,974�e]�(�15��)Total Mechanical and Electrical Equipment��6,217�j�  �283��291��301��89,194�e]�(X�  2.5.2.1 Corra Linn Unit 2 Life Extension
The Corra Linn Unit 2 Life Extension was approved by Commission Order C-5-09. The
generating unit will be returned into service in December 2011, with completion of final
project tasks including final efficiency testing of the turbine in the second quarter of 2012.
The project will be fully closed out in the fourth quarter of 2012 after final inspection of the
turbine.
The costs are estimated to be $3.42 million in 2012.
2.5.2.2 All Plants Station Service
This project was approved by Commission Order G-147-06 and involves the installation of
new equipment and back-up power sources to ensure operational reliability and to address
environmental concerns at all four FortisBC generating plants. South Slocan was completed
in 2009 and Corra Linn was completed in 2010. Lower Bonnington is scheduled for
completion in 2011 and Upper Bonnington will be completed in 2012. The costs are
estimated to be $0.67 million in 2012.�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kej�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Kej�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ��@���R�hhC   ��e@@���R�G@�      G@��     t�ah?]�(�)�N�2.5.2.3�ehA]�(]�(jd  Nje  e]�(N�This project was��Loject was approved by Commission Order G-195-10 and involves replacing seven�e]�(�+��existing PSI Quac��RJ PSI Quad 4 meters with five new PML-7650 meters. This is a two-year project that�e]�(�>��will start in 2011��$rt in 2011 and be completed in 2012.�e]�(�5��The costs are est��.sts are estimated to be $0.09 million in 2012.�e]�(NN�2.5.2.4�e]�(N�The Corra Linn��Rrra Linn Unit 3 Life Extension was completed in 2000 and was the second project to�e]�(N�be undertaken in��Qertaken in the FortisBC Upgrade/Life Extension (ULE) program which began in 1998.�e]�(N�Since the time of��Qhe time of the project; the scope of the ULE program has evolved and the scope of�e]�(N�the more recent��	re recent�e]�(�2��required to prope��Ud to properly extend the life of all components of the generator unit. The Corra Linn�e]�(N�Unit 3 Completior��SCompletion project will include items which were not completed as part of the scope�e]�(�+��of the original life��Vriginal life extension in 2000, but were later determined as necessary upgrades on all�e]�(�5��remaining life ext��Sing life extension projects. All the other generating units on the ULE program have�e]�(N�receiveu��>a Ine necessaly upgrades [0 Meel lne Company $ rellablilly ana�e]�(N�Stanaara: Fropos��$ra: Froposea WOrk inciuaes insalling�e]�(N�Oll SpllI cOnainme��comainmen, upgrading�e]�(NN�1�e]�(NN�2 5 25�e]�(�)��As noted previou=��Md previouslv, the Companv has determined that the Upper Bonninaton Old Plant,�e]�(�3��with many compc��Tany components approaching 100 vears in age, will require a rebuild at some point in�e]�(N�the future. Currer��ire.�e]�(�5��of failure;��Qe, overall benefit to our customers, and the potential rate impact as a result of�e]�(K �undertaking the��Haking the upgrade. The assessment demonstrated the need to maintain this�e]�(N�generation resour��Wtion resource for the benefit of FortisBC customers , but also indicated that since the�e]�(�3��units are still oper��]re still operating satisfactorily , a full rebuild project for the Upper Bonnington Old Plant�e]�(N�is not required at��Oequired at this time. Recognizing the age of the equipment; the Company expects�e]�(N�that normal operz��Rrmal operation of this equipment could require higher maintenance costs over time.�e]�(N�Since many of the��Mnany of the major components of the Upper Bonnington generating units 1-4 are�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kfj�  Nubh)��}�(hNh]�((hhC   @��@���R�hhC   ��4B@���R�G@�      G@��     t�(hhC   @���?���R�hhC   �#�-@���R�G@�      G@��     t�eh?]�hA]�(]�(�1��2.5.2.12�e]�(�2�K e]�(�3�K e]�(�4��ato the high cost to repair: Monitoring of the coils has indicated that their current condition is�e]�(�5�K e]�(�6�K e]�(�7��2.5.2.13�e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�L7�K e]�(�6�K e]�(�7�K e]�(�8��PThis project is estimated to cost $3.97 million with an in-service date of 2026.�e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2��Sfacilities and the need to ensure the dams and spillway structures meet the minimum�e]�(�3��Xrequirements of the Provincial Water License Act and the Canadian Dam Safety Guidelines_�e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kij�  Nubh)��}�(hNh]�(hhC   ���7@���R�hhC   �Z@���R�G@�      hhC   @D�@���R�t�ah?]�(K �CElW"��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(K jW  jX  jY  jZ  j[  j\  j]  e]�(K �Dallly�NN�(S��OOOs)�NNe]�(K �&Upper Bonnington, Lower Bonnington and��250��259��264�NNNe]�(�2��AII Plants Safety and Security��471��475��424��437�NNe]�(K �AII Plants Fire Safety�NN�1,001��1,031��1,065��738�e]�(K �$AII Plants Surveillance and Security�NNNNN�5,991�e]�(K �Dam Safety Instrumentation�NNNNN�2,391�e]�(K �(Total Dam, Public Worker Safety Projects��721��734��1,689��1,468��1,065��9,120�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kkj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Kkj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@Q]gq�G@R��    G@��_�q�G@��f`   t�ah?]�(X�  dams are not governed by the BC Building Code, FortisBC has a responsibility to its employees and considers the fire upgrades as an important part of its safety program. FortisBC proposes to continue fire system upgrades and install fire rated doors and partitions within facilities to ensure that personnel can safely exit during a fire. Public safety was recently introduced as part of the Canadian Dam Safety Guidelines. There are inherent risks surrounding the daily operation of dams including strong currents, rapidly changing water levels and automatic equipment startup such as spillway gates. To increase public awareness and safety around its facilities FortisBC is implementing a four-year program in 2012 to protect the public from hazards and restrict public access to hazardous locations. This program will include the installation of barriers, gates and fencing. Table 2.5.3 - Dam, Public and Worker Safety�j  j  j  j  j  j  j  ehA]�(]�(X�  dams are not governed by the BC Building Code, FortisBC has a responsibility to its
employees and considers the fire upgrades as an important part of its safety program.
FortisBC proposes to continue fire system upgrades and install fire rated doors and
partitions within facilities to ensure that personnel can safely exit during a fire.
Public safety was recently introduced as part of the Canadian Dam Safety Guidelines. There
are inherent risks surrounding the daily operation of dams including strong currents, rapidly
changing water levels and automatic equipment startup such as spillway gates. To increase
public awareness and safety around its facilities FortisBC is implementing a four-year
program in 2012 to protect the public from hazards and restrict public access to hazardous
locations. This program will include the installation of barriers, gates and fencing.
Table 2.5.3 - Dam, Public and Worker Safety�NNNNNNNe]�(j  �&Dam, Public and Worker Safety Projects��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �=Upper Bonnington, Lower Bonnington and
Corra Linn Fire Panels��250��259��264�j�  j�  j�  e]�(j�  �All Plants Safety and Security��471��475��424��437�j�  j�  e]�(j�  �All Plants Fire Safety�j�  j�  �1,001��1,031��1,065��738�e]�(j�  �$All Plants Surveillance and Security�j�  j�  j�  j�  j�  �5,991�e]�(j�  �Dam Safety Instrumentation�j�  j�  j�  j�  j�  �2,391�e]�(j�  �(Total Dam, Public Worker Safety Projects��721��734��1,689��1,468��1,065��9,120�e]�(X�  2.5.3.1 Upper Bonnington, Lower Bonnington and Corra Linn
Fire Panels
This project involves the installation of a fire alarm panel at Upper Bonnington, Lower
Bonnington and Corra Linn Plants. Presently there is no alarm system in the plant except for
the water deluge system for the generating units. The proposed fire alarm panel will be multi
zone and will include fire full stations; audible and visual alarms; and fire and smoke
detectors. This alarm panel is for employee safe egress only. The panel will not include
controls nor will it be linked to a suppression system. The fire panel will annunciate to a
central monitoring location.
This project is estimated to cost $0.77 million with an in-service date of 2014.
2.5.3.2 All Plants Safety and Security
This project is to upgrade safety and security at the four FortisBC generating plants by
increasing the level of hazard awareness brought about by the posting of signs and the�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kkj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Kkj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ���S@���R�hhC   ���S@���R�G@�      hhC   �i�p@���R�t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j�  j�  j�  j�  j�  j�  j�  e]�(NNN�(S��OOOs)�NNe]�(�Cost��1,171��1,158��1,203��1,144��1,182��20,902�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kmj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Kmj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@d��   G@k\    G@}p�   G@p���   t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNe]�(�Cost��1,171��1,158��1,203��1,144��1,182��20,902�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kmj�  j�  )��}�(j�  �3Table 2.5.4 - All Plants Minor Sustainment Capital �j�  Kmj�  (G@g@     G@h��   G@{�    G@j�    t�ububh)��}�(hNh]�(hhC    ��9@���R�hhC   �8S@���R�G@�      hhC   �ؚ�@���R�t�ah?]�(�17��tworks Syste��m��nent Expene��ditures 200=��5�NehA]�(]�(j  j  j  j  j  j   Ne]�(N�2005��2006��2007��2008��2009��2010�e]�(�Expenditure Categories��Actual��Actual��Actual��Actual��Actual��Actual�e]�(NNN�(SOOC��Os)�NNe]�(�Transmission and Stations��58,271��45,091��69,068��46,961��49,985��80,647�e]�(�Distribution (net of CIAC)��25,305��28,909��25,411��24,755��23,658��23,923�e]�(�Telecommunications, SCADA��708��1,161��1,184��2,872��2,549��2,168�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Koj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Koj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@Kv}��m�G@R��    G@����m�G@��f`   t�ah?]�(X`  1 Detailed updates to the 2005 SDP were filed in 2005, 2006 and 2008 and included updated 2 load forecasts and equipment condition assessments. The Company has substantially 3 executed the SDP by way of its 2006, 2007-2008 and 2009-2010 Capital Expenditure Plans. 4 With a few exceptions as identified in the SDP updates, the major 2005 SDP projects 5 planned during the medium term will have been completed by year-end 2011. 6 In the six years since the 2005 SDP was issued, the Vaseux Lake Terminal station and bulk 7 transmission system Remedial Action Schemes (RAS) have been completed, the Big White 8 transmission line and substation have been completed along with many other distribution 9 substation and capacity upgrades in both the Okanagan and Kootenay regions. The 10 Okanagan Transmission Reinforcement (OTR) project, the largest project ever completed by 11 FortisBC, is needed to continue to supply reliable transmission service to the entire 12 Okanagan region and is scheduled for completion by the summer of 2012. The 13 implementation of these projects marks substantial completion of the bulk system upgrades 14 identified in the 2005 SDP, and provides a solid foundation for the future. 15 The Table 2.6.1 below outlines capital expenditures during the period between 2005 and 16 2010. 17 Table 2.6.1 - Networks System Development Expenditures 2005 - 2010�j  j  j  j  j  j  ehA]�(]�(X`  1 Detailed updates to the 2005 SDP were filed in 2005, 2006 and 2008 and included updated
2 load forecasts and equipment condition assessments. The Company has substantially
3 executed the SDP by way of its 2006, 2007-2008 and 2009-2010 Capital Expenditure Plans.
4 With a few exceptions as identified in the SDP updates, the major 2005 SDP projects
5 planned during the medium term will have been completed by year-end 2011.
6 In the six years since the 2005 SDP was issued, the Vaseux Lake Terminal station and bulk
7 transmission system Remedial Action Schemes (RAS) have been completed, the Big White
8 transmission line and substation have been completed along with many other distribution
9 substation and capacity upgrades in both the Okanagan and Kootenay regions. The
10 Okanagan Transmission Reinforcement (OTR) project, the largest project ever completed by
11 FortisBC, is needed to continue to supply reliable transmission service to the entire
12 Okanagan region and is scheduled for completion by the summer of 2012. The
13 implementation of these projects marks substantial completion of the bulk system upgrades
14 identified in the 2005 SDP, and provides a solid foundation for the future.
15 The Table 2.6.1 below outlines capital expenditures during the period between 2005 and
16 2010.
17 Table 2.6.1 - Networks System Development Expenditures 2005 - 2010�NNNNNNe]�(�Expenditure Categories��2005��2006��2007��2008��2009��2010�e]�(N�Actual��Actual��Actual��Actual��Actual��Actual�e]�(N�($000s)�NNNNNe]�(�Transmission and Stations��58,271��45,091��69,068��46,961��49,985��80,647�e]�(�Distribution (net of CIAC)��25,305��28,909��25,411��24,755��23,658��23,923�e]�(�5Telecommunications, SCADA,
and Protection and Control��708��1,161��1,184��2,872��2,549��2,168�e]�(X  18 Note: excluding cost of removal.
19 2.6.2 MAJOR PROJECTS UNDERTAKEN SINCE 2005 SYSTEM DEVELOPMENT
20 PLAN
21 The most significant projects completed since 2005 include:
22 • Comprehensive rehabilitation initiatives on 44 Line to Osoyoos and 49 Line to
23 Summerland;
24 • Completion of distribution system rebuilds and voltage upgrades in Rossland,
25 Warfield and West Trail;�NNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Koj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Koj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   �b��?���R�hhC    �2@���R�G@�      G@��     t�ah?]�(�2�N�0to improve import capacity into Okanagan region;�ehA]�(]�(j�  Nj�  e]�(�3�NK e]�(�4�NK e]�(�5�NK e]�(�6�N�Mbackup to the Creston area was completed in 2004 and included preparation for�e]�(NNK e]�(�8�NK e]�(�9�NK e]�(�10�NK e]�(�11�N�P(CPC) new Brilliant Terminal Station and the new Warfield Terminal Station. This�e]�(�12�NK e]�(�13�NK e]�(�14�NK e]�(�10�NK e]�(�16�NK e]�(�18�N�Psvstem. The 230 kV bus was temporarilv operated at 161 kV until the first 230 kV�e]�(�1o�NK e]�(�20�N�Id d�e]�(�21�NK e]�(�22�NK e]�(�23�NK e]�(�24�N�Terminal near Oliver;�e]�(�25�N�LCompletion of the Kelowna Area Upgrade including the DG Bell Terminal 230 kV�e]�(�26�NK e]�(�27�NK e]�(�28�N�#Lee Terminal station as well as the�e]�(�29�N�Tprotection to improve capacity and reliability of supply to the City of Kelowna; and�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kpj�  Nubh)��}�(hNh]�(hhC   �"?P@���R�hhC   @}gX@���R�G@�      hhC   �`�@���R�t�ah?]�(�Region��63 kV��138 kV��161��230 kV��Total�ehA]�(]�(j�  j�  j�  j�  j�  j�  e]�(NNN�(kms)�NNe]�(�North Okanagan��2��114��0��114��230�e]�(�South Okanagan��164��104��56��31��355�e]�(�Kootenay��396��3��87��52��538�e]�(�Boundary��164��0��103��0��266�e]�(�Total��726��221��246��197��1,390�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Krj�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  Krj�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@aG�   G@pHR    G@~�(�   G@wK�   t�ah?]�(�Region��63 kV��138 kV��161 kV��230 kV��Total�ehA]�(]�(�Region��63 kV��138 kV��161 kV��230 kV��Total�e]�(j  �(kms)�NNNNe]�(�North Okanagan�j�  �114��0��114��230�e]�(�South Okanagan��164��104��56��31��355�e]�(�Kootenay��396�j�  �87��52��538�e]�(�Boundary��164�j.  �103�j.  �266�e]�(�Total��726��221��246��197��1,390�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Krj�  j�  )��}�(j�  �DTable 2.7.2 - Transmission Line Lengths by Region and Voltage Class �j�  Krj�  (G@`��   G@nZx�   G@:    G@p x    t�ububh)��}�(hNh]�(G@w� �   G@h?#�   G@x��   G@mv5�   t�ah?]�j  ahA]�(]�j  a]��ACK�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �to Mica GS (BC Hydro)�j�  Ksj�  (G@g��   G@g/�   G@r�)�   G@h�i`   t�ububh)��}�(hNh]�(G@kN!�   G@p�    G@p@!    G@qV��   t�ah?]�(�NI��C�ehA]�]�(�NI�jc  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �AreaVernon �j�  Ksj�  (G@sH�   G@m!@   G@u�X    G@p:�@   t�ububh)��}�(hNh]�(G@u��   G@p�    G@w;{�   G@r�i�   t�ah?]�j  ahA]�(]�j  a]��VNT�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �AreaVernon �j�  Ksj�  (G@sH�   G@m!@   G@u�X    G@p:�@   t�ububh)��}�(hNh]�(G@sj�   G@s�+    G@t�3�   G@v���   t�ah?]��LEE�ahA]�(]��LEE�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �VNTgridto SEL (BC Hydro)�j�  Ksj�  (G@b@�@   G@p9��   G@���@   G@q�4�   t�ububh)��}�(hNh]�(G@r&�   G@w�x�   G@sj�   G@zv��   t�ah?]�j  ahA]�(]�j  a]��DGB�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �Kelowna AreaLEE�j�  Ksj�  (G@j�g�   G@t�f�   G@t�%    G@v�p    t�ububh)��}�(hNh]�(G@o3�   G@z�O    G@p�f�   G@}zؠ   t�ah?]�j  ahA]�(]�j  a]��RGA�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �DGB�j�  Ksj�  (G@rd    G@x���   G@s{g    G@y��    t�ububh)��}�(hNh]�(G@o3�   G@1�   G@p�f�   G@��[�   t�ah?]�j  ahA]�(]�j  a]��VAS�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �to SEL (BC Hydro)Area�j�  Ksj�  (G@f��    G@|;D@   G@|��@   G@~���   t�ububh)��}�(hNh]�(G@mj��   G@�˷    G@p�`   G@�{�   t�ah?]�j  ahA]�(]�j  a]��BEN�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �GGenerationto Princeton 63 kV�j�  Ksj�  (G@W��`   G@�4    G@�!�   G@���   t�ububh)��}�(hNh]�(G@{��   G@�1��   G@|2[    G@��    t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ksj�  j�  )��}�(j�  �"(FortisBC)GGenerationto Princeton �j�  Ksj�  (G@W��`   G@�r    G@�!�   G@�G�   t�ububh)��}�(hNh]�(G@| ʪ��G@e���   G@|���333G@l���   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �
plantsLBOG�j�  Ktj�  (G@o;ؠ   G@b���   G@ux��   G@d�׀   t�ububh)��}�(hNh]�(G@p��    G@fo$    G@r{2    G@l���   t�ah?]��SLC�ahA]�(]��SLC�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �BC HydroGplants�j�  Ktj�  (G@q��@   G@c,p    G@��    G@gZ�    t�ububh)��}�(hNh]�(G@t�|UUUUG@mx@   G@vf��   G@q�N�   t�ah?]�j  ahA]�(]�j  a]��KCL�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �GPower Corp.(BC Hydro)�j�  Ktj�  (G@a��   G@j�@   G@�3�    G@m�:@   t�ububh)��}�(hNh]�(G@qĴ    G@s�T    G@t�|UUUUG@u��   t�ah?]�(�B��TS�ehA]�]�(j�  �TS�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �BRXGCorp. plants�j�  Ktj�  (G@_l?@   G@rN+�   G@m��   G@t0��   t�ububh)��}�(hNh]�(G@}巠   G@w
\@   G@h(�   G@z-@   t�ah?]��AAL�ahA]�(]��AAL�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �Trail/Garea�j�  Ktj�  (G@kxH�   G@u1 `   G@�C�   G@w��`   t�ububh)��}�(hNh]�(G@r�b   G@w�.    G@tP��   G@z��    t�ah?]�j  ahA]�(]�j  a]��WTS�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �CastlegarTrail/G�j�  Ktj�  (G@j@   G@vW��   G@z��   G@x[q`   t�ububh)��}�(hNh]�(G@y{��   G@x�`   G@z�q�   G@{�^    t�ah?]�j  ahA]�(]�j  a]��SEL�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �areaAALCastlegar �j�  Ktj�  (G@j@   G@wr�`   G@T�   G@y&`   t�ububh)��}�(hNh]�(G@jJi`   G@z��    G@p*    G@|na`   t�ah?]�(�AS��M�ehA]�]�(�AS�j:  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �
SELWTSarea�j�  Ktj�  (G@k�6    G@x=�`   G@zۛ    G@z��    t�ububh)��}�(hNh]�(G@t�$�   G@|�`   G@v�`   G@��@   t�ah?]��ESS�ahA]�(]��ESS�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �Bd/ASM�j�  Ktj�  (G@c��   G@{$��   G@nş@   G@}a��   t�ububh)��}�(hNh]�(G@w�s�   G@�]�   G@y��   G@���   t�ah?]��WAN�ahA]�(]��WAN�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �G(FortisBC)ESS�j�  Ktj�  (G@c+`   G@}�^�   G@x�f    G@�*d    t�ububh)��}�(hNh]�(G@{Ah�   G@�]�   G@|���333G@���   t�ah?]�j  ahA]�(]�j  a]��NLY�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Ktj�  j�  )��}�(j�  �G(FortisBC)ESS�j�  Ktj�  (G@c+`   G@}�^�   G@x�f    G@�*d    t�ububh)��}�(hNh]�(hhC    '�?���R�hhC    �B@���R�G@�      G@��     t�ah?]�(�1��2.7.6��6�ehA]�(]�(jz  j{  j|  e]�(�2��The solution to a reliz��1�e]�(�3��option: In many case��cases, however,�e]�(�4��developed to mitigate�K e]�(�5��define the most cost�K e]�(�6��includes, in addition��Ition to project financial costs, a consideration of reliability costs and�e]�(�7��	benefits,�K e]�(�8��employs quantitative��Mative analytical methods to define the costs and benefits associated with the�e]�(�9��reliability attributes of��es of each project.�e]�(�0��Figure 2.7.6 (a) shom��Pshows that the cost to the utility increases with increasing reliability, due to�e]�(�1��the higher capital exp��L/ expenditures required to provide a higher level of system reliability. The�e]�(�2��costs to customers d��Gers decrease with increasing reliability, due to avoided costs of power�e]�(�3��interruptions to reside��Eesidential, commercial and industrial customers. The sum of these two�e]�(�4��costs will be the total��Ntotal cost to society: The optimum level of reliability is therefore the point�e]�(�5��where the total cost i��ost is minimized.�e]�(�6�N�Figure 2.7.6 (a)�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K|j�  Nubh)��}�(hNh]�(G@b�1ꪪ�G@q���   G@{�KP   G@����   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  a]�j  a]�j  a]�j  a]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K}j�  j�  )��}�(j�  �6Figure 2.7.6 (b) - FortisBC Customer Damage Functions �j�  K}j�  (G@e9��   G@o�0�   G@|�z`   G@p��    t�ububh)��}�(hNh]�(G@e�*���G@r\�j���G@w��    G@~Ȫ���t�ah?]�j  ahA]�(]�j  a]�j  a]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K~j�  j�  )��}�(j�  �.Figure 2.7.6 (c) - SAIFI for FortisBC and CEA �j�  K~j�  (G@h�`   G@p\�@   G@{    G@qO�    t�ububh)��}�(hNh]�(G@fA�`   G@[�!UUUUG@w���   G@r���UUUt�ah?]�j  ahA]�(]�j  a]�j  a]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Kj�  j�  )��}�(j�  �*Figure 2.7.6 (d) - SAIDI FortisBC and CEA �j�  Kj�  (G@i���   G@S�w    G@z�O    G@W�f    t�ububh)��}�(hNh]�(hhC    X�@���R�hhC   ��aD@���R�G@�      G@��     t�ah?]�(K �Vde��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(K j�  j�  j�  j�  j�  j�  j�  e]�(K �
Tanomssion�NN�(S��OOOs)�NNe]�(�1��#Okanagan Transmission Reinforcement��2,219�NNNNNe]�(�2��$Ellison to Sexsmith Transmission Tie��7,121��413�NNNNe]�(�3�� Grand Forks Transformer Addition��2,491��4,714��1,274��7,549�NNe]�(�4��*Kelowna Bulk Transformer Capacity Addition�N�3,720��10,832��11,014�NNe]�(�5��*42 Line Meshed Operation (Huth and Oliver)�NN�278�NNNe]�(�6��Capacitors at Bentley Terminal�NNN�875��4,389�Ne]�(N�Reconductor 52 Line and 53 Line�NNN�875��1,479��4,403�e]�(�8��Meshing Kelowna Loop�NN�2,753��2,798��2,577�Ne]�(�9��)Summerland Substation Transformer Upgrade�NN�2,152��4,427�NNe]�(�10��Beaver Valley Solution�NNNN�758��20,776�e]�(K �,RG Anderson Distribution Transformer Upgrade�NNNN�3,031��4,061�e]�(K �DG Bell Static VAR Compensator�NNNN�3,031��34,285�e]�(�13��DG Bell 230 kV Ring Bus�NNNNN�35,383�e]�(K �$DG Bell Second 230/138kV Transformer�NNNNN�18,746�e]�(�15��'Vaseux Lake Third 500/230kV Transformer�NNNNN�31.581�e]�(K �'Vaseux Lake Third 500/230kV Transformer�NNNNN�31,581�e]�(K �Boundary Area Supply�NNNNN�9,629�e]�(�17��"Reconductor 31 Line (Creston Area)�NNNNN�2,307�e]�(�18��,Stoney Creek Second Distribution Transformer�NNNNN�17,328�e]�(�19��/Playmor 25 kV Distribution Transformer Addition�NNNNN�15,612�e]�(�20��+Reconductor 50 Line (Recreation to Saucier)�NNNNN�295�e]�(�21��/Reconductor 50 Line (FA Lee to Springfield Tap)�NNNNNNe]�(K �.Reconductor 51 Line and 60 Line (DG Bell to OK�NNNNN�9,184�e]�(K �/Reconductor 54 Line (DG Bell to Black Mountain)�NNNNNNe]�(�24��(FA Lee Distribution Transformer Addition�NNNNN�12,029�e]�(K �New Enterprise Substation�NNNNN�35,798�e]�(K �1Sexsmith Second Distribution Transformer Addition�NNNNN�9,377�e]�(K �0Saucier Second Distribution Transformer Addition�NNNNN�7,415�e]�(K �2Benvoulin Second Distribution Transformer Addition�NNNNN�11,752�e]�(�29��0Ellison Second Distribution Transformer Addition�NNNNN�7,850�e]�(�30��0DG Bell Second Distribution Transformer Addition�NNNNN�10,041�e]�(�31��New Central Okanagan Station�NNNNN�37,004�e]�(�32��Creston Area Capacity Increase�NNNNN�14,017�e]�(�33��Total Transmission Growth��11,832��8,847��17,287��27,537��15,265��348,873�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(h]�(j^  j$	  j$	  j$	  j$	  j$	  j$	  j$	  eh]�(G@Co�\   G@R��    G@�ygq�G@��f`   t�ah?]�(�41 Table 2.8 (a) - Transmission and Stations Projects�j  j  j  j  j  j  j  ehA]�(]�(�41 Table 2.8 (a) - Transmission and Stations Projects�NNNNNNNe]�(j  �Transmission Growth��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �#Okanagan Transmission Reinforcement��2,219�j�  j�  j�  j�  j�  e]�(j�  �$Ellison to Sexsmith Transmission Tie��7,121��413�j�  j�  j�  j�  e]�(j�  � Grand Forks Transformer Addition��2,491��4,714��1,274��7,549�j�  j�  e]�(j�  �*Kelowna Bulk Transformer Capacity Addition�j�  �3,720��10,832��11,014�j�  j�  e]�(j�  �*42 Line Meshed Operation (Huth and Oliver)�j�  j�  �278�j�  j�  j�  e]�(j�  �Capacitors at Bentley Terminal�j�  j�  j�  �875��4,389�j�  e]�(j�  �Reconductor 52 Line and 53 Line�j�  j�  j�  �875��1,479��4,403�e]�(j�  �Meshing Kelowna Loop�j�  j�  �2,753��2,798��2,577�j�  e]�(j�  �)Summerland Substation Transformer Upgrade�j�  j�  �2,152��4,427�j�  j�  e]�(�10��Beaver Valley Solution�j�  j�  j�  j�  �758��20,776�e]�(�11��,RG Anderson Distribution Transformer Upgrade�j�  j�  j�  j�  �3,031��4,061�e]�(�12��DG Bell Static VAR Compensator�j�  j�  j�  j�  �3,031��34,285�e]�(�13��DG Bell 230 kV Ring Bus�j�  j�  j�  j�  j�  �35,383�e]�(�14��$DG Bell Second 230/138kV Transformer�j�  j�  j�  j�  j�  �18,746�e]�(�15��'Vaseux Lake Third 500/230kV Transformer�j�  j�  j�  j�  j�  �31,581�e]�(�16��Boundary Area Supply�j�  j�  j�  j�  j�  �9,629�e]�(�17��"Reconductor 31 Line (Creston Area)�j�  j�  j�  j�  j�  �2,307�e]�(�18��5Stoney Creek Second Distribution Transformer
Addition�j�  j�  j�  j�  j�  �17,328�e]�(�19��/Playmor 25 kV Distribution Transformer Addition�j�  j�  j�  j�  j�  �15,612�e]�(�20��+Reconductor 50 Line (Recreation to Saucier)�j�  j�  j�  j�  j�  �295�e]�(�21��/Reconductor 50 Line (FA Lee to Springfield Tap)�j�  j�  j�  j�  j�  j�  e]�(�22��7Reconductor 51 Line and 60 Line (DG Bell to OK
Mission)�j�  j�  j�  j�  j�  �9,184�e]�(�23��/Reconductor 54 Line (DG Bell to Black Mountain)�j�  j�  j�  j�  j�  j�  e]�(�24��(FA Lee Distribution Transformer Addition�j�  j�  j�  j�  j�  �12,029�e]�(�25��New Enterprise Substation�j�  j�  j�  j�  j�  �35,798�e]�(�26��1Sexsmith Second Distribution Transformer Addition�j�  j�  j�  j�  j�  �9,377�e]�(�27��0Saucier Second Distribution Transformer Addition�j�  j�  j�  j�  j�  �7,415�e]�(�28��2Benvoulin Second Distribution Transformer Addition�j�  j�  j�  j�  j�  �11,752�e]�(�29��0Ellison Second Distribution Transformer Addition�j�  j�  j�  j�  j�  �7,850�e]�(�30��0DG Bell Second Distribution Transformer Addition�j�  j�  j�  j�  j�  �10,041�e]�(�31��New Central Okanagan Station�j�  j�  j�  j�  j�  �37,004�e]�(�32��Creston Area Capacity Increase�j�  j�  j�  j�  j�  �14,017�e]�(�33��Total Transmission Growth��11,832��8,847��17,287��27,537��15,265��348,873�e]�(j  NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j$	  j$	  j$	  j$	  j$	  j�  eh]�(hhC   �\,$@���R�hhC   �ϲB@���R�G@�      G@��     t�ah?]�(N�3��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(NjF  jG  jH  jI  jJ  jK  jL  e]�(N�
Tansmssion�NNN�OOOs)�NK e]�(�34��&Transmission Line Condition Assessment��522��485��480��547��543�K e]�(�35�� Transmission Line Rehabilitation��3,372��2,621��2,509��2,424��2,820��50,158�e]�(�36�� Transmission Line Urgent Repairs��594��620��616��622��661��11,543�e]�(�37��(Transmission Line Right of Way Easements��400��400��416��423��440��7,754�e]�(�38��.6 Line /26 Line River Crossing Reconfiguration��1,185�NNNNNe]�(�39��"27 Line Rebuild (Corra Linn-Salmo)��1,161�NNNNNe]�(�40��'21-24 Lines Rebuild (Generation Plants)��2,219�NNNNNe]�(�41��19 Line/29 Line Reconfiguration�N�791�NNNNe]�(�42��)20 Line Rebuild (Warfield Terminal-Salmo)�N�4,664�NNNNe]�(�43��330 Line Lake Crossing Assessment and Rehabilitation�NNN�802��1,521�Ne]�(�44��Total Transmission Sustainment��9,453��9,581��4,021��4,818��5,984��79,671�e]�(�45�NNNNNNNe]�(K �Stations Sustainment��2012��2013��2014��2015��2016�K e]�(K �)Environmental Compliance (PCB Mitigation)��11,269��11,553��4,574�NNK e]�(�48��Station Urgent Repairs��818��907��879��977��942�K e]�(�49��)Station AssessmentlMinor Planned Projects��1,343��1,354��1,410��1,433��1,489��26,245�e]�(�50��,Add Arc Flash Detection to Legacy Metal-Clad��539��544��566��1,140��1,184�K e]�(�51��(Huth Low Voltage Breaker Replacement (2)�N�69��550�NNK e]�(�52��&Switchaear Replacement Proaram (13 kV)�NN�1,651�N�983��2.651�e]�(K �Ground Grid Unarades�NN�748�N�790�K e]�(�54��.DG Bell 138 kV Breaker and Voltage Transformer�NN�338��938�NNe]�(�55��#Osoyoos 63 kV Breaker Additions (2)�NNN�364��2,359�Ne]�(�56��Bulk Oil Breaker Replacements�NNN�733��761��2,972�e]�(�57��Station Oil Containment�NNN�445��462��2,664�e]�(�58��'Minimum Oil Circuit Breaker Replacement�NNNNN�21,175�e]�(�59��+Major Transmission Transformer Replacements�NNNNN�32,017�e]�(�60��%Distribution Transformer Replacements�NNNNN�10,978�e]�(�61��Stations Sustainment Total��13,969��14,427��10,716��6,030��8,970�K e]�(�62�NNNNNNNe]�(�63��(Total Transmission and Stations Projects��35,255��32,854��32,024��38,385��30,220��550,714�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@@`Y`   G@R��    G@���d�6NG@��f`   t�ah?]�(�=1 Table 2.8 (a) - Transmission and Stations Projects cont’d�j  j  j  j  j  j  j  ehA]�(]�(�=1 Table 2.8 (a) - Transmission and Stations Projects cont’d�NNNNNNNe]�(j  �Transmission Sustainment��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(�34��&Transmission Line Condition Assessment��522��485��480��547��543��10,216�e]�(�35�� Transmission Line Rehabilitation��3,372��2,621��2,509��2,424��2,820��50,158�e]�(�36�� Transmission Line Urgent Repairs��594��620��616��622��661��11,543�e]�(�37��(Transmission Line Right of Way Easements��400��400��416��423��440��7,754�e]�(�38��.6 Line /26 Line River Crossing Reconfiguration��1,185�j�  j�  j�  j�  j�  e]�(�39��"27 Line Rebuild (Corra Linn-Salmo)��1,161�j�  j�  j�  j�  j�  e]�(�40��'21-24 Lines Rebuild (Generation Plants)��2,219�j�  j�  j�  j�  j�  e]�(�41��19 Line/29 Line Reconfiguration�j�  �791�j�  j�  j�  j�  e]�(�42��)20 Line Rebuild (Warfield Terminal-Salmo)�j�  �4,664�j�  j�  j�  j�  e]�(�43��330 Line Lake Crossing Assessment and Rehabilitation�j�  j�  j�  �802��1,521�j�  e]�(�44��Total Transmission Sustainment��9,453��9,581��4,021��4,818��5,984��79,671�e]�(�45�j  NNNNNNe]�(�46��Stations Sustainment��2012��2013��2014��2015��2016��2017-31�e]�(�47��)Environmental Compliance (PCB Mitigation)��11,269��11,553��4,574�j�  j�  j�  e]�(�48��Station Urgent Repairs��818��907��879��977��942��17,065�e]�(�49��)Station Assessment/Minor Planned Projects��1,343��1,354��1,410��1,433��1,489��26,245�e]�(�50��7Add Arc Flash Detection to Legacy Metal-Clad
Switchgear��539��544��566��1,140��1,184�j�  e]�(�51��(Huth Low Voltage Breaker Replacement (2)�j�  �69��550�j�  j�  j�  e]�(�52��&Switchgear Replacement Program (13 kV)�j�  j�  �1,651�j�  �983��2,651�e]�(�53��Ground Grid Upgrades�j�  j�  �748�j�  �790��6,403�e]�(�54��7DG Bell 138 kV Breaker and Voltage Transformer
Addition�j�  j�  �338��938�j�  j�  e]�(�55��#Osoyoos 63 kV Breaker Additions (2)�j�  j�  j�  �364��2,359�j�  e]�(�56��Bulk Oil Breaker Replacements�j�  j�  j�  �733��761��2,972�e]�(�57��Station Oil Containment�j�  j�  j�  �445��462��2,664�e]�(�58��'Minimum Oil Circuit Breaker Replacement�j�  j�  j�  j�  j�  �21,175�e]�(�59��+Major Transmission Transformer Replacements�j�  j�  j�  j�  j�  �32,017�e]�(�60��%Distribution Transformer Replacements�j�  j�  j�  j�  j�  �10,978�e]�(�61��Stations Sustainment Total��13,969��14,427��10,716��6,030��8,970��122,170�e]�(�62�j  j  j  j  j  j  j  e]�(�63��(Total Transmission and Stations Projects��35,255��32,854��32,024��38,385��30,220��550,714�e]�(j  NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j$	  eh]�(hhC   ����?���R�hhC   ���G@���R�G@�      G@��     t�ah?]�(�1��2.8.1��.8.1�ehA]�(]�(j�  j�  j�  e]�(�2��The OTR Project wa��Bject was approved by Commission Order C-5-08. To allow independent�e]�(�3��switching of the Fort=��Mhe FortisBC 230 kV transformers, BC Hydro is required to install a new 500 kV�e]�(�4��circuit breaker on the��Kr on the BC Hydro 500 kV side of the station with associated protection and�e]�(�5��control changes. BC��HBC Hydro has submitted its project plan for this required work, with the�e]�(�6��scheduled in-service��Nservice date delayed from the fall of 2011 to summer 2012. This delay resulted�e]�(�7��from extended discu��Id discussion and negotiations with respect to BC Hydro cost estimates and�e]�(�8��management. Fortis)��KFortisBC signed off the facilities agreement at the end of February 2011 to�e]�(�9��secure the in-service��D-service date. This component of work has no material impacts to the�e]�(�0��completion of FortisE��'FortisBC's new and upgraded facilities:�e]�(�1��The BC Hydro VaseL��Lo Vaseux Terminal 500 kV work will be completed in 2012 with expenditures of�e]�(�2��S2.2 million in 2012.��1�e]�(�3��Project is forecast to��cast to be under budget:�e]�(�4��2.8.2��.8.2�e]�(�5��The Ellison and Duch��Lnd Duck Lake Substations currently are fed radially from the FA Lee Terminal�e]�(�0��Slalion��LiC.�e]�(�7��transmission line intc��Nline into the area, it is not possible to completely restore supply until that�e]�(�8��transmission line is��Sline is repaired. Additionally, there is minimal distribution backup into this area�e]�(�9��as the adiacent Sexs��Ont Sexsmith distribution source is alreadv heavilv loaded and is a lon distance�e]�(�0��from the maioritv of t��Oritv of the load concentration (five kilometres and areater) . There are also a�e]�(�0��from the majority of��Nrity of the load concentration (five kilometres and greater): There are also a�e]�(�1��number of large cust��Fge customers in this area including the University of British Columbia�e]�(�2��Okanagan; Kelowna��Kelowna International Airport and Kelowna Flightcraft. These customers would�e]�(�3��be impacted��oy an extended outage.�e]�(�4��The need for the Elli=��Pthe Ellison to Sexsmith tie was identified in the application for a CPCN for the�e]�(�5��Ellison Substation, a��,ation, approved by Commission Order C-4-07 _�e]�(�6��to be constructed in��Ected in 2010. In its 2009 System Development Plan Update, the Company�e]�(�7��rescheduled the tran��Khe transmission loop for the Sexsmith, Ellison and Duck Lake substations to�e]�(�8��the 2011��Mter timeframe. With the addition of new distribution load (BC Hydro customers�e]�(�9��in the Winfield area)��Jd area) onto the Duck Lake Substation in 2011, a transmission outage on 46�e]�(�0��Line will affect appro��4t approximately 9,700 customers served by this line_�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012 I S PNTEGRATEDYSTEMLAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j^  j$	  j^  eh]�(hhC   ���(@���R�hhC   �,�D@���R�G@�      G@��     t�ah?]�(�5��2.8.3�K ehA]�(]�(jn  jo  K e]�(N�Introduction and Pr�K e]�(�8��The Grand Forks Ter�K e]�(�9��is intended to addres�K e]�(�10��1 .�K e]�(�11��2 .�K e]�(�12��There is a significant�K e]�(�13��projects are consider�K e]�(�14��The full project is pro�K e]�(�15��transmission transfor�K e]�(�16��high-capacity commi�K e]�(�17��constructed. In 2014/�K e]�(�18��aging�K e]�(�19��	salvaged:�K e]�(�21��storage of the transfc�K e]�(�22��link between Gr�K e]�(�23��installation of the trar�K e]�(�24��the salvage costs for�K e]�(�25��Expenditure Plan app�K e]�(�26��The following section�K e]�(�27��leading to the propos�K e]�(�28��Grand Forks Area�K e]�(�29��The Grand Forks Ter�K e]�(�30��supply for Grand For�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�((hhC   `�� @���R�hhC   @��=@���R�G@�      G@��     t�(hhC    ]t�?���R�hhC   ���1@���R�G@�      G@��     t�eh?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3��
solutions:�e]�(N�Paqe 96�e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC    ��A@���R�hhC    .,D@���R�G@�      hhC   ����@���R�t�ah?]�(NN�Optio��n�ehA]�(]�(NNjJ  jK  e]�(�Delems��1��2��3�e]�(�:Provides N-1 transmission reliability for Grand Forks area�NNNe]�(�=Minimizes substation construction at the Grand Forks terminal�NNNe]�(�CAllows salvage of 9 and 10 Line between Rossland and Christina Lake�NNNe]�(�LReduces exposure to transmissionldistribution underbuild over-voltage events�NNNe]�(�DProvides high-capacity communications between Okanagan and Kootenays�NNNe]�(�MReduces dependence and ongoing lease costs for third-party telecommunications�NNNe]�(�MProvides opportunity for additional ongoing revenue from surplus fibre leases�NNNe]�(�8Takes advantage of spare transformer from Oliver station�NNNe]�(�LLowest environmental and public impact from transmission line infrastructure�NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@U%�UUUG@X�    G@�3    G@q:=�   t�ah?]�(�.Table 2.8.3 - Comparison of Benefits by Option�j  ehA]�(]�(�Benefits��Option�NNe]�(Nja
  j�  j�  e]�(�:Provides N-1 transmission reliability for Grand Forks area��X�js  js  e]�(�=Minimizes substation construction at the Grand Forks terminal�js  j  js  e]�(�CAllows salvage of 9 and 10 Line between Rossland and Christina Lake�js  js  j  e]�(�LReduces exposure to transmission/distribution underbuild over-voltage events�js  js  j  e]�(�DProvides high-capacity communications between Okanagan and Kootenays�js  j  j  e]�(�MReduces dependence and ongoing lease costs for third-party telecommunications�js  j  j  e]�(�MProvides opportunity for additional ongoing revenue from surplus fibre leases�js  j  j  e]�(�8Takes advantage of spare transformer from Oliver station�js  js  j  e]�(�LLowest environmental and public impact from transmission line infrastructure�js  js  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �/Table 2.8.3 - Comparison of Benefits by Option �j�  K�j�  (G@h�    G@T��    G@{�;@   G@XN�    t�ububh)��}�(hNh]�(hhC   �r(@���R�hhC   �@S@@���R�G@�      G@��     t�ah?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�5�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   �ԻB@���R�hhC   �o�D@���R�G@�      G@��     t�ah?]�(�KELOWNA�NN�POWER FI��LOW IN % OF NC�K ehA]�(]�(j�  NNj�  j�  K e]�(�PEAK��	CONDITION��YEAR�N�EE�K e]�(�(MW)�NN�T3��T4�K e]�(N�AIl elements in-service�N�81��79�K e]�(�369��
LEE T3 out��2012-13�N�101�K e]�(N�
DGB T2 out�N�86��85�K e]�(N�73L out (RGA-LEE)�N�90��89�K e]�(N�AIl elements in-service�N�84��83�K e]�(�384��
LEE T3 out��2013-14�N�106�K e]�(N�
DGB T2 out�N�90��89�K e]�(N�73L out (RGA-LEE)�N�96��95�K e]�(N�AIl elements in-service�N�84��82�K e]�(�395��
LEE T3 out��2014-15�N�103�K e]�(N�
DGB T2 out�N�94��93�K e]�(N�73L out (RGA-LEE)�N�100��100�K e]�(N�AIl elements in-service�N�86��85�K e]�(�407��
LEE T3 out��2015-16�N�106�K e]�(N�
DGB T2 out�N�96��96�K e]�(N�73L out (RGA-LEE)�N�104��104�K e]�(N�AIl elements in-service�N�89��89�K e]�(�417��
LEE T3 out��2016-17�N�110�K e]�(N�
DGB T2 out�N�99��99�K e]�(N�73L out (RGA-LEE)�N�108��108�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@TN�X���G@X.    G@�&�   G@7
0   t�ah?]�(�KELOWNA WINTER PEAK LOAD (MW)�j  j  �-POWER FLOW IN % OF NORMAL OR EMERGENCY RATING�j  j  ehA]�(]�(�KELOWNA
WINTER
PEAK
LOAD
(MW)�j  j  �-POWER FLOW IN % OF NORMAL OR
EMERGENCY RATING�NNe]�(N�	CONDITION��YEAR
(WINTER)��LEE�N�DGB�e]�(Nj  j  �T3��T4��T2�e]�(j  �All elements in-service�j  �81��79��55�e]�(�369��
LEE T3 out��2012-13�j�  �101��59�e]�(j  �
DGB T2 out�j  �86��85�j�  e]�(j  �73L out (RGA-LEE)�j  �90��89�j�  e]�(j  �All elements in-service�j  �84��83��57�e]�(�384��
LEE T3 out��2013-14�j�  �106��61�e]�(j  �
DGB T2 out�j  �90��89�j�  e]�(j  �73L out (RGA-LEE)�j  �96��95�j�  e]�(j  �All elements in-service�j  �84��82��66�e]�(�395��
LEE T3 out��2014-15�j�  �103��69�e]�(j  �
DGB T2 out�j  �94��93�j�  e]�(j  �73L out (RGA-LEE)�j  �100��100�j�  e]�(j  �All elements in-service�j  �86��85��67�e]�(�407��
LEE T3 out��2015-16�j�  �106��70�e]�(j  �
DGB T2 out�j  �96��96�j�  e]�(j  �73L out (RGA-LEE)�j  �104��      �104�j�  e]�(j  �All elements in-service�j  �89��89��67�e]�(�417��
LEE T3 out��2016-17�j�  �110��72�e]�(j  �
DGB T2 out�j  �99��99�j�  e]�(j  �73L out (RGA-LEE)�j  �108��108�j�  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �CTable 2.8.4 - Kelowna Transformation Capacity - Load Flow Analysis �j�  K�j�  (G@a�    G@S�w    G@�    G@W�f    t�ububh)��}�(hNh]�(hhC    a�?���R�hhC    ��4@���R�G@�      G@��     t�ah?]�hA]�(]�(�1��YVaseux Lake Terminal station, and a 63 kV line (42 Line) from the RG Anderson Terminal in�e]�(�2��ZPenticton. Load in this area can also be served via a 138 kV line from Princeton (43 Line)�e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8��At the present time,�e]�(�9��\substation in Penticton via lines 52 Line and 53 Line, and to Oliver Substation in the south�e]�(�0��Wvia line 42 Line. Approximately 50,000 residents are served through the Huth substation�e]�(�1��\which feeds three substations connected to it via line 49 Line, one substation connected via�e]�(�2��Xline 47 Line, and another two substations connected via line 42 Line. Figure 2.8.5 below�e]�(�3��Vshows how the south Okanagan system is configured and where the following projects are�e]�(�4��!proposed relative to each other .�e]�(�5��VThe South Okanagan Area Upgrade consists of the 42 Line meshed operations project, the�e]�(�6��\capacitor addition at Bentley Terminal project and the reconductoring of 52 and 53 Lines for�e]�(N�Ta total $12.30 million over the 2014 - 18 timeframe with an in-service date of 2018.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   `�K@���R�hhC   ��U@���R�G@�      hhC   �ؼw@���R�t�ah?]�(�Project��Option A development��Option B development�ehA]�(]�(j�  j�  j�  e]�(�42 Line Meshed Operation��2014��2014�e]�(�Install Capacitors at Bentley��2015��2015�e]�(�Reconductor 52 and 53 Lines��2017+��2013�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(h]�(j$	  j$	  j^  eh]�(G@Z^�P   G@mq�   G@�c3    G@sp��   t�ah?]�(�Project��Option A development schedule��Option B development schedule�ehA]�(]�(�Project��Option A development
schedule��Option B development
schedule�e]�(�42 Line Meshed Operation��2014��2014�e]�(�Install Capacitors at Bentley��2015��2015�e]�(�Reconductor 52 and 53 Lines��2017+��2013�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �1Table 2.8.5.4 - Project Planned Completion Dates �j�  K�j�  (G@gq�   G@k;�    G@{�'`   G@m"7�   t�ububh)��}�(h]�(j^  j$	  j�  eh]�(hhC   �a�?���R�hhC   @��C@���R�G@�      G@��     t�ah?]�(�1��2.8.6��2.8.6�ehA]�(]�(j5  j6  j7  e]�(�2��Presently the Kelowr��Tsently the Kelowna 138 kV transmission system is operated with normally open points.�e]�(�3��This operating config��Woperating configuration can result in widespread and lengthy outages following a single�e]�(�4��contingency: The pro��Vingency. The proposed solution is to operate the system meshed but in order to do that�e]�(�5��there will need to be��2 will need to be some�e]�(�6��transmission lines.��Nsmission lines. This project is planned to be staged over the next five years.�e]�(�7��The first stage is to ir��Ufirst stage is to install fiber-optic multiplexing equipment at seven substations for�e]�(�8��SCADA��IDA,�e]�(�9��2012/13. The seconc��T2/13. The second stage is to install protection relays and perform necessary station�e]�(�0��modifications to allov��Jlifications to allow the Kelowna 138 kV transmission system to be operated�e]�(�1��meshed. This will be��9hed. This will be staged over the 2014 to 2016 timeframe.�e]�(�2��This project is estima��Sproject is estimated to cost $2.75 million in 2014, $2.80 million in 2015 and $2.58�e]�(�3��million in 2016 with��,on in 2016 with an in-service date of 2017 .�e]�(�4��2.8.7��2.8.7�e]�(�4��2.8.7�K e]�(�47��40��40r�e]�(�5��The Summerland Su�K e]�(�6��municipal utility with��Xicipal utility with a distribution wholesale supply. The load on the existing Summerland�e]�(�7��T1 transformer is fore��Qransformer is forecast to exceed 95 percent of the contract Demand Limit in 2015.�e]�(�8��Under the terms of th��Uer the terms of the wholesale supply agreement, FortisBC would be required to upgrade�e]�(�9��the supply capacity ir��Asupply capacity in order to continue to provide reliable service.�e]�(�0��the existing 20 MVA��Vexisting 20 MVA unit with the next larger FortisBC standard transformer which would be�e]�(�1��rated at 40 MVA. For��Ud at 40 MVA Forecasts indicate that this capacity would remain adequate out to the 20�e]�(�2��year planning horizor��planning horizon:�e]�(�3��The following graph��Yfollowing graph Figure 2.8.7 illustrates the historical and forecast load for both summer�e]�(�4��and winter to the yea��Swinter to the year 2031. The 95 percent threshold of the summer and winter contract�e]�(�5��demand limit (15.2 M��Tand limit (15.2 MA and 19 MVA respectively) is also shown. The graph illustrates the�e]�(�6��violation of the 95 pe��Qtion of the 95 percent demand limit in 2015 for winter and 2025 for summer loads.�e]�(�7��This project is estimz��Vproject is estimated to cost $6.58 million (82.15 million in 2014 and $4.43 million in�e]�(�8��2015) with an in-serv��#5) with an in-service date of 2016.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@VpQ�   G@R��    G@�z30   G@��f`   t�ah?]��.Figure 2.8.7 - Summerland Forecast Load Growth�ahA]�(]��.Figure 2.8.7 - Summerland Forecast Load Growth�a]���25000
20000
15000 )AVk(
Summer
daoL
Winter
10000
Summer Contract Limit
Winter Contract Limit
5000
0
4002 6002 8002 0102 2102 4102 6102 8102 0202 2202 4202 6202 8202 0302
Year�a]�X]  2.8.8 BEAVER VALLEY SOUTH SOLUTION
The Beaver Valley region includes the load supplied by the Fruitvale, Hearns, Beaver Park
and Glenmerry substations. The area has been reconfigured multiple times in recent years
in an effort to defer the need for major capacity upgrade projects. After the projects
proposed in the 2012 - 13 Capital Plan have been implemented (namely the Glenmerry
Feeder 2 to Feeder 3 tie), the system can no longer be reconfigured to accommodate further
load growth; at that time a station upgrade project will be required. In 2018 the Beaver Park,
Fruitvale and Hearns Substation are forecast to exceed transformer nameplate rating. In
that same year the Glenmerry T1 transformer will be operating at approximately 98 percent
capacity.
This project entails expanding the existing Beaver Park Substation and installing a new 25
kV distribution transformer. The existing Beaver Park Feeder 2 running through the Beaver
Valley will be converted to 25 kV and extended further east to interconnect into the Fruitvale
and Hearns substations. At that time, the Fruitvale and Hearns substations could be�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ��:a@���R�hhC   ��(`@���R�G@�      hhC    �~@���R�t�ah?]�hA]�(]�(K N�2016��2017-31�e]�(K N�(SOC��JOs)�e]�(K �DG Bell Static VAR Compensator��3,031��34,285�e]�(K �DG Bell 230 kV Ring Bus�N�35,383�e]�(K �%DG Bell Second 230/138 kV Transformer�N�18,746�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2ADTU2.8.9 RGNDERSON ISTRIBUTION RANSFORMER PGRADE �j�  K�j�  (G@b�K�   G@S�w    G@}׉�   G@W��    t�ububh)��}�(hNh]�(G@b�p�   G@xz�P   G@~G�   G@}{�   t�ah?]�(j  j  �2016��2017-31�ehA]�(]�(j  j  �2016��2017-31�e]�(NN�($000s)�Ne]�(ja
  �DG Bell Static VAR Compensator��3,031��34,285�e]�(j�  �DG Bell 230 kV Ring Bus�j�  �35,383�e]�(j�  �%DG Bell Second 230/138 kV Transformer�j�  �18,746�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �)Table 2.8.10 - DG Bell Terminal Upgrades �j�  K�j�  (G@i�
@   G@w_ˀ   G@z���   G@xS@   t�ububh)��}�(h]�(j$	  j$	  j$	  eh]�(hhC   �A�@���R�hhC    ��B@���R�G@�      G@��     t�ah?]�(�1��2.8.11��2.8.11�ehA]�(]�(j  j  j  e]�(�2��Based on the current��1�e]�(�3��contingency (N-1)��Lcy (N-1) outage of one of the Vaseux Lake Terminal station transformers, the�e]�(�4��power flow will exceec��Hw will exceed the emergency rating of the remaining transformer by 2025.�e]�(�5��200 MVA 500/230 kV��G500/230 kV transformer and associated switching, protection and control�e]�(�6��equipment will be req��SIt will be required to avoid this situation. The station was originally designed to�e]�(�7��accommodate the adc��,date the addition of this third transformer.�e]�(�8��This project is reliabili��Qct is reliability driven and is dependent on growth of the Okanagan area load. If�e]�(�9��this load was offset b)��Qwas offset by the addition of appropriately-sited generation resources within the�e]�(�0��region, then the N-1��4en the N-1 overload would be deferred or eliminated.�e]�(�1��This project is estimat��Tct is estimated to cost $31.58 million with $1.78 million in 2023, $11.79 million in�e]�(�2��2024 and $18.02 millic��81S18.02 million in 2025 with an in-service date of 2026.�e]�(�3��2.8.12��2.8.12�e]�(�4��In 2025, following a si��Pollowing a single contingency outage of one of the AS Mawdsley transformers, the�e]�(�5��power flow on the othe��OW on the other transformer will exceed its summer emergency rating: In order to�e]�(�6��eliminate this problem��Nthis problem the AS Mawdsley transformers will need to be replaced with larger�e]�(�7��capacity units. Consid��Sunits. Consideration will be given for down-rating the operating voltage of 11 Line�e]�(�8��and 48 Line from 161��ne from 161 kV to 138 kV in�e]�(�9��This project is estimat��Sct is estimated to cost $1.54 million in 2024 and $8.09 million in 2025 with an in-�e]�(�U��Ogi viuo��alc�e]�(�1��2.8.13��2.8.13�e]�(�2��The substation load fc��Ttation load forecast currently predicts that the power flow on 31 Line supplying the�e]�(�0��Ulesioi��	ubslalion�e]�(�4��cunulionio.��D �e]�(�0��IL��M alu�e]�(�0��
Iecouucioi��loI�e]�(N�VvM��uuluitiCpiaugmiigiilo iGyuitu�e]�(�0��	Oauiligs.��Ao�e]�(�y��Incluaea��I ne�e]�(�0��lransmission Capacily��i0n capacity Iltalions�e]�(�1��This project is estimat��Fct is estimated to cost $2.31 million with an in-service date of 2030_�e]�(NN�Paae 115�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(h]�(j^  j$	  j$	  eh]�(hhC   ��>@���R�hhC   �&�3@���R�G@�      G@��     t�ah?]�(�1��2.8.14��2.8.14�ehA]�(]�(j�  j�  j�  e]�(�2��The current load forec��Rnt load forecast indicates future transformation capacity limitations in the Trail�e]�(�3��area.��Psently, the Stoney Creek substation supplies two 13 kV distribution feeders from�e]�(�4��the single Stoney Cre��PStoney Creek T1 transformer. The growing load in this area is forecast to exceed�e]�(�5��the capacity of the Stc��Rity of the Stoney Creek T1 Transformer by the year 2024. A proposed solution is to�e]�(�6��add a second distribut��Nond distribution transformer with new feeder positions in order to relieve the�e]�(�7��existing unit:��nit _�e]�(�8��This project is estimat��Gct is estimated to cost $17.33 million with an in-service date of 2024.�e]�(�9��2.8.15��2.8.15�e]�(�10��The current load foree��Rnt load forecast indicates future transformation capacity limitations in the South�e]�(�11��Slocan area. Presentl-��ea.�e]�(�12��from the single Playm��Qingle Playmor T1 transformer. The growing load in this area is forecast to exceed�e]�(�13��the capacity of the Plz��Rity of the Playmor T1 Transformer by the year 2027 . A proposed solution is to add�e]�(�14��a second distribution��Sdistribution transformer with new feeder positions in order to relieve the existing�e]�(�15��unit.�K e]�(�16��This project will begin��Oct will begin in 2025 with an in-service date of 2027 . The estimated costs are�e]�(�17��S1.70 million in 2025,��?ion in 2025, $3.90 million in 2026 and $10.02 million in 2027 _�e]�(�18��2.8.16��2.8.16�e]�(�19��Eight spans of 50 Line��Jns of 50 Line from the Recreation substation to the Saucier substation are�e]�(�20��constructed with 477��Qed with 477 kcmil ASC conductor while the rest of the line has a larqer 927 kcmil�e]�(�21��ASC conductor.��NJuctor.In 2025 followina a sinale-continaencv outaae of 50 Line between the FA�e]�(�22��Lee Terminal and the��Rinal and the Sexsmith substation, the flow on this section of line will exceed its�e]�(�23��summer emergency ra��Smergency rating: Within the 20 year forecast period the entire line will need to be�e]�(�24��upgraded to a larger��Lto a larger conductor to accommodate the forecast load growth; however , the�e]�(�25��project can be comple��Pn be completed in two stages because of this eight span section of line with 477�e]�(�26��kcmil ASC conductor;��U; conductor, which currently limits the available capacity on the line. Completion of�e]�(�27��this project will remov��Yct will remove the capacity limitation on this section of 50 Line, thus allowing the fulk�e]�(�28��capacity of the line to��Nf the line to be utilized. Figure 2.8.16 below is a single-line diagram of the�e]�(�29��Kelowna area which��Marea which shows how 50 Line supplies the various substations within Kelowna.�e]�(�30��The estimated cost of��ated cost of�e]�(�31��is estimated to cost SC��!ed to cost $0.30 million in 2024.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(h]�(j$	  j$	  j^  eh]�((hhC    ��@���R�hhC   `�,C@���R�G@�      G@��     t�(hhC   ��@���R�hhC    0V>@���R�G@�      G@��     t�eh?]�(�1��2.8_�K ehA]�(]�(j&  j'  K e]�(�2��The second sta�K e]�(�3��larger conducto�K e]�(�4��sufficient that de�K e]�(�5��exceed the sun�K e]�(�6��upgrades to sta�K e]�(�7��This is a growth�K e]�(�8��required due to�K e]�(�9��the line will nee�K e]�(�0��Rebuild Plan (A�K e]�(�1��by the transmis:�K e]�(�2��This project is�K e]�(�3��2.8_�K e]�(�4��In 2030, the fore�K e]�(�5��the DG Bell Ter�K e]�(�6��Mission substat�K e]�(�8��60 Lines betwee�K e]�(N�Ddcci! #idetra�K e]�(�3��Du 4cc #�K e]�(�5��
IOauinigs.�K e]�(�4��MCluueu�K e]�(�0��capltal project�K e]�(�U0��ir�K e]�(�8��1S piujecl 13�K e]�(�0��DoiVi0o�K e]�(N�The load foreca�K e]�(N�DG Bell Termin�K e]�(N�DU��Ju�e]�(NNNe]�(�1�NK e]�(�2��Prior to�K e]�(�3��Kelownz�K e]�(�4��substati�K e]�(�5��1_�K e]�(�6��2.�K e]�(�7��3_�K e]�(�8��4.�K e]�(�9��Item 4 r�K e]�(�0��conn�K e]�(�1��applicat�K e]�(�2��unusual�K e]�(�3��the risk�K e]�(�4��
risk in tr�K e]�(�5��While th�K e]�(�6��has nov�K e]�(�7��the area�K e]�(�8��teeders�K e]�(�9��expecte�K e]�(�0��ransior�K e]�(�1��ana tne�K e]�(�L��uio0�K e]�(�5��Deyona�K e]�(�4��exlausl�K e]�(�5��	WIII be r�K e]�(�6��associa�K e]�(N�	inis tran�K e]�(�8��torecast�K e]�(�9��addition�K e]�(�0��the FA�K e]�(�1��area wil�K e]�(NNK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   �Eh@���R�hhC    ��E@���R�G@�      G@��     t�ah?]�(�1�K ehA]�(]�(j�  K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�0�K e]�(NK e]�(�8�K e]�(�0�K e]�(�9�K e]�(NK e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   `�@���R�hhC    sN&@���R�G@�      G@��     t�ah?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(NK e]�(�6�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�U�K e]�(�1�K e]�(�3��0million in 2019 with an in-service date of 2019.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   @-y5@���R�hhC   ��mC@���R�G@�      G@��     t�ah?]�(N�	Ynana Ola��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(Njc   jd   je   jf   jg   jh   ji   e]�(K �
Tansmssion�NN�(Sq��JOOs)�NNe]�(K �&Transmission Line Condition Assessment��522��485��480��547��543��10,216�e]�(K � Transmission Line Rehabilitation��3,372��2,621��2,509��2,424��2,820��50,158�e]�(K � Transmission Line Urgent Repairs��594��620��616��622��661��11,54=�e]�(�4��Transmission Line Right of��400��400��416��423��440��7,754�e]�(�5��6 Line/26 Line River Crossing��1,185�NNNNNe]�(�6��"27 Line Rebuild (Corra Linn-Salmo)��1,161�NNNNNe]�(�7��'21-24 Lines Rebuild (Generation Plants)��2,219�NNNNNe]�(K �19 Line/29 Line Reconfiguration�N�791�NNNNe]�(K �)20 Line Rebuild (Warfield Terminal-Salmo)�N�4,664�NNNNe]�(K �$30 Line Lake Crossing Assessment and�NNN�802��1,521�Ne]�(K �Total Transmission Sustainment��9,453��9,581��4,021��4,818��5,984��79,67�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@PAv�$�IG@R��    G@���m��G@��f`   t�ah?]�(�E1 2.9 Transmission Sustainment 2 Table 2.9 - Transmission Sustainment�j  j  j  j  j  j  j  ehA]�(]�(�E1 2.9 Transmission Sustainment
2 Table 2.9 - Transmission Sustainment�NNNNNNNe]�(j  �Transmission Sustainment��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �&Transmission Line Condition Assessment��522��485��480��547��543��10,216�e]�(j�  � Transmission Line Rehabilitation��3,372��2,621��2,509��2,424��2,820��50,158�e]�(j�  � Transmission Line Urgent Repairs��594��620��616��622��661��11,543�e]�(j�  �(Transmission Line Right of Way
Easements��400��400��416��423��440��7,754�e]�(j�  �-6 Line/26 Line River Crossing
Reconfiguration��1,185�j�  j�  j�  j�  j�  e]�(j�  �"27 Line Rebuild (Corra Linn-Salmo)��1,161�j�  j�  j�  j�  j�  e]�(j�  �'21-24 Lines Rebuild (Generation Plants)��2,219�j�  j�  j�  j�  j�  e]�(j�  �19 Line/29 Line Reconfiguration�j�  �791�j�  j�  j�  j�  e]�(j�  �)20 Line Rebuild (Warfield Terminal-Salmo)�j�  �4,664�j�  j�  j�  j�  e]�(�10��330 Line Lake Crossing Assessment and
Rehabilitation�j�  j�  j�  �802��1,521�j�  e]�(�11��Total Transmission Sustainment��9,453��9,581��4,021��4,818��5,984��79,671�e]�(X�  3 FortisBC’s Transmission Sustainment capital programs are:
4 • Transmission Line Condition Assessment;
5 • Transmission Line Rehabilitation;
6 • Transmission Line Urgent Repair;
7 • Transmission Line Right of Way Easements; and
8 • Transmission Line Rebuild
9 These projects are described in the following sections.
0 2.9.1 TRANSMISSION LINE CONDITION ASSESSMENT
1 The Transmission Line Condition assessment program is based on an eight-year cycle of
2 patrolling and testing all of FortisBC’s transmission line facilities. The program consists of a
3 pole “test and treat” and a condition assessment. The test and treat component involves
4 drilling test holes in each pole to confirm the condition of the pole, and the addition of a
5 chemical treatment to reduce internal rot in the pole.
6 The program extends the life of the pole and ensures the integrity of the lines as well as
7 employee and public safety. The test and treat program deals with the portion of pole at and�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    Ǻ>@���R�hhC   ���U@���R�G@�      hhC   @O||@���R�t�ah?]�(�18�N�Ta��ble 2.9_��1��mission��Line Co��ndition��Assessn��nent�NNehA]�(]�(j!  Nj!  j!  j!  j!  j!  j!  j!  j!  NNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��152��639��413��343��469��522��485��480��547��543��10,216�e]�(�19�N�2.9.2��TF��ANSMISS��ION LINE��REHABIL��ITATION�NNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    �=@���R�hhC    �Q@���R�G@�      hhC   ��l�@���R�t�ah?]�(�25��concerns��and to m��
aintain re��	liable se��vvice to��ortisBC��customer��~S_�NNNehA]�(]�(jW!  jX!  jY!  jZ!  j[!  j\!  j]!  j^!  j_!  NNNe]�(K NN�Table��2.9.2��ransmis��sion Lin��e Rehab��	ilitation�NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOOs)�NNNNK e]�(NNNNNN�(SOOOs)�NNNNK e]�(�Cost��1,051��1,329��1,441��1,905��1,604��3,372��2,621��2,509��2,424��2,820��50,158�e]�(�COdt��Tuu |��T,ulu��T,TtT [��Juu��uutt��Uiui L��Liol [��Liuuo��Litlt��Liulu��Uu,�e]�(NNNNNNNNNN�Page 1��129�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J���m�G@R��    G@��V�m�G@��f`   t�ah?]�(X�  1 below ground level on all the poles. The condition assessment is aimed at the above ground 2 portion of the pole and reviews the condition of the pole top, anchoring/guying, cross-arms, 3 insulators and other hardware items. Any items which do not pass inspection during the 4 condition assessment are documented and identified for correction in the following year’s 5 rehabilitation budget. 6 The detailed methods and criteria applied in the assessment program are further described 7 in Appendix G. The program cost forecasts are based on rolling average estimates 8 combined with the Company’s knowledge of the distribution lines expected to be assessed. 9 The costs of performing condition assessments vary from line to line depending upon factors 10 including the length of line segment being addressed, the proportion of the line requiring 11 treatment, and the terrain. These factors are taken into consideration when calculating the 12 forecast expenditures.sam 13 The program is managed in an eight-year cycle to help levelize both budgets and resource 14 requirements. The condition assessment and test and treat programs are intended to review 15 a complete set of transmission lines within the given assessment year. The eight-year cycle 16 is driven by the chemical treatment applied to the wood poles; this chemical is only effective 17 in preventing rot for approximately eight years. 18 Table 2.9.1 - Transmission Line Condition Assessment�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X�  1 below ground level on all the poles. The condition assessment is aimed at the above ground
2 portion of the pole and reviews the condition of the pole top, anchoring/guying, cross-arms,
3 insulators and other hardware items. Any items which do not pass inspection during the
4 condition assessment are documented and identified for correction in the following year’s
5 rehabilitation budget.
6 The detailed methods and criteria applied in the assessment program are further described
7 in Appendix G. The program cost forecasts are based on rolling average estimates
8 combined with the Company’s knowledge of the distribution lines expected to be assessed.
9 The costs of performing condition assessments vary from line to line depending upon factors
10 including the length of line segment being addressed, the proportion of the line requiring
11 treatment, and the terrain. These factors are taken into consideration when calculating the
12 forecast expenditures.sam
13 The program is managed in an eight-year cycle to help levelize both budgets and resource
14 requirements. The condition assessment and test and treat programs are intended to review
15 a complete set of transmission lines within the given assessment year. The eight-year cycle
16 is driven by the chemical treatment applied to the wood poles; this chemical is only effective
17 in preventing rot for approximately eight years.
18 Table 2.9.1 - Transmission Line Condition Assessment�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��152��639��413��343��469��522��485��480��547��543��10,216�e]�(XI  19 2.9.2 TRANSMISSION LINE REHABILITATION
20 The specific rehabilitation projects for various transmission lines involve expenditures for
21 structural stabilization of the defects identified for rehabilitation in previous years’
22 assessments. Included in the scope of work is stubbing of poles, replacement of cross-arms
23 and poles, insulator changes and guy wire changes.
24 This project is required to address public and employee safety issues, environmental
25 concerns and to maintain reliable service to FortisBC customers.
26 Table 2.9.2 - Transmission Line Rehabilitation�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��1,051��1,329��1,441��1,905��1,604��3,372��2,621��2,509��2,424��2,820��50,158�e]�(j  NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   @cj<@���R�hhC   `�dQ@���R�G@�      hhC   �"o@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j�!  j�!  j�!  j�!  j�!  j�!  j�!  j�!  j�!  j�!  j�!  j�!  e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��514��362��526��487��491��594��620��616��622��661��11,543�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ���<@���R�hhC   @�cT@���R�G@�      hhC    �g|@���R�t�ah?]�(�14�N�Tal��le 2.9.4��E��nission��Line Rig��ht of Wa��y Easen��nents�NNehA]�(]�(j#"  Nj$"  j%"  j&"  j'"  j("  j)"  j*"  j+"  NNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��170��135��235��118��358��400��400��416��423��440��7,754_�e]�(�15�N�2.9.5��TR��ANSMISS��ION LINE��REBUILD�NNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J���m�G@R��    G@��V�m�G@��f`   t�ah?]�(X�  1 2.9.3 TRANSMISSION LINE URGENT REPAIR 2 The Urgent Repairs project is required to replace transmission line facilities that fail in- 3 service due to severe weather, vandalism or other unexpected reasons. The project is 4 required to address public and employee safety issues, environmental concerns and to 5 maintain reliable service to FortisBC customers. 6 Table 2.9.3 - Transmission Line Urgent Repair�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X�  1 2.9.3 TRANSMISSION LINE URGENT REPAIR
2 The Urgent Repairs project is required to replace transmission line facilities that fail in-
3 service due to severe weather, vandalism or other unexpected reasons. The project is
4 required to address public and employee safety issues, environmental concerns and to
5 maintain reliable service to FortisBC customers.
6 Table 2.9.3 - Transmission Line Urgent Repair�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��514��362��526��487��491��594��620��616��622��661��11,543�e]�(X  7 2.9.4 TRANSMISSION LINE RIGHT OF WAY EASEMENTS
8 This program is required to acquire outstanding rights of way or non-easement land rights
9 for transmission and distribution lines that are in trespass. Many of the transmission lines
10 have no or limited access to sections of the right of way. Access is required for the operation
11 and maintenance of these lines. This program has historically been used to obtain
12 easements to address existing trespass situations. Easements for new projects are obtained
13 as part of the new project and are not included in this program.
14 Table 2.9.4 - Transmission Line Right of Way Easements�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��170��135��235��118��358��400��400��416��423��440��7,754�e]�(Xw  15 2.9.5 TRANSMISSION LINE REBUILD
16 The Ten Year Transmission Rebuild Plan found in Appendix F, includes transmission
17 rebuilds that are focused on the replacement of:
18 • Previously inspected “red tagged” structures and cross-arms;
19 • Stubbed poles that have deteriorated enough at the pole tops and cross-arms to
20 justify complete replacement; and
21 • Correction of circuit spacing issues, and improved anchoring where needed.
22 The plan identifies seven 63 kV transmission lines located in the Kootenay region requiring
23 significant rehabilitation/rebuild. As well, there are two transmission reconfiguration projects
24 required in the Kootenay region. These line rebuild and reconfiguration projects have been
25 assessed consistent with the criteria of the Transmission Rehabilitation program and require
26 a large amount of pole replacements and upgrading.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(]�(G@�    G@CVP@   G@�d@�   G@�*�   e]�(G@~���   G@EV    G@����   G@j�0    eeh?]�hA]�(]�K a]�Na]��xpendi�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�]�(G@}C�   G@Gd�`   G@�Y�    G@��Ӏ   eah?]�hA]�]�Naah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012 I S PNTEGRATEDYSTEMLAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@nz�P   G@�Tz�   G@x"�p   G@�T��   t�ah?]�(�Year��2012�ehA]�(]�(�Year��2012�e]�(�Cost ($millions)��1.19�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �@Table 2.9.5 (d) - 6 Line/26 Line River Crossing Reconfiguration �j�  K�j�  (G@cz�   G@����   G@~`   G@�A��   t�ububh)��}�(hNh]�]�(G@{�Y�   G@7��`   G@��J�   G@y�    eah?]�hA]�]��
piUjeul 13�aah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@nz�P   G@{��P   G@x"�p   G@}�\0   t�ah?]�(�5 (e) - 19 Line / 29��
Line Recon�ehA]�(]�(�Year��2012�e]�(�Cost ($millions)��0.79�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �4Table 2.9.5 (e) - 19 Line / 29 Line Reconfiguration �j�  K�j�  (G@gn�   G@z��    G@{ش    G@{���   t�ububh)��}�(hNh]�]�(G@���@   G@K���   G@���   G@q�1�   eah?]�hA]�]��2012�aah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@k�   G@v���   G@y�(�   G@x�G�   t�ah?]�(�Year��2012��2013�ehA]�(]�(�Year��2012��2013�e]�(�Cost ($millions)��0.80��1.52�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �7Table 2.9.5 (f) - 30 Line Lake Crossing Rehabilitation �j�  K�j�  (G@fQ�   G@u�y�   G@|fO`   G@vz�`   t�ububh)��}�(hNh]�(hhC   `��>@���R�hhC   `WD@���R�G@�      G@��     t�ah?]�(NN�2012��2013��2014��2015��2016�K ehA]�(]�(NNj#  j#  j#  j#  j#  K e]�(N�Dlalions�NN�(SOC��JOs)�NK e]�(�1��Environmental Compliance��11,269��11,553��4,574�NNK e]�(�2��Station Urgent Repairs��818��907��879��977��942�K e]�(K �Station AssessmentlMinor��1,343��1,354��1,410��1,433��1,489�K e]�(�4��Add Arc Flash Detection to��539��544��566��1,140��1,184�K e]�(K �Huth Low Voltage Breaker�N�69��550�NNK e]�(K �Switchgear Replacement�NN�1,651�N�983��2,651�e]�(K �Ground Grid Upgrades�NN�748�N�790��6,403�e]�(K �Ground Grid Upgrades�NN�748�N�790�K e]�(K �DG Bell 138 KV Breaker and�NN�338��938�NK e]�(�9��Osoyoos 63 kV Breaker�NNN�364��2,359�Ne]�(�10��Bulk Oil Breaker�NNN�733��761�K e]�(�11��Station Oil Containment�NNN�445��462��2,664�e]�(�12��Minimum Oil Circuit Breaker�NNNNN�21,175�e]�(�13��Major Transmission�NNNNN�32,017�e]�(�14��Distribution Transformer�NNNNN�10,978�e]�(�15��Station Sustainment Total��13,969��14,427��10,716��6,030��8,970��122,170�e]�(K �Stalion Sustalnment��13,909��14,42��10,( 10��O,usu��O,9(u��I22,|(u�e]�(K �2.10.1��MENTAL C��	OMPLIANCE��(PCB M��	ITIGATION�NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@U���8�G@X.    G@��3    G@�8     t�ah?]�(j  �Stations��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j  �Stations��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �)Environmental Compliance
(PCB Mitigation)��11,269��11,553��4,574�j�  j�  j�  e]�(j�  �Station Urgent Repairs��818��907��879��977��942��17,065�e]�(j�  �)Station Assessment/Minor
Planned Projects��1,343��1,354��1,410��1,433��1,489��26,245�e]�(j�  �7Add Arc Flash Detection to
Legacy Metal-Clad
Switchgear��539��544��566��1,140��1,184�j�  e]�(j�  �(Huth Low Voltage Breaker
Replacement (2)�j�  �69��550�j�  j�  j�  e]�(j�  �&Switchgear Replacement
Program (13 kV)�j�  j�  �1,651�j�  �983��2,651�e]�(j�  �Ground Grid Upgrades�j�  j�  �748�j�  �790��6,403�e]�(j�  �7DG Bell 138 kV Breaker and
Voltage Transformer Addition�j�  j�  �338��938�j�  j�  e]�(j�  �#Osoyoos 63 kV Breaker
Additions (2)�j�  j�  j�  �364��2,359�j�  e]�(�10��Bulk Oil Breaker
Replacements�j�  j�  j�  �733��761��2,972�e]�(�11��Station Oil Containment�j�  j�  j�  �445��462��2,664�e]�(�12��'Minimum Oil Circuit Breaker
Replacement�j�  j�  j�  j�  j�  �21,175�e]�(�13��+Major Transmission
Transformer Replacements�j�  j�  j�  j�  j�  �32,017�e]�(�14��%Distribution Transformer
Replacements�j�  j�  j�  j�  j�  �10,978�e]�(�15��Station Sustainment Total��13,969��14,427��10,716��6,030��8,970��122,170�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �!Table 2.10 - Station Sustainment �j�  K�j�  (G@l�@   G@S�w    G@y*ƀ   G@W�f    t�ububh)��}�(hNh]�(hhC   `0d7@���R�hhC   ���P@���R�G@�      hhC    ��n@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j$  j$  j$  j$  j$  j	$  j
$  j$  j$  j$  j$  j$  e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��418��599��782��639��674��818��907��879��977��942��17.06=�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J�\   G@R��    G@��    G@��f`   t�ah?]�(X  1 This is an ongoing program to repair failed substation equipment across the service territory. 2 Annual spending varies due to the expected severity and number of equipment failures. The 3 proposed spending is consistent with historical trend. 4 Table 2.10.2 - Station Urgent Repairs�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X  1 This is an ongoing program to repair failed substation equipment across the service territory.
2 Annual spending varies due to the expected severity and number of equipment failures. The
3 proposed spending is consistent with historical trend.
4 Table 2.10.2 - Station Urgent Repairs�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��418��599��782��639��674��818��907��879��977��942��17,065�e]�(X�  5 2.10.3 STATION ASSESSMENT AND MINOR PLANNED PROJECTS
6 The Station Condition Assessment program reviews seven to eight stations per year for
7 items which may impact safety, reliability or the environment. All stations reviewed are
8 tracked in a ten-year cycle. Information gathered during these inspections results in projects
9 planned for the following year in the Station Minor Planned program. Projects conducted
10 under this program can include the replacement of instrument transformers, station service
11 transformers, switches and other equipment that is at end-of-life. Other projects in this
12 program, such as the DC Supply Replacement project and the Gap Type Surge Arrestors
13 Replacement program, increase station reliability
14 The DC Supply Replacement project replaces failed back-up substation batteries, chargers
15 and distribution equipment. DC station supplies are required to maintain supply to critical
16 protection and control systems that manage risk to station equipment and the
17 communications systems that allow visibility from the FortisBC System Control Center.
18 These systems also help prevent extended fault conditions. DC supplies are load tested on
19 a regular basis to ensure that a station power outage will not affect protection and control
20 equipment operations nor result in adverse operating conditions.
21 The Gap Type Surge Arrestor Replacement program replaces the existing gap type surge
22 arrestors. This program was introduced in the 2009 - 2010 Capital Expenditure Plan and
23 approved by order G-11-09. Since gap type arrestors are made of porcelain, they can fail
24 violently, damaging adjacent equipment and presenting a risk to personnel in the vicinity.
25 The arrestors being replaced under this program are located on transformers near the
26 transformer bushings to eliminate the risk of an arrester failure removing a transformer from
27 service due to bushing damage.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ��]:@���R�hhC   �/�P@���R�G@�      G@��     t�ah?]�(N�DeR#M��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(Nj]$  j^$  j_$  j`$  ja$  jb$  jc$  e]�(N�NTujeci Nalle�NN�(SOOC��Os)�NNe]�(K �Add Arc Flash Detection to��539��544��566��1,140��1,184�Ne]�(�2��Huth Low Voltage Breaker�N�69��550�NNNe]�(�3��Switchgear Replacement�NN�1,651�N�983��2,651�e]�(�4��Ground Grid Upgrades�NN�748�N�790��6,403�e]�(�5��DG Bell 138 kV Breaker and�NN�338��938�NNe]�(�6��Osoyoos 63 kV Breaker�NNN�364��2,359�Ne]�(�7��Bulk Oil Breaker Replacements�NNN�733��761��2,972�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    Q�3@���R�hhC   ���O@���R�G@�      hhC   �E�n@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j�$  j�$  j�$  j�$  j�$  j�$  j�$  j�$  j�$  j�$  j�$  j�$  e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��2,148��1,509��286��286��708��1,343��1,354��1,410��1,433��1,489��26,245�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J�\   G@R��    G@��    G@��f`   t�ah?]�(�>1 Table 2.10.3 - Station Assessment and Minor Planned Projects�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�>1 Table 2.10.3 - Station Assessment and Minor Planned Projects�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��2,148��1,509��286��286��708��1,343��1,354��1,410��1,433��1,489��26,245�e]�(X�  2 2.10.4 SPECIFIC STATION PROJECTS
3 The specific station projects planned are identified in the table below.
4 Table 2.10.4 - Specific Station Projects
2012 2013 2014 2015 2016 2017-31
Project Name
($000s)
Add Arc Flash Detection to
1 539 544 566 1,140 1,184 -
Legacy Metal-Clad Switchgear
Huth Low Voltage Breaker
2 - 69 550 - - -
Replacement (2)
Switchgear Replacement
3 - - 1,651 - 983 2,651
Program (13 kV)
4 Ground Grid Upgrades - - 748 - 790 6,403
DG Bell 138 kV Breaker and
5 - - 338 938 - -
Voltage Transformer Addition
Osoyoos 63 kV Breaker
6 - - - 364 2,359 -
Additions (2)
7 Bulk Oil Breaker Replacements - - - 733 761 2,972
5 2.10.4.1 Add Arc Flash Detection to Legacy Metal-Clad
6 Switchgear
7 FortisBC has a large number of distribution substations equipped with older, non arc-
8 resistant metal-clad switchgear. 2011 expenditures for this program were approved by Order
9 G-195-10. This type of switchgear presents a risk of injury if a fault occurs within the
10 switchgear when employees are inside or nearby. Arc flashes occur when a short circuit
11 flows through air. Arc flash incidents release substantial amounts of energy in a very short
12 period of time, resulting in explosive, high temperature events with severe consequences to
13 employees and equipment.
14 The long-term goal is to retire this type of equipment, but in the interim some measures must
15 be taken to reduce the risk of injury where practical. The program would install arc flash
16 detector relays in legacy metal-clad installations. These devices reduce the fault detection
17 time, and thus exposure duration, associated with a metal-clad insulation failure. These
18 relays would trip either the transformer high-side breaker or low-side main breaker, as�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@P�p�   G@k\    G@��Q�   G@zX��   t�ah?]�(j  �Project Name��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j  �Project Name��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �7Add Arc Flash Detection to
Legacy Metal-Clad Switchgear��539��544��566��1,140��1,184�j�  e]�(j�  �(Huth Low Voltage Breaker
Replacement (2)�j�  �69��550�j�  j�  j�  e]�(j�  �&Switchgear Replacement
Program (13 kV)�j�  j�  �1,651�j�  �983��2,651�e]�(j�  �Ground Grid Upgrades�j�  j�  �748�j�  �790��6,403�e]�(j�  �7DG Bell 138 kV Breaker and
Voltage Transformer Addition�j�  j�  �338��938�j�  j�  e]�(j�  �#Osoyoos 63 kV Breaker
Additions (2)�j�  j�  j�  �364��2,359�j�  e]�(j�  �Bulk Oil Breaker Replacements�j�  j�  j�  �733��761��2,972�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �)Table 2.10.4 - Specific Station Projects �j�  K�j�  (G@j��   G@h�0�   G@z*,`   G@j��    t�ububh)��}�(hNh]�(hhC   �.@���R�hhC    
/@���R�G@�      G@��     t�ah?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(K K e]�(K K e]�(�4�K e]�(�5�K e]�(�0�K e]�(�6�K e]�(�0�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1��^voltage breaker to trip during fault conditions on the low voltage bus, de-energizing the 8 kV�e]�(�22�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�'6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0��"necessary in-service date of 2014.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   �@���R�hhC   `1A@���R�G@�      G@��     t�ah?]�(NK ehA]�(]�(NK e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�5�K e]�(NK e]�(�3�K e]�(NK e]�(�)�K e]�(NK e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�5�K e]�(�5�K e]�(NK e]�(NK e]�(NK e]�(NK e]�(NK e]�(NK e]�(�4�K e]�(NK e]�(NK e]�(�D�K e]�(�D�K e]�(NK e]�(�)�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   �vL@���R�hhC   �1la@���R�G@�      hhC   �|@���R�t�ah?]�(NNN�Table��2.10.4.4��Ground��Grid Upe��grades�NNK ehA]�(]�(NNNj�%  j�%  j�%  j�%  j�%  NNK e]�(�Year��2014��2016��2018��2020��2022��2024��2026��2028��2030�K e]�(NNNNN�(SOO��Os)�NNNK e]�(�Cost��748��790��741��799��937��872��984��1,043��1,028�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2.10.4.4 Ground Grid Upgrades �j�  K�j�  (G@h��   G@S��   G@w)�    G@W���   t�ububh)��}�(hNh]�(G@RH��   G@R��    G@�j��   G@��f`   t�ah?]�(X�  2.10.4.4 Ground Grid Upgrades This program seeks to improve station ground grids at locations where the existing grid has a decreased ability to provide a solid ground reference. Ground grid effectiveness can be decreased due to deterioration of their above and below-ground connections, corrosion to the grounding conductors, increases in fault levels since the original design of the ground grid, repeated high-magnitude fault currents which can weaken connections, and adverse soil conditions. Ground grids are inspected during the Station Condition Assessments inspections on a ten year cycle. Historically, condition assessment of the ground grids has indicated poor grounding at some stations. Where above ground connections show corrosion or damage, second stage testing is conducted to establish the sufficiency of the below ground connections. Anomalies found in this process are subjected to a full ground grid study, and projects arising out of the ground grid study are the focus of this program. The 2010 assessments identified one station for second stage testing to take place in 2011. The next phase of the Ground Grid Upgrade program will begin remediation work in 2014 with the aim of correcting deficiencies every second year. This will allow sufficient time to undertake testing and identify grids which need remediation. Reliable, solidly grounded substation grids are necessary for employee safety in substations, for public safety near the stations, and for proper operation of protection relays to ensure safe and rapid clearing of system faults. This project is estimated to cost $7.94 million with costs incurred every two years beginning in 2014 as shown below. Table 2.10.4.4 - Ground Grid Upgrades�j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X�  2.10.4.4 Ground Grid Upgrades
This program seeks to improve station ground grids at locations where the existing grid has
a decreased ability to provide a solid ground reference. Ground grid effectiveness can be
decreased due to deterioration of their above and below-ground connections, corrosion to
the grounding conductors, increases in fault levels since the original design of the ground
grid, repeated high-magnitude fault currents which can weaken connections, and adverse
soil conditions. Ground grids are inspected during the Station Condition Assessments
inspections on a ten year cycle. Historically, condition assessment of the ground grids has
indicated poor grounding at some stations.
Where above ground connections show corrosion or damage, second stage testing is
conducted to establish the sufficiency of the below ground connections. Anomalies found in
this process are subjected to a full ground grid study, and projects arising out of the ground
grid study are the focus of this program. The 2010 assessments identified one station for
second stage testing to take place in 2011.
The next phase of the Ground Grid Upgrade program will begin remediation work in 2014
with the aim of correcting deficiencies every second year. This will allow sufficient time to
undertake testing and identify grids which need remediation.
Reliable, solidly grounded substation grids are necessary for employee safety in
substations, for public safety near the stations, and for proper operation of protection relays
to ensure safe and rapid clearing of system faults.
This project is estimated to cost $7.94 million with costs incurred every two years beginning
in 2014 as shown below.
Table 2.10.4.4 - Ground Grid Upgrades�NNNNNNNNNNe]�(�Year��2014��2016��2018��2020��2022��2024��2026��2028��2030��Total�e]�(j  �($000s)�NNNNNNNNNe]�(�Cost��748��790��741��799��937��872��984��1,043��1,028��7,941�e]�(X�  2.10.4.5 DG Bell 138 kV Breaker Addition
The DG Bell Terminal station has been designed and provisioned to have a four-element
ring bus in the 138 kV portion of the station. At this time, only three circuit breakers are
installed. The addition of the fourth circuit breaker will improve reliability, operational
flexibility and simplify the substation protection schemes. The recent installation of a
capacitor bank to the existing node between Circuit Breaker 14 (CB14) and Circuit Breaker�NNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@T�     G@R��    G@�>f`   G@��f`   t�ah?]�X�  12 (CB12) has increased the number of devices connected to this bus section. Currently, the DG Bell T1 and T2 transformers, the mobile transformer connection and the capacitor bank are all included in the same protection zone. A fault with one piece of equipment will cause all units in this zone to experience an outage. Refer to the “Before” section of Figure 2.10.4.5 below for this configuration. Figure 2.10.4.5 - DG Bell Terminal “Before and After” Configuration�ahA]�(]�X�  12 (CB12) has increased the number of devices connected to this bus section. Currently,
the DG Bell T1 and T2 transformers, the mobile transformer connection and the capacitor
bank are all included in the same protection zone. A fault with one piece of equipment will
cause all units in this zone to experience an outage. Refer to the “Before” section of Figure
2.10.4.5 below for this configuration.
Figure 2.10.4.5 - DG Bell Terminal “Before and After” Configuration�a]�j  a]�X�  The addition of CB13 will increase reliability and simplify the protection schemes at the
substation by increasing the sectionalization of equipment at the station, as well as providing
trip coordination with upstream and downstream protection equipment. The station was
designed for the CB13 addition; the required isolating disconnect switches are already
installed. At present, the 138 kV node encompasses the low voltage side of the terminal
transformer T2 (230 kV-138 kV), the distribution transformer T1, and capacitor bank and the
mobile transformer connection. Any faults in this node jeopardize all these pieces of
equipment.
Along with the addition of CB13, voltage transformers will be added to the high voltage side
of transformer T1 between circuit breakers 12 and 13 (CB12 and CB13) in accordance with
FortisBC design standards.
The Bulk Oil Breaker Replacement program will impact this project as both CB12 and CB14
are bulk oil breakers and are thus scheduled for replacement.�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@VpQ�   G@R��    G@�z30   G@��f`   t�ah?]��AFigure 2.10.4.7 - High Voltage Bulk Oil Circuit Breaker by Region�ahA]�(]��AFigure 2.10.4.7 - High Voltage Bulk Oil Circuit Breaker by Region�a]���srekaerB
2
tiucriC
1 3 4 liO
kluB
fo
rebmuN
1
0-29 30-39 40-49 50-59 60-69
Bulk Oil Circuit Breaker Age (Years)
South Okanagan North Okanagan Kootenay�a]�X�  This program proposes the replacement of two breakers per year over the period of 2015 to
2020. The breakers will be replaced based on the asset condition assessment consistent
with FortisBC’s proposed Asset Management program, currently being developed. This
project is estimated to cost $4.47 million dollars over the 2015 - 2017 timeframe.
2.10.4.8 Station Oil Containment
Legacy substations often do not have oil containment pits to prevent oil release into the
environment. Many of the historical substation sites are in locations which would now be
considered environmentally sensitive. To reduce the risk of transformer oil contaminating
soil, groundwater and nearby waterways, this program will retrofit oil containment pits for
legacy substations, either adding containment pits where they do not currently exist or
upgrading containment pits that are considered inadequate. The work will be performed to
mitigate stations that pose the highest risk to the surrounding environment.�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@sa,UUUUG@[��    G@v+\    G@v��    t�ah?]�j�  ahA]�(]�j�  a]�ja
  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �BFigure 2.10.4.7 - High Voltage Bulk Oil Circuit Breaker by Region �j�  K�j�  (G@b\@   G@S�w    G@~c��   G@W�f    t�ububh)��}�(hNh]�(hhC   �Ӵ@���R�hhC   �u�L@���R�G@�      G@��     t�ah?]�hA]�(]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�10�K e]�(�11�K e]�(�12�K e]�(�13�K e]�(�14�K e]�(�15�K e]�(�6�K e]�(NK e]�(�16�K e]�(�AO�K e]�(�CU�K e]�(�21�K e]�(�LL�K e]�(�Lo�K e]�(�)c�K e]�(�LO�K e]�(�3o�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@VpQ�   G@R��    G@�zp�   G@��f`   t�ah?]���this schedule. This will be consistent with the proposed FortisBC Asset Management program. Figure 2.10.5 (a) - Major Transmission Transformers�ahA]�(]���this schedule. This will be consistent with the proposed FortisBC Asset Management
program.
Figure 2.10.5 (a) - Major Transmission Transformers�a]��_9
8
7
sremrofsnarT
6
5
fo
4
rebmuN
3
2
1
0
1-10 11-19 20-28 29-37 38-46
Transformer Age (Years)�a]�X�  The replacement of transmission transformers in the North Okanagan will be performed on
an as needed basis, with the existing transformation in the North Okanagan monitored and
assessed consistent with the FortisBC Asset Management plan. In the next five year period,
an additional 200 MVA transmission transformer is planned for the Kelowna area, as further
described above in Section 2.8.4.
The completion of the Okanagan Transmission Reinforcement (OTR) project has increased
the average health of the terminal transformer group on the South Okanagan. A new
terminal transformer was added RG Anderson substation in 2010; the second transformer
T1 is 35 years old. This older transformer will be monitored and assessed consistent with
the Asset Management Plan.
There are three transformers in the Kootenay region which are older than 40 years. These
include AS Mawdsley T1 and T2 transformers, and the Grand Forks Terminal T1
transformer - respectively 46, 40 and 46 years old.�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�((hhC    ��@���R�hhC   �O /@���R�G@�      G@��     t�(hhC   �g@���R�hhC    /=@���R�G@�      G@��     t�eh?]�hA]�(]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�1�K e]�(�2�K e]�(�3�K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(K K e]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(K K e]�(�9�K e]�(�0�K e]�(�1�K e]�(�2�K e]�(K K e]�(�4�K e]�(NK e]�(�0�K e]�(NK e]�(�0�K e]�(�I�K e]�(�0�K e]�(NK e]�(�2�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   ��@���R�hhC   �`eI@���R�G@�      G@��     t�ah?]�(N�Dak#��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(Nj'  j '  j!'  j"'  j#'  j$'  j%'  e]�(N�
Dsinbution�NN�(SOC��Os)�NNe]�(�1��New Connects System Wide��11,057��10,780��11,446��11,536��12,076��211,955=�e]�(�2��Small Growth Projects��1,069��888��1,321��1,752��1,523��26,841�e]�(�3��Distribution Unplanned Growth��924��930��1,031��1,033��1,044��18,682�e]�(�4��%Glenmerry Feeder 2-Glenmerry Feeder 1��596�NNNNNe]�(�5��%Ellison Feeder 2 to Sexsmith Feeder 1�N�1,161�NNNNe]�(�6��Hollywood Feeder 5 Upgrades�NN�1,172�NNNe]�(�7��"Kaleden Feeder 1 Capacity Upgrades�NN�1,330�NNNe]�(�8��$Grand Forks Terminal Feeder Addition�NNNN�4,530�Ne]�(�9��(Kettle Valley to Nk'Mip Distribution Tie�NNNNN�7,699�e]�(�10��DG Bell Feeder 4 Addition�NNNNN�2,115=�e]�(�11��Total Distribution Growth��13,646��13,759��16,300��14,320��19,172��267,293�e]�(�12�NNNNNNNe]�(�13��Distribution Sustainment�NNNNNNe]�(�14��+41 Line Salvage and Distribution Underbuild��2,067�NNNNNe]�(�15��&Distribution Line Condition Assessment��1,410��1,398��1,530��1,509��1,569��27,970�e]�(�16�� Distribution Line Rehabilitation��5,298��3,517��3,592��3,840��3,865��69,449�e]�(�17��Distribution Line Rebuilds��1,679��1,660��2,214��2,251��2,335��41,153�e]�(�18��Distribution Urgent Repairs��2,411��2,315��2,480��2,606��2,605��46,384�e]�(�19��Forced Upgrades and Lines Moves��2,012��2,413��2,382��2,144��2,462��42,572�e]�(�20��'Distribution Line Small Planned Capital��726��826��853��867��870��15,719�e]�(�21��)Environmental Compliance (PCB Mitigation)�NNNNN�16,386�e]�(�22��Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706��259,634_�e]�(�23��Total Distribution Projects��29,249��25,889��29,351��27,537��32,878��526,927�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012 I S PNTEGRATEDYSTEMLAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@D�LV��OG@R��    G@�f�'bv'G@��f`   t�ah?]�(�#1 Table 3.0 - Distribution Projects�j  j  j  j  j  j  j  ehA]�(]�(�#1 Table 3.0 - Distribution Projects�NNNNNNNe]�(j  �Distribution Growth��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �New Connects System Wide��11,057��10,780��11,446��11,536��12,076��211,955�e]�(j�  �Small Growth Projects��1,069��888��1,321��1,752��1,523��26,841�e]�(j�  �Distribution Unplanned Growth��924��930��1,031��1,033��1,044��18,682�e]�(j�  �.Glenmerry Feeder 2-Glenmerry Feeder 1 Tie
Line��596�j�  j�  j�  j�  j�  e]�(j�  �)Ellison Feeder 2 to Sexsmith Feeder 1 Tie�j�  �1,161�j�  j�  j�  j�  e]�(j�  �Hollywood Feeder 5 Upgrades�j�  j�  �1,172�j�  j�  j�  e]�(j�  �"Kaleden Feeder 1 Capacity Upgrades�j�  j�  �1,330�j�  j�  j�  e]�(j�  �$Grand Forks Terminal Feeder Addition�j�  j�  j�  j�  �4,530�j�  e]�(j�  �(Kettle Valley to Nk'Mip Distribution Tie�j�  j�  j�  j�  j�  �7,699�e]�(�10��DG Bell Feeder 4 Addition�j�  j�  j�  j�  j�  �2,115�e]�(�11��Total Distribution Growth��13,646��13,759��16,300��14,320��19,172��267,293�e]�(�12�j  j  j  j  j  j  j  e]�(�13��Distribution Sustainment�j  j  j  j  j  j  e]�(�14��:41 Line Salvage and Distribution Underbuild
Rehabilitation��2,067�j�  j�  j�  j�  j�  e]�(�15��&Distribution Line Condition Assessment��1,410��1,398��1,530��1,509��1,569��27,970�e]�(�16�� Distribution Line Rehabilitation��5,298��3,517��3,592��3,840��3,865��69,449�e]�(�17��Distribution Line Rebuilds��1,679��1,660��2,214��2,251��2,335��41,153�e]�(�18��Distribution Urgent Repairs��2,411��2,315��2,480��2,606��2,605��46,384�e]�(�19��Forced Upgrades and Lines Moves��2,012��2,413��2,382��2,144��2,462��42,572�e]�(�20��'Distribution Line Small Planned Capital��726��826��853��867��870��15,719�e]�(�21��)Environmental Compliance (PCB Mitigation)�j�  j�  j�  j�  j�  �16,386�e]�(�22��Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706��259,634�e]�(�23��Total Distribution Projects��29,249��25,889��29,351��27,537��32,878��526,927�e]�(X  2 For the purposes of this planning exercise, two timeframes were used for reviewing
3 distribution projects. First, detailed planning studies and reviews were developed to identify
4 projects required within the 2012 to 2016 timeframe. Distribution projects beyond that
5 timeframe have only been scheduled in high-level terms since these projects are sensitive to
6 localized developments and load growth. As a result, the in-service date uncertainty for
7 these projects is greater for those projects beyond the five year horizon.
8 The following projects are required to address capacity, reliability and safety issues related
9 to the FortisBC distribution system and to ensure that all customers continue to receive safe,
10 reliable and cost-effective electric service.�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    �8@���R�hhC   ���R@���R�G@�      hhC   �5�{@���R�t�ah?]�(�15�NN�	able 3.1.��1��bution��New Cor��anects $��ystem��lide�NK ehA]�(]�(jz(  NNj{(  j|(  j}(  j~(  j(  j�(  j�(  NK e]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOOs�NNNNK e]�(�Cost��8,861��12,845��8,782��8,660��8,758��11,057��10,780��11,446��11,536��12,076��211,955�e]�(�16�N�3.1.2��SM��ALL GRC��WTH PR��OJECTS�NNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   �d�@���R�hhC   @N�N@���R�G@�      hhC   @��~@���R�t�ah?]�(K �on��	y related��upgrade��	under $50��10,000.��ts�ehA]�(]�(K j�(  j�(  j�(  j�(  j�(  j�(  e]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(�Cost��1,069��888��1,321��1,752��1,523��26,84�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J�\   G@R��    G@��    G@��f`   t�ah?]�(XT  1 3.1 Distribution Growth 2 The Distribution Growth projects are separated into two main categories; the Small Growth 3 projects including all capacity related projects under $500,000, and all other distribution 4 upgrade projects that are over $500,000. Each of the projects identified have been through 5 a planning procedure and are necessary to continue to provide reliable service. The 6 planning criteria by which these projects were evaluated are discussed in the Distribution 7 Planning Manual at Appendix H. 8 3.1.1 NEW CONNECTS SYSTEM WIDE 9 This project includes the installation of new electric services requiring additions to FortisBC 10 overhead or underground distribution facilities. These capital expenditures allow FortisBC to 11 meet its obligation to provide reliable service to customers in the service area. This project 12 will also fund any “forced upgrade” costs associated with upgrading FortisBC facilities to 13 provide service for the extension or drop service. 14 Capital expenditures are net of customer contributions. 15 Table 3.1.1 - Distribution New Connects System Wide�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(XT  1 3.1 Distribution Growth
2 The Distribution Growth projects are separated into two main categories; the Small Growth
3 projects including all capacity related projects under $500,000, and all other distribution
4 upgrade projects that are over $500,000. Each of the projects identified have been through
5 a planning procedure and are necessary to continue to provide reliable service. The
6 planning criteria by which these projects were evaluated are discussed in the Distribution
7 Planning Manual at Appendix H.
8 3.1.1 NEW CONNECTS SYSTEM WIDE
9 This project includes the installation of new electric services requiring additions to FortisBC
10 overhead or underground distribution facilities. These capital expenditures allow FortisBC to
11 meet its obligation to provide reliable service to customers in the service area. This project
12 will also fund any “forced upgrade” costs associated with upgrading FortisBC facilities to
13 provide service for the extension or drop service.
14 Capital expenditures are net of customer contributions.
15 Table 3.1.1 - Distribution New Connects System Wide�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��8,861��12,845��8,782��8,660��8,758��11,057��10,780��11,446��      �11,536��12,076��211,955�e]�(X�  16 3.1.2 SMALL GROWTH PROJECTS
17 The Small Growth Projects are projects that relate to capacity upgrades, feeder ties, and
18 load transfers and are required to keep pace with normal load growth on the distribution
19 system and to ensure continuing acceptable standards of service. These service standards
20 include operation of facilities at or below normal continuous thermal limits; voltage
21 consistent with Canadian Standards Association (CSA) recommended levels and short
22 circuit levels in a range to allow for safe operation of the electrical system. Capacity
23 increases must also be designed to provide sufficient redundancy to maintain supply during
24 planned and unplanned outages on the distribution system. The small growth projects are
25 defined by a distribution capacity related upgrade under $500,000.
26 Table 3.1.2 - Distribution Small Growth Projects
Year 2012 2013 2014 2015 2016 2017-31
($000s)
Cost 1,069 888 1,321 1,752 1,523 26,841�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@b�p   G@���   G@~��   G@�	�   t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNe]�(�Cost��1,069��888��1,321��1,752��1,523��26,841�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �1Table 3.1.2 - Distribution Small Growth Projects �j�  K�j�  (G@gٙ�   G@��`�   G@{��`   G@�u��   t�ububh)��}�(hNh]�(hhC    Ґ9@���R�hhC   ��G@���R�G@�      hhC   ���y@���R�t�ah?]�(�12�NN�Tabl��e 3.1.3��Distribu��tion Unp��lanned��Growth�NNK ehA]�(]�(j5)  NNj6)  j7)  j8)  j9)  j:)  j;)  NNK e]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016�K e]�(NNNNNN�(SOOOs)�NNNNK e]�(�Cost��1,065��834��604��750��981��924��930��1,031��1,033��1,044�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012 I S PNTEGRATEDYSTEMLAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J�\   G@R��    G@��    G@��f`   t�ah?]�(X�  1 3.1.3 DISTRIBUTION UNPLANNED GROWTH 2 Capacity upgrades and line extensions are required periodically to keep pace with normal 3 load growth on the distribution system and to ensure continuing acceptable standards of 4 service. These service standards include operation of facilities at or below normal 5 continuous thermal limits; voltage consistent with CSA recommended levels and short circuit 6 levels in a range to allow for safe operation of the electrical system. Capacity increases must 7 also be designed to provide sufficient redundancy to maintain supply during planned and 8 unplanned outages on the distribution system. 9 This program includes service upgrades, voltage regulation, tie to accommodate load 10 splitting, single-phase to three-phase upgrades and conductor upgrades that are necessary 11 due to load growth, but were unforeseen at the time the expenditure plan was prepared. 12 Table 3.1.3 - Distribution Unplanned Growth�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X�  1 3.1.3 DISTRIBUTION UNPLANNED GROWTH
2 Capacity upgrades and line extensions are required periodically to keep pace with normal
3 load growth on the distribution system and to ensure continuing acceptable standards of
4 service. These service standards include operation of facilities at or below normal
5 continuous thermal limits; voltage consistent with CSA recommended levels and short circuit
6 levels in a range to allow for safe operation of the electrical system. Capacity increases must
7 also be designed to provide sufficient redundancy to maintain supply during planned and
8 unplanned outages on the distribution system.
9 This program includes service upgrades, voltage regulation, tie to accommodate load
10 splitting, single-phase to three-phase upgrades and conductor upgrades that are necessary
11 due to load growth, but were unforeseen at the time the expenditure plan was prepared.
12 Table 3.1.3 - Distribution Unplanned Growth�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��1,065��834��604��750��981��924��930��1,031��1,033��1,044��18,682�e]�(X�  13 3.1.4 GLENMERRY FEEDER 2 TO GLENMERRY FEEDER 1 TIE LINE
14 Glenmerry Feeder 1 is a short, lightly loaded feeder that supplies the community of Casino
15 near Trail, British Columbia. It has 1.8 kilometres of three-phase line and at the 1.6 kilometre
16 mark has a 5.5 kilometre single-phase tap. With a forecast 2011 winter peak of 0.281 MVA
17 this feeder is underutilized and not operating in an optimum configuration.
18 The Fruitvale substation transformer is currently exceeding the nameplate capacity rating
19 during peak conditions. In order to alleviate this overload, a 2010 project transferred load
20 from Fruitvale Feeder 1 onto Beaver Park Feeder 2, and then from Beaver Park Feeder 1 to
21 Glenmerry Feeder 2. Also during 2010 however, two large developments (Waneta
22 Expansion and Firebird Technologies) took place on Beaver Park Feeder 2 and will drive the
23 Beaver Park transformer into an overloaded condition in 2011. Load transfer between
24 Beaver Park Feeder 1 and Glenmerry Feeder 2 will allow the Beaver Park T1 transformer to
25 operate below its nameplate rating, but the transfer will cause Glenmerry Feeder 2 to
26 operate outside of its normal rated limits (400 amp).
27 This project involves the construction of a river crossing to extend the Glenmerry Feeder 1
28 across the Columbia River to tie into Glenmerry Feeder 2 and allow load transfer from
29 Glenmerry Feeder 2 to Glenmerry Feeder 1. The project will offload feeders Glenmerry�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    ��?���R�hhC    '=@���R�G@�      G@��     t�ah?]�(�1��3.1.8��3.1.8�ehA]�(]�(j�)  j�)  j�)  e]�(�2��Christina Lake T1 trz��Lake T1 transformer is forecast to exceed its nominal nameplate capacity of 5�e]�(�3��MVA in winter 2016_��Oter 2016. Because the load supplied from this station is relatively low and the�e]�(�4��customer growth in��Lrowth in the area is not very high, the Company did an analysis to determine�e]�(�5��whether offloading th��Kloading this station would be possible with existing assets or with minimal�e]�(�6��investment to suppo��to support a future plan:�e]�(N�A location approximz��Opproximately 7 kilometres east of Grand Forks Terminal station on the 9 Line/10�e]�(�8��Line corridor provide��)r�e]�(�9��Terminal station to ir��Kation to interconnect with Ruckles Feeder 5 and Christina Lake Feeder 1. By�e]�(�0��interconnecting at th��$ting at this location, 2.2 MVA and 1�e]�(�1��Feeder 5 and Christi��Nnd Christina Lake Feeder 1 respectively with the added support of a regulator.�e]�(�2��This project will there��Qt will therefore eliminate the need to upgrade Christina Lake T1 transformer from�e]�(�3��a��5erspective until beyond the 20-year planning horizon:�e]�(�4��9 Line and 10 Line��F10 Line will no longer be required between Rossland and Christina Lake�e]�(�5��provided the Grand��Pe Grand Forks Terminal station transformer installation project; as discussed in�e]�(�6��section 2.8.3 above,��Q3 above, is implemented. As part of that project, Christina Lake will be supplied�e]�(N�from only one line OL��Lne line out of Grand Forks Terminal station and therefore the remaining line�e]�(�8��between Grand Fork��Nand Forks Terminal station's tap point (approximately three kilometres east of�e]�(�9��Grand Forks��Js) to Ruckles and Christina Lake can be decommissioned or rehabilitated as�e]�(�0��distribution:�Ne]�(�1��To create an expres=��Jn express feeder to the interconnection point; underbuild would need to be�e]�(�3��transmission line bet��Pn line between the Ruckles tap point with the interconnection point converted to�e]�(�4��distribution. As both��KAs both 9 Line and 10 Line from Grand Forks Terminal station to the Ruckles�e]�(�5��tap point are in poor��Oe in poor condition and neither circuit has sufficient clearance to allow for a�e]�(�6��distribution underbui��Runderbuild circuit to be installed, this project was estimated based on building a�e]�(�7��new circuit from Gra��Nfrom Grand Forks Terminal station to the Ruckles tap point and then converting�e]�(�8��the redundant line be��Mant line between the tap point and the interconnection point to distribution:�e]�(�9��This project falls in��Q falls in line with the Company's 20 year plan to have a 25 kV express feeder out�e]�(�0��of Grand Forks Tern��Lprks Terminal station to offload Ruckles and Christina Lake completely. This�e]�(�1��project puts the line��5�e]�(�2��until Christina Lake��,na Lake or Ruckles is once again overloaded_�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC    nGB@���R�hhC    m�V@���R�G@�      G@��     t�ah?]�(N�Dx L��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(Nj*  j*  j*  j*  j*  j*  j *  e]�(N�
DisiDution�NN�(Soo��JOs)�NNe]�(�1�� 41 Line Salvage and Distribution��2,067�NNNNNe]�(�2��Distribution Line Condition��1,410��1,398��1,530��1,509��1,569�K e]�(�3�� Distribution Line Rehabilitation��5,298��3,517��3,592��3,840��865��69,445�e]�(�4��Distribution Line Rebuilds��1,679��1,660��2,214��2,251��2,335��41,153�e]�(�4��DiSulion��T,on9��T,oou��2,214��L,lo |��2,ssj��41,TJC�e]�(�0��DiSrIDUlion Urgent Kepairs��2,4| |��2,51J��4,4ou��Z,ouo��L,ouj��40,504�e]�(�0��rorced upgrades��2,u 12��L,4|3��2,5oz��2,144��4,402��4w,0( C�e]�(�7��Disulipulion��726��826��853��867��870��15,719�e]�(�8��Environmental Compliance (PCB�NNNNN�16,386�e]�(�9��Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706�K e]�(�9��Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706��259,634�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@Q�G�UUUG@R��    G@�w�   G@��f`   t�ah?]�(XQ  Expenditure Plan], section 4.1.2.1). This is a more cost effective approach to this problem and defers the larger capital expenditure of adding a fourth feeder. However, eventually there will be a requirement for a fourth feeder from the DG Bell Terminal station in order to provide relief to the existing DG Bell feeders and continue to provide reliable service to the south Mission area. This project is estimated to cost $2.12 million with a projected start date of 2018 and a necessary in-service date of 2018. 3.2 Distribution Sustainment Table 3.2 - Distribution Sustainment Expenditures�j  j  j  j  j  j  j  ehA]�(]�(XQ  Expenditure Plan], section 4.1.2.1). This is a more cost effective approach to this problem
and defers the larger capital expenditure of adding a fourth feeder. However, eventually
there will be a requirement for a fourth feeder from the DG Bell Terminal station in order to
provide relief to the existing DG Bell feeders and continue to provide reliable service to the
south Mission area.
This project is estimated to cost $2.12 million with a projected start date of 2018 and a
necessary in-service date of 2018.
3.2 Distribution Sustainment
Table 3.2 - Distribution Sustainment Expenditures�NNNNNNNe]�(j  �Distribution Sustainment��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �:41 Line Salvage and Distribution
Underbuild Rehabilitation��2,067�j�  j�  j�  j�  j�  e]�(j�  �&Distribution Line Condition
Assessment��1,410��1,398��1,530��1,509��1,569��27,970�e]�(j�  � Distribution Line Rehabilitation��5,298��3,517��3,592��3,840��3,865��69,449�e]�(j�  �Distribution Line Rebuilds��1,679��1,660��2,214��2,251��2,335��41,153�e]�(j�  �Distribution Urgent Repairs��2,411��2,315��2,480��2,606��2,605��46,384�e]�(j�  �Forced Upgrades and Lines Moves��2,012��2,413��2,382��2,144��2,462��42,572�e]�(j�  �'Distribution Line Small Planned
Capital��726��826��853��867��870��15,719�e]�(j�  �)Environmental Compliance (PCB
Mitigation)�j�  j�  j�  j�  j�  �16,386�e]�(j�  �Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706��259,634�e]�(Xw  3.2.1 41 LINE SALVAGE AND DISTRIBUTION UNDERBUILD REHABILITATION
41 Line will no longer be required by the end of 2011 after the Okanagan Transmission
Reinforcement and the Huth Bus Reconfiguration projects are complete. 41 Line is an older
vintage line and is in poor condition (approximately $850,000 worth of rehabilitation is
required from a recent condition assessment). 41 Line has distribution underbuild along
most of its length so even though the transmission circuit can be salvaged, the distribution
underbuild must remain.
The scope of this project is to salvage the 41 Line transmission conductor and structures
that do not have distribution underbuild along the full right of way while keeping the
distribution underbuild poles in place. The purpose of this project is to salvage out the
abandoned plant that poses a safety risk. Recently there have been many concerns with�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ���8@���R�hhC   ��_@���R�G@�      hhC   �<�~@���R�t�ah?]�(�24�N�1��	Table 3.2��22��ribution��Line Cor��ndition��Assessm��ent�NNehA]�(]�(j�*  Nj�*  j�*  j�*  j�*  j�*  j�*  j�*  j�*  NNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOOs_�NNNNNe]�(�Cost��938��692��659��605��992��1,410��1,398��1,530��1,509��1,569��27,970�e]�(�25�N�3.2 3��DI=��STRIBUTI��ON LINE��	REHABILIF��TATION�NNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@J�\   G@R��    G@��    G@��f`   t�ah?]�(X�  1 treatment, and the terrain. These factors are taken into consideration when calculating the 2 forecast expenditures. 3 Worst Performing Feeders Program 4 As outlined in the response to BCUC IR 59.1.1 (Exhibit B-3) regarding FortisBC’s 2010 5 Annual Review and 2011 Revenue Requirements, FortisBC does not have a specific 6 program that defines distribution projects purely based on reliability statistics. 7 A Worst Performing Feeders Program involves, identifying the poorest performing feeders 8 and the cause(s) of the poor performance, developing and implementing an action plan to 9 correct the problems and assessing the effectiveness (improved reliability and cost- 10 effectiveness) the action plan had on the system. The poorest performing feeder is identified 11 using reliability statistics derived from the average duration of customer interruptions (SAIDI) 12 and the average frequency off customers interrupted (SAIFI). A Worst Performing Feeders 13 Program enables a focused allocation of resources on specific feeders to address system 14 reliability improvements by identifying the worst performing areas of the system using 15 defined criteria, identifying causes for poor performance, and identifying and implementing a 16 cost-effective solution. 17 FortisBC currently implements a slightly different version of a worst performing feeder 18 program. The Company implements an eight-year condition assessment/rehabilitation cycle 19 on all distribution circuits to ensure the assets are sound and unlikely of failing. Every year 20 the distribution system is upgraded with new reclosers/switches or maintenance to correct 21 deficiencies brought forward from operations using the unplanned growth and/or small 22 planned capital budgets. Between these processes and the brushing program the Company 23 is continually improving the worst performing feeders throughout the service territory. 24 Table 3.2.2 - Distribution Line Condition Assessment�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X�  1 treatment, and the terrain. These factors are taken into consideration when calculating the
2 forecast expenditures.
3 Worst Performing Feeders Program
4 As outlined in the response to BCUC IR 59.1.1 (Exhibit B-3) regarding FortisBC’s 2010
5 Annual Review and 2011 Revenue Requirements, FortisBC does not have a specific
6 program that defines distribution projects purely based on reliability statistics.
7 A Worst Performing Feeders Program involves, identifying the poorest performing feeders
8 and the cause(s) of the poor performance, developing and implementing an action plan to
9 correct the problems and assessing the effectiveness (improved reliability and cost-
10 effectiveness) the action plan had on the system. The poorest performing feeder is identified
11 using reliability statistics derived from the average duration of customer interruptions (SAIDI)
12 and the average frequency off customers interrupted (SAIFI). A Worst Performing Feeders
13 Program enables a focused allocation of resources on specific feeders to address system
14 reliability improvements by identifying the worst performing areas of the system using
15 defined criteria, identifying causes for poor performance, and identifying and implementing a
16 cost-effective solution.
17 FortisBC currently implements a slightly different version of a worst performing feeder
18 program. The Company implements an eight-year condition assessment/rehabilitation cycle
19 on all distribution circuits to ensure the assets are sound and unlikely of failing. Every year
20 the distribution system is upgraded with new reclosers/switches or maintenance to correct
21 deficiencies brought forward from operations using the unplanned growth and/or small
22 planned capital budgets. Between these processes and the brushing program the Company
23 is continually improving the worst performing feeders throughout the service territory.
24 Table 3.2.2 - Distribution Line Condition Assessment�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��938��692��659��605��992��1,410��1,398��1,530��1,509��1,569��27,970�e]�(��25 3.2.3 DISTRIBUTION LINE REHABILITATION
26 The project involves expenditures for structural stabilization of multiple distribution lines
27 based on the detailed Distribution Line Condition Assessment program which is based on an
28 eight-year cycle.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   @�<@���R�hhC   @	aW@���R�G@�      hhC   �m\q@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(jW+  jX+  jY+  jZ+  j[+  j\+  j]+  j^+  j_+  j`+  ja+  jb+  e]�(NNNNNN�(SOOOs)�NNNNNe]�(�Cost��1,375��3,727��3,294��3,086��2,303��5,298��3,517��3,592��3,840��3,865��69,44c�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   �$:@���R�hhC   ��=\@���R�G@�      hhC   ���}@���R�t�ah?]�(�18�NNN�	Table 3.3��4��ribution��Line Reb��uilds�NNNehA]�(]�(j�+  NNNj�+  j�+  j�+  j�+  j�+  NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SooO��S)�NNNNe]�(�Cost��1,528��1,310��1,371��1,240��2,071��1,679��1,660��2,214��2,251��2,335��41,153�e]�(�19�N�3.2.5�N�JISTRIBU��	TION LINE�� URGENT��REPAIRS�NNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@I33-��nG@R��    G@��ĒI%G@��f`   t�ah?]�(Xz  1 The key stakeholders in this project are the property owners and the general public along 2 the route of the subject lines. The interest of property owners and the general public relates 3 to the potential for property damage or personal injury in the event that the lines failed 4 mechanically. A proactive preventive maintenance program that minimizes the risk of 5 structural failure best serves their interests. 6 This project is ongoing and required to address public and employee safety issues, 7 environmental concerns and to maintain reliable service to FortisBC customers. 8 Table 3.2.3 – Distribution Line Rehabilitation�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(Xz  1 The key stakeholders in this project are the property owners and the general public along
2 the route of the subject lines. The interest of property owners and the general public relates
3 to the potential for property damage or personal injury in the event that the lines failed
4 mechanically. A proactive preventive maintenance program that minimizes the risk of
5 structural failure best serves their interests.
6 This project is ongoing and required to address public and employee safety issues,
7 environmental concerns and to maintain reliable service to FortisBC customers.
8 Table 3.2.3 – Distribution Line Rehabilitation�NNNNNNNNNNNNNNNNNNe]�(�Year��2007�N�2008�N�2009�N�2010�N�2011�N�2012�N�2013�N�2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNNNNNNNNe]�(�Cost��1,375�N�3,727�N�3,294�N�3,086�N�2,303�N�5,298�N�3,517�N�3,592��3,840��3,865��69,449�e]�(X  9 3.2.4 DISTRIBUTION LINE REBUILDS
10 On a regular basis Distribution Planning Engineers undertake site assessments of the
11 distribution system in their respective areas. They review the system from a safety, reliability
12 and capacity perspective. Any sections of line that have deficiencies are identified and a
13 proactive project is established to correct the problem. This project involves the replacement
14 of aged and deteriorated equipment. Items include rebuilding failing overhead and
15 underground conductor, replacing rotted poles and platforms, replacing leaking
16 transformers, and installing ground grids at ungrounded services. These deficiencies are
17 identified through site assessments and normal daily operations.
18 Table 3.2.4 - Distribution Line Rebuilds�NNNNNNNNNNNNNNNNNNe]�(�Year��2007��2008�N�2009�N�2010�N�2011�N�2012�N�2013�N�2014�N�2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNNNNNNNNe]�(�Cost��1,528��1,310�N�1,371�N�1,240�N�2,071�N�1,679�N�1,660�N�2,214�N�2,251��2,335��41,153�e]�(Xi  19 3.2.5 DISTRIBUTION LINE URGENT REPAIRS
20 Component failures on the distribution system due to inclement weather, defective
21 equipment, animal intrusions, vandalism, abnormal operating conditions, vehicle collisions
22 and human error cause outages or present risks that must be addressed in an expedient
23 manner to ensure that employee and public safety is not at risk and electrical service
24 continuity is maintained. This project is ongoing and involves the repair or replacement of
25 distribution equipment that fail in-service due to severe weather, vandalism or for other
26 unexpected urgent reasons.�NNNNNNNNNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    ��4@���R�hhC   ���S@���R�G@�      hhC    A\p@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j,  j,  j,  j,  j,  j,  j,  j,  j,  j,  j,  j,  e]�(NNNNNN�(SoOO��s)�NNNNe]�(�Cost��2,117��3,258��2,370��2,008��2,842��2,411��2,315��2,480��2,606��2,605��46,384�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@H��|   G@R��    G@�&�@   G@��f`   t�ah?]�(�01 Table 3.2.5 - Distribution Line Urgent Repairs�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�01 Table 3.2.5 - Distribution Line Urgent Repairs�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��2,117��3,258��2,370��2,008��2,842��2,411��2,315��2,480��2,606��2,605��46,384�e]�(X  2 3.2.6 FORCED UPGRADES AND LINE MOVES
3 This program is required to complete distribution upgrades driven by third-party requests.
4 These requests can be a result of highway/road widening or improvements, miscellaneous
5 customer line moves or an upgrade resulting from new customer connects.
6 Relocation of distribution lines due to highway/road widening or improvements will be
7 initiated based on requests from the BC Ministry of Transportation and Infrastructure and/or
8 municipalities. Miscellaneous customer line move requests where FortisBC does not have
9 sufficient land rights for the facilities located on customer property are also included in this
10 project.
11 • Requests which are received from governing authorities (e.g. Ministry of
12 Transportation and Infrastructure or municipalities) within the FortisBC service area
13 to relocate distribution lines located on road allowance or highway rights of way to
14 accommodate road widening or improvements.
15 • Customer requests to move FortisBC distribution line facilities.
16 • Requests to satisfy issues where FortisBC does not have sufficient land rights for the
17 distribution line facilities located on customer property. In each case, alternatives will
18 be considered and the most cost effective solution will be selected. This includes
19 having the FortisBC land department attempt to negotiate land rights for non-
20 easement issues.
21 • Third-party utility requests for upgrade of FortisBC distribution line plant to
22 accommodate a shared use arrangement.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    ��:@���R�hhC   `��Q@���R�G@�      hhC   �4}{@���R�t�ah?]�(�12�NN�Table�� 3.2.7��	Distribu-��tion Smz��	II Planne��	d Capita_�NNNehA]�(]�(jm,  NNjn,  jo,  jp,  jq,  jr,  js,  NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SooO��s)�NNNNe]�(�Cost��1,080��572��642��868��805��726��826��853��867��870��15,719�e]�(�13�N�3.2.8�N�NVIRONN��AENTAL��OMPLIAN��CE (PCB��	MITIGATIC��N)�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@H�6�m�G@R��    G@�&���m�G@��f`   t�ah?]�(�.1 Table 3.2.6 - Forced Upgrades and Line Moves�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�.1 Table 3.2.6 - Forced Upgrades and Line Moves�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��1,573��385��2,016��3,768��1,572��2,012��2,413��2,382��2,144��2,462��42,572�e]�(XE  2 3.2.7 DISTRIBUTION SMALL PLANNED CAPITAL
3 This program is similar to the Distribution Condition Assessment and Rehabilitation
4 programs but captures off-cycle work required to keep the distribution lines safe and
5 reliable. This program involves expenditures for repairs that are identified on the distribution
6 system as a result of storm damage, clearance problems, aging equipment, reports by
7 powerline technicians and other inspections outside of the normal assessment cycle. The
8 repairs to address these concerns are required to maintain a safe and reliable distribution
9 system and are generally non-urgent in nature and are normally completed within one year
10 of the initial request. The planned expenditures for this program are based on historical
11 information.
12 Table 3.2.7 - Distribution Small Planned Capital�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��1,080��572��642��868��805��726��826��853��867��870��15,719�e]�(X�  13 3.2.8 ENVIRONMENTAL COMPLIANCE (PCB MITIGATION)
14 The Canadian Environmental Protection Act PCB Regulations (SOR/2008-273) came into
15 effect on September 5, 2008. The purpose of the PCB Regulations is to improve the
16 protection of Canada’s environment and the health of Canadians by minimizing the risks
17 posed by the use, storage and release of polychlorinated biphenyls (PCBs) and by
18 accelerating the elimination of these substances.
19 By the end of 2014, FortisBC will meet regulations for all substation equipment including
20 small volume units such as bushings and instrument transformers. Expenditures of
21 approximately $27.40 million will be required to meet the station equipment compliance
22 requirement by December 31, 2014.
23 Compliance will be required by 2025 for all distribution oil-filled equipment to less than 50
24 mg/kg PCB. Planning and testing for the future removal and destruction of PCB-
25 contaminated overhead distribution equipment will start in 2015, based on best
26 management practice and experience gained in the earlier PCB program. FortisBC expects
27 to spend $16.40 million from 2017 to 2025 to meet these regulations.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC    �?���R�hhC    L B@���R�G@�      G@��     t�ah?]�(�1��4.��TELECOMMUNICATIONS, SCADA��3OTE��TECTION AND CONTROL�ehA]�(]�(j�,  j�,  j�,  j�,  j�,  e]�(�2��4��4.1�NNe]�(�3��FortisB��
tisBC owns��Isive��''e communications network in support of�e]�(�4��the safe��.safe , reliable and efficient operation of the��ectric�� ric grid throughout the Companys�e]�(�5��service��*ice area. Over 100 locations including 9 t��inal��*al stations, 52 distribution and switching�e]�(�6��stations��*ions, 4 generation plants, 16 mountain-tor��dio re��)�e]�(�7��connec��)nected to the network. As well, the compa��owns��ins and operates communications�e]�(�8��equipm��*ipment at a number of third-party substati��; and��)Id generation sites that are necessary to�e]�(�9��operate��&rate the interconnected system: Commun��tion��!1 links are also provided between�e]�(�10��neighbe��)ghbouring entities such as BC Hydro, Colu��ia Pc��'Power Corporation, and Teck Metals Ltd_�e]�(�11��for the��)he exchange of operational data and prot:��ion ci�K e]�(�12��Specific��
cifically,��Ites th��; the�e]�(NN�#Teleprotection (Transmission lines)�N�Security�e]�(NN�Asset monitoring and control�N�Phone services�e]�(NN�Revenue metering�N�Very High Frequency (VHF) radio�e]�(NN�Remedial Action Schemes (RAS)�N�Corporate IntranetlInternet�e]�(�14��In addit��-ddition to the applications listed above, otE��traffi��(ffic that is expected to be added to the�e]�(�15��FortisB��$tisBC network in the future include:�NNe]�(NN�Video surveillance�N�&Distribution automation and protection�e]�(NN� Advanced Metering Infrastructure�N�
Smart Grid�e]�(NN�(AMI)�N� Transformer condition monitoring�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   ����?���R�hhC    �{Q@���R�G@�      G@��     t�ah?]�hA]�(]�(�L��	limu-pany�e]�(�3��OTwo communications technologies capable of providing the required bandwidth and�e]�(�4��Qreliability are high-bandwidth microwave and fibre-optic communications. Each has�e]�(�5��#advantages and disadvantages. For a�e]�(�6��Ucommunications tends to be more appropriate for spanning very long distances, whereas�e]�(�7��Ffibre-optic cable is generally more suitable for shorter path lengths:�e]�(�8��WFortisBC's preferred method of communications technology is fibre-optic communications.�e]�(�9��This�e]�(�10��Uextend and enhance its fibre-optic network as required to support recent transmission�e]�(�11��Tinfrastructure upgrades Fibre-optic communications offers numerous advantages from a�e]�(�12��`utility point-of-view, including essentially unlimited bandwidth, extremely high reliability and�e]�(�13��Nsecurity, relatively low installation costs and Iow ongoing maintenance costs.�e]�(�14��From a�e]�(�15��Wcomposed of two separate, linear fibre-optic routes, one in the Okanagan and one in the�e]�(�16��Kootenay region.�e]�(�17��ZAs can be seen in the following diagram the System Control Centre, located in Warfield, is�e]�(�18��Zconnected directly to the Kootenay fibre system but there is no infrastructure in place to�e]�(�19��Winterconnect the Okanagan system. FortisBC presently has access to two digital circuits�e]�(�20��Qbetween the Vaseux Lake Terminal station and BC Hydro's Kootenay Canal Generating�e]�(�21��>Station provided by a third-party to facilitate this backhaul:�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ���?���R�hhC    ���?���R�G@�      G@��     t�ah?]�(�2�N�additions to the system:�ehA]�(]�(j�-  Nj�-  e]�(�3�N�KThere is no guarantee of lease. Many utilities are exploring deploying next�e]�(�4�N�Pgeneration networks based on packet switching technology which may or may not be�e]�(�5�N�Xcapable of carrying legacy time-division multiplexing (TDM) traffic with the reliability�e]�(�6�N�Nof the current Synchronous Optical Network (SONET) systems The provider of the�e]�(�7�N�Qcircuits is expected to begin their migration sooner than FortisBC and this could�e]�(�8�N�Nimpact the leased circuits and the ability to monitor and control the Okanagan�e]�(�9�N� systems from the control centre.�e]�(�10�N�4.2.2�e]�(�11��Figu��Vre 4.2.1 above also illustrates the absence of path or route diversity on the FortisBC�e]�(�12��bach��Qbone system. Even without path redundancy, multiple fibres can be used to achieve�e]�(�13��equi��Epment redundancy, and this has proven to be a robust solution so far.�e]�(�14��On��She other hand, a physical break in the fibre, or severe degradation in the physical�e]�(�15��pror��Oerties of the fibre due to environmental factors are risks that could sever the�e]�(�L��ine��8)Oin Ol Tibre allure ana mne aiStant ena 0l ine nelwork:�e]�(�18��Inis��Yiinear pnysical topology OT tne FOrtISBC Tlpre-optic nerwork introauces & reasonapie riSK�e]�(�19��of pr��6olonged communication outages at multiple substations.�e]�(�20�N�di�e]�(�Zu��Valc��anomn�e]�(�L |��uuui��4�e]�(�LL��Ciro��ulaio�e]�(�24�N�4.2.3�e]�(�25��Kelc��Twna has grown to become the largest city in the interior of British Columbia and the�e]�(�26��majc��Tr economic hub for the region. Robust; reliable communications are important because�e]�(�27��the��Oransmission network in the area is more interconnected (meshed) than in smaller�e]�(�28��mun��[icipalities. Consequently, any faults have a magnified effect on the stability of the power�e]�(�29��syst��em.�e]�(�30��and��QSaucier substations are not connected to the backbone of the FortisBC network and�e]�(�31��rely��/on low speed, equipment for all communications:�e]�(K N�Page 178�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   ��2@���R�hhC   ���O@���R�G@�      G@��     t�ah?]�(NN�2012��2013��2014��2015��2016��2017-31�ehA]�(]�(NNj,.  j-.  j..  j/.  j0.  j1.  e]�(�1��Growth�NN�(Sq��OOs)�NNe]�(�2��)Kelowna 138 kV Loop Fibre and Multiplexer��1,212��2,549�NNNK e]�(�3��'Kootenay Remedial Action Scheme-Install�NNN�1,166��3,162�K e]�(�4��%Syncrophasor Data Collection Platform�NNNN�623�K e]�(�5��'Okanagan Remedial Action Scheme-Install�NNNNN�9,172�e]�(�6��&Princeton to Oliver Fibre Installation�NNNNN�14,349�e]�(N�"Total Telecom SCADA Protection and��1,212��2,549�N�1,166��3,786��23,521�e]�(�8�NNNNNNNe]�(�9��$Telecom SCADA Protection and Control�NNNNNNe]�(�10��Communication Upgrades��410��400��763��776��587��7,234�e]�(�11��SCADA Systems Sustainment��707��733��784��811��843�K e]�(�12��'Backbone Transport Technology Migration�NN�410��6,652�NK e]�(�13��Station Smart Device Upgrades�NN�704��363��741�K e]�(�14��Telecommunications Ring Closure�NNNNNK e]�(�15��"Total Telecom SCADA Protection and��1,117��1,133��2,661��8,601��2,171�K e]�(�16��"Total Telecom SCADA Protection and��2,329��3,682��2,661��9,768��5,957�K e]�(N�4.3.1�NNNNNK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(G@R�ێ8�G@R��    G@�ms�8�G@��f`   t�ah?]�(XL  4.3 Telecommunications, SCADA Protection and Control Projects Table 4.3 below outlines the forecast expenditures for SCADA, Telecommunications and Protection and Control projects over the forecast period. The projects themselves are then further described below. Table 4.3 - Telecommunications, SCADA Protection and Control Projects�j  j  j  j  j  j  j  ehA]�(]�(XL  4.3 Telecommunications, SCADA Protection and Control Projects
Table 4.3 below outlines the forecast expenditures for SCADA, Telecommunications and
Protection and Control projects over the forecast period. The projects themselves are then
further described below.
Table 4.3 - Telecommunications, SCADA Protection and Control Projects�NNNNNNNe]�(j  j  �2012��2013��2014��2015��2016��2017-31�e]�(ja
  �Growth��($000s)�NNNNNe]�(j�  �6Kelowna 138 kV Loop Fibre and Multiplexer
Installation��1,212��2,549�j�  j�  j�  j�  e]�(j�  �?Kootenay Remedial Action Scheme-Install
Redundant Backup System�j�  j�  j�  �1,166��3,162�j�  e]�(j�  �%Syncrophasor Data Collection Platform�j�  j�  j�  j�  �623�j�  e]�(j�  �?Okanagan Remedial Action Scheme-Install
Redundant Backup System�j�  j�  j�  j�  j�  �9,172�e]�(j�  �&Princeton to Oliver Fibre Installation�j�  j�  j�  j�  j�  �14,349�e]�(j�  �1Total Telecom SCADA Protection and
Control Growth��1,212��2,549�j�  �1,166��3,786��23,521�e]�(j�  j  j  j  j  j  j  j  e]�(j�  �0Telecom SCADA Protection and Control
Sustainment�j  j  j  j  j  j  e]�(�10��Communication Upgrades��410��400��763��776��587��7,234�e]�(�11��SCADA Systems Sustainment��707��733��784��811��843��14,863�e]�(�12��'Backbone Transport Technology Migration�j�  j�  �410��6,652�j�  j�  e]�(�13��Station Smart Device Upgrades�j�  j�  �704��363��741��3,932�e]�(�14��Telecommunications Ring Closure�j�  j�  j�  j�  j�  �4,265�e]�(�15��6Total Telecom SCADA Protection and
Control Sustainment��1,117��1,133��2,661��8,601��2,171��30,294�e]�(�16��3Total Telecom SCADA Protection and
Control Projects��2,329��3,682��2,661��9,768��5,957��53,815�e]�(X_  4.3.1 GROWTH PROJECTS
4.3.1.1 Kelowna 138 kV Loop Fibre Installation
This project is a multiple-year effort to improve the capacity and reliability of the
communications infrastructure in the Kelowna area and will lay the groundwork for future
power system reliability improvements. The project includes the installation of multi-strand
single-mode fibre cable which will supplement existing fibre leased under a long-term
Indefeasible Right of Use (IRU) and existing FortisBC-owned fibre optic cabling. This will
complete a physical fibre-optic ring to all Kelowna area substations. In addition, high speed�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@V�     G@D"4    G@n��@   G@K��   t�ububh)��}�(hNh]�(hhC   ��@���R�hhC   `{\C@���R�G@��     hhC   ��d�@���R�t�ah?]�(�Option��Capital��NPV��
Total Cost��SCADA��Tele-��Other��Highly��Future��No Reliance��
Simplified��Fully�ehA]�(]�(j/  j	/  j
/  j/  j/  j/  j/  j/  j/  j/  j/  j/  e]�(�A��
S1,816,000��S675,467��
S2,491,467��X�N�X�NNNNNe]�(�B��
S5,449,000��S675,467��
S6,124,467��X��X�N�X�NN�X�Ne]�(�C��
S6,203,000��S675,467��
S6,878,467��X��X��X��X�NNNNe]�(�D��S631,000��
81,688,668��
S2,319,668��X�NNNNNNNe]�(�E��
S3,215,000��SO��
S3,215,000��X��X��X��X��X��X��X�Ne]�(�F��
S3,761,000��SO��
53,761,000��X��X��X��X��X��X��X�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@1�Q�   G@R��    G@�ĸP   G@�6f`   t�ah?]�(�N1 Table 4.3.1.1 - Kelowna 138 kV Loop Fibre Installation - Option Cost Summary�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�N1 Table 4.3.1.1 - Kelowna 138 kV Loop Fibre Installation - Option Cost Summary�NNNNNNNNNNNe]�(�Option��Capital
Cost��NPV
Lease
Cost1��
Total Cost��SCADA��Tele-
protection��Other
Operational
Traffic2��Highly
Reliable3��Future
Capacity4��No Reliance
on Third
Party��Simplified
Maintenance5��Fully
Redundant�e]�(�A��
$1,816,000��$675,467��
$2,491,467�js  j  js  j  j  j  j  j  e]�(j�  �
$5,449,000��$675,467��
$6,124,467�js  js  j  js  j  j  js  j  e]�(jc  �
$6,203,000��$675,467��
$6,878,467�js  js  js  js  j  j  j  j  e]�(�D��$631,000��
$1,688,668��
$2,319,668�js  j  j  j  j  j  j  j  e]�(�E��
$3,215,000��$0��
$3,215,000�js  js  js  js  js  js  js  j  e]�(�F��
$3,761,000��$0��
$3,761,000�js  js  js  js  js  js  js  js  e]�(X�  2 1 NPV Lease Cost uses simplified method to calculate the present value of recurring monthly costs over 30 years (no inflation of lease costs)
3 2 Other operational traffic includes operational WAN, phones or corporate WAN services available to substations
4 3 A system is considered highly reliable if its availability is sufficient to assume outage length will not be increased by its outages (>99.95%)
5 4 Future capacity considers the ability to accommodate unknown future applications on the infrastructure (assuming high bandwidth)
6 5 A system is considered to have simplified maintenance if all maintainable facilities are located within FortisBC assets (substations)�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �2012ISPNTEGRATED YSTEM LAN�j�  K�j�  (G@R      G@D"4    G@le�@   G@K��   t�ububh)��}�(hNh]�(hhC   ����?���R�hhC   `��:@���R�G@�      G@��     t�ah?]�(�1�N�.1.3�ehA]�(]�(j�/  Nj�/  e]�(�2��The�K e]�(�3��	collectir�K e]�(�4��dispatcL��;rtisBC has a large number of SEL relays that calculate this�e]�(�5��informa��ut as�e]�(�6��	the reli=��Bsystem: At the conclusion of the project; system operators will be�e]�(�7��able to��erences between sites and�e]�(�8��effectiv�K e]�(�9��The init�K e]�(�10�N�@sh Columbia's transmission technology and infrastructure remains�e]�(�11�NK e]�(�12�NK e]�(�13�NK e]�(�16�NK e]�(�17�NK e]�(�18�N�9to present the information to FortisBC's dispatchers; and�e]�(�19�NK e]�(�20�NK e]�(�21��The�K e]�(�22�N�.1.4�e]�(�24��FortisB��<ave jointly and independently conducted planning studies and�e]�(�25��conting�K e]�(�26��specific��t will�e]�(�27��System��Dloadlgeneration or open transmission lines to maintain the integrity�e]�(�28��of the r�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(hhC   �C(8@���R�hhC    ~�F@���R�G@�      hhC   @�	�@���R�t�ah?]�(NN�2012��2013��2014��2015��2016��2017-31�ehA]�(]�(NNj�/  j�/  j�/  j�/  j�/  j�/  e]�(NNNN�(SO��OOs)�NNe]�(�1��Communication Upgrades��410��400��763��776��587��7,234�e]�(�2��SCADA Systems Sustainment��707��733��784��811��843��14,863�e]�(�3��'Backbone Transport Technology Migration�NN�410��6,652�NNe]�(�4��Station Smart Device Upgrades�NN�704��363��741��3,932�e]�(�5��Telecommunications Ring Closure�NNNNN�4,265�e]�(�6��"Total Telecom SCADA Protection and��1,117��1,133��2,661��8,601��2,171��30,294�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBC I�j�  K�j�  (G@V�     G@D"4    G@b4!�   G@K��   t�ububh)��}�(hNh]�(G@Q�K'q�G@R��    G@�p6�q�G@��f`   t�ah?]�(�I4.3.2 SUSTAINMENT PROJECTS Table 4.3.2 - Sustainment Project Expenditures�j  j  j  j  j  j  j  ehA]�(]�(�I4.3.2 SUSTAINMENT PROJECTS
Table 4.3.2 - Sustainment Project Expenditures�NNNNNNNe]�(j  j  �2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �Communication Upgrades��410��400��763��776��587��7,234�e]�(j�  �SCADA Systems Sustainment��707��733��784��811��843��14,863�e]�(j�  �'Backbone Transport Technology Migration�j�  j�  �410��6,652�j�  j�  e]�(j�  �Station Smart Device Upgrades�j�  j�  �704��363��741��3,932�e]�(j�  �Telecommunications Ring Closure�j�  j�  j�  j�  j�  �4,265�e]�(j�  �6Total Telecom SCADA Protection and
Control Sustainment��1,117��1,133��2,661��8,601��2,171��30,294�e]�(Xf  4.3.2.1 Communication Upgrades
This project will upgrade and update telecommunications routes and will improve
emergency response capability. Some FortisBC telecom equipment is near or beyond its
designed operational life. Individual components are unreliable, and the manufacturers no
longer supply spare parts. In some extreme cases, equipment can no longer be tested or
adjusted because it fails when test systems are operated. This results in delays returning
equipment to service. This equipment can also cause failure of the transmission and
distribution systems it supports or prevent restoration efforts, exposing the system to
possible equipment damage, extended outage times and possibly causing public safety
issues. FortisBC plans to pursue a two-fold strategy to address this issue; upgrade parts of
the telecom system regularly over several years, and prepare an emergency response plan
and supply spare new systems that may be used in emergency restoration.
Specifically, the following projects have been identified:
• Jungle Mux Laser upgrade - Current SONET backbone speed in the Kootenay
area is an OC1 (50 Mbps), and this is scheduled for upgrading to an OC3 (155
Mbps) in 2012.
• Upgrade Backhaul to North Warfield Substation - Point-Point 900 MHz MDS
LEDR radio has had reliability problems. A replacement 900 MHz or 2 GHz link will
be examined and installed tentatively in 2013.
• Distribution Substation Automation Completion - FortisBC has recently
completed a project to automate distribution substations throughout the service
region. During these upgrades, some stations were deliberately omitted from the�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   ��CP@���R�hhC   �)�X@���R�G@�      hhC    q�q@���R�t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(jt0  ju0  jv0  jw0  jx0  jy0  jz0  e]�(NNN�(SOOOS��S)�NNe]�(�Cost��410��400��763��776��587��7,234�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   ��P@���R�hhC    �\@���R�G@�      hhC   �5|@���R�t�ah?]�(N�	Table 4.=��12.2��	DA Syster��
1S Sustair��1ment�NehA]�(]�(Nj�0  j�0  j�0  j�0  j�0  Ne]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(NNN�(SOOO=�NNNe]�(NNN�(Suuu:�NNK e]�(�Oosi��(0 (��(Jo��(04��0 | |��040�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(G@`���   G@pS�    G@~�    G@sS�   t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNe]�(�Cost��410��400��763��776��587��7,234�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �'Table 4.3.2.1 - Communication Upgrades �j�  K�j�  (G@jp�   G@nq��   G@z�Q    G@p+�    t�ububh)��}�(hNh]�(G@`���   G@~�p   G@~�    G@��
@   t�ah?]�(�Year��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(�Year��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNe]�(�Cost��707��733��784��811��843��14,863�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �*Table 4.3.2.2 - SCADA Systems Sustainment �j�  K�j�  (G@h�`   G@}���   G@{�    G@~�:�   t�ububh)��}�(hNh]�(hhC   `��;@���R�hhC   �a�A@���R�G@�      hhC    �[�@���R�t�ah?]�hA]�(]�(�reliability a��nd effective��5 of the high vol=��oltage protect�e]�(K �2014��015�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@gj=p   G@�t)    G@{���   G@��)    t�ah?]�(�Year��2014��2015��2016��2017-31�ehA]�(]�(�Year��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNe]�(�Cost��704��363��741��3,932�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �.Table 4.3.2.4 - Station Smart Device Upgrades �j�  K�j�  (G@ho\    G@��    G@{Y
    G@�`<    t�ububh)��}�(hNh]�(hhC    �%@���R�hhC   ��*C@���R�G@�      G@��     t�ah?]�(N�	OeCMc D +��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(NjC1  jD1  jE1  jF1  jG1  jH1  jI1  e]�(N�cndral�NN�(SOOOs��5)�NNe]�(�1��&Kootenay Long Term Facilities Strategy��6,020��10,477�NNNNe]�(�2��Trail Office Lease Purchase�N�10,000�NNNNe]�(�3��Okanagan Long Term Solution��69��75��3,984�NNNe]�(�4��Central Warehousing��1,755�NNNNNe]�(�5�� Advanced Metering Infrastructure��4,501��27,931��6,099�NNNe]�(�6��Information Systems�NNNNNNe]�(N�Infrastructure Sustainment��1,111��1,118��1,207��1,244��1,355��23,967�e]�(�8��"Desktop Infrastructure Sustainment��1,115��1,122��1,239��1,277��1,321��23,368�e]�(�9��Application Enhancements��1,235��1,242��1,296��1,336��1,382��24,447�e]�(�10��Application Sustainment��1,179��1,210��1,276��1,341��1,441��40,220�e]�(�11��!PowerSense DSM Reporting Software��1,032�N�131��135��140��1,205�e]�(�12��Vehicles��2,541��2,574��2,699��2,796��2,906��51,411�e]�(�13��Metering Changes��403��406��212��222��234��8,179�e]�(�14��Telecommunications��121��183��191��196��203��3,595=�e]�(�15��	Buildings��1,362��883��601��278��287��7,273�e]�(�18��Furniture and Fixtures��121��122��508��105��108��2,071�e]�(�19��Tools and Equipment��528��457��477��491��508��8,236�e]�(�20��Total General Plant��23,093��57,800��19,920��9,423��9,885��193,974�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  Nubh)��}�(hNh]�(G@J��I$�G@R��    G@�}Ǟy�G@��f`   t�ah?]�(�(1 Table 5.0 - General Plant Expenditures�j  j  j  j  j  j  j  ehA]�(]�(�(1 Table 5.0 - General Plant Expenditures�NNNNNNNe]�(j  �General Plant��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �&Kootenay Long Term Facilities Strategy��6,020��10,477�j�  j�  j�  j�  e]�(j�  �Trail Office Lease Purchase�j  �10,000�j�  j�  j�  j�  e]�(j�  �Okanagan Long Term Solution��69��75��3,984�j�  j�  j�  e]�(j�  �Central Warehousing��1,755�j�  j�  j�  j�  j�  e]�(j�  � Advanced Metering Infrastructure��4,501��27,931��6,099�j�  j�  j�  e]�(j�  �Information Systems�j  j  j  j  j  j  e]�(j�  �Infrastructure Sustainment��1,111��1,118��1,207��1,244��1,355��23,967�e]�(j�  �"Desktop Infrastructure Sustainment��1,115��1,122��1,239��1,277��1,321��23,368�e]�(j�  �Application Enhancements��1,235��1,242��1,296��1,336��1,382��24,447�e]�(�10��Application Sustainment��1,179��1,210��1,276��1,341��1,441��40,220�e]�(�11��!PowerSense DSM Reporting Software��1,032�j�  �131��135��140��1,205�e]�(�12��Vehicles��2,541��2,574��2,699��2,796��2,906��51,411�e]�(�13��Metering Changes��403��406��212��222��234��8,179�e]�(�14��Telecommunications��121��183��191��196��203��3,595�e]�(�15��	Buildings��1,362��883��601��278��287��7,273�e]�(�18��Furniture and Fixtures��121��122��508��105��108��2,071�e]�(�19��Tools and Equipment��528��457��477��491��508��8,236�e]�(�20��Total General Plant��23,093��57,800��19,920��9,423��9,885��193,974�e]�(X�  2 5.1 Kootenay Long Term Facilities Strategy
3 This project is prompted by the aging and inadequate sizing of current facilities at FortisBC’s
4 South Slocan, Castlegar and System Control Centre. A long term space strategy for these
5 sites is being developed to deal with the following key drivers:
6 • Condition assessment of the South Slocan facilities has identified numerous safety,
7 environmental, structural issues that need to be addressed;
8 • The System Control Centre site is currently undersized and using a portable trailer.
9 NERC and BC Mandatory Reliability Standards are under review and it is believed a
10 number of site upgrades will be necessary;
11 • The Castlegar operations site is inadequately sized and the current property
12 entrance/exit to the road is difficult and unsafe;
13 • Opportunity for synergies in staffing, crew dispatching and shop requirements;�NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   @�J @���R�hhC   �IFQ@���R�G@�      G@��     t�ah?]�hA]�(]�(�4�K e]�(�5�K e]�(�6�K e]�(�7�K e]�(�8�K e]�(�9�K e]�(�10�K e]�(�11�K e]�(�12�K e]�(�13�K e]�(�14�K e]�(�15�K e]�(�16�K e]�(�17�K e]�(�18�K e]�(�19�K e]�(�20�K e]�(�21�K e]�(�LZ�K e]�(�23�K e]�(�24�K e]�(�Zo�K e]�(�26�K e]�(�2i�K e]�(�2o�K e]�(�29�K e]�(�30�K e]�(�31�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC    �y3@���R�hhC   `�R@���R�G@�      hhC   ��o@���R�t�ah?]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�ehA]�(]�(j�2  j�2  j�2  j�2  j�2  j�2  j�2  j�2  j�2  j�2  j�2  j�2  e]�(NNNNNN�(Sooo��s)�NNNNe]�(�Cost��6,655��4,543��4,768��4,309��4,682��5,671��4,692��5,150��5,334��5,638��113,208�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(G@H��|   G@R��    G@�&�@   G@��f`   t�ah?]�(�!1 Table 5.6 - Information Systems�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�!1 Table 5.6 - Information Systems�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��6,655��4,543��4,768��4,309��4,682��5,671��4,692��5,150��5,334��5,638��113,208�e]�(XG	  2 5.6.1 INFRASTRUCTURE SUSTAINMENT
3 The infrastructure sustainment project includes replacing out-dated hardware and software
4 (operating systems and related server software) in the primary and backup data centres and
5 supporting infrastructure (switches and routers that tie the Wide Area Network together).
6 There is approximately $3.2 million worth of hardware and software associated with the
7 FortisBC’s Information System infrastructure. The life expectancy of the hardware
8 infrastructure components is a maximum of five years, based on industry standards and
9 manufacturers’ support. Operating systems are typically upgraded every two years to
10 maintain vendor support. The budget is developed based on the replacement of the oldest
11 equipment (five year maximum life expectancy), failed equipment and minimum software
12 upgrades to maintain manufacturer support. This strategy of asset management avoids the
13 complete replacement of all equipment every five years at one time, as well as the work
14 disruption that would result.
15 Equipment and software designated for upgrade typically include servers at end of life, disk
16 drives that have passed maximum life expectancy (over 20 terabytes of disk space in each
17 data centre), networking infrastructure replacements (failed switches, routers and hubs) and
18 operating system and database upgrades.
19 5.6.2 DESKTOP INFRASTRUCTURE SUSTAINMENT
20 Desktop Infrastructure Sustainment includes Microsoft Windows operating system, Microsoft
21 Office Suite and other job specific hardware and software upgrades for FortisBC’s personal
22 computer (PC) environment. It is a phased approach to keeping approximately 670 PCs
23 current and supportable, rather than replacing all PC equipment and software every five
24 years. The life expectancy of the desktop hardware is a maximum of five years based on
25 industry standards and manufacturers’ support. The phased replacement strategy avoids
26 the resourcing and disruption issues that occur with complete replacement of all personal
27 computer equipment every five years. The total value of FortisBC’s desktop hardware and
28 related peripherals is approximately $3 million. The Desktop Infrastructure Sustainment
29 budget is developed based on the replacement of the oldest (maximum five year life
30 expectancy) and failed equipment.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(K hhC   �A�^@���R�G@�      G@��     t�ah?]�(�4��	Table 5.6��ormation��Systems��Projects�NNK ehA]�(]�(j3  j3  j 3  j!3  j"3  NNK e]�(NN�2012��2013��2014��2015��2016��2017-31�e]�(N�monialion Dysieinis�NN�(Sq��OOs)�NK e]�(�1��Infrastructure Sustainment��1,111��1,118��1,207��1,244��1,355�K e]�(�2��"Desktop Infrastructure Sustainment��1,115��1,122��1,239��1,277��1,321��23,�e]�(�3��Application Enhancements��1,235��1,242��1,296��1,336��1,382�K e]�(�4��Application Sustainment��1,179��1,210��1,276��1,341��1,441�K e]�(�5��!PowerSense DSM Reporting Software��1,032�N�131��135��140�K e]�(�6��Information Systems Total��5,672��4,692��5,150��5,334��5,638�K e]�(�5�NNNNNNK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(G@PW��q�G@R��    G@��UGq�G@��f`   t�ah?]�(Xg  1 visibility of vehicle locations in relation to customers and improved restoration information by 2 relating customers to the electrical network. 3 Other enhancements will be undertaken on a priority, benefit and resource availability basis. 4 These enhancements can be driven by legislative, regulatory or business processes. 5 5.6.4 APPLICATION SUSTAINMENT 6 This project will fund the annual sustainment requirements for all FortisBC applications 7 including CIS, SAP, AM/FM and all other applications used at FortisBC. Annual upgrades 8 maintain support and avoid potential productivity or reliability issues, as well as making new 9 functionality and features available that the vendors have developed through continued 0 investment in their products. 1 5.6.5 POWERSENSE DSM REPORTING SOFTWARE 2 This project is to implement software to be used by the PowerSense department to track, 3 calculate savings and report on DSM projects from inception to completion. Due to the 4 expanding and increasing number of programs and DSM projects, this software is required 5 to capture the appropriate customer transaction information, improve internal workflow 6 processes to provide better customer service, advance monitoring and evaluation, and 7 ensure optimal expenditures. This software will track interactions with each customer from 8 project initiation to completion and provide robust reporting capabilities. FortisBC does not 9 currently have software that is capable of meeting the requirements of the expanded DSM 0 program. The solution will be selected based on its ability to meet requirements at the most 1 reasonable cost, while aligning with FortisBC application standards. The DSM Tracking and 2 Reporting Software project is the only sizeable software implementation project, outside of 3 AMI, that is planned in the 2012-13 timeframe. 4 Table 5.6 - Information Systems Projects�j  j  j  j  j  j  j  ehA]�(]�(Xg  1 visibility of vehicle locations in relation to customers and improved restoration information by
2 relating customers to the electrical network.
3 Other enhancements will be undertaken on a priority, benefit and resource availability basis.
4 These enhancements can be driven by legislative, regulatory or business processes.
5 5.6.4 APPLICATION SUSTAINMENT
6 This project will fund the annual sustainment requirements for all FortisBC applications
7 including CIS, SAP, AM/FM and all other applications used at FortisBC. Annual upgrades
8 maintain support and avoid potential productivity or reliability issues, as well as making new
9 functionality and features available that the vendors have developed through continued
0 investment in their products.
1 5.6.5 POWERSENSE DSM REPORTING SOFTWARE
2 This project is to implement software to be used by the PowerSense department to track,
3 calculate savings and report on DSM projects from inception to completion. Due to the
4 expanding and increasing number of programs and DSM projects, this software is required
5 to capture the appropriate customer transaction information, improve internal workflow
6 processes to provide better customer service, advance monitoring and evaluation, and
7 ensure optimal expenditures. This software will track interactions with each customer from
8 project initiation to completion and provide robust reporting capabilities. FortisBC does not
9 currently have software that is capable of meeting the requirements of the expanded DSM
0 program. The solution will be selected based on its ability to meet requirements at the most
1 reasonable cost, while aligning with FortisBC application standards. The DSM Tracking and
2 Reporting Software project is the only sizeable software implementation project, outside of
3 AMI, that is planned in the 2012-13 timeframe.
4 Table 5.6 - Information Systems Projects�NNNNNNNe]�(j  �Information Systems��2012��2013��2014��2015��2016��2017-31�e]�(NN�($000s)�NNNNNe]�(ja
  �Infrastructure Sustainment��1,111��1,118��1,207��1,244��1,355��23,967�e]�(j�  �"Desktop Infrastructure Sustainment��1,115��1,122��1,239��1,277��1,321��23,368�e]�(j�  �Application Enhancements��1,235��1,242��1,296��1,336��1,382��24,447�e]�(j�  �Application Sustainment��1,179��1,210��1,276��1,341��1,441��40,220�e]�(j�  �!PowerSense DSM Reporting Software��1,032�j�  �131��135��140��1,205�e]�(j�  �Information Systems Total��5,672��4,692��5,150��5,334��5,638��113,208�e]�(j�  NNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   @�F@���R�hhC    ��V@���R�G@�      hhC    �@���R�t�ah?]�(�Description�K ehA]�(]�(j�3  K e]�(�3/4 Tons and Smaller�K e]�(�#Service Vehicles (3/4 and One Tons)��160,000 km (Gasoline_�e]�(�2Line Trucks (Digger or Aerial) 2 and 4 Wheel Drive��10 years�e]�(�Trailers��20 years�e]�(�*Specialty and Small Horsepower (Forklifts,��Individual Review�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �5.7Vehicles�j�  K�j�  (G@]VÀ   G@S�w    G@h9��   G@W�f    t�ububh)��}�(hNh]�(G@Y��   G@pB��   G@�tz�   G@z�30   t�ah?]�(�Description��Trigger�ehA]�(]�(�Description��Trigger�e]�(�Passenger Vehicles��)160,000 km (Gasoline)
200,000 km (Diesel)�e]�(�3/4 Tons and Smaller��)160,000 km (Gasoline)
200,000 km (Diesel)�e]�(�7Service Vehicles (3/4 and One Tons)
2 and 4 Wheel Drive��)160,000 km (Gasoline)
200,000 km (Diesel)�e]�(�2Line Trucks (Digger or Aerial) 2 and 4 Wheel Drive��10 years / 200,000 km�e]�(�Trailers��20 years�e]�(�=Specialty and Small Horsepower (Forklifts,
Snowmobiles, ATVs)��Individual Review�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �-Table 5.7 (a) - Replacement Criteria Trigger �j�  K�j�  (G@iY��   G@nRʀ   �      G@z�J`   G@p�    t�ububh)��}�(hNh]�(hhC   �K�A@���R�hhC   ���Q@���R�G@�      hhC   ���t@���R�t�ah?]�(�11�NNN�Table��2 5.8��letering��Changes��5�NNNehA]�(]�(j4  NNNj4  j4  j4  j4  j4  NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOO��S)�NNNNe]�(�Cost��481��115��136��181��472��403��406��212��222��234��8,179�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   �o�<@���R�hhC   `d�F@���R�G@�      hhC   @��|@���R�t�ah?]�(�20��Centre.�NNNNNNNNNNehA]�(]�(j;4  j<4  NNNNNNNNNNe]�(�21�NNN�Table��	 5.7 - Te��lecomm��	unication��IS�NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SoOO��S)�NNNNe]�(�Cost��221��258��86��54��368��121��183��191��196��203��3,595=�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �	FBC IOS C�j�  K�j�  (G@V�     G@D"4    G@c�@   G@K��   t�ububh)��}�(hNh]�(G@H��!���G@R��    G@�&��fffG@��f`   t�ah?]�(X:  1 emission service truck. Also, in concert with FortisBC Energy Inc., the Company has begun 2 exploring opportunities for using natural gas vehicles. The Company will continue to monitor 3 and evaluate the progress of all new green vehicle technologies as part of its future 4 purchases. 5 Table 5.7 (b) - Vehicles�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X:  1 emission service truck. Also, in concert with FortisBC Energy Inc., the Company has begun
2 exploring opportunities for using natural gas vehicles. The Company will continue to monitor
3 and evaluate the progress of all new green vehicle technologies as part of its future
4 purchases.
5 Table 5.7 (b) - Vehicles�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��4,387��1,277��1,947��1,225��2,738��2,541��2,574��2,699��2,796��2,906��51,411�e]�(X�  6 5.8 Metering Changes
7 This project involves the purchase of new metering infrastructure driven by customer growth,
8 as well as replacement for metering equipment that fails during the metering compliance or
9 the meter re-test program. Metering infrastructure includes meters, current transformers,
10 potential transformers and ancillary equipment.
11 Table 5.8 - Metering Changes�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��481��115��136��181��472��403��406��212��222��234��8,179�e]�(X�  12 5.9 Telecommunications
13 The telecommunications capital budget is used to purchase new or replacement
14 communications equipment. This equipment includes landline equipment, VHF field
15 communications equipment, microwave substation controls and the installation of isolation
16 equipment when installing Telus lines into substations. These installations will provide voice
17 as well as data and control communications as required.
18 The communications budget also covers upgrades and/or replacement of equipment that is
19 used for remote control and operation of field devices from the FortisBC System Control
20 Centre.
21 Table 5.7 - Telecommunications�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��221��258��86��54��368��121��183��191��196��203��3,595�e]�(j  NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   �u8@���R�hhC   �	eY@���R�G@�      hhC    �o}@���R�t�ah?]�(�19�NNNN�Table 5_��10��dings�NNNNehA]�(]�(j�4  NNNNj�4  j�4  j�4  NNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SOOO��S)�NNNK e]�(�Cost��1,820��1,599��1,271��948��1,288��1,362��883��601��278��287��7,273�e]�(�20��5.11��Furni��	iture ane��	1 Fixture��S�NNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �5.10 Buildings �j�  K�j�  (G@]VÀ   G@S�w    G@iHu`   G@W�f    t�ububh)��}�(hNh]�(G@H��|   G@R��    G@�&�@   G@��f`   t�ah?]�(X~  1 5.10 Buildings 2 FortisBC has 15 office/yard sites (ranging in age from 8 to 88 years) throughout the 3 Kootenay, Boundary, Okanagan and Similkameen regions totaling approximately 229,500 4 square feet of office, shop and warehouse space and approximately 51 acres of yard space. 5 Of this, 125,000 square feet is owned and 104,500 square feet is leased. 6 Building audits have been carried out at all sites to identify and prioritize maintenance and 7 upgrade requirements. The audits contain location, age, square footage and description of 8 each building component. After inspection, each component is rated in years for estimated 9 life, effective life and remaining life. 10 All upgrades take into account immediate and long-term requirements that meet FortisBC 11 standards to best serve customers and comply with BC Mandatory Reliability, BC Energy 12 Plan and Public Safety Standards. FortisBC also commits to maintaining facility and yard 13 assets in a responsible manner while providing employees with a worksite that meets 14 FortisBC health, safety and environmental standards. 15 All facilities in the service area are considered in the planning process, including the 16 FortisBC gas division locations. Changes in work processes, staffing requirements and 17 integration of work crews and administrative staff are also recognized during the planning 18 process. 19 Table 5.10 - Buildings�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(X~  1 5.10 Buildings
2 FortisBC has 15 office/yard sites (ranging in age from 8 to 88 years) throughout the
3 Kootenay, Boundary, Okanagan and Similkameen regions totaling approximately 229,500
4 square feet of office, shop and warehouse space and approximately 51 acres of yard space.
5 Of this, 125,000 square feet is owned and 104,500 square feet is leased.
6 Building audits have been carried out at all sites to identify and prioritize maintenance and
7 upgrade requirements. The audits contain location, age, square footage and description of
8 each building component. After inspection, each component is rated in years for estimated
9 life, effective life and remaining life.
10 All upgrades take into account immediate and long-term requirements that meet FortisBC
11 standards to best serve customers and comply with BC Mandatory Reliability, BC Energy
12 Plan and Public Safety Standards. FortisBC also commits to maintaining facility and yard
13 assets in a responsible manner while providing employees with a worksite that meets
14 FortisBC health, safety and environmental standards.
15 All facilities in the service area are considered in the planning process, including the
16 FortisBC gas division locations. Changes in work processes, staffing requirements and
17 integration of work crews and administrative staff are also recognized during the planning
18 process.
19 Table 5.10 - Buildings�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��1,820��1,599��1,271��948��1,288��1,362��883��601��278��287��7,273�e]�(X�  20 5.11 Furniture and Fixtures
21 This project is required for the replacement of deteriorated furniture and the
22 addition/modification of furniture to accommodate changing needs within the organization.
23 FortisBC has completed an inventory of furniture at all sites, categorizing the condition of the
24 furniture into three categories - disposal, poor and good. Using this inventory together with
25 the Company’s Environment Health and Safety Standard 108, (Section 2.2) “Monitoring the
26 Work Environment”, the capital requirements are upgraded each year. Typically chairs are
27 replaced every five years and workstations are reviewed for functionality every 8 to 10
28 years.�NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(hhC   @QV;@���R�hhC   @� O@���R�G@�      hhC   ��s@���R�t�ah?]�(�8�NNN�Table��5.12��ols and��Equipme��nt�NNNehA]�(]�(jF5  NNNjG5  jH5  jI5  jJ5  jK5  NNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(NNNNNN�(SoOO��s)�NNNNe]�(�Cost��936��587��525��507��622��528��457��477��491��508��8,236�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(G@H�6�m�G@R��    G@�&���m�G@��f`   t�ah?]�(�%1 Table 5.11 - Furniture and Fixtures�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�%1 Table 5.11 - Furniture and Fixtures�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��248��237��294��268��182��121��122��508��105��108��2,071�e]�(X  2 5.12 Tools and Equipment
3 This project involves the purchase of tools and equipment necessary to construct, operate,
4 and maintain the generation, transmission, and distribution system. It encompasses all
5 capital expenditures for tools and equipment in excess of $1,000 and includes replacement
6 tools that have reached the end of their service life, as well as additional tools that are more
7 appropriate for the various trades from an ergonomic and/or safety perspective.
8 Table 5.12 - Tools and Equipment�NNNNNNNNNNNe]�(�Year��2007��2008��2009��2010��2011��2012��2013��2014��2015��2016��2017-31�e]�(j  �($000s)�NNNNNNNNNNe]�(�Cost��936��587��525��507��622��528��457��477��491��508��8,236�e]�(j  NNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  K�j�  j�  )��}�(j�  �FBCI. ORTISNC�j�  K�j�  (G@V�     G@D"4    G@d���   G@K��   t�ububh)��}�(hNh]�(G@Rgܰ   G@X�D����G@��	UUUG@{1��UUUt�ah?]�(�North Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�North
Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�SEX T1��2 9,001��2 9,450��2 8,418��2 9,635��3 0,655��3 1,780��3 2,695��3 3,622��3 4,487��4 6,173�e]�(�GLE T2��1 9,412��2 4,520��2 5,819��2 7,343��2 8,195��2 8,594��3 0,091��3 0,944��3 1,686��4 2,308�e]�(�GLE T3��2 7,661��2 8,243��2 9,061��3 0,017��3 0,935��3 1,802��3 2,492��3 3,189��3 3,855��4 2,607�e]�(�HOL T1��3 0,101��2 6,742��2 8,096��2 9,312��3 1,770��3 2,888��3 3,792��3 4,750��3 5,664��4 7,759�e]�(�HOL T3��1 9,431��1 6,400��1 7,213��1 7,885��1 8,655��1 9,312��1 9,816��2 0,378��2 0,910��2 8,010�e]�(�DGB T1��2 0,271��2 8,265��2 9,437��3 0,448��3 1,348��3 2,214��3 3,626��3 4,580��3 5,370��4 7,268�e]�(�DUC T1��8 ,090��6,350��7,185��7,556��7,795��8,050��8,281��8,516��8,751��1 1,713�e]�(�DUC BCH�j�  �3 5,700��3 6,700��3 7,200��3 7,400��3 7,600��3 7,700��3 7,800��3 8,100��4 0,381�e]�(�JOR T1��3 ,157��3,330��3,489��3,636��3,778��3,906��4,018��4,132��4,239��5,676�e]�(�OKM T1��2 3,821��2 3,614��2 5,751��2 7,178��2 7,904��2 8,717��2 9,255��2 9,952��3 0,575��3 8,549�e]�(�OKM T2��1 1,283��1 0,301��1 0,745��1 1,242��1 1,671��1 1,835��1 2,363��1 2,699��1 3,022��1 7,420�e]�(�LEE tert��1 4,921��1 4,921��1 4,790��1 5,414��1 0,947��1 1,316��1 1,642��1 1,972��1 2,281��1 6,446�e]�(�BLK T1��1 2,299��1 2,691��13,349��1 3,974��1 6,255��1 6,633��1 7,155��1 7,661��1 8,128��2 4,260�e]�(�ELL T1��1 4,625��1 4,988��2 0,364��2 1,611��2 2,874��2 4,574��2 4,967��2 5,728��2 6,436��3 5,404�e]�(�BWS T1��1 3,719��1 6,281��1 6,964��1 7,558��1 8,010��1 9,134��1 9,484��2 0,003��2 0,502��2 7,495�e]�(�BEV T1�j�  �1 9,480��2 0,493��2 1,457��2 2,417��2 3,327��2 3,731��2 4,450��2 5,118��3 3,638�e]�(�	REC T1/T2��2 7,953��2 9,307��3 0,341��3 3,933��3 4,839��3 5,372��3 6,037��3 6,565��3 7,042��4 3,331�e]�(�SAU T1��2 3,000��2 5,570��2 6,966��2 8,065��2 8,652��2 8,990��2 9,567��3 0,010��3 0,419��3 5,566�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��298,746��3 66,152��3 85,183��4 03,462��4 14,099��4 26,045��4 36,711��4 46,953��4 56,585��5 84,004�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M
j�  j�  )��}�(j�  �3North Okanagan Distribution Load Forecast (Winter) �j�  M
j�  (G@n޸`   G@Q�z�   G@�#    G@U��   t�ububh)��}�(hNh]�(G@Rg�UUUG@X��UUUG@��	:���G@|��UUUt�ah?]�(�South Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�South
Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�RGA T3��1 6,280��1 6,512��1 6,708��1 6,955��1 7,152��1 7,293��1 7,431��1 7,571��1 7,707��1 9,236�e]�(�OKF T1��9 ,985��1 0,074��1 0,170��1 0,270��1 0,311��1 0,506��1 0,574��1 0,647��1 0,720��1 1,653�e]�(�WEB T1��7 ,181��8,833��9,268��9,341��9,399��9,434��9,531��9,650��9,705��1 0,537�e]�(�AWA T1��7 ,252��7,413��7,521��7,792��7,813��7,824��7,902��7,981��8,057��8,736�e]�(�KAL T1��6 ,600��6,689��6,780��7,261��7,340��8,020��8,085��8,150��8,209��8,920�e]�(�SUM T2��1 8,121��1 8,499��1 8,749��1 8,972��1 9,181��1 9,364��1 9,521��1 9,677��1 9,820��2 1,536�e]�(�WAT T1��1 7,523��1 7,760��1 8,001��1 8,215��1 8,415��1 8,590��1 8,741��1 8,891��1 9,029��2 0,676�e]�(�	WES T1/T2��2 4,262��2 4,683��2 5,017��2 5,315��2 5,593��2 5,837��2 6,046��2 6,255��2 6,446��2 8,736�e]�(�TRC T1��6 ,786��7,261��7,232��7,145��7,014��7,421��7,433��7,446��7,471��8,145�e]�(�PIN T1��7 ,604��7,626��7,730��7,822��7,908��7,983��8,048��8,112��8,171��8,879�e]�(�PIN T2��1 2,741��1 5,055��1 5,211��1 5,355��1 5,553��1 5,727��1 5,841��1 5,959��1 6,074��1 7,472�e]�(�OSO T1��7 ,321��7,420��7,520��7,610��7,694��7,767��7,830��7,751��7,803��8,481�e]�(�OSO T2��9 ,385��5,061��5,129��5,191��5,248��5,298��5,341��5,383��5,422��5,892�e]�(�NKM T1��6 ,758��1 0,690��1 1,321��1 1,943��1 2,495��1 2,668��1 2,768��1 2,867��1 2,957��1 4,079�e]�(�KER T1��1 3,002��1 2,948��1 3,525��1 3,848��1 3,907��1 3,881��1 4,028��1 4,214��1 4,321��1 5,531�e]�(�HED T1��5 ,216��5,521��5,570��5,605��5,628��5,694��5,772��5,807��5,842��6,350�e]�(�OLI T1��9 ,457��8,287��8,239��8,555��8,930��8,902��8,838��8,927��9,049��9,821�e]�(�PRI T4��1 8,657��2 0,608��2 0,835��2 1,019��2 1,172��2 1,506��2 1,659��2 1,814��2 1,963��2 3,875�e]�(�HUT T4/5/6/7��9 ,646��9,777��9,909��1 0,027��1 0,137��1 0,234��1 0,317��1 0,399��1 0,475��1 1,382�e]�(�HUT T8��7 ,000��7,095��7,191��7,276��7,356��7,426��7,487��7,547��7,601��8,260�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��220,777��2 27,811��2 31,627��2 35,514��2 38,246��2 41,375��2 43,191��2 45,048��2 46,840��2 68,198�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �2South Okanagan Distribution Load Forecast (Winter)�j�  Mj�  (G@n�     G@Q�z�   G@�@    G@U��   t�ububh)��}�(hNh]�(G@Rg�����G@X   G@���   G@%����t�ah?]�(�Kootenay��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�Kootenay��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�KAS T1��8 ,815��9,189��9,197��9,399��9,527��9,533��9,581��9,640��9,714��1 0,335�e]�(�COF T3��6 ,276��6,479��6,642��6,690��6,725��6,748��6,807��6,860��6,892��7,336�e]�(�CRA T5��6 ,577��6,767��6,811��6,969��7,015��7,052��7,079��7,128��7,180��7,639�e]�(�CRE T1��1 3,279��1 3,532��1 3,683��1 3,784��1 3,975��1 4,038��1 4,114��1 4,204��1 4,284��1 5,212�e]�(�CRE T2��1 1,879��1 3,108��1 3,305��1 3,426��1 3,492��1 3,601��1 3,689��1 3,779��1 3,851��1 4,747�e]�(�AAL T2��1 4,207��1 4,461��1 3,969��1 3,997��1 4,124��1 4,191��1 4,471��1 4,440��1 4,510��1 5,470�e]�(�VAL T1��4 ,543��4,841��4,890��4,934��4,975��5,011��5,042��5,072��5,100��5,431�e]�(�VAL T2��4 ,581��4,627��4,674��4,716��4,755��4,790��4,819��4,848��4,875��5,191�e]�(�PAS T1��3 ,547��3,520��3,528��3,539��3,696��3,657��3,669��3,691��3,718��3,962�e]�(�PLA T1��1 3,533��1 3,723��1 3,830��1 3,960��1 4,066��1 4,193��1 4,270��1 4,352��1 4,432��1 5,369�e]�(�TAR T1��3 ,000��2,959��2,950��2,940��2,928��2,955��2,947��2,944��2,943��2,945�e]�(�COT T1��594��663��670��676��682��687��691��695��699��744�e]�(�SAL T1��7 ,405��7,559��7,648��7,657��7,757��7,835��7,865��7,911��7,950��8,472�e]�(�HER T1��1 ,832��1,857��1,876��1,893��1,909��1,922��1,934��1,946��1,957��2,084�e]�(�FRU T1��6 ,000��7,163��7,075��7,052��6,988��6,892��7,195��7,185��7,194��7,660�e]�(�YMR T1��1 ,387��1,482��1,497��1,511��1,523��1,534��1,544��1,553��1,562��1,663�e]�(�CAS T1��1 2,045��1 2,149��1 2,106��1 2,148��1 2,487��1 2,522��1 2,560��1 2,617��1 2,699��1 3,534�e]�(�BLU T1��7 ,940��8,083��8,183��8,246��8,303��8,374��8,424��8,476��8,520��9,073�e]�(�OOT T1��8 ,307��9,442��9,511��9,564��9,694��9,782��9,816��9,871��9,927��1 0,576�e]�(�BEP T1��8 ,440��1 0,886��9,475��9,590��9,664��8,823��8,846��8,910��1 0,078��1 0,729�e]�(�GLM T1��1 1,216��1 1,818��1 4,081��1 4,163��1 4,240��1 4,328��1 4,429��1 4,540��1 4,607��1 5,552�e]�(�STC T1��7 ,700��8,458��8,457��8,414��8,690��8,621��8,721��8,756��8,801��9,381�e]�(�CSC T1��1 0,330��1 0,849��1 0,947��1 0,950��1 1,005��1 1,160��1 1,231��1 1,285��1 1,333��1 2,078�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��173,433��1 83,614��1 85,007��1 86,217��1 88,218��1 88,249��1 89,743��1 90,705��1 92,826��2 05,181�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �,Kootenay Distribution Load Forecast (Winter)�j�  Mj�  (G@p�z�   G@Q�z�   G@�m    G@U��   t�ububh)��}�(hNh]�(G@Rg�j���G@X�I    G@���   G@l�*���t�ah?]�(�Boundary��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�Boundary��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�CHR T1��4 ,884��4,968��5,015��5,057��5,097��5,131��5,160��5,189��5,216��5,531�e]�(�RUC T1��6 ,699��6,806��6,920��7,130��7,125��7,101��7,164��7,223��7,273��7,696�e]�(�RUC T2��9 ,140��8,877��8,939��9,191��9,294��9,313��9,314��9,386��9,462��1 0,019�e]�(�GFT T3��9 ,964��1 0,883��1 0,986��1 1,079��1 1,165��1 1,240��1 1,304��1 1,368��1 1,426��1 2,116�e]�(�	KET T1/T2��1 1,354��1 1,660��1 1,734��1 1,787��1 1,887��1 2,041��1 2,071��1 2,131��1 2,191��1 2,935�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��4 2,041��4 3,194��4 3,594��4 4,244��4 4,567��4 4,825��4 5,014��4 5,297��4 5,568��4 8,297�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �-Boundary Distribution Load Forecast (Winter) �j�  Mj�  (G@p���   G@Q�z�   G@��?`   G@U��   t�ububh)��}�(hNh]�(G@Rg��UUUG@X��UUUUG@��]    G@z��5UUUt�ah?]�(�North Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�North
Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�SEX T1��2 5,810��2 7,667��2 6,209��2 7,334��2 8,579��2 9,459��3 0,521��3 1,579��3 2,490��4 6,064�e]�(�GLE T2��2 2,820��2 5,180��2 6,595��2 7,727��2 9,098��3 0,298��3 1,302��3 2,388��3 3,310��4 7,210�e]�(�GLE T3��2 6,458��2 8,614��2 9,640��3 0,772��3 1,423��3 2,219��3 2,983��3 3,817��3 4,550��4 4,731�e]�(�HOL T1��2 0,953��2 1,745��2 2,918��2 4,259��2 6,322��2 7,247��2 8,137��2 9,113��3 0,030��4 2,561�e]�(�HOL T3��2 2,470��1 6,819��1 7,687��1 8,650��1 9,367��2 0,071��2 0,664��2 1,380��2 2,049��3 1,271�e]�(�DGB T1��2 1,734��2 4,098��2 5,253��2 6,317��2 7,181��2 8,450��2 9,312��3 0,328��3 1,213��4 4,244�e]�(�DUC T1�j�  �5,079��5,840��6,029��6,428��6,576��6,784��7,019��7,238��1 0,266�e]�(�DUC BCH�j�  �2 8,553��2 9,827��2 9,684��3 0,843��3 0,717��3 0,886��3 1,158��3 1,514��3 5,393�e]�(�JOR T1��1,706��1,787��1,881��1,971��2,050��2,123��2,190��2,266��2,335��3,311�e]�(�OKM T1��1 9,617��2 1,473��2 3,301��2 4,507��2 4,959��2 5,410��2 6,063��2 6,644��2 7,164��3 4,396�e]�(�OKM T2��1 2,474��9,287��9,733��1 0,356��1 0,743��1 1,094��1 1,428��1 1,834��1 2,221��1 7,306�e]�(�LEE tert��1 2,665��1 3,340��1 3,197��1 3,830��9,312��9,644��9,949��1 0,294��1 0,611��1 5,042�e]�(�BLK T1��1 1,289��1 0,909��11,373��1 1,779��1 3,552��1 4,718��1 4,844��1 5,325��1 5,785��2 2,439�e]�(�ELL T1��1 8,915��1 4,121��1 9,371��2 0,592��2 1,709��2 3,241��2 3,917��2 4,757��2 5,526��3 6,188�e]�(�BWS T1��2,578��3,884��4,111��4,335��4,543��4,676��4,808��4,984��5,144��7,289�e]�(�BEV T1�j�  �1 9,480��2 0,587��2 1,675��2 2,663��2 3,625��2 4,101��2 4,983��2 5,790��3 6,564�e]�(�	REC T1/T2��2 0,821��2 5,868��2 6,570��2 9,444��2 9,430��2 9,136��3 0,138��3 0,281��3 0,347��3 2,679�e]�(�SAU T1��2 1,597��2 4,265��2 5,820��2 6,411��2 6,351��2 6,209��2 6,776��2 7,008��2 7,112��2 9,180�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��2 61,907��3 22,168��3 39,910��3 55,674��3 64,553��3 74,915��3 84,803��3 95,157��4 04,430��5 36,134�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �3North Okanagan Distribution Load Forecast (Summer) �j�  Mj�  (G@n1�   G@Q�z�   G@�M?`   G@U��   t�ububh)��}�(hNh]�(G@Rg��UUUG@X��   G@��J*���G@|h}    t�ah?]�(�South Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�South
Okanagan��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�RGA T3��1 3,296��1 6,971��1 7,145��1 7,306��1 7,564��1 7,668��1 7,883��1 8,036��1 8,184��2 0,062�e]�(�OKF T1��7,371��8,405��8,557��8,594��8,847��8,662��8,888��8,970��9,038��9,964�e]�(�WEB T1��8,878��8,560��8,882��8,999��9,096��9,235��9,238��9,362��9,442��1 0,413�e]�(�AWA T1��4,201��4,612��4,712��4,763��4,803��4,882��4,905��4,957��4,997��5,514�e]�(�KAL T1��3,859��4,999��5,072��5,540��5,601��6,266��6,321��6,382��6,437��7,100�e]�(�SUM T2��1 2,541��1 3,614��1 3,814��1 3,998��1 4,153��1 4,294��1 4,419��1 4,558��1 4,683��1 6,195�e]�(�WAT T1��1 6,732��1 6,978��1 7,227��1 7,457��1 7,650��1 7,825��1 7,982��1 8,155��1 8,310��2 0,196�e]�(�	WES T1/T2��1 9,146��1 9,810��2 0,101��2 0,369��2 0,595��2 0,799��2 0,982��2 1,184��2 1,365��2 3,566�e]�(�TRC T1��5,623��6,398��6,471��6,370��6,564��6,594��6,686��6,733��6,773��7,486�e]�(�PIN T1��6,974��7,914��8,030��8,137��8,227��8,309��8,382��8,463��8,535��9,414�e]�(�PIN T2��9,824��1 2,974��1 3,097��1 3,292��1 3,446��1 3,625��1 3,709��1 3,835��1 3,960��1 5,400�e]�(�OSO T1��7,840��7,953��8,070��7,931��8,031��8,115��8,423��8,272��8,339��9,203�e]�(�OSO T2��7,870��6,820��6,920��7,012��7,090��7,160��7,223��7,293��7,355��8,113�e]�(�NKM T1��9,247��1 4,196��1 4,899��1 5,593��1 6,228��1 6,350��1 6,545��1 6,700��1 6,838��1 8,569�e]�(�KER T1��1 0,537��1 1,803��1 2,003��1 2,298��1 2,362��1 2,565��1 2,594��1 2,734��1 2,860��1 4,176�e]�(�HED T1��1,707��2,221��2,238��2,223��2,193��2,248��2,296��2,307��2,316��2,559�e]�(�OLI T1��7,006��7,322��7,429��7,528��7,611��7,688��7,755��7,829��7,896��8,710�e]�(�PRI T4��1 2,648��1 8,071��1 8,192��1 8,259��1 8,249��1 8,512��1 8,841��1 8,962��1 9,082��2 1,062�e]�(�HUT T4/5/6/7��9,419��9,557��9,698��9,827��9,936��1 0,034��1 0,122��1 0,220��1 0,308��1 1,369�e]�(�HUT T8��7,108��7,212��7,318��7,416��7,498��7,572��7,639��7,713��7,779��8,580�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��1 81,827��2 06,391��2 09,875��2 12,913��2 15,744��2 18,403��2 20,834��2 22,666��2 24,498��2 47,651�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �2South Okanagan Distribution Load Forecast (Summer)�j�  Mj�  (G@n3@   G@Q�z�   G@�;|�   G@U��   t�ububh)��}�(hNh]�(G@Rg�    G@X�����G@��J�UUUG@~�g:���t�ah?]�(�Kootenay��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�Kootenay��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�KAS T1��5,090��4,694��4,579��4,533��4,696��4,567��4,668��4,659��4,672��4,849�e]�(�COF T3��4,500��4,907��4,911��4,951��4,947��5,006��5,002��5,018��5,036��5,223�e]�(�CRA T5��2,700��3,677��3,636��3,580��3,708��3,770��3,717��3,722��3,737��3,886�e]�(�CRE T1��7,800��8,491��8,680��8,863��8,877��8,910��8,866��8,936��8,982��9,301�e]�(�CRE T2��6,500��9,227��9,226��9,412��9,404��9,435��9,450��9,489��9,535��9,877�e]�(�AAL T2��8,600��8,926��8,581��8,148��8,261��8,425��8,569��8,489��8,464��8,825�e]�(�VAL T1��2,000��2,065��2,076��2,086��2,095��2,103��2,109��2,117��2,124��2,202�e]�(�VAL T2��5,349��5,402��5,432��5,459��5,481��5,501��5,519��5,538��5,556��5,761�e]�(�PAS T1��2,050��2,631��2,629��2,501��2,649��2,798��2,672��2,679��2,687��2,806�e]�(�PLA T1��6,000��6,256��6,205��6,151��6,184��6,201��6,272��6,271��6,280��6,519�e]�(�TAR T1��3,360��3,000��3,000��3,000��3,000��3,000��3,000��3,000��3,000��3,000�e]�(�COT T1��165��262��263��264��266��267��267��268��269��279�e]�(�SAL T1��4,800��5,251��5,208��5,194��5,219��5,249��5,285��5,289��5,301��5,502�e]�(�HER T1��700��1,077��1,082��1,088��1,092��1,096��1,100��1,104��1,107��1,148�e]�(�FRU T1��4,600��4,292��4,261��4,131��4,187��4,235��4,271��4,263��4,261��4,432�e]�(�YMR T1��750��763��767��770��774��776��779��782��784��813�e]�(�CAS T1��8,900��8,870��8,807��8,927��9,131��9,103��9,072��9,107��9,161��9,498�e]�(�BLU T1��5,960��5,683��5,707��5,736��5,751��5,765��5,795��5,814��5,831��6,046�e]�(�OOT T1��4,980��5,042��5,058��5,070��5,075��5,096��5,128��5,141��5,154��5,345�e]�(�BEP T1��7,900��1 0,137��8,560��8,522��8,586��7,735��7,769��7,780��8,885��9,222�e]�(�GLM T1��1 0,000��1 0,532��1 2,676��1 2,947��1 2,995��1 2,906��1 2,910��1 3,029��1 3,091��1 3,547�e]�(�STC T1��6,061��5,275��5,273��5,262��5,637��5,562��5,465��5,499��5,541��5,753�e]�(�CSC T1��4,400��5,031��4,991��4,933��4,985��5,063��5,059��5,061��5,072��5,269�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��1 13,165��1 21,490��1 21,609��1 21,531��1 23,000��1 22,567��1 22,747��1 23,057��1 24,530��1 29,104�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �,Kootenay Distribution Load Forecast (Summer)�j�  Mj�  (G@pP     G@Q�z�   G@��I�   G@U��   t�ububh)��}�(hNh]�(G@Rg�j���G@X���   G@��\ʪ��G@lx��   t�ah?]�(�Boundary��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�ehA]�(]�(�Boundary��	Base Year��Year 0��Year 1��Year 2��Year 3��Year 4��Year 5��Year 6��Year 7��Year 20�e]�(j  �2010��2011��2012��2013��2014��2015��2016��2017��2018��2031�e]�(�CHR T1��4,366��4,444��4,517��4,586��4,643��4,695��4,742��4,794��4,840��5,407�e]�(�RUC T1��6,600��6,502��6,664��6,842��6,865��7,013��7,020��7,113��7,191��8,028�e]�(�RUC T2��8,380��8,411��8,662��8,806��8,857��8,967��9,055��9,170��9,255��1 0,335�e]�(�GFT T3��6,600��7,154��7,272��7,382��7,475��7,558��7,634��7,717��7,792��8,705�e]�(�	KET T1/T2��3,400��5,889��5,738��5,523��5,225��5,273��5,736��5,688��5,662��6,343�e]�(j  j  j  j  j  j  j  j  j  j  j  e]�(�Totals��2 9,346��3 2,399��3 2,854��3 3,138��3 3,065��3 3,507��3 4,187��3 4,482��3 4,739��3 8,819�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �-Boundary Distribution Load Forecast (Summer) �j�  Mj�  (G@pDz�   G@Q�z�   G@��r�   G@U��   t�ububh)��}�(h]�(j^  j$	  j^  eh]�(G@h=4��G@X1�    G@z!ez.��G@���   t�ah?]�(�Year��Summer Peak��Winter Peak�ehA]�(]�(�Year��Summer
Peak��Winter Peak�e]�(j  �(MW)�Ne]�(�2011��652��843�e]�(�2012��661��856�e]�(�2013��669��869�e]�(�2014��678��880�e]�(�2015��685��890�e]�(�2016��688��895�e]�(�2017��692��902�e]�(�2018��697��910�e]�(�2019��703��918�e]�(�2020��708��926�e]�(�2021��714��935�e]�(�2022��720��943�e]�(�2023��726��951�e]�(�2024��732��960�e]�(�2025��738��969�e]�(�2026��744��977�e]�(�2027��750��986�e]�(�2028��756��995�e]�(�2029��763��1004�e]�(�2030��769��1013�e]�(�2031��775��1022�e]�(�2032��782��1031�e]�(�2033��788��1041�e]�(�2034��795��1051�e]�(�2035��802��1061�e]�(�2036��808��1071�e]�(�2037��815��1081�e]�(�2038��823��1092�e]�(�2039��830��1103�e]�(�2040��837��1113�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �4Summer and Winter “1-in-20” Peak Load Forecasts �j�  Mj�  (G@e���   G@Q�x    G@{�T@   G@U^g    t�ububh)��}�(h]�(j$	  j$	  j$	  eh]�(hhC    ��8@���R�hhC   �#�[@���R�G@�      hhC   @pp�@���R�t�ah?]�(�Data��Data Source��Date Extracted�ehA]�(]�(jP>  jQ>  jR>  e]�(�Future Land Use: future zoning��"City of Kelowna Official Community��	June 2010�e]�(�Land use base map: see��Satellite imagery from Integral��2010�e]�(�Current zoning��City of Kelowna��	June 2010�e]�(�Transportation��ESRI��2010�e]�(�ALR Land��BC Land Commission��	July 2010�e]�(�Raw�� FortisBC and Primary Engineering��August 2010�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  Nubh)��}�(hNh]�(hhC    ʸ3@���R�hhC   ���T@���R�G@�      hhC   �`�y@���R�t�ah?]�(�	From Unit�K ehA]�(]�(j|>  K e]�(�Acre�K e]�(�Mile�K e]�(�Mile�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  Nubh)��}�(hNh]�(G@I��@   G@jW��   G@s|��   G@r>=p   t�ah?]��"From Unit To Unit Conversion Ratio�ahA]�(]��"From Unit To Unit Conversion Ratio�a]��Acre feet^2 43560 = 208.71^2�a]��Acre km^2 0.00404685642�a]��Acre mile 0.0015625�a]��Mile km 1.609344�a]��Mile feet 5280�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  j�  )��}�(j�  �Table 1 -Units �j�  M j�  (G@R�    G@i��   G@`��   G@j?��   t�ububh)��}�(hNh]�(G@I��@   G@t��   G@�r�P   G@}���   t�ah?]��$Data Type Data Source Date Extracted�ahA]�(]��$Data Type Data Source Date Extracted�a]��wFuture Land Use: future zoning City of Kelowna Official Community June 2010
classes, parks, agriculture, etc. Plan 2020�a]��NLand use base map: see Satellite imagery from Integral 2010
Appendix Analytics�a]��(Current zoning City of Kelowna June 2010�a]��Transportation ESRI 2010�a]��%ALR Land BC Land Commission July 2010�a]��uRaw developments FortisBC and Primary Engineering August 2010
distribution planners and designers
in the Kelowna area�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  j�  )��}�(j�  �Table 2 – Data Sources �j�  M j�  (G@R�    G@t!��   G@d!�    G@t���   t�ububh)��}�(hNh]�(hhC   ��F=@���R�hhC   @�iS@���R�G@�      hhC   �>�@���R�t�ah?]�hA]�(]�(�&Table 3 - Weather normalized 4pm summe�K e]�(�Class�K e]�(�
Industrial�K e]�(�
Commercial�K e]�(�LL_Oacldacd ino��L Jao #�e]�(�Tow Recidential��4 2546�e]�(�LuVv��TJJTo�e]�(NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M(j�  Nubh)��}�(hNh]�(G@I��@   G@}�(�   G@�r�P   G@��G�   t�ah?]��Class 2010 Peak Load (kVA)�ahA]�(]��Class 2010 Peak Load (kVA)�a]��Industrial 22.81�a]��Commercial 27.48�a]��High Residential 16.83�a]��Medium Residential 10.88�a]��Low Residential 4.3546�a]��Agriculture 0.58�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M(j�  j�  )��}�(j�  �OTable 3 - Weather normalized 4pm summer coincident peak load by class per acre �j�  M(j�  (G@R�    G@}L)�   G@wނ    G@}�)�   t�ububh)��}�(hNh]�(hhC   �C@7@���R�hhC    ��_@���R�G@�      G@��     t�ah?]�(�Table 2��ecast Growth��h Rates 2010 to 2��030�NNNNehA]�(]�(j�>  j ?  j?  j?  NNNNe]�(N�2010��2011��2013��2016��2020��2025�K e]�(�
Commercial��1.5%��1.5%��2.8%��4%��5%��5.6%��4.8%�e]�(�
Industrial��1.5%��1.5%��2.8%��4%��5%��5.6%��4.8%�e]�(�Single��1.5%��1.5%��2.8%��4%��5%��5.6%��4.8%�e]�(�Single��1.5%��1.5%��2.8%��4%��5%��5.6%��4.8%�e]�(�Single��1.5%��1.5%��2.8%��4%��5%��5.6%�K e]�(�Based on availa��able data a��nd for simplici��growth rate��2s for all classe��S were assume��dto be the sar�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M+j�  Nubh)��}�(hNh]�(G@I��@   G@Q�   G@�r�P   G@���   t�ah?]��"2010 2011 2013 2016 2020 2025 2030�ahA]�(]��"2010 2011 2013 2016 2020 2025 2030�a]��)Commercial 1.5% 1.5% 2.8% 4% 5% 5.6% 4.8%�a]��)Industrial 1.5% 1.5% 2.8% 4% 5% 5.6% 4.8%�a]��<Single 1.5% 1.5% 2.8% 4% 5% 5.6% 4.8%
Family Low
Residential�a]��?Single 1.5% 1.5% 2.8% 4% 5% 5.6% 4.8%
Family
Medium
Residential�a]��=Single 1.5% 1.5% 2.8% 4% 5% 5.6% 4.8%
Family High
Residential�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M+j�  j�  )��}�(j�  �/Table 2 – Forecast Growth Rates 2010 to 2030 �j�  M+j�  (G@R�    G@~�
    G@n�b    G@G
    t�ububh)��}�(hNh]�(hhC   @��,@���R�hhC    B�@@���R�G@�      G@��     t�ah?]�(�Regional Factor��com��ind_��igh res:��med�K ehA]�(]�(jd?  je?  jf?  jg?  jh?  K e]�(�Known Developments: Commercial��2��0��2��1�K e]�(�Known Developments: Industrial��0��2��0�NK e]�(�Known Developments: Residential��1��1��1��2�K e]�(�Future Land Use: Commercial��2�N�0�NK e]�(�&Future Land Use: First Nations Reserve��-1��-1��-1��-1�K e]�(�%Future Land Use: Future Urban Reserve��1��1��1��1�K e]�(�Future Land Use: Industrial��0��2�NNK e]�(�3Future Land Use: Multi Family Residential (cluster)��0�N�1��1�K e]�(�0Future Land Use: Multi Family Residential (high)��0�N�1��1��0�e]�(�/Future Land Use: Multi Family Residential (low)��0��0��0��0��2�e]�(�)Future Land Use: Multi Family Residential��0�NN�2�Ne]�(�Future Land Use: Park��-2��-2��-2��-2�K e]�(�*Future Land Use: Single Family Residential��0�N�1��1�K e]�(�Urban Pole (downtown)��2��2��1��0�K e]�(�Urban Pole (Glenmore) - 5 miles��1�NNNK e]�(�Urban Pole (Mission) _ 5 miles��1�N�1�NK e]�(�
Urban Pole��1��0��1��1�K e]�(�Urban Pole (University) ��2�N�2��0�K e]�(�	Elevation��0��=��1��0.5�K e]�(�Slope��-1��-1��-1��-1�K e]�(�ALR��-2��-2��-2��-2�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M-j�  Nubh)��}�(hNh]�(G@I��@   G@b�3@   G@�tQ�   G@|�\0   t�ah?]��6Regional Factor com. ind. high res. med. res. low res.�ahA]�(]��6Regional Factor com. ind. high res. med. res. low res.�a]��(Known Developments: Commercial 2 0 2 1 2�a]��(Known Developments: Industrial 0 2 0 0 0�a]��)Known Developments: Residential 1 1 1 2 1�a]��%Future Land Use: Commercial 2 0 0 0 0�a]��5Future Land Use: First Nations Reserve -1 -1 -1 -1 -1�a]��/Future Land Use: Future Urban Reserve 1 1 1 1 1�a]��%Future Land Use: Industrial 0 2 0 0 0�a]��=Future Land Use: Multi Family Residential (cluster) 0 0 1 1 0�a]��:Future Land Use: Multi Family Residential (high) 0 0 1 1 0�a]��9Future Land Use: Multi Family Residential (low) 0 0 0 0 2�a]��<Future Land Use: Multi Family Residential 0 0 0 2 0
(medium)�a]��$Future Land Use: Park -2 -2 -2 -2 -2�a]��4Future Land Use: Single Family Residential 0 0 1 1 0�a]��+Urban Pole (downtown) – 15miles 2 2 1 0 1�a]��+Urban Pole (Glenmore) – 5 miles 1 1 1 1 1�a]��*Urban Pole (Mission) – 5 miles 1 0 1 1 1�a]��*Urban Pole (Rutland) – 5 miles 1 0 1 1 1�a]��-Urban Pole (University) – 5 miles 2 0 2 0 1�a]��Elevation 0 -1 1 0.5 0�a]��Slope -1 -1 -1 -1 -1�a]��ALR -2 -2 -2 -2 -2�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M-j�  j�  )��}�(j�  �4Table 3 – Regional Factors Affecting Class Growth �j�  M-j�  (G@R�    G@a�3    G@pN�`   G@b�3    t�ububh)��}�(hNh]�(G@I��@   G@d��   G@|�cp   G@q+�    t�ah?]��7Proximity Factor com. ind. high res. med. res. low res.�ahA]�(]��7Proximity Factor com. ind. high res. med. res. low res.�a]�� Local Road (0.5 miles) 1 1 1 1 1�a]��'Secondary Highway (0.5 miles) 1 1 1 1 0�a]��%Primary Highway (0.5 miles) 1 1 0 0 0�a]��Ramp (0.5 miles) 1 1 0 0 0�a]��Water (0.5 miles) 2 2 2 2 2�a]��3Public Utilities Service (0.5 miles) -1 -1 -1 -1 -1�a]��Airport (0.5 miles) 1 1 1 0 0�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M/j�  j�  )��}�(j�  �Table4 – Proximity Factors  �j�  M/j�  (G@R�    G@c]    G@f/�    G@d}    t�ububh)��}�(h]�j$	  ah]�((G@I��@   G@r�fp   G@|�cp   G@w�\0   t�(G@J@     G@`5�   G@�|     G@��G�   t�(G@J@     G@K0     G@�|     G@��G�   t�eh?]��6Surround Factor com. ind. high res. med. res. low res.�ahA]�(]��6Surround Factor com. ind. high res. med. res. low res.�a]��Water (0.5 miles) 1 0 1 1 1�a]��#Industrial (0.5 miles) 0 2 -1 -1 -1�a]�� Commercial (0.5 miles) 1 0 0 0 0�a]��.Urban Grasses/Parks/etc. (0.5 miles) 1 0 2 2 1�a]��$Available land (0.5 miles) 0 0 1 1 1�a]�X�	  The Agricultural Land Reserve (ALR) is a provincial land use zone designated in 1973 to preserve agricultural
land for farming. With a total area of 4.7 million hectares, the ALR covers approximately 5% of British
Columbia’s land base1 and is comprised of both private and Crown lands.
The Agricultural Land Commission (ALC) is the provincial agency responsible for administering the ALR. The
purpose of the Commission is to:
1. preserve agricultural land;
2. encourage farming on agricultural land in collaboration with other communities of interest;
3. encourage local governments, First Nations, provincial government and its agencies to enable and
accommodate farm use of agricultural land and uses compatible with agriculture in their plans, bylaws
and policies.2
The ALC makes decisions on applications to include or exclude agricultural land from the ALR. It also controls
non-farm uses and subdivision within the ALR. As an independent administrative tribunal, the Commission
considers all information relevant to each application and weighs the likely impact of the proposal against the
long-term goal of preserving agricultural land and encouraging farming. The type of information considered
includes: agricultural capability, suitability, current land use, the property in relation to surrounding lands, related
agricultural concerns, and community planning objectives. Finally, the picture is broadened further to consider
the provincial interest of preservation of agricultural land for British Columbia.
Agriculture is BC’s third largest primary industry, generating $2.4 billion annually in farm cash receipts and
providing for approximately half of BC’s food requirements3. Without the ALR, the extent and diversity of this
important economic sector would be at risk and much of the agricultural lands that exist today would likely have
been lost to encroaching urban development. The ALR, thus, provides a secondary benefit in that it serves as an
urban containment boundary, helping to control urban sprawl.
By maintaining the ALR boundary and by regulating non-farm uses and subdivisions within its boundaries, the
Agricultural Land Commission plays a key role in preserving farmland for agricultural development and enabling
future agricultural expansion throughout British Columbia. Providing a stable land base ultimately leads to
increased food security for the province’s rising population.
2. STUDY PURPOSE�a]�X9  The ALC and the Ministry of Agriculture and Lands (MAL) undertook this study to understand the impacts of the
ALR with regard to its influence on agricultural land use in the City of Kelowna. Since the ALR was established
over 34 years ago, it is useful to examine how agriculture has been shaped by the ALR and to illustrate its
effectiveness in protecting farmland and sustaining the agriculture sector.
To meet the purpose of the study, all ALR exclusion applications in the study area (Section 3) were reviewed and
digitally mapped to reveal what the agricultural landscape may have looked like if all exclusion applications had
been allowed. Inclusion applications were also reviewed, but were not included in the analysis as there were only
two approved inclusions in the City of Kelowna between 1973 and 2006, totaling less than one hectare.
1 Provincial Agricultural Land Commission, Application Information Package, 2003.
2 Agricultural Land Commission Act, S.B.C. 2002, c.3.
3 B.C. Ministry of Agriculture and Lands, Fast Stats: Agriculture and Food (2005). Page 49 of 59�a]�X!  Table 1: ALR Statistics for the City of Kelowna
Total Area (ha) % of Total Area
Kelowna Area 21,656
ALR in Kelowna at ALR designation (1973) 6 10,054 46%
Land excluded from the ALR (1973-2006) 7 1,303 6%
Area of ALR (2007) 8,751 40%
Area of ALR in farm use8 (2001) 6,319 29%
4. METHODOLOGY�a]�X�  Information on exclusion applications was acquired from ALC application records. These records were used to
identify all parcels that had an application(s) for exclusion from the ALR that were approved or refused by the
Commission between 1973 and 2006. This data was brought into a Geographic Information System (GIS) and
overlaid with agricultural land use data acquired by MAL in 2001.
5. ANALYSIS�a]�Xm  The following questions guided the analysis:
 How much of the land area of Kelowna was originally and is currently in the ALR?
 What would Kelowna have looked like if land was not in a land reserve?
 What was the area of ALR exclusion applications, where were they located, and were they approved or
refused?
 For parcels that were refused exclusion from the ALR, what were the primary land uses and agricultural
activities, based on the MAL 2001 agricultural land use inventory? Note: It was assumed that if the exclusion
applications had been approved, the agricultural land use would not be present.
6. RESULTS�a]�X]  From 1974 to 2006, 188 exclusion applications were recorded in the City of Kelowna. Map 2 shows the
properties with exclusion applications; approximately 13% of the original ALR area has been excluded or has
been approved to be excluded from the Reserve. Although the presence of the ALR has not completely prevented
the loss of agricultural lands, in all probability, farmland would have been lost at a much faster rate9 if the ALR
had not existed, eroding the value and sustainability of the agricultural sector in Kelowna.
6 This number is calculated by adding the total area of the current ALR and the total area of land excluded or approved to be excluded from
the ALR.
7 The figures presented in this report were calculated using GIS. These reflect final decisions of the Commission where conditions have
been met and thus might be somewhat different than those reported by the ALC which reflect all decisions including conditional
decisions of the Commission.
8 Ministry of Agriculture and Lands, Kelowna land use inventory dataset, 2001.
9 Rate of loss is 43 hectares per year over thirty years.
Page 51 of 59�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M/j�  j�  )��}�(j�  �0Table5 – Surround Factors Airport (0.5 miles) �j�  M/j�  (G@LQ�   G@p=�    G@e�>�   G@r�f�   t�ububh)��}�(hNh]�(G@J�Q�   G@g�R    G@�oȦfffG@��G�   t�ah?]�(�NTable 2: Reduction in ALR Land if All Exclusion Applications had been Approved�j  ehA]�(]�(�NTable 2: Reduction in ALR Land if All Exclusion Applications had been Approved�Ne]�(j  �	Area (ha)�e]�(�ALR Area at designation��10,054�e]�(�ALR Area (2007)��8751�e]�(�#ALR exclusions approved (1973-2006)��-1,303�e]�(�"ALR exclusions refused (1973-2006)��-2,367�e]�(�,ALR Area if all exclusions had been approved��6,384 (63%)�e]�(�Page 52 of 59�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MGj�  j�  )��}�(j�  �0approved if all exclusion applications had been �j�  MGj�  (G@K      G@K-�    G@�0D�   G@S�3    t�ububh)��}�(hNh]�(G@L���333G@_h�    G@~<�����G@�_3@   t�ah?]�X^  Kelowna: Number of Applications Received Annually 20 (1973 - 2006) 200 18 180 16 160 fo 14 140 snoitacilppA rebmuN snoitacilppA 12 120 10 100 evitalumuC 8 80 fo 6 60 .oN 4 40 2 20 0 0 3791 5791 7791 9791 1891 3891 5891 7891 9891 1991 3991 5991 7991 9991 1002 3002 6002 Year Number of Applications Cumulative Number of Applications Trend (1973 - 2006)�ahA]�(]�X^  Kelowna: Number of Applications Received Annually
20 (1973 - 2006) 200
18 180
16 160
fo
14 140 snoitacilppA rebmuN
snoitacilppA
12 120
10 100
evitalumuC
8 80
fo
6 60 .oN
4 40
2 20
0 0
3791 5791 7791 9791 1891 3891 5891 7891 9891 1991 3991 5991 7991 9991 1002 3002 6002
Year
Number of Applications Cumulative Number of Applications Trend (1973 - 2006)�a]�X8  igure 3 illustrates the area excluded annually from 1973 to 2006. In 1988, the largest area was e
he ALR; in this year, 8 exclusion applications were approved, totaling 239 hectares. Since the in
ALR, the area excluded annually has been declining.
igure 3: Area of ALR excluded by year (1973 – 2006) in Kelowna�a]�X  Kelowna: Area Excluded by Year
300 (1973 - 2006) 1400
1200
250
1000 )aH(
200
aerA
800 )aH(
150
aerA evitalumuC
600
100
400
50
200
0 0
3791 5791 7791 9791 1891 3891 5891 7891 9891 1991 3991 5991 7991 9991 2002 4002
Year
Area (Ha) Cumulative Area (Ha) Trend (1973 - 2006)�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MJj�  j�  )��}�(j�  �OFigure 2:    Number of ALR exclusion applications received annually in Kelowna �j�  MJj�  (G@K      G@Z|�`   G@{;�@   G@]I-�   t�ububh)��}�(hNh]�(G@W��UUUG@�8{   G@p�c���G@����ZZZt�ah?]�j  ahA]�(]�j  a]�j  a]�j  a]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MJj�  j�  )��}�(j�  �+(1973 - 2006)Kelowna: Area Excluded by Year�j�  MJj�  (G@g ��   G@�    G@vc    G@�[�    t�ububh)��}�(hNh]�(G@J�r$�I%G@a�    G@�o�UUUG@���   t�ah?]�(X�  Table 3: Area of ALR Exclusion Applications ALR Area (ha) ALR Area (%) Refused Exclusion 2,367 24% Approved Exclusion 1,303 13% Total Applications 3,670 37% Agricultural Activities on Parcels with Refused Exclusion Applications Map 3 illustrates the agricultural activities occurring on areas with refused exclusion applications within Kelowna’s ALR. Table 4 shows a general breakdown of the types of land use activities occurring on these parcels.�j  j  j  ehA]�(]�(X�  Table 3: Area of ALR Exclusion Applications
ALR Area (ha) ALR Area (%)
Refused Exclusion 2,367 24%
Approved Exclusion 1,303 13%
Total Applications 3,670 37%
Agricultural Activities on Parcels with Refused Exclusion Applications
Map 3 illustrates the agricultural activities occurring on areas with refused exclusion applications within
Kelowna’s ALR. Table 4 shows a general breakdown of the types of land use activities occurring on these
parcels.�NNNe]�(�YTable 4: Overview of Land Use (2001) on Parcels Refused Exclusion from the ALR in Kelowna�NNNe]�(j  �Number of
Parcels��Total Area (ha)��% of Total Area Refused�e]�(�Agricultural land use��279��1,764��75%�e]�(�Non-agricultural land use��125��553��23%�e]�(�Land use data unavailable��19��50��2%�e]�(�Total��423��2,367��100%�e]�(X�  In 2001, agriculture was the primary land use on 75% (1,764 hectares) of parcels refused exclusion. 23% (553
hectares) were not being farmed and were being used for some other use. Of these unfarmed parcels, 13% were
either unused or were being used for hobby agriculture. The remaining 10% were permanently alienated from
agricultural through activities such as residential use, industrial use, etc.10.
If the ALR had not existed, it is assumed that at least 1,764 hectares of actively farmed land in Kelowna would
have been lost. Without the ALR, it is likely that much more land would have been converted to urban and other
non-farm land uses.�NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MKj�  j�  )��}�(j�  �Refused Exclusion Applications �j�  MKj�  (G@K      G@K%�    G@m��`   G@P��    t�ububh)��}�(hNh]�(G@aR=�   G@a�    G@|���   G@o�=�   t�ah?]�(�+Table 3: Area of ALR Exclusion Applications�j  j  ehA]�(]�(�+Table 3: Area of ALR Exclusion Applications�NNe]�(j  �ALR Area (ha)��ALR Area (%)�e]�(�Refused Exclusion��2,367��24%�e]�(�Approved Exclusion��1,303��13%�e]�(�Total Applications��3,670��37%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MKj�  j�  )��}�(j�  �Refused Exclusion Applications �j�  MKj�  (G@K      G@K%�    G@m��`   G@P��    t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�((G@J�n�t]G@W�     G@�r�6'bvG@�(��   t�(G@Z��   G@W�     G@�&     G@tuG�   t�eh?]�(X�  Table 5: Primary Agricultural Activities on Parcels with Refused Exclusion Applications Number of Area of Use Primary Agricultural Activity % of Total Area Refused Parcels (ha) Orchard 149 852 48% Beef operations 19 409 23% Forage or pasture 70 315 18% Other agricultural activities 20 80 5% Extensive livestock 12 67 4% Nursery and/or greenhouse 5 24 ~ 1% Vineyard and/or winery 4 17 ~ 1% TOTAL 279 1,764 100% Economic Value of Agriculture on Parcels Refused Exclusion Table 6 summarizes the types of primary agricultural activities occurring on properties with refused exclusion applications. The estimated annual crop value was calculated by multiplying the average gross farm receipts per hectare by the total number of hectares for each commodity.�j  j  j  ehA]�(]�(X�  Table 5: Primary Agricultural Activities on Parcels
with Refused Exclusion Applications
Number of Area of Use
Primary Agricultural Activity % of Total Area Refused
Parcels (ha)
Orchard 149 852 48%
Beef operations 19 409 23%
Forage or pasture 70 315 18%
Other agricultural activities 20 80 5%
Extensive livestock 12 67 4%
Nursery and/or greenhouse 5 24 ~ 1%
Vineyard and/or winery 4 17 ~ 1%
TOTAL 279 1,764 100%
Economic Value of Agriculture on Parcels Refused Exclusion
Table 6 summarizes the types of primary agricultural activities occurring on properties with refused exclusion
applications. The estimated annual crop value was calculated by multiplying the average gross farm receipts per
hectare by the total number of hectares for each commodity.�NNNe]�(�UTable 6: Estimated Potential Annual Value of Agriculture on Parcels Refused Exclusion�NNNe]�(�Primary Agricultural Activity��Area Refused
Exclusion (ha)��0Average Annual Gross
Farm Receipts
($/hectare)11��Estimated Annual Crop
Value�e]�(�Orchard��852��$4,740��
$4,038,348�e]�(�Beef operations��409��$189��$ 77,478�e]�(�Forage or pasture��315��$165��$ 52,009�e]�(�Other agricultural activities��80��$607��$ 48,590�e]�(�Extensive livestock��67��$501��$ 33,592�e]�(�Nursery and/or greenhouse��24��$28,344��	$ 680,246�e]�(�Vineyard and/or winery��17��$7,653��	$ 130,094�e]�(�Total��1,764�j  �
$5,060,357�e]�(j  NNNe]�(�WTable 5: Primary Agricultural Activities on Parcels
with Refused Exclusion Applications�NNNe]�(�Primary Agricultural Activity��Number of
Parcels��Area of Use
(ha)��% of Total Area Refused�e]�(�Orchard��149��852��48%�e]�(�Beef operations��19��409��23%�e]�(�Forage or pasture��70��315��18%�e]�(�Other agricultural activities��20��80��5%�e]�(�Extensive livestock��12��67��4%�e]�(�Nursery and/or greenhouse�j�  �24��~ 1%�e]�(�Vineyard and/or winery�j�  �17��~ 1%�e]�(�TOTAL��279��1,764��100%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MMj�  Nubh)��}�(hNh]�(G@A��    G@A���   G@���   G@����   t�ah?]��FortisBC Transmission System�ahA]�(]��FortisBC Transmission System�a]�X&
  n oo
rdit
yc
!( Distribution Substation 63 KV Vernon He n Cn
Lumby Nakusp ") Generating Station 132 KV/138 KV Bo
Coldstream Meadow C%,ree Tek C oT
rminal and Distribution Substation 161 KV
XY Terminal Substation 230 KV
Merritt
Lake Country
DUC New Denver !(
ELL Silverton !(
GLE
SEX Kaslo REC !( %,!( L E E
KAS !(!(!( B KL !(
SAU !( !(
!(
OKM
BEV !(%, ROJ
Peachland OHBGD L
Slocan
VAL !(
BWS !(
COF CRA !(
!(
Summerland
Balfour
SUM
!(
AWA !(
TRC !(
WES PAS !(
PRI Princeton W HUE TB LBO COR Nelson RGA SLC RSM Co o n n e c tio n %, !( !(!(!( %, !(
WAT T B C H y d ro !( ") ")") ")
PLA
UBO COT KAL TAR BSS !( !( !( T Co o n n B C e c tio n H y d ro !(
CAS
HED !(
OKF KRA !( !(!(
!(
BTS YMR Castlegar C o n n e c tio no !( !(
VAS OOT Keremeos OLI BLU t !( %, T B C H y d ro !(
Salmo KER XY%,!( !(
BEN SAL NIP !( Co o n n e c tio n
AAL RosslandWTS STC FruitvaleHER Creston %, T B C H y d ro
Greenwood CSA CSM GLM FRU CRE !(XYXYXY !( !( !(
OsoyoosNKM KET GFT Trail ESS Grand Forks CHR BEP OSO Midway !( !( !( %, !( !( !( !(
RUC WAN
")
7 3.5 0 7 14 21 28
Substation Names Kilometers
DUC!( !(
PAS Nelson ID Station Name ID Station Name ID Station Name ID Station Name ID Station Name
!(
LBO AAL A.A. LAMBERT TERMINAL COR CORRA LINN PLANT #4 HER HEARNS OLI OLIVER SLC SOUTH SLOCAN PLANT #3
ELL!( SLC ") RSM ASM A.S. MAWDSLEY TERMINAL COT COTTONWOOD HOL HOLLYWOOD OOT OOTISCHENIA STC STONEY CREEK
")")
") COR
GLE !( AWA ARAWANA CRA CRAWFORD BAY HUT HUTH AVENUE OSO OSOYOOS SUM SUMMERLAND
SEX!( PLA UBO !(COT %, BEN BENTLEY TERMINAL CRE CRESTON JOR JOE RICHE PAS PASSMORE TAR TARRYS LE E
REC
!( Kelowna BEP BEAVER PARK CSC CASCADE KAL KALEDEN PIN PINE STREET TRC TROUT CREEK
SAU!( !( !( !( !( TAR!( BEV BENVOULIN DGB D.G. BELL TERMINAL KAS KASLO PLA PLAYMORE UBO UPPER BONNINGTON PLANT #2 B L K
!( BLK BLACK MOUNTAIN DUC DUCK LAKE KER KEREMEOS PRI PRINCETON VAL VALHALLA H O L OJ R
OKM BSS
!( BEV BLU BLUEBERRY ELL ELLISON KET KETTLE VALLEY REC RECREATION VAS VASEUX TERMINAL
KRA!( CAS !(!(
%, !(YMR BSS BRILLIANT SWITCHING (CPC) ESS EMERALD SWITCHING (TECK) KRA KRAFT RGA R.G. ANDERSON TERMINAL WAN WANETA PLANT #5 (TECK)
!( BTS D G B
Castlega!(rOOT BTS BRILLIANT TERMINAL (CPC) FRU FRUITVALE LBO LOWER BONNINGTON PLANT #1 RSM ROSEMONT SWITCHING WAT WATERFORD
BWS BIG WHITE GFT GRAND FORKS TERMINAL LEE F.A. LEE TERMINAL RUC RUCKLES WEB WEST BENCH
BLU!( CAS CASTLEGAR GLE GLENMORE NKM NK'MIP SAL SALMO WES WESTMINSTER
Salmo
!( CHR CHRISTINA LAKE GLM GLENMERRY OKF O.K. FALLS SAU SAUCIER WTS WARFIELD TERMINAL
KELOWNA KOOTENAY SAL COF COFFEE CREEK HED HEDLEY OKM OKANAGAN MISSION SEX SEXSMITH YMR YMIR
HER!(�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MPj�  Nubh)��}�(hNh]�(G@|�?����G@���uUUUG@�l]@   G@�_��ffft�ah?]�(�ID��Station Name��ID��Station Name��ID St��
ation Name��ID��Station Name��ID��Station Name�ehA]�(]�(�ID��Station Name��ID��Station Name��ID St��
ation Name��ID��Station Name��ID��Station Name�e]�(�AAL��A.A. LAMBERT TERMINAL��COR��CORRA LINN PLANT #4��HER H��EARNS��OLI��OLIVER��SLC��SOUTH SLOCAN PLANT #3�e]�(�ASM��A.S. MAWDSLEY TERMINAL��COT��
COTTONWOOD��HOL H��OLLYWOOD��OOT��OOTISCHENIA��STC��STONEY CREEK�e]�(�AWA��ARAWANA��CRA��CRAWFORD BAY��HUT H��
UTH AVENUE��OSO��OSOYOOS��SUM��
SUMMERLAND�e]�(�BEN��BENTLEY TERMINAL��CRE��CRESTON��JOR J��OE RICHE��PAS��PASSMORE��TAR��TARRYS�e]�(�BEP��BEAVER PARK��CSC��CASCADE��KAL K��ALEDEN��PIN��PINE STREET��TRC��TROUT CREEK�e]�(�BEV��	BENVOULIN��DGB��D.G. BELL TERMINAL��KAS K��ASLO��PLA��PLAYMORE��UBO��UPPER BONNINGTON PLANT #2�e]�(�BLK��BLACK MOUNTAIN��DUC��	DUCK LAKE��KER K��EREMEOS��PRI��	PRINCETON��VAL��VALHALLA�e]�(�BLU��	BLUEBERRY��ELL��ELLISON��KET K��ETTLE VALLEY��REC��
RECREATION��VAS��VASEUX TERMINAL�e]�(�BSS��BRILLIANT SWITCHING (CPC)��ESS��EMERALD SWITCHING (TECK)��KRA K��RAFT��RGA��R.G. ANDERSON TERMINAL��WAN��WANETA PLANT #5 (TECK)�e]�(�BTS��BRILLIANT TERMINAL (CPC)��FRU��	FRUITVALE��LBO L��OWER BONNINGTON PLANT #1��RSM��ROSEMONT SWITCHING��WAT��	WATERFORD�e]�(�BWS��	BIG WHITE��GFT��GRAND FORKS TERMINAL��LEE F.��A. LEE TERMINAL��RUC��RUCKLES��WEB��
WEST BENCH�e]�(�CAS��	CASTLEGAR��GLE��GLENMORE��NKM N��K'MIP��SAL��SALMO��WES��WESTMINSTER�e]�(�CHR��CHRISTINA LAKE��GLM��	GLENMERRY��OKF O��	.K. FALLS��SAU��SAUCIER��WTS��WARFIELD TERMINAL�e]�(�COF��COFFEE CREEK��HED��HEDLEY��OKM O��KANAGAN MISSION��SEX��SEXSMITH��YMR��YMIR�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MPj�  j�  )��}�(j�  �DUCSubstation Names�j�  MPj�  (G@^�    G@��   G@��`   G@��Ӏ   t�ububh)��}�(hNh]�(G@"�Z�E�tG@(      G@���P   G@�\��   t�ah?]�(�(2012 Long Term Capital Plan Appendix D-2�j  ehA]�(]�(�(2012 Long Term Capital Plan
Appendix D-2�Ne]�(Nj  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MRj�  Nubh)��}�(hNh]�(G@B�ϰ   G@A������G@���   G@��"UUUUt�ah?]��3FortisBC Transmission Lines - North Okanagan Region�ahA]�(]��3FortisBC Transmission Lines - North Okanagan Region�a]�X�  Lumby
Vernon
Coldstream
7 2 L
Lake Country
7 4 L
DUC !(
L16 4 L6
L47
ELL !(
L27
GLE 5 0 L
SEX !( L E E
%, L05
REC L85
!( 5 5 L
!( KLB
SAU !( !( !(
!(
Kelowna H O L J O R 5 7 L
!(
OKM L75
BEV
!(
L15
5 4 L
ID Station Name ID Station Name %, BEV BENVOULIN HOL HOLLYWOOD L06 D G B 7 3 L
BLK BLACK MOUNTAIN JOR JOE RICHE
BWS BIG WHITE LEE F.A. LEE TERMINAL
DGB D.G. BELL TERMINAL OKM OKANAGAN MISSION Peachland
DUC DUCK LAKE REC RECREATION
!( BWS
ELL ELLISON SAU SAUCIER
GLE GLENMORE SEX SEXSMITH
4 2 0 4 8 12 16
Kilometers
7 3 L
!( Distribution Substation 63 KV
t
") Generating Station 132 KV/138 KV
%, Terminal and Distribution Substation 161 KV
XY Terminal Substation 230 KV�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MTj�  Nubh)��}�(hNh]�(G@B�ϰ   G@�P�5UUUG@tm�   G@�X:5UUUt�ah?]�(�ID��Station Name��ID��Station Name�ehA]�(]�(�ID��Station Name��ID��Station Name�e]�(�BEV��	BENVOULIN��HOL��	HOLLYWOOD�e]�(�BLK��BLACK MOUNTAIN��JOR��	JOE RICHE�e]�(�BWS��	BIG WHITE��LEE��F.A. LEE TERMINAL�e]�(�DGB��D.G. BELL TERMINAL��OKM��OKANAGAN MISSION�e]�(�DUC��	DUCK LAKE��REC��
RECREATION�e]�(�ELL��ELLISON��SAU��SAUCIER�e]�(�GLE��GLENMORE��SEX��SEXSMITH�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MTj�  j�  )��}�(j�  �	BEVOKMJOR�j�  MTj�  (G@~/�    G@�-    G@��X    G@��`   t�ububh)��}�(hNh]�(G@I#�`   G@���@   G@i�_�   G@�0��   t�ah?]�(j  j  j  j  j  j  j  ehA]�(]�(j  j  j  j  j  j  j  e]�(Nj  Nj  Nj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MTj�  j�  )��}�(j�  �BWSPeachland�j�  MTj�  (G@w���   G@��y    G@���    G@���   t�ububh)��}�(hNh]�(G@C%�   G@A������G@���   G@��"UUUUt�ah?]��^SAU !( ( B L K FortisBC Transmission Lines - South Okanagan Region !( !( H O L !( OKM !( J O R�ahA]�(]��^SAU !( ( B L K
FortisBC Transmission Lines - South Okanagan Region !( !(
H O L !(
OKM !( J O R�a]�Xv  BEV L15 !( 5 7 L 5 7 L
54
L06 %, L
D BG 7 3 L
Peachland
BWS !(
L37
Summerland
SUM !(
AWA 4 9 L !(
TRC !(
L54
4 9 L
WES
WEB !(
!(
HUT !( %,
Princeton
RGA !(
PRI WAT %,
4 L7
KAL 5 6 L !(
L67
4 3 L 4 1 L
!(
HED 4 3 A L !(
OKF
L24
ID Station Name ID Station Name
AWA ARAWANA OLI OLIVER VAS %,
BEN BENTLEY TERMINAL OSO OSOYOOS 04 L
BWS BIG WHITE PIN PINE STREET
DGB D.G. BELL TERMINAL PRI PRINCETON Keremeos
OLI HED HEDLEY RGA R.G. ANDERSON TERMINAL 4 3 L !(
KER XY%,!(
HUT HUTH AVENUE SUM SUMMERLAND PIN
KAL KALEDEN TRC TROUT CREEK BEN 4 L3
KER KEREMEOS VAS VASEUX TERMINAL
4 8 L
KET KETTLE VALLEY WAT WATERFORD
NKM NK'MIP WEB WEST BENCH
OKF O.K. FALLS WES WESTMINSTER
L66
4 2 0 4 8 12 16
L44
Kilometers KET
!(
!( Distribution Substation 63 KV Osoyoos t !(
") Generating Station 132 KV/138 KV NKM Midway !(
!(
%, Terminal and Distribution Substation 161 KV OSO
XY Terminal Substation 230 KV�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MVj�  Nubh)��}�(hNh]�(G@C%�   G@�H�����G@t��    G@�,�ꪪ�t�ah?]�(�ID��Station Name��ID��Station Name�ehA]�(]�(�ID��Station Name��ID��Station Name�e]�(�AWA��ARAWANA��OLI��OLIVER�e]�(�BEN��BENTLEY TERMINAL��OSO��OSOYOOS�e]�(�BWS��	BIG WHITE��PIN��PINE STREET�e]�(�DGB��D.G. BELL TERMINAL��PRI��	PRINCETON�e]�(�HED��HEDLEY��RGA��R.G. ANDERSON TERMINAL�e]�(�HUT��HUTH AVENUE��SUM��
SUMMERLAND�e]�(�KAL��KALEDEN��TRC��TROUT CREEK�e]�(�KER��KEREMEOS��VAS��VASEUX TERMINAL�e]�(�KET��KETTLE VALLEY��WAT��	WATERFORD�e]�(�NKM��NK'MIP��WEB��
WEST BENCH�e]�(�OKF��
O.K. FALLS��WES��WESTMINSTER�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MVj�  j�  )��}�(j�  �OKFHED�j�  MVj�  (G@{��@   G@|���   G@�o�    G@~V`   t�ububh)��}�(hNh]�(G@H���   G@���@   G@g	#h   G@�0��   t�ah?]�(j  j  j  j  j  j  j  ehA]�(]�(j  j  j  j  j  j  j  e]�(Nj  Nj  Nj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MVj�  j�  )��}�(j�  �	BENPINKER�j�  MVj�  (G@�b�    G@�N@   G@��`   G@�ܡ@   t�ububh)��}�(hNh]�(hhC   ���8@���R�hhC   `��J@���R�G@�      G@��     t�ah?]�(�ID��Station Name��D�NehA]�(]�(j�B  j�B  j�B  Ne]�(�AAL��AA LAMBERT TERMINAL��GLM�Ne]�(�ASM��AS. MAWDSLEY TERMINAL��HED�Ne]�(�AWA��ARAWANA��HER��VAL�e]�(�BEP��BEAVER PARK��KAS�Ne]�(�BLU��	BLUEBERRY��KRA�Ne]�(�BSS��BRILLIANT SWITCHING (CPC)��LBO�Ne]�(�BTS��BRILLIANT TERMINAL (CPC)��OOT�Ne]�(�CAS��	CASTLEGAR��PAS�Ne]�(�CHR��CHRISTINA LAKE��PLA�Ne]�(�COF��COFFEE CREEK��RSM�Ne]�(�COR��CORRA LINN PLANT #4��RUC�Ne]�(�COT��
COTTONWOOD��SAL�Ne]�(�CRA��CRAWFORD BAY��SLC�Ne]�(�CRE��CRESTON��STC�Ne]�(�CSC��CASCADE��UBO�Ne]�(�DUC��	DUCK LAKE��VAL�Ne]�(�ESS��EMERALD SWITCHING (TECK)��WAN�Ne]�(�FRU��	FRUITVALE��WTS�Ne]�(�GFT��GRAND FORKS TERMINAL��YMR��1L,23L,24L,281�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  j�  )��}�(j�  �+FortisBC Transmission Lines Kootenay Region�j�  MXj�  (G@C��@   G@A�j@   G@���`   G@Q���   t�ububh)��}�(hNh]�(G@A��    G@A������G@���   G@��"UUUUt�ah?]��-FortisBC Transmission Lines - Kootenay Region�ahA]�(]��-FortisBC Transmission Lines - Kootenay Region�a]�Xj  Silverton
!( Distribution Substation 63 KV Kaslo t
") Generating Station 132 KV/138 KV KAS !(
%, Terminal and Distribution Substation 161 KV
XY Terminal Substation 230 KV
4 2 0 4 8 12 16
Kilometers
ID Station Name ID Station Name
3 7 L
AAL A.A. LAMBERT TERMINAL GLM GLENMERRY Slocan
ASM A.S. MAWDSLEY TERMINAL HED HEDLEY VAL !(
AWA ARAWANA HER HEARNS
BEP BEAVER PARK KAS KASLO
BLU BLUEBERRY KRA KRAFT
BSS BRILLIANT SWITCHING (CPC) LBO LOWER BONNINGTON PLANT #1
COF Kimberley !(
BTS BRILLIANT TERMINAL (CPC) OOT OOTISCHENIA
CRA !(
CAS CASTLEGAR PAS PASSMORE
CHR CHRISTINA LAKE PLA PLAYMORE
19L
COF COFFEE CREEK RSM ROSEMONT SWITCHING
COR CORRA LINN PLANT #4 RUC RUCKLES
Balfour
COT COTTONWOOD SAL SALMO
CRA CRAWFORD BAY SLC SOUTH SLOCAN PLANT #3 38L
CRE CRESTON STC STONEY CREEK
CSC CASCADE UBO UPPER BONNINGTON PLANT #2
DUC DUCK LAKE VAL VALHALLA
ESS EMERALD SWITCHING (TECK) WAN WANETA PLANT #5 (TECK)
FRU FRUITVALE WTS WARFIELD TERMINAL !(
PAS
GFT GRAND FORKS TERMINAL YMR YMIR 1 3 L12,L 2, 3 42,L 2,L 8 L
27L
32L
19L
LBO Nelson
SLC !(
RSM
")
2 9 L ") ")
")
COR !(
PLA
UBO 27L
25L
COT !(
TAR !(
25L
BSS
79L
6L
KRA CAS 27L !(
!(
BTS
26L
YMR !( !(
OOT
6L
BLU 26L !(
27L
20L
Salmo
7 7 L !(
HER SAL
!(
20L
AAL %,
Rossland WTS STC
1L FRU Fruitvale Creston !(XY !( 3 1 L
ASM GLM XYXY !(
CSC 10L CRE !(
20L 20L !( !(
ESS BEP
T Grand For9 kL s CHR 9L 9L %, 1 0 L !( 1 0 L 1 1 L
18L !(
RUC
WAN�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  Nubh)��}�(hNh]�(G@F�ȳ333G@d8
    G@e~��   G@ec�    t�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(j  j  j  j  �      j  j  j  j  e]�(j  Nj  Nj  Nj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  j�  )��}�(j�  �KASKasloSilverton�j�  MXj�  (G@���   G@Rsʀ   G@��7`   G@[>�   t�ububh)��}�(hNh]�(G@F�ȳ333G@fm�UUUUG@tބ�   G@wf�����t�ah?]�(�ID��Station Name��ID��Station Name�ehA]�(]�(�ID��Station Name��ID��Station Name�e]�(�AAL��A.A. LAMBERT TERMINAL��GLM��	GLENMERRY�e]�(�ASM��A.S. MAWDSLEY TERMINAL��HED��HEDLEY�e]�(�AWA��ARAWANA��HER��HEARNS�e]�(�BEP��BEAVER PARK��KAS��KASLO�e]�(�BLU��	BLUEBERRY��KRA��KRAFT�e]�(�BSS��BRILLIANT SWITCHING (CPC)��LBO��LOWER BONNINGTON PLANT #1�e]�(�BTS��BRILLIANT TERMINAL (CPC)��OOT��OOTISCHENIA�e]�(�CAS��	CASTLEGAR��PAS��PASSMORE�e]�(�CHR��CHRISTINA LAKE��PLA��PLAYMORE�e]�(�COF��COFFEE CREEK��RSM��ROSEMONT SWITCHING�e]�(�COR��CORRA LINN PLANT #4��RUC��RUCKLES�e]�(�COT��
COTTONWOOD��SAL��SALMO�e]�(�CRA��CRAWFORD BAY��SLC��SOUTH SLOCAN PLANT #3�e]�(�CRE��CRESTON��STC��STONEY CREEK�e]�(�CSC��CASCADE��UBO��UPPER BONNINGTON PLANT #2�e]�(�DUC��	DUCK LAKE��VAL��VALHALLA�e]�(�ESS��EMERALD SWITCHING (TECK)��WAN��WANETA PLANT #5 (TECK)�e]�(�FRU��	FRUITVALE��WTS��WARFIELD TERMINAL�e]�(�GFT��GRAND FORKS TERMINAL��YMR��YMIR�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  j�  )��}�(j�  �KASKasloSilverton�j�  MXj�  (G@���   G@Rsʀ   G@��7`   G@[>�   t�ububh)��}�(hNh]�(G@A���UUUG@A������G@������G@��"UUUUt�ah?]��-BWS FortisBC Transmission Lines - Boundary !(�ahA]�(]��-BWS FortisBC Transmission Lines - Boundary !(�a]�X�  L37
!( Distribution Substation 63 KV
") Generating Station 132 KV/138 KV
%, Terminal and Distribution Substation 161 KV
XY Terminal Substation 230 KV
AWA 4 2 0 4 8 12 16
!(
Kilometers PAS
ID Station Name L54
BEN BENTLEY TERMINAL 91 L
CSC CASCADE
WES CHR CHRISTINA LAKE
ESS EMERALD SWITCHING (TECK)
GFT GRAND FORKS TERMINAL 92 L
KET KETTLE VALLEY
RUC RUCKLES %,
WTS WARFIELD TERMINAL
RGA
4 7 L
2 L5
BSS
6 L
L67
KRA CAS
!(
!(
2 6 L
!(
L6
BLU !(
%,
VAS
4 0 L
7 7 L
OLI
XY%,
BEN STC !(
WTS
Rossland
4 8 L 1 L
!(
ASM GLM XYXY XY
!(
1 0 L
CSC
Greenwood L02
6 6 L !(
ESS !(
1 1 L 1 1 L
KET 9 L 1 0 L 9 L 1 L8
%, 9 L 1 0 L
GFT Grand Forks CHR W !( !(
!(
Osoyoos RUC !(
NKM
Midway !(
!(
OSO
t�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MZj�  Nubh)��}�(hNh]�(G@�i0   G@e��    G@��i\   G@f�Y    t�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(j  j  j  j  j  j  j  j  e]�(j  Nj  Nj  Nj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MZj�  j�  )��}�(j�  �AWA�j�  MZj�  (G@I��@   G@c�ؠ   G@R�@   G@e�%    t�ububh)��}�(hNh]�(G@�i0   G@g}    G@�Bz�   G@q.C@   t�ah?]�(�ID��Station Name�ehA]�(]�(�ID��Station Name�e]�(�BEN��BENTLEY TERMINAL�e]�(�CSC��CASCADE�e]�(�CHR��CHRISTINA LAKE�e]�(�ESS��EMERALD SWITCHING (TECK)�e]�(�GFT��GRAND FORKS TERMINAL�e]�(�KET��KETTLE VALLEY�e]�(�RUC��RUCKLES�e]�(�WTS��WARFIELD TERMINAL�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MZj�  j�  )��}�(j�  �AWA�j�  MZj�  (G@I��@   G@c�ؠ   G@R�@   G@e�%    t�ububh)��}�(hNh]�(G@U%��   G@]
����G@�K�333G@|o\(   t�ah?]�(�Line��Location��Year Assessed��Poles��Estimated Rehabilitation Cost�ehA]�(]�(�Line��Location��Year
Assessed��Poles��Estimated
Rehabilitation
Cost�e]�(j  j  j  j  �($000s)�e]�(ja
  �Warfield to Stoney Creek��2009��15��2.9�e]�(�25��(Slocan to Playmor to Tarrys to Brilliant��2009��299��17.2�e]�(�29��Slocan Valley��2009��140��247�e]�(�31��Lambert to Creston��2009��105��75�e]�(�30��Coffee Creek to Crawford Bay��2009��26��69�e]�(�50��7FA Lee to Sexsmith to Glenmore to Recreation to
Saucier��2009��320��76�e]�(�49��/Huth to West Bench to Trout Creek to Summerland��2009��310��277�e]�(j  �Subtotal��2009��1,215��764.1�e]�(�30��South Slocan to Coffee Creek��2010��522��972�e]�(�41��&Huth to Kaleden to OK Falls to Oliver1��2010��580��792�e]�(�42��%Huth to Kaleden to OK Falls to Oliver��2010��420��115�e]�(�45��&RG Anderson to Westminster to Naramata��2010��290��229�e]�(�45A��45 Line to Downtown Penticton��2010��48��106�e]�(�46��FA Lee to Duck Lake��2010��87��60�e]�(�47��Huth to Waterford��2010��50��14�e]�(j  �Subtotal��2010��1,997��2,288�e]�(j  �Total�j  �3,212��$3,052.1�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M^j�  j�  )��}�(j�  �iRehabilitation Costs 2009-2010 Table 1 – Transmission Line Condition Assessment Projects and Estimated �j�  M^j�  (G@]ue    G@R��   G@��   G@XOˀ   t�ububh)��}�(hNh]�(hhC   @s�@���R�hhC   �aC@���R�G@�      G@��     t�ah?]�(�4�K ehA]�(]�(j�D  K e]�(�5�K e]�(�6�K e]�(NK e]�(�8�K e]�(�9�K e]�(�10�K e]�(�11�K e]�(�12�K e]�(�13�K e]�(�14�K e]�(�15�K e]�(�16�K e]�(�17�K e]�(N�Iccumicnucu�e]�(�18�K e]�(�19�K e]�(�19��Ypoor condition with numerous steel stubbed structures in urgent need of replacement, sub-�e]�(�21��Uall been reviewed and documented on an individual structure basis and a detailed work�e]�(�22�K e]�(�23�K e]�(�24�K e]�(�25�K e]�(�26�K e]�(�27�K e]�(�28�K e]�(�29�K e]�(�30��Wdistribution along with the counts of which structures are recommended for replacement:�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mjj�  Nubh)��}�(hNh]�(G@nxQ�UUUG@b�G����G@x"�@   G@g���UUUt�ah?]�(�Year��2013�ehA]�(]�(�Year��2013�e]�(�Cost ($millions)��4.66�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mkj�  j�  )��}�(j�  �20 Line Expenditure Plan �j�  Mkj�  (G@o33@   G@aH`   G@w��   G@b��@   t�ububh)��}�(hNh]�(G@mXQ�UUUG@y�3    G@w��@   G@|p�   t�ah?]�(�Year��2012�ehA]�(]�(�Year��2012�e]�(�Cost ($millions)��1.16�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mnj�  j�  )��}�(j�  �27 Line Expenditure Plan �j�  Mnj�  (G@n3@   G@x��`   G@wg�   G@yk�`   t�ububh)��}�(hNh]�(G@m&fUUUUG@rF�@   G@w�    G@t�3    t�ah?]�(�Year��2012�ehA]�(]�(�Year��2012�e]�(�Cost ($millions)��2.22�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mqj�  j�  )��}�(j�  �21-24 Line Expenditure Plan �j�  Mqj�  (G@m�    G@q_g`   G@w�`   G@r`   t�ububh)��}�(hNh]�(G@`�VUUUUG@]�    G@��%    G@|��   t�ah?]�(j  �21 LINE POLE VINTAGE CHART�ehA]�(]�(�10�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j�  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j�  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�3 3�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNN�1 1 1 1�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  j  j  j  Nj  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mrj�  j�  )��}�(j�  �1221 LINE POLE VINTAGE CHART�j�  Mrj�  (G@[��`   G@U���   G@��    G@_�S`   t�ububh)��}�(hNh]�(G@`�!�   G@]���   G@��Ѐ   G@{�w�   t�ah?]�(j  �22 LINE POLE VINTAGE CHART�ehA]�(]�(�12�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�10�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�9
8�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�3 3 3
2�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  N�1 1 1�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Msj�  j�  )��}�(j�  �1422 LINE POLE VINTAGE CHART�j�  Msj�  (G@[��   G@U�z�   G@`   G@_lS�   t�ububh)��}�(hNh]�(G@`�QUUUG@]�F@   G@�
�@   G@|;�    t�ah?]�(j  �23 LINE POLE VINTAGE CHART�ehA]�(]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�21�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�15�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�12�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j�  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�55�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j�  N�3 3
2 2�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  N�1 1 1 1�NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mtj�  j�  )��}�(j�  �2423 LINE POLE VINTAGE CHART�j�  Mtj�  (G@[��    G@Uُ`   G@��   G@_�    t�ububh)��}�(hNh]�(G@m&fUUUUG@zP��   G@w�    G@|�p�   t�ah?]�(�Year��2012�ehA]�(]�(�Year��2012�e]�(�Cost ($millions)��0.79�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Myj�  j�  )��}�(j�  �19/29 Line Reconfiguration �j�  Myj�  (G@mu   G@yi��   G@w�d`   G@zR�   t�ububh)��}�(hNh]�(hhC   `HG@���R�hhC   �1+E@���R�G@�      G@��     t�ah?]�hA]�(]�(�	West Side�K e]�(�	West Side�K e]�(�	West Side�K e]�(�	West Side�K e]�(�	West Side�K e]�(�	East Side�K e]�(�	East Side�K e]�(�	East Side��Some old 518" hadwrare�e]�(�	East Side�K e]�(�	East Side��Considerable�e]�(�	East Side�K e]�(�	East Side�K e]�(�General�K e]�(�General�K e]�(�Genetal�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  Nubh)��}�(hNh]�(G@j��UUUUG@��G�   G@x�    G@�+34   t�ah?]�(�Year��2012��2013�ehA]�(]�(�Year��2012��2013�e]�(�Cost ($millions)��0.802��1.521�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �%30 Line Lake Crossing Rehabilitation �j�  M�j�  (G@j6f`   G@�ev    G@yT4    G@��M@   t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  j$	  �	yearmonth�j$	  j$	  j�E  j�E  j�E  j�E  j$	  j$	  eh]�(G@R��   G@^��   G@�.�   G@�8dL   t�ah?]�(�Kootenay�j  �2011��2012��2013��2014��2015��2016��2017��2018��2019��2020��Total��Comments�ehA]�(]�(�Kootenay�N�2011��2012��2013��2014��2015��2016��2017��2018��2019��2020��Total��Comments�e]�(�Line��Section��($000s)�NNNNNNNNNNNe]�(ja
  �!New line from Stoney Creek to WTS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(j�  � Brilliant to Castlegar to Celgar�j  �$ 1 ,185�j  j  j  j  j  j  j  j  �$ 1,185��6/26L Reconfiguration project.�e]�(j�  �New from BSS to BTS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(j�  �New from BSS to BTS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(j�  �WTS to Christina Lake�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(j�  �(Grand Forks to Ruckles to Christina Lake�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�10��WTS to Christina Lake�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�10��(Grand Forks to Ruckles to Christina Lake�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�11E��Warfield to Grand Forks�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�11W��Grand Forks to Kettle Valley�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�12��South Slocan to Canal Plant�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�13��South Slocan to Canal Plant�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�18��Waneta to Beaver Park�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�19��South Slocan to Slocan City�j  j  �$ 791�j  j  j  j  j  j  j  �$ 791��19/29L Reconfiguration project.�e]�(�20��WTS to Salmo�j  j  �$ 4 ,663�j  j  j  j  j  j  j  �$ 4,663��20 Line Rebuild�e]�(�21�� Lower Bonnington to South Slocan�j  �$ 2 ,219�j  j  j  j  j  j  j  j  �$ 2,219��21-24 Line Rebuild�e]�(�22�� Upper Bonnington to South Slocan�j  Nj  j  j  j  j  j  j  j  NNe]�(�23��Corra Linn to South Slocan�j  Nj  j  j  j  j  j  j  j  NNe]�(�24��Corra Linn to South Slocan�j  Nj  j  j  j  j  j  j  j  NNe]�(�25��Brilliant to South Slocan�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�26�� Brilliant to Castlegar to Celgar�j  j  j  j  j  j  j  j  j  j  �$ -��See 6L comments�e]�(�27��Corra Linn to Salmo�j  �$ 1 ,161�j  j  j  j  j  j  j  j  �$ 1,161��27 Liine Rebuild�e]�(�28��"Upper Bonnington to City of Nelson�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�29��South Slocan to Passmore tap�j  j  j  j  j  j  j  j  j  j  �$ -��See 19L comments�e]�(�30��South Slocan to Coffee Creek�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�31��Lambert to Creston�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�32��Crawford Bay to Lambert�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�34��WTS to Mawdsley�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�37��Coffee Creek to Kaslo�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�38��Coffee Creek to Crawford Bay�j  j  j  j  �$ 802��$ 1,521�j  j  j  j  �$ 2,323��30L Lake Crossing Rehab�e]�(�62��
WTS to ESS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�77��
BTS to WTS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�79��BTS to Canal Plant�j  j  j  j  j  j  j  j  j  j  �$ -�j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �?Attachment 1 – Ten Year Transmission Plan Cost Summary Table �j�  M�j�  (G@R      G@R_��   G@|�    G@U_��   t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  eh]�(G@R	��UUUG@R	��E�G@������G@���>���t�ah?]�(�North Okanagan�j  �2011��2012��2013��2014��2015��2016��2017��2018��2019��2020��Total��Comments�ehA]�(]�(�North Okanagan�N�2011��2012��2013��2014��2015��2016��2017��2018��2019��2020��Total��Comments�e]�(�Line��Section��($000s)�NNNNNNNNNNNe]�(�46��LEE to Duck Lake�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�50��!LEE to OKM via SEX, GLE, REC, SAU�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�51��
OKM to BEV�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�54��
DGB to BLK�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�55��LEE to HOL to 50L�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�57��BLK to JOR to BWS�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�58��
LEE to BLK�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�60��
BEV to DGB�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�61��
DUC to ELL�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�72��Vernon to LEE�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�73��
LEE to RGA�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�74��Vernon to LEE�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�South Okanagan�Nj  NNNNNNNNNNNe]�(�40��Vaseux to Bentley�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�41��7Huth to Oliver (In future will become distribution tie)�j  j  j  j  j  j  j  j  j  j  �$ -��IOnce OTR and Huth Bus
reconfiguration are complete, 41L
will be salvaged.�e]�(�42��Huth to Oliver�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�43��Bentley to Princeton�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�44��Oliver to Osoyoos�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�45��RGA to Naramata�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�47��Huth to Waterford�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�48��Kettle Valley to Bentley�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�49��Huth to Summerland�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�52��RGA to Huth�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�53��RGA to Huth�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�56��BCH Line to Princeton�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�66��Bentley to NK'Mip�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�68��Bentley to Oliver�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�69��Bentley to Oliver�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(�75/76��Vasuex to RGA�j  j  j  j  j  j  j  j  j  j  �$ -�j  e]�(X~  TOTAL - REBUILD/Special Project Funding $ - $ 4 ,565 $ 5 ,454 $ - $ 802 $ 1,521 $ - $ - $ - $ - $ 12,342
Annual Transmission Line Expenditures 2011 2012 2013 2014 2015 2016 2017 2018 2019 2020
Total
Rebuild/Special Project Funding $ - $ 4 ,565 $ 5 ,454 $ - $ 802 $ 1,521 $ - $ - $ - $ - $ 12,342
Test&Treat/Condition Assessment $ 443 $ 522 $ 485 $ 480 $ 547 $ 543 $ 543 $ 457 $ 614 $ 583 $ 5,217
Rehabilitation (Annual Pole Replacement) $ 1 ,518 $ 3 ,372 $ 2 ,621 $ 2 ,509 $ 2,424 $ 2,820 $ 2,562 $ 2 ,696 $ 2,481 $ 3,053 $ 26,056
Total by Year $ 1 ,961 $ 8 ,459 $ 8 ,560 $ 2 ,989 $ 3,773 $ 4,884 $ 3,105 $ 3 ,153 $ 3,095 $ 3,636 $ 43,615��+TOTAL - REBUILD/Special Project Funding $ -�N�$ 4 ,565��$ 5 ,454��$ -��$ 802��$ 1,521��$ -��$ -��$ -��$ -��$ 12,342�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Z�`0   G@�.h   G@�?��   G@�+��   t�ah?]�(�%Annual Transmission Line Expenditures��2011��2012��2013��2014��2015��2016��2017��2018��2019��2020�j  ehA]�(]�(�%Annual Transmission Line Expenditures��2011��2012��2013��2014��2015��2016��2017��2018��2019��2020�Ne]�(j  NNNNNNNNNN�Total�e]�(�Rebuild/Special Project Funding��$ -��$ 4 ,565��$ 5 ,454��$ -��$ 802��$ 1,521��$ -��$ -��$ -��$ -��$ 12,342�e]�(�%Test&Treat/Condition Assessment $ 443�N�$ 522��$ 485��$ 480��$ 547��$ 543��$ 543��$ 457��$ 614��$ 583��$ 5,217�e]�(�1Rehabilitation (Annual Pole Replacement) $ 1 ,518�N�$ 3 ,372��$ 2 ,621��$ 2 ,509��$ 2,424��$ 2,820��$ 2,562��$ 2 ,696��$ 2,481��$ 3,053��$ 26,056�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��TOTAL - REBUILD/Special Project Funding$         -4,565$      5,454$      $         -802$        $     1,521-$         $         --$         -$         12,342$     �j�  M�j�  (G@[3�    G@~�2�   G@��   G@�	@   t�ububh)��}�(hNh]�(G@Z�`0   G@����   G@�?��   G@���>���t�ah?]�(�Total by Year��$ 1 ,961��$ 8 ,459��$ 8 ,560��$ 2 ,989��$ 3,773��$ 4,884��$ 3,105��$ 3 ,153��$ 3,095��$ 3,636��$ 43,615�ehA]�]�(�Total by Year��$ 1 ,961��$ 8 ,459��$ 8 ,560��$ 2 ,989��$ 3,773��$ 4,884��$ 3,105��$ 3 ,153��$ 3,095��$ 3,636��$ 43,615�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �RTotalAnnual Transmission Line Expenditures2011201220132014201520162017201820192020�j�  M�j�  (G@[3    G@�Aɀ   G@���`   G@����   t�ububh)��}�(hNh]�((G@a�͆�iG@VH    G@~`D�N�G@��   t�(G@a���333G@R�    G@~`����G@��   t�(G@a�����G@R�    G@~`��ϑG@���   t�eh?]�(�Line #��Section��Cond. Assess. Date�ehA]�(]�(�Line #��Section��Cond. Assess.
Date�e]�(j�  �(Grand Forks to Ruckles to Christina Lake��2011�e]�(�10��(Grand Forks to Ruckles to Christina Lake��2011�e]�(�43��Bentley to Princeton��2011�e]�(�43A��Tap to Apex Mine��2011�e]�(j�  �WTS to Christina Lake��2012�e]�(�10��WTS to Christina Lake��2012�e]�(�11E��Warfield to Grand Forks��2012�e]�(�48��Kettle Valley to Bentley��2012�e]�(j�  �'BSS to Castlegar to Blueberry to Celgar��2013�e]�(�18��Waneta to Beaver Park��2013�e]�(�19��South Slocan to Slocan City��2013�e]�(�26�� Brilliant to Castlegar to Celgar��2013�e]�(�32��Crawford Bay to Lambert��2013�e]�(�20��WTS to Salmo��2014�e]�(�62��
WTS to ESS��2014�e]�(�72��Vernon to LEE��2014�e]�(�73��
LEE to RGA��2014�e]�(�21�� Lower Bonnington to South Slocan��2015�e]�(�22�� Upper Bonnington to South Slocan��2015�e]�(�23��Corra Linn to South Slocan��2015�e]�(�24��Corra Linn to South Slocan��2015�e]�(�27��Corra Linn to Salmo��2015�e]�(�77��
BTS to WTS��2015�e]�(�79��BTS to Canal Plant��2015�e]�(�52��RGA to Huth��2015�e]�(�53��RGA to Huth��2015�e]�(�56��BCH Line to Princeton��2015�e]�(�11W��Grand Forks to Kettle Valley��2016�e]�(�12��South Slocan to Canal Plant��2016�e]�(�13��South Slocan to Canal Plant��2016�e]�(�28��"Upper Bonnington to City of Nelson��2016�e]�(�37��Coffee Creek to Kaslo��2016�e]�(�51��
OKM to BEV��2016�e]�(�54��
DGB to BLK��2016�e]�(�58��
LEE to BLK��2016�e]�(�60��
BEV to DGB��2016�e]�(�74��Vernon to LEE��2016�e]�(�Line #��Section��Cond. Assess.
Date�e]�(�44��Oliver to Osoyoos��2016�e]�(ja
  �Stoney Creek to WTS��2017�e]�(�25��Brilliant to South Slocan��2017�e]�(�31��Lambert to Creston��2017�e]�(�34��WTS to Mawdsley��2017�e]�(�38��Coffee Creek to Crawford Bay��2017�e]�(�50��!LEE to OKM via SEX, GLE, REC, SAU��2017�e]�(�55��LEE to HOL to 50L��2017�e]�(�61��
DUC to ELL��2017�e]�(�40��Vaseux to Bentley��2017�e]�(�45/45A��RGA to Naramata��2017�e]�(�49��Huth to Summerland��2017�e]�(�75/76��Vaseux to RGA��2017�e]�(j�  �
BSS to BTS��2018�e]�(j�  �
BSS to BTS��2018�e]�(�30��South Slocan to Coffee Creek��2018�e]�(�46��LEE to Duck Lake��2018�e]�(�42��Huth to Oliver��2018�e]�(�47��Huth to Waterford��2018�e]�(�10��(Grand Forks to Ruckles to Christina Lake��2018�e]�(j�  �(Grand Forks to Ruckles to Christina Lake��2019�e]�(�43��Bentley to Princeton��2019�e]�(�43A��Tap to Apex Mine��2019�e]�(j�  �WTS to Christina Lake��2020�e]�(�10��WTS to Christina Lake��2020�e]�(�11E��Warfield to Grand Forks��2020�e]�(�48��Kettle Valley to Bentley��2020�e]�(j�  �'BSS to Castlegar to Blueberry to Celgar��2021�e]�(�18��Waneta to Beaver Park��2021�e]�(�19��South Slocan to Slocan City��2021�e]�(�26�� Brilliant to Castlegar to Celgar��2021�e]�(�32��Crawford Bay to Lambert��2021�e]�(�20��WTS to Salmo��2022�e]�(�62��
WTS to ESS��2022�e]�(�57��BLK to JOR to BWS��2022�e]�(�72��Vernon to LEE��2022�e]�(�73��
LEE to RGA��2022�e]�(�21�� Lower Bonnington to South Slocan��2023�e]�(�Line #��Section��Cond. Assess.
Date�e]�(�22�� Upper Bonnington to South Slocan��2023�e]�(�23��Corra Linn to South Slocan��2023�e]�(�24��Corra Linn to South Slocan��2023�e]�(�27��Corra Linn to Salmo��2023�e]�(�77��
BTS to WTS��2023�e]�(�79��BTS to Canal Plant��2023�e]�(�52��RGA to Huth��2023�e]�(�53��RGA to Huth��2023�e]�(�56��BCH Line to Princeton��2023�e]�(�66��Bentley to Nk'Mip��2023�e]�(�11W��Grand Forks to Kettle Valley��2024�e]�(�12��South Slocan to Canal Plant��2024�e]�(�13��South Slocan to Canal Plant��2024�e]�(�28��"Upper Bonnington to City of Nelson��2024�e]�(�37��Coffee Creek to Kaslo��2024�e]�(�44��Oliver to Osoyoos��2024�e]�(�51��
OKM to BEV��2024�e]�(�54��
DGB to BLK��2024�e]�(�58��
LEE to BLK��2024�e]�(�60��
BEV to DGB��2024�e]�(�74��Vernon to LEE��2024�e]�(ja
  �Stoney Creek to WTS��2025�e]�(�25��Brilliant to South Slocan��2025�e]�(�31��Lambert to Creston��2025�e]�(�34��WTS to Mawdsley��2025�e]�(�38��Coffee Creek to Crawford Bay��2025�e]�(�40��Vaseux to Bentley��2025�e]�(�45/45A��RGA to Naramata��2025�e]�(�49��Huth to Summerland��2025�e]�(�68��Bentley to Oliver��2025�e]�(�69��Bentley to Oliver��2025�e]�(�75/76��Vaseux to RGA��2025�e]�(�50��!LEE to OKM via SEX, GLE, REC, SAU��2025�e]�(�55��LEE to HOL to 50L��2025�e]�(�61��
DUC to ELL��2025�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �BAttachment 2 – Transmission Condition Assessment Plan 2011-2025 �j�  M�j�  (G@]�Q�   G@R_��   G@�'    G@U_��   t�ububh)��}�(hNh]�(hhC   �+X>@���R�hhC   @��j@���R�G@�      hhC   ���@���R�t�ah?]�hA]�(]�(�Date��Rev��Description��Author��Approvea�e]�(�
05/25/2011��0��Transmission System Programs��DK,��MA�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@P�p����G@����   G@��   G@�K38   t�ah?]�(�Date��Rev��Description��Author��Approved�ehA]�(]�(�Date��Rev��Description��Author��Approved�e]�(�
05/25/2011�j.  �Transmission System Programs��
DK, BM, DM��MA�e]�(j  j  j  j  j  e]�(j  j  j  j  j  e]�(j  j  j  j  j  e]�(j  NNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �-Document 801-03 Transmission System Programs �j�  M�j�  (G@j�=`   G@w �   G@y��   G@y&S�   t�ububh)��}�(hNh]�(G@b���   G@ta��   G@c�p�   G@v9�   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �,Assessing Vegetation Control Responsibility:�j�  M�j�  (G@ZG�    G@rQ+�   G@s^�    G@r�j`   t�ububh)��}�(hNh]�(G@[�/&fffG@y��   G@]�)    G@{�G�   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �,Assessing Vegetation Control Responsibility:�j�  M�j�  (G@ZG�    G@rQ+�   G@s^�    G@r�j`   t�ububh)��}�(hNh]�(hhC   �.<@���R�hhC   ���P@���R�G@�      G@��     t�ah?]�hA]�(]�(K �2��20�e]�(N�9.2.2�K e]�(�9.3��3�K e]�(N�9.3.1�K e]�(�10.0��Record Location and Retention�K e]�(�11.0��Definitions�K e]�(�Appe��endix�K e]�(�A.1��1�K e]�(�A.2��2�K e]�(�A.3��3�K e]�(�A.4��4�K e]�(�A.5��5�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �*Wood Pole Testing and Re-TreatmentFortisBC�j�  M�j�  (G@p�
    G@C��    G@}�Q�   G@Q1�   t�ububh)��}�(hNh]�(G@a�Q�   G@wq�p   G@�(�333G@}q�p   t�ah?]�(�Pole or stub was inspected:��e“—DATE –“ is the four- digit year the pole or stub was tested. Name of the inspection company�ehA]�]�(�Pole or stub was inspected:��e“—DATE –“ is the four-
digit year the pole or
stub was tested.
Name of the inspection
company�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �Rejected Poles �j�  M�j�  (G@a���   G@e'��   G@m��   G@f�s`   t�ububh)��}�(hNh]�(G@a�Q�   G@~XQ�   G@�(�333G@���    t�ah?]�(�NInternal decay, treated with Boron, CuNap, Fumigant: data is entered in arc fm�j  ehA]�]�(�NInternal decay, treated with
Boron, CuNap, Fumigant:
data is entered in arc fm�j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �Rejected Poles �j�  M�j�  (G@a���   G@e'��   G@m��   G@f�s`   t�ububh)��}�(hNh]�(G@a�Q�   G@_�    G@�z'bvG@k�     t�ah?]�(�UPole has been inspected, pole wrap installed at groundline: data is entered in arc fm��{Name of the inspection company or wrap manufacturer “—DATE –“ is the four- digit year the pole or stub was wrapped.�ehA]�]�(�UPole has been inspected,
pole wrap installed at
groundline: data is entered
in arc fm��{Name of the inspection
company or wrap
manufacturer
“—DATE –“ is the four-
digit year the pole or
stub was wrapped.�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  M�j�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(G@a�Q�   G@m��    G@�z'bvG@u��p   t�ah?]��(Pole requires replacement, DO NOT CLIMB:�ahA]�(]��(Pole requires replacement, DO NOT CLIMB:�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  M�j�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(G@a�Q�   G@v�z�   G@�z'bvG@~޸P   t�ah?]��rPole must be stubbed. Pole found to be weakened due to rot, insects or mechanical damage. CLIMB ONLY IF SUPPORTED:�ahA]�(]��rPole must be stubbed. Pole found to be weakened due to rot, insects or
mechanical damage. CLIMB ONLY IF SUPPORTED:�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �DO NOT CLIMB:�j�  M�j�  (G@r~�`   G@n7,�   G@x<-�   G@o��`   t�ububh)��}�(hNh]�(hhC    �P@���R�hhC   `_�K@���R�G@�      hhC   ���@���R�t�ah?]�hA]�(]�(�
Tag Colour��Year Stubbing or�e]�(K Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �FortisBC�j�  M�j�  (G@u�\    G@C��    G@x��@   G@I:��   t�ububh)��}�(hNh]�(G@c#�   G@cJ=�   G@}��   G@sh��   t�ah?]�(�
Tag Colour��Tag Installation Year��%Year Stubbing or Replacement to Occur�ehA]�(]�(�
Tag Colour��Tag Installation
Year��%Year Stubbing or
Replacement to
Occur�e]�(j  j  j  e]�(j  j  j  e]�(�Red replace�j  j  e]�(�	Blue stub�j  j  e]�(�	Green ntz�j  j  e]�(j  j  j  e]�(j  j  j  e]�(j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  M�j�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(hhC   ��L@���R�hhC   @��`@���R�G@�      hhC    ���@���R�t�ah?]�(�Aspects and Critical��Monitoring Method��Primary Responsibility��
Monitoring�ehA]�(]�(jIJ  jJJ  jKJ  jLJ  e]�(�Pole integrity evaluation��DOSS��Maintenance Planner��Ongoing�e]�(�Pole testing / re-��Field audit��Line Construction��Weekly during�e]�(�Pesticide application��Visual audit��Line Construction��Weekly during�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �E8 5 Adding a Missing Distribution Pole Pushbrace or Transmission Pole�j�  M�j�  (G@[      G@`�@   G@�A�    G@a�j�   t�ububh)��}�(hNh]�(G@[Ǯ   G@xg�   G@��L���G@��   t�ah?]�(�Aspects and Critical Activities��Monitoring Method��Primary Responsibility��Monitoring Frequency�ehA]�(]�(�Aspects and Critical
Activities��Monitoring Method��Primary Responsibility��Monitoring
Frequency�e]�(�Pole integrity evaluation��( DOSS
 Incident database
 ArcFM�� Maintenance Planner��Ongoing�e]�(�Pole testing / re-
treatment��Field audit��Line Construction
Manager��Weekly during
contract term�e]�(�"Pesticide application
and handling��Visual audit��Line Construction
Manager��Weekly during
contract term�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �H9.1   Monitoring 9.0   Monitoring, Measuring, and Continual Improvement �j�  M�j�  (G@R �   G@m?�    G@| }�   G@q�   t�ububh)��}�(hNh]�(hhC    ɜF@���R�hhC    F�R@���R�G@�      G@��     t�ah?]�(�Records��Location of Records��	Retention�ehA]�(]�(j�J  j�J  j�J  e]�(�FortisBC one-Call��Line Construction Manager's�Ne]�(�Damage claims�NNe]�(�!DOSS reports (vegetation contacts��DOSS database�Ne]�(�Pesticide application logs��Contractor's�Ne]�(�Pesticide licenses and permits��Contractor's files,if required�Ne]�(�Pesticide spill reports��Contractor's files�Ne]�(�#Pesticide transportation; handling;��Contractor's�Ne]�(�#Pre and post treatment observations��Contractor files�Ne]�(�Incident reports��Incident Management�Ne]�(�LlGiOGO��uonlraclon S mes,�Ne]�(�cid��Senior Maintenance Planner's�Ne]�(�%contractors (e.g: start-up; quarteriy��files�Ne]�(�Safe Work Plans��Maintenance Planning�K e]�(�Town/City Agreements�NNe]�(�Inspection Contractor audit (by��Line Construction Manager files�Ne]�(�&Inspection Contractor audit (in-house)��Inspection Contractor files�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �*Wood Pole Testing and Re-TreatmentFortisBC�j�  M�j�  (G@p�
    G@C��    G@}�Q�   G@Q1�   t�ububh)��}�(hNh]�(G@Y��   G@l�3�   G@�B�PהG@���    t�ah?]�(�Records��Location of Records�� Retention Classification Code(s)�ehA]�(]�(�Records��Location of Records�� Retention
Classification
Code(s)�e]�(�FortisBC one-Call��#Line Construction Manager’s
files�j  e]�(�Damage claims�j  j  e]�(�2DOSS reports (vegetation contacts
causing outages)��DOSS database�j  e]�(�Pesticide application logs��Contractor’s files,�j  e]�(�Pesticide licenses and permits��!Contractor’s files, if required�j  e]�(�Pesticide spill reports��Contractor’s files�j  e]�(�+Pesticide transportation, handling,
storage��Contractor’s files,�j  e]�(�#Pre and post treatment observations��Contractor files�j  e]�(�Incident reports��5Incident Management
database and contractor’s files�j  e]�(�Licenses and permits��!Contractor’s files, if required�j  e]�(�Meeting minutes - internal�j  j  e]�(�QMeeting minutes – meetings with
contractors (e.g. start-up, quarterly
meetings)��$Senior Maintenance Planner’s
files�j  e]�(�Road Use Agreements�j  j  e]�(�HSafe Work Plans
 Line Construction Manager
 Inspection Contractors��p Maintenance Planning
Coordinator’s files
 Maintenance Planning
Coordinator’s and contractor’s
files�j  e]�(�Town/City Agreements�j  j  e]�(�)Inspection Contractor audit (by
FortisBC)��Line Construction Manager files�j  e]�(�&Inspection Contractor audit (in-house)��Inspection Contractor files�j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �%10.0   Record Location and Retention �j�  M�j�  (G@R      G@`��   G@t)X�   G@aɝ�   t�ububh)��}�(hNh]�(hhC   @�/,@���R�K G@�      G@��     t�ah?]�hA]�(]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(�Ground Line�K e]�(�Jment is the property of Fort�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@IQ�y���G@K  fffG@��Q����G@rc����t�ah?]�(�/Appendix 2 ‐ Clearances Violation Spreadsheet�j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�/Appendix 2 ‐ Clearances Violation Spreadsheet�NNNNNNNNNNNNe]�(�Date��Time
(24Hr)��Clearance Violation Between�N�Elevation at Each Structure�N�Span Between
Structures��Conductor Type��Weather
Conditions��Ambient
Temperature�� Clearance Where
Violation Occurs��PLT��Comments�e]�(NN�Structure #��Structure #�NNNNNNNNNe]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@SnrI$�G@w�H   G@~c�   G@�734   t�ah?]�(�(on a 120 Volt base)��Minimum��Maximum�ehA]�(]�(�(on a 120 Volt base)��Minimum��Maximum�e]�(�Three Phase Voltage��115 V��127 V�e]�(�Single Phase Voltage��113 V��127 V�e]�(Xm  he minimum voltages shown above apply when the source voltage is set
nd the maximum voltages shown above apply when the source voltage is s
. The 123.5 V and 126.5 V levels reflect the typical operating range o
ubstation.
.1.3 Background
lanning assesses the need for voltage support to ensure that custo
cceptable voltage at their utilization point in accordance with CSA Stand
235-83: “Preferred Voltage Levels for AC systems 1 to 50 000 V”. Thi
utlines the recommended steady state voltage variation limits for circuits up
t the utilization point (i.e. plug in) as follows:
Table 2 – CSA Preferred Voltage Levels�NNe]�(j  �NORMAL��EXTREME�e]�(�Three Phase��110 V – 125 V��108 V – 127 V�e]�(�Single Phase��108 V – 125 V��104 V – 127 V�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �#Table 1- Planning Voltage Criteria �j�  M�j�  (G@jI�@   G@v!��   G@yOŠ   G@v��   t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  j$	  eh]�((G@Q��Z�G@p��@   G@��MyC^G@���UUUUt�(G@Q��Z�G@Rg�    G@��MyC^G@v�(�   t�eh?]�(�Summer Rating Table�j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�Summer Rating Table�NNNNNNNNNNe]�(�Conductor
Type��Normal Rating�NNNN�Emergency Rating�NNNNe]�(N�Ampacity
(Amps)��MVA by Voltage�NNN�Ampacity
(Amps)��MVA By Voltage�NNNe]�(NN�25kV
LL
3Ø��14.4kV
LG
1Ø��13kV
LL
3Ø��7.2kV
LG
1Ø�N�25kV
LL
3Ø��14.4kV
LG
1Ø��13kV
LL
3Ø��7.2kV
LG
1Ø�e]�(�#8 Cu��92��4.0��2.3��2.1��1.2��102��4.4��2.5��2.3��1.3�e]�(�#6 Cu��125��5.4��3.1��2.8��1.6��136��5.9��3.4��3.1��1.8�e]�(�#4 Cu��164��7.1��4.1��3.7��2.1��182��7.9��4.6��4.1��2.4�e]�(�#3 Cu��192��8.3��4.8��4.3��2.5��210��9.1��5.3��4.8��2.8�e]�(�2ACSR��180��7.7��4.0��4.1��2.3��196��8.5��4.9��4.4��2.5�e]�(�#2 Cu��222��9.6��5.5��5.0��2.9��245��10.6��6.1��5.5��3.2�e]�(�2/0 ACSR��278��12.0��7.0��6.3��3.6��366��15.8��9.2��8.2��4.8�e]�(�3/0 ACSR��321��13.9��8.0��7.2��4.2��425��18.4��10.6��9.6��5.5�e]�(�90 KCMIL Cu��256��11.1��6.4��5.8��3.3��282��12.2��7.1��6.3��3.7�e]�(�266 ACSR��429��18.6��10.7��9.7��5.6��572��24.8��14.3��12.9��7.4�e]�(�336 ACSR��496��21.5��12.4��11.2��6.4��664��28.8��16.6��15.0��8.6�e]�(�397 ACSR��550��23.8��13.8��12.4��7.2��739��32.0��18.5��16.6��9.6�e]�(�477 ACSR��609��26.4��15.2��13.7��7.9��819��35.5��20.5��18.4��10.6�e]�(�927 AAC��912��39.5��22.8��20.5��11.9��1022��44.3��25.6��23.0��13.3�e]�(�Winter Rating Table�NNNNNNNNNNe]�(�Conductor
Type��Normal Rating�NNNN�Emergency Rating�NNNNe]�(N�Ampacity
(Amps)��MVA by Voltage�NNN�Ampacity
(Amps)��MVA By Voltage�NNNe]�(NN�25kV
LL
3Ø��14.4kV
LG
1Ø��13kV
LL
3Ø��7.2kV
LG
1Ø�N�25kV
LL
3Ø��14.4kV
LG
1Ø��13kV
LL
3Ø��7.2kV
LG
1Ø�e]�(�#8 Cu��127��5.5��3.2��2.9��1.7��134��5.8��3.3��3.0��1.7�e]�(�#6 Cu��171��7.4��4.3��3.8��2.2��178��7.7��4.4��4.0��2.3�e]�(�#4 Cu��229��9.9��5.7��5.1��2.9��238��10.3��5.9��5.4��3.1�e]�(�#3 Cu��264��11.4��6.6��5.9��3.4��276��11.9��6.9��6.2��3.6�e]�(�2ACSR��248��10.7��6.2��5.6��3.2��285��12.3��7.1��6.4��3.7�e]�(�#2 Cu��305��13.2��7.6��6.9��4.0��319��13.8��8.0��7.2��4.2�e]�(�2/0 ACSR��385��16.7��9.6��8.7��5.0��444��19.2��11.1��10.0��5.8�e]�(�3/0 ACSR��446��19.3��11.2��10.0��5.8��516��22.3��12.9��11.6��6.7�e]�(�90 KCMIL Cu��353��15.3��8.8��7.9��4.6��370��16.0��9.3��8.3��4.8�e]�(�266 ACSR��598��25.9��15.0��13.5��7.8��695��30.1��17.4��15.6��9.0�e]�(�336 ACSR��693��30.0��17.3��15.6��9.0��806��34.9��20.2��18.1��10.5�e]�(�397 ACSR��770��33.3��19.3��17.3��10.0��898��38.9��22.5��20.2��11.7�e]�(�477 ACSR��854��37.0��21.4��19.2��11.1��995��43.1��24.9��22.4��12.9�e]�(�927 AAC��1288��55.8��32.2��29.0��16.7��1357��58.8��33.9��30.6��17.6�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �/Table 3 – Overhead Conductor Ampacity Limits �j�  M�j�  (G@e`     G@m�s�   G@{��`   G@oys�   t�ububh)��}�(hNh]�(G@Y+�$   G@|&�H   G@~��   G@~���   t�ah?]�(�Voltage Regulators��100% of Nameplate Rating�ehA]�(]�(�Voltage Regulators��100% of Nameplate Rating�e]�(�Switches and Cutouts��100% of Continuous Rating�e]�(�!Distribution Service Transformers��100% of Nameplate Rating�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �/Table 2.3.3 – Distribution Equipment Capacity�j�  M�j�  (G@e��    G@zX��   G@{G̀   G@{��   t�ububh)��}�(hNh]�(hhC    N�m@���R�hhC   ���B@���R�G@�      G@��     t�ah?]�(NNNNNNNNNNNNNNN�BUR��Buried IN]OUCT��(C)�NN�LOAD��CABLE��IR�ehA]�(]�(NNNNNNNNNNNNNNNj#M  j$M  j%M  NNj&M  j'M  j(M  e]�(�STUD��2��Aeeeiene��Mdae��GROuuin6��DuctSize��PHASES��CONDUCTC��	CONDUCTOR��CONFIGURATION��NUR��MBER��Vf - 1��0��1��U��U��U��=��2.6��VF =��U =��UF�e]�(K �12 Cu��U��534-3i03��BOTH��5��R�NN�1��ILE��1��166��203��3��171��191��208��175��213��136��162��184�e]�(K �12��V��534-3103��BOTHENDS��
PVC FOR UG��R�NN�3��IBLE��1��165��201��11��173��193��210��170��218��147��175��199�e]�(K �11��V��534-4102��	Bo1H ENOS��5��R�NN�1��IBuE��1��190��231��1��195��218��238��200��244��155��186��211�e]�(N�11��V��534-4102��	B01H}ENDS��PvC FOR��3�NN�3 CABLE��IBLE��11��186��226��6��195��217��236��202��245��167��199��225�e]�(�5��1i A��25��534 4103��	BOTH}ENOS��3��R�NN�1��BLE��1��149��181��1��153��171��187��157��191��122��146��186�e]�(�6��M1 N��25��534-4103��	BOTH}ENDS��4"��3�NN�3��'B48��1��144��175��5��151��168��183��157��190��130��154��174�e]�(�7��350 A��V��534-3104��	BOTH_ENDS��3��3�NN�1��BLE��3��334��412��2��356��401��440��378��488��323��390�K e]�(�8��350��1��534-3104��	BO1H ENDS��4��3�NN�3��JBLE��1��322��391��1��339��379��413��355��433��298��356��404�e]�(�9��500 N��V��534-4109��
BO1H  ENDS��4��3�NN�1��VBLE��3��389��483��3��418��473��520��447��557��405��490��562�e]�(�10��500��1��514-4109��ONE��
pvC FOR UG��3�NN�1��9Le��3��473��570��0��506��565��611��538��851��467��556��629�e]�(�11��750 N��J��534-3105��BOTH��
PvC FOR Ug��3�N�1��1��ILe��3��428��536��6��463��526��582��499��628��460��562��650�e]�(�12��750 A��J��534-3105��ONE]END��PvC FOR��3�NN�Cable��'aL€��3��589��711��1��633��705��766��677��821��592��707��803�e]�(�13��750 A��4��534-4111��	Bo1X ENDS��PvC��3��1��1��1��B4��3��437��545��5��474��537��592��512��640��468��588��654�e]�(�14��750 N��1��534-4111��ONE END��47��3�NN�1��BLE��3��590��709��9��635��705��784��679��819��588��700��791�e]�(�15��1000��4��534-3107��BO1H��
PVC FoR UC��3�NN�1��BL��3��470��589��9��511��581��643��554��698��517��631��732�e]�(�16��1000��4��534-3107��ONEEND��Pvc��3�NN�1��BL��3��706��852��2��762��849��923��020��995��722��862��979�e]�(K �1000��u��534-4108��	BO1H_ENDS��PvC FOR��3�NN�1��BLE��3��529��656��8��576��651��717��625��780��383��710��821�e]�(K �I0o0�N�534-4108��ONE END��
PvC For UG��3�NN�1��BL€��3��079��1060��0��947��1054��114��1015��1229��894��1085��1207�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(hhC   �L=@���R�hhC    �i@���R�G@�      hhC    �G�@���R�t�ah?]�(�Date��Rev��Author��Approved�ehA]�(]�(j�N  j�N  j�N  j�N  e]�(�
03/29/2011��0��DK,�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@P�p����G@����   G@��   G@�K38   t�ah?]�(�Date��Rev��Description��Author��Approved�ehA]�(]�(�Date��Rev��Description��Author��Approved�e]�(�
03/29/2011�j.  �5Distribution overhead and underground system
programs��
DK, BM, DM��GW, MA�e]�(j  j  j  j  j  e]�(j  j  j  j  j  e]�(j  j  j  j  j  e]�(j  NNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �-Document 801-02 Distribution System Programs �j�  M�j�  (G@ks3    G@yy��   G@x��    G@{�    t�ububh)��}�(hNh]�(G@b��@   G@sq��   G@c�\    G@uI�   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �,Assessing Vegetation Control Responsibility:�j�  M�j�  (G@ZG�    G@qa`   G@sa�   G@rp@   t�ububh)��}�(hNh]�(G@[�|�333G@x��   G@]�)    G@z�G�   t�ah?]�j  ahA]�(]�j  a]�j  a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �,Assessing Vegetation Control Responsibility:�j�  M�j�  (G@ZG�    G@qa`   G@sa�   G@rp@   t�ububh)��}�(hNh]�(hhC   �y;@���R�hhC   ��Q@���R�G@�      G@��     t�ah?]�hA]�(]�(K �2��20�e]�(N�9.2.2�K e]�(�9.3��3�K e]�(N�9.3.1�K e]�(�10.0��Record Location and Retention�K e]�(�11.0��Definitions�K e]�(�Apper��endix A�K e]�(�A.1��1�K e]�(�A.2��2�K e]�(�A.3��3�K e]�(�A.4��4�K e]�(�A.5��5�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �*Wood Pole Testing and Re-TreatmentFortisBC�j�  M�j�  (G@p�
    G@C��    G@}�Q�   G@Q1�   t�ububh)��}�(hNh]�(G@a�Q�   G@wq�p   G@�(�333G@}q�p   t�ah?]�(�Pole or stub was inspected:��e“—DATE –“ is the four- digit year the pole or stub was tested. Name of the inspection company�ehA]�]�(�Pole or stub was inspected:��e“—DATE –“ is the four-
digit year the pole or
stub was tested.
Name of the inspection
company�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  j�  )��}�(j�  �Rejected Poles �j�  M j�  (G@a���   G@e'��   G@m��   G@f�s`   t�ububh)��}�(hNh]�(G@a�Q�   G@~XQ�   G@�(�333G@���    t�ah?]�(�NInternal decay, treated with Boron, CuNap, Fumigant: data is entered in arc fm�j  ehA]�]�(�NInternal decay, treated with
Boron, CuNap, Fumigant:
data is entered in arc fm�j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M j�  j�  )��}�(j�  �Rejected Poles �j�  M j�  (G@a���   G@e'��   G@m��   G@f�s`   t�ububh)��}�(hNh]�(G@a�Q�   G@_�    G@�z'bvG@k�     t�ah?]�(�UPole has been inspected, pole wrap installed at groundline: data is entered in arc fm��{Name of the inspection company or wrap manufacturer “—DATE –“ is the four- digit year the pole or stub was wrapped.�ehA]�]�(�UPole has been inspected,
pole wrap installed at
groundline: data is entered
in arc fm��{Name of the inspection
company or wrap
manufacturer
“—DATE –“ is the four-
digit year the pole or
stub was wrapped.�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  Mj�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(G@a�Q�   G@m��    G@�z'bvG@u��p   t�ah?]��(Pole requires replacement, DO NOT CLIMB:�ahA]�(]��(Pole requires replacement, DO NOT CLIMB:�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  Mj�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(G@a�Q�   G@v�z�   G@�z'bvG@~޸P   t�ah?]��rPole must be stubbed. Pole found to be weakened due to rot, insects or mechanical damage. CLIMB ONLY IF SUPPORTED:�ahA]�(]��rPole must be stubbed. Pole found to be weakened due to rot, insects or
mechanical damage. CLIMB ONLY IF SUPPORTED:�a]�j  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �DO NOT CLIMB:�j�  Mj�  (G@r~�`   G@n7,�   G@x<-�   G@o��`   t�ububh)��}�(hNh]�(hhC    �P@���R�hhC   `_�K@���R�G@�      hhC   ���@���R�t�ah?]�hA]�(]�(�
Tag Colour��Year Stubbing or�e]�(K Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �FortisBC�j�  Mj�  (G@u�\    G@C��    G@x��@   G@I:��   t�ububh)��}�(hNh]�(G@c#�   G@cJ=�   G@}��   G@sh��   t�ah?]�(�
Tag Colour��Tag Installation Year��%Year Stubbing or Replacement to Occur�ehA]�(]�(�
Tag Colour��Tag Installation
Year��%Year Stubbing or
Replacement to
Occur�e]�(j  j  j  e]�(j  j  j  e]�(�Red replace�j  j  e]�(�	Blue stub�j  j  e]�(�	Green ntz�j  j  e]�(j  j  j  e]�(j  j  j  e]�(j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �bRevision Date: March 24, 2010 Version No: 7 Document No: M10-04 Wood Pole Testing and Re-Treatment�j�  Mj�  (G@R     G@L9�    G@��D`   G@V�.�   t�ububh)��}�(hNh]�(hhC   ��L@���R�hhC   @��`@���R�G@�      hhC    ���@���R�t�ah?]�(�Aspects and Critical��Monitoring Method��Primary Responsibility��
Monitoring�ehA]�(]�(j�O  j�O  j�O  j�O  e]�(�Pole integrity evaluation��DOSS��Maintenance Planner��Ongoing�e]�(�Pole testing / re-��Field audit��Line Construction��Weekly during�e]�(�Pesticide application��Visual audit��Line Construction��Weekly during�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �E8 5 Adding a Missing Distribution Pole Pushbrace or Transmission Pole�j�  Mj�  (G@[      G@`�@   G@�A�    G@a�j�   t�ububh)��}�(hNh]�(G@[Ǯ   G@xg�   G@��L���G@��   t�ah?]�(�Aspects and Critical Activities��Monitoring Method��Primary Responsibility��Monitoring Frequency�ehA]�(]�(�Aspects and Critical
Activities��Monitoring Method��Primary Responsibility��Monitoring
Frequency�e]�(�Pole integrity evaluation��( DOSS
 Incident database
 ArcFM�� Maintenance Planner��Ongoing�e]�(�Pole testing / re-
treatment��Field audit��Line Construction
Manager��Weekly during
contract term�e]�(�"Pesticide application
and handling��Visual audit��Line Construction
Manager��Weekly during
contract term�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �H9.1   Monitoring 9.0   Monitoring, Measuring, and Continual Improvement �j�  Mj�  (G@R �   G@m?�    G@| }�   G@q�   t�ububh)��}�(hNh]�(hhC    ɜF@���R�hhC    F�R@���R�G@�      G@��     t�ah?]�(�Records��Location of Records��	Retention�ehA]�(]�(j)P  j*P  j+P  e]�(�FortisBC one-Call��Line Construction Manager's�Ne]�(�Damage claims�NNe]�(�!DOSS reports (vegetation contacts��DOSS database�Ne]�(�Pesticide application logs��Contractor's�Ne]�(�Pesticide licenses and permits��Contractor's files,if required�Ne]�(�Pesticide spill reports��Contractor's files�Ne]�(�#Pesticide transportation; handling;��Contractor's�Ne]�(�#Pre and post treatment observations��Contractor files�Ne]�(�Incident reports��Incident Management�Ne]�(�LlGiOGO��uonlraclon S mes,�Ne]�(�cid��Senior Maintenance Planner's�Ne]�(�%contractors (e.g: start-up; quarteriy��files�Ne]�(�Safe Work Plans��Maintenance Planning�K e]�(�Town/City Agreements�NNe]�(�Inspection Contractor audit (by��Line Construction Manager files�Ne]�(�&Inspection Contractor audit (in-house)��Inspection Contractor files�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �*Wood Pole Testing and Re-TreatmentFortisBC�j�  Mj�  (G@p�
    G@C��    G@}�Q�   G@Q1�   t�ububh)��}�(hNh]�(G@Y��   G@l�3�   G@�B�PהG@���    t�ah?]�(�Records��Location of Records�� Retention Classification Code(s)�ehA]�(]�(�Records��Location of Records�� Retention
Classification
Code(s)�e]�(�FortisBC one-Call��#Line Construction Manager’s
files�j  e]�(�Damage claims�j  j  e]�(�2DOSS reports (vegetation contacts
causing outages)��DOSS database�j  e]�(�Pesticide application logs��Contractor’s files,�j  e]�(�Pesticide licenses and permits��!Contractor’s files, if required�j  e]�(�Pesticide spill reports��Contractor’s files�j  e]�(�+Pesticide transportation, handling,
storage��Contractor’s files,�j  e]�(�#Pre and post treatment observations��Contractor files�j  e]�(�Incident reports��5Incident Management
database and contractor’s files�j  e]�(�Licenses and permits��!Contractor’s files, if required�j  e]�(�Meeting minutes - internal�j  j  e]�(�QMeeting minutes – meetings with
contractors (e.g. start-up, quarterly
meetings)��$Senior Maintenance Planner’s
files�j  e]�(�Road Use Agreements�j  j  e]�(�HSafe Work Plans
 Line Construction Manager
 Inspection Contractors��p Maintenance Planning
Coordinator’s files
 Maintenance Planning
Coordinator’s and contractor’s
files�j  e]�(�Town/City Agreements�j  j  e]�(�)Inspection Contractor audit (by
FortisBC)��Line Construction Manager files�j  e]�(�&Inspection Contractor audit (in-house)��Inspection Contractor files�j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �%10.0   Record Location and Retention �j�  Mj�  (G@R      G@`��   G@t)X�   G@aɝ�   t�ububh)��}�(hNh]�(hhC   @�/,@���R�K G@�      G@��     t�ah?]�hA]�(]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(K K e]�(�Ground Line�K e]�(�Jment is the property of Fort�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M	j�  Nubh)��}�(hNh]�(G@IQ�y���G@K  fffG@��Q����G@rc����t�ah?]�(�/Appendix 2 ‐ Clearances Violation Spreadsheet�j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�/Appendix 2 ‐ Clearances Violation Spreadsheet�NNNNNNNNNNNNe]�(�Date��Time
(24Hr)��Clearance Violation Between�N�Elevation at Each Structure�N�Span Between
Structures��Conductor Type��Weather
Conditions��Ambient
Temperature�� Clearance Where
Violation Occurs��PLT��Comments�e]�(NN�Structure #��Structure #�NNNNNNNNNe]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  Nubh)��}�(hNh]�(G@<�v�m�G@Ls3*���G@�Q��I%G@~"^3333t�ah?]�(ja
  �Generation Projects��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�ehA]�(]�(ja
  �Generation Projects��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�e]�(j�  �Physical Infrastructure�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(j�  �1All Plants Concrete and Structural Rehabilitation��570��617��647��665��686��667��669��705��710��787��801��3,112��778��794��843��871��900��912��906��999�e]�(j�  �#Upper Bonnington Spill Gate Rebuild��1,085�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�55��CLLoowweerr BBoonnnniinnggttoonn PPoowweerrhhoouussee WWiinnddoowwss��336666��88�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(j�  �@Upper Bonnington, South Slocan and Corra Linn Powerhouse Windows�j�  �430�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(j�  �:Corra Linn Spillway Concrete and Spill Gate Rehabilitation�j�  j�  �7,874��865��1,786��1,728��1,728��1,828��1,840��2,055��1,046�j�  j�  j�  j�  �1,724��1,783��1,806��1,787�j�  e]�(j�  �5Upper Bonnington Overflow Spillway Concrete Resurface�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �5,833��6,006��6,190��6,282��6,266�j�  e]�(j�  �%South Slocan Spillway Concrete Repair�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �10,519��10,278��10,463��11,110�j�  j�  j�  j�  j�  e]�(�1100��?AAllll PPllaannttss SSuuppeerrssttrruuccttuurree UUppggrraaddee��--��--��--��--��--��--��--��--��--��--��553366��552299��551133��552200��555533��557722��559933��660033��660000��--�e]�(�11��#Lower Bonnington Spill Gate Rebuild�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �1,779�j�  j�  j�  j�  j�  j�  j�  j�  e]�(�12��'Remaining Powerhouse Window Replacement�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �958��989��1,002�j�  j�  e]�(�13��&Total Physical Infrastructure Projects��2,021��1,055��8,521��1,530��2,472��2,395��2,397��2,532��2,551��2,843��3,000��16,551��12,165��12,384��18,338��10,131��10,455��10,605��9,560��999�e]�(�14�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�1155��CMMeecchhaanniiccaall aanndd EElleeccttrriiccaall EEqquuiippmmeenntt�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�16�� Corra Linn Unit 2 Life Extension��3,423�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�17��All Plants Station Service��672�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�18��=Lower Bonnington and Upper Bonnington Plant Totalizer Upgrade��90�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�19��Corra Linn Unit 3 Completion��722�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�20��1Upper Bonnington OOld Plant Various Unit Upgrades��1,311�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�21��CUpper Bonnington, Lower Bonnington and Corra Linn Plants Automation�j�  j�  �283��291��301�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�22��"All Plants Heating and Ventilation�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �895��496��858��485��927��528��993��555��2,973�j�  e]�(�23��$Upper Bonnington Old Unit Repowering�j�  j�  j�  j�  j�  �6,961��14,998��17,506��16,099��1,516�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�24�� All Plants Fire Water Suppppl yy�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �618��612��595��606�j�  j�  j�  j�  j�  j�  e]�(�25�� Mechanical Equipment Replacement�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �2,060��2,279�e]�(�26�� Electronic Equipment Replacement�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �2,060��2,279�e]�(�27��"Corra Linn Unit 3 Generator Rewind�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �347��3,997�j�  j�  j�  j�  j�  e]�(�28��%Corra Linn Unit 3 Turbine Replacement�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �347��3,627�j�  j�  j�  j�  j�  e]�(�2299��QUUppppeerr BBoonnnniinnggttoonn UUnniitt 66 TTuurrbbiinnee RReeppllaacceemmeenntt�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �334477��
33,,662277�j�  j�  j�  j�  j�  e]�(�30��)Total Mechanical and Electrical Equipment��6,218�j�  �283��291��301��6,961��14,998��17,506��16,099��1,516��1,512��1,108��1,454��2,131��12,179��528��993��555��7,094��4,559�e]�(�31�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�32��Dam, Public and Worker Safety�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�33��=Upper Bonnington, Lower Bonnington and Corra Linn Fire Panels��250��259��264�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�3344��8AAllll PPllaannttss SSaaffeettyy aanndd SSeeccuurriittyy��447711��447755��442244��443377��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--�e]�(�35��All Plants Fire Safety�j�  j�  �1,001��1,031��1,065��738�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�36��$All Plants Surveillance and Security�j�  j�  j�  j�  j�  �1,452��1,452��1,538��1,549�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�37��Dam Safety Instrumentation�j�  j�  j�  j�  j�  �581��580��613��618�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�38��(Total Dam, Public Worker Safety Projects��721��734��1,689��1,468��1,065��2,771��2,031��2,151��2,167�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�3399�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�40��All Plants Minor Sustainment�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�41��$All Plants Minor Sustainment Capital��1,171��1,158��1,203��1,144��1,182��1,142��1,141��1,208��1,216��1,361��1,385��1,371��1,335��1,361��1,452��1,504��1,555��1,575��1,560��1,735�e]�(�42��+Total All Plants Minor Sustainment Projects��1,171��1,158��1,203��1,144��1,182��1,142��1,141��1,208��1,216��1,361��1,385��1,371��1,335��1,361��1,452��1,504��1,555��1,575��1,560��1,735�e]�(�43�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�44��Total Generation Projects��10,131��2,947��11,696��4,433��5,019��13,269��20,567��23,397��22,033��5,720��5,279��18,418��14,358��15,270��31,970��12,163��13,004��12,736��18,213��7,293�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �599�j�  Mj�  (G@e�/    G@K�A�   G@|�@   G@N`    t�ububh)��}�(hNh]�(G@<����:G@Ls3*���G@�Q���!G@����   t�ah?]�(�45��Transmission Growth��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�ehA]�(]�(�45��Transmission Growth��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�e]�(�46��#Okanagan Transmission Reinforcement��2,219�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e�p      ]�(�47��$Ellison to Sexsmith Transmission Tie��7,122��413�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�48��<Grand Forks Transformer Addition - Option 1 - Single Breaker��2,491��4,714��1,274��7,549�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�4499��9KKeelloowwnnaa BBuullkk CCaappaacciittyy AAddddiittiioonn�j�  �
33,,772200��1100,,883322��1111,,001144�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�50��*42 Line Meshed Operation (Huth and Oliver)�j�  j�  �278�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�51��Capacitors at Bentley Terminal�j�  j�  j�  �875��4,389�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�52��Reconductor 52 Line & 53 Line�j�  j�  j�  �875��1,479��2,288��2,115�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�53��Meshing Kelowna Loop�j�  j�  �2,753��2,798��2,577�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�5544��OSSuummmmeerrllaanndd SSuubbssttaattiioonn TTrraannssffoorrmmeerr UUppggrraaddee��--��--��
22 ,115522��
44 ,442277��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--�e]�(�55��Beaver Valley South Solution�j�  j�  j�  j�  �758��10,463��10,313�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�56��,RG Anderson Distribution Transformer Upgrade�j�  j�  j�  j�  �3,031��4,061�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�57��DG Bell Static VAR Compensator�j�  j�  j�  j�  �3,031��14,486��19,799�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�58��DG Bell 230 kV Ring Bus�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �3,554��13,573��18,256�j�  e]�(�5599��DDDGG BBeellll SSeeccoonndd 223300//113388kkVV TTrraannssffoorrmmeerr�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �
22 ,000088��
66 ,118888��1100 ,555500�j�  e]�(�60��'Vaseux Lake Third 500/230kV Transformer�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �1,777��11,787��18,017�j�  j�  j�  j�  j�  j�  e]�(�61��Boundary Area Supply�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �1,536��8,093�j�  j�  j�  j�  j�  j�  e]�(�62��"Reconductor 31 Line (Creston Area)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �2,307�j�  e]�(�63��5Stoney Creek Second Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �17,328�j�  j�  j�  j�  j�  j�  j�  e]�(�64��/Playmor 25 kV Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �1,697��3,899��10,016�j�  j�  j�  j�  e]�(�65��(Reconductor 50 Line (Recreation-Saucier)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �295�j�  j�  j�  j�  j�  j�  j�  e]�(�66��,Reconductor 50 Line (FA Lee-Springfield Tap)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�67��2Reconductor 51 Line & 60 Line (DG Bell-OK Mission)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �4,608��4,576�j�  e]�(�6688��SRReeccoonndduuccttoorr 5544 LLiinnee ((DDGG BBeellll -BBllaacckk MMoouunnttaaiinn))�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�69��(FA Lee Distribution Transformer Addition�j�  j�  j�  j�  j�  �5,713��6,316�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�70��New Enterprise Substation�j�  j�  j�  j�  j�  j�  �9,871��12,608��13,319�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�71��1Sexsmith Second Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  �3,072��6,305�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�72��0Saucier Second Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  j�  j�  �3,566��3,848�j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�7733��`BBeennvvoouulliinn SSeeccoonndd DDiissttrriibbuuttiioonn TTrraannssffoorrmmeerr AAddddiittiioonn��--��--��--��--��--��--��--��--��--��--��--��--��--��--��
33 ,778855��
77 ,996666��--��--��--��--�e]�(�74��0Ellison Second Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �2,839��5,011�j�  j�  j�  j�  e]�(�75��3Duck Lake to New North Kelowna Substation (78 Line)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�76��)DG Bell Distribution Transformer Addition�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �10,041�j�  e]�(�77��2New North Kelowna Substation to Sexsmith (80 Line)�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�7788��5NNeeww CCeennttrraall OOkkaannaaggaann SSttaattiioonn�j�  j�  j�  j�  j�  �1144 ,228833��1155 ,005599��
77 ,666622�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�79��Creston Area Capacity Increase�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  �7,108��6,909�j�  j�  j�  j�  j�  j�  j�  e]�(�80��Total Transmission Growth��11,832��8,847��17,287��27,537��15,265��51,293��63,474��23,343��19,624��3,566��3,848��8,885��37,854��27,807��10,523��22,993��5,562��24,369��45,730�j�  e]�(�81�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�82��Transmission Sustainment�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�83��&Transmission Line Condition Assessment��522��485��480��547��543��543��457��614��583��628��770��732��597��654��613��771��768��711��841��935�e]�(�84�� Transmission Line Rehabilitation��3,372��2,621��2,509��2,424��2,820��2,562��2,696��2,481��3,053��3,353��3,130��3,753��3,437��3,039��3,601��3,089��3,965��3,809��3,446��4,744�e]�(�85�� Transmission Line Urgent Repairs��594��620��616��622��661��627��616��667��668��764��773��766��726��739��815��831��863��868��853��967�e]�(�86��(Transmission Line Right of Way Easements��400��400��416��423��440��415��409��446��443��518��524��516��484��493��549��560��583��586��572��658�e]�(�87��06 Line /26 Line River Crossingg Reconfigguration��1,,185�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�88��"27 Line Rebuild (Corra Linn-Salmo)��1,161�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�89��'21-24 Lines Rebuild (Generation Plants)��2,219�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�90��19 Line/29 Line Reconfiguration�j�  �791�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�91��)20 Line Rebuild (Warfield Terminal-Salmo)�j�  �4,664�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�9922��<3300 LLiinnee LLaakkee CCrroossssiinngg AAsssseessssmmeenntt��--��--��--��880022��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--�e]�(�93��$30 Line Lake Crossing Rehabilitation�j�  j�  j�  j�  �1,521�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�94��Total Transmission Sustainment��9,453��9,581��4,021��4,818��5,984��4,147��4,179��4,208��4,747��5,262��5,197��5,767��5,244��4,925��5,579��5,250��6,178��5,974��5,711��7,304�e]�(�95��)Total Transmission Growth and Sustainment��21,285��18,428��21,308��32,355��21,249��55,440��67,653��27,551��24,371��8,828��9,046��14,652��43,099��32,732��16,101��28,243��11,740��30,343��51,441��7,304�e]�(�96�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�9977��%SSttaattiioonn SSuussttaaiinnmmeenntt�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�98��)Environmental Compliance (PCB Mitigation)��11,269��11,553��4,574�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�99��Station Urgent Repairs��818��907��879��977��942��930��907��997��975��1,135��1,142��1,133��1,071��1,094��1,204��1,229��1,274��1,285��1,260��1,429�e]�(�100��)Station Assessment/Minor Planned Projects��1,343��1,354��1,410��1,433��1,489��1,405��1,388��1,510��1,500��1,750��1,772��1,745��1,639��1,669��1,859��1,894��1,972��1,982��1,936��2,225�e]�(�101��7Add Arc Flash Detection to Legacy Metal-Clad Switchgear��539��544��566��1,140��1,184�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�110022��9HHu tthh LLow VVo llttage BBreakker RRepllacementt ((22))�j�  �6699��555500�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�103��&Switchgear Replacement Program (13 kV)�j�  j�  �1,651�j�  �983�j�  �1,645�j�  �1,006�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�104��Ground Grid Upgrades�j�  j�  �748�j�  �790�j�  �741�j�  �799�j�  �937�j�  �872�j�  �984�j�  �1,043�j�  �1,028�j�  e]�(�105��7DG Bell 138 kV Breaker and Voltage Transformer Addition�j�  j�  �338��938�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�106��'Osoyyoos 63 kV Breaker Additions ((2 ))�j�  j�  j�  �364��2,359�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�107��Bulk Oil Breaker Replacements�j�  j�  j�  �733��761��720��712��773��768�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�108��Station Oil Containment�j�  j�  j�  �445��462��852��433��470��910�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�109��'Minimum Oil Circuit Breaker Replacement�j�  j�  j�  j�  j�  �1,134��1,121��1,219��1,211��1,411��1,429��1,408��1,323��1,348��1,499��1,528��1,590��1,598��1,562��1,793�e]�(�110��+Major Transmission Transformer Replacements�j�  j�  j�  j�  j�  j�  j�  �1,536��7,971�j�  j�  j�  �1,613��8,778�j�  j�  j�  �1,953��10,166�j�  e]�(�111111��HDDiissttrriibbuuttiioonn TTrraannssffoorrmmeerr RReeppllaacceemmeennttss�j�  j�  j�  j�  j�  �771144��
22,,333388�j�  j�  j�  �887733��
22,,887733�j�  j�  j�  �993333��
33,,224466�j�  j�  j�  e]�(�112��Station Sustainment Total��13,969��14,427��10,716��6,030��8,970��5,756��9,283��6,505��15,139��4,297��6,152��7,159��6,518��12,889��5,546��5,583��9,126��6,817��15,952��5,447�e]�(�113�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�114��(Total Transmission and Stations Projects��35,254��32,854��32,024��38,385��30,220��61,195��76,936��34,056��39,510��13,125��15,197��21,812��49,617��45,622��21,647��33,826��20,866��37,161��67,393��12,751�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �447�j�  Mj�  (G@��Z�   G@K�A�   G@�[�@   G@NP    t�ububh)��}�(hNh]�(G@<����:G@Ls3*���G@�Q���!G@�N��   t�ah?]�(�115��Distribution Growth��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�ehA]�(]�(�115��Distribution Growth��2012��2013��2014��2015��2016��2017��2018��2019��2020��2021��2022��2023��2024��2025��2026��2027��2028��2029��2030��2031�e]�(�116��New Connects System Wide��11,057��10,780��11,446��11,536��12,076��11,298��11,226��12,182��12,117��14,131��14,324��14,094��13,236��13,474��15,017��15,296��15,931��16,008��15,638��17,982�e]�(�117��Small Growth Projects��1,069��888��1,321��1,752��1,523��1,439��1,423��1,546��1,536��1,788��1,810��1,784��1,679��1,710��1,900��1,936��2,015��2,025��1,981��2,270�e]�(�118��Distribution Unplanned Growth��924��930��1,031��1,033��1,044��1,007��1,005��1,077��1,072��1,241��1,256��1,239��1,173��1,195��1,320��1,345��1,398��1,407��1,380��1,569�e]�(�111199��VGGlleennmmeerrrryy FFeeeeddeerr 22- GGlleennmmeerrrryy FFeeeeddeerr 11 TTiiee LLiinnee��559966�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�120��)Ellison Feeder 2 to Sexsmith Feeder 1 Tie�j�  �1,161�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�121��Hollywood Feeder 5 Upgrades�j�  j�  �1,172�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�122��"Kaleden Feeder 1 Capacity Upgrades�j�  j�  �1,330�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�123��$Grand Forks Terminal Feeder Addition�j�  j�  j�  j�  �4,530�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�112244��KKKeettttllee VVaalllleeyy ttoo NNkk 'MMiipp DDiissttrriibbuuttiioonn TTiiee��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��--��
77 ,669999��--�e]�(�125��DG Bell Feeder 4 Addition�j�  j�  j�  j�  j�  j�  �2,115�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�126��Total Distribution Growth��13,646��13,759��16,300��14,320��19,172��13,744��15,770��14,805��14,725��17,159��17,389��17,117��16,088��16,379��18,237��18,577��19,344��19,440��26,698��21,821�e]�(�127�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�128��Distribution Sustainment�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�112299��4DDiissttrriibbuuttiioonn UUrrggeenntt RReeppaaiirrss��
22 ,441111��
22 ,331155��
22 ,448800��
22 ,660066��
22 ,660055��
22 ,449933��
22 ,449955��
22 ,669933��
22 ,666655��
33 ,007722��
33 ,111144��
33 ,007777��
22 ,991177��
22 ,997755��
33 ,227766��
33 ,333377��
33 ,446677��
33 ,449911��
33 ,442288��
33 ,888855�e]�(�130��&Distribution Line Condition Assessment��1,410��1,398��1,530��1,509��1,569��1,549��1,472��1,635��1,574��1,810��1,924��1,840��1,726��1,841��1,976��2,052��2,073��2,048��2,101��2,348�e]�(�131�� Distribution Line Rehabilitation��5,298��3,517��3,592��3,840��3,865��3,674��3,816��4,164��4,049��4,506��4,487��4,661��4,279��4,360��5,047��5,189��5,268��5,116��4,934��5,899�e]�(�132��Distribution Line Rebuilds��1,679��1,660��2,214��2,251��2,335��2,222��2,203��2,379��2,370��2,726��2,762��2,731��2,588��2,636��2,907��2,962��3,077��3,098��3,041��3,451�e]�(�133��'Distribution Line Small Planned Capital��726��826��853��867��870��858��839��908��899��1,046��1,054��1,043��986��1,006��1,110��1,131��1,176��1,183��1,160��1,320�e]�(�134��Forced Upgrades and Lines Moves��2,012��2,413��2,382��2,144��2,462��2,339��2,260��2,425��2,475��2,822��2,851��2,822��2,680��2,719��3,006��3,064��3,185��3,203��3,143��3,575�e]�(�135��:41 Line Salvage and Distribution Underbuild Rehabilitation��2,067�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�136��)Environmental Compliance (PCB Mitigation)�j�  j�  j�  j�  j�  �1,611��1,598��1,724��1,718��1,973��1,999��1,977��1,876��1,911�j�  j�  j�  j�  j�  j�  e]�(�137��Total Distribution Sustainment��15,603��12,129��13,051��13,216��13,706��14,746��14,683��15,928��15,751��17,955��18,191��18,151��17,052��17,447��17,321��17,737��18,246��18,140��17,807��20,479�e]�(�138��Total Distribution Projjects��29,,249��25,,889��29,,351��27,,537��32,,878��28,,489��30,,453��30,,733��30,,476��35,,115��35,,580��35,,267��33,,140��33,,826��35,,558��36,,314��37,,590��37,,580��44,,505��42,,300�e]�(�139�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�140��+Telecom SCADA Protection and Control Growth�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�141��&Kelowna 138 kV Loop Fibre Installation��1,212��2,549�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�142��?Kootenay Remedial Action Scheme-Install Redundant Backup System�j�  j�  j�  �1,166��3,162�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�114433��GSSyynnccrroopphhaassoorr DDaattaa CCoolllleeccttiioonn PPllaattffoorrmm�j�  j�  j�  j�  �662233�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�144��?Okanagan Remedial Action Scheme-Install Redundant Backup System�j�  j�  j�  j�  j�  j�  j�  �3,072��6,100�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�145��&Princeton to Oliver Fibre Installation�j�  j�  j�  j�  j�  j�  j�  j�  j�  �7,133��7,216�j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�146��1Total Telecom SCADA Protection and Control Growth��1,212��2,549�j�  �1,166��3,786�j�  j�  �3,072��6,100��7,133��7,216�j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�147�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�114488��[TTeelleeccoomm SSCCAADDAA PPrrootteeccttiioonn aanndd CCoonnttrrooll SSuussttaaiinnmmeenntt�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�149��Communication Upgrades��410��400��763��776��587��553��409��446��443��518��524��516��484��493��549��560��583��586��572�j�  e]�(�150��SCADA Systems Sustainment��707��733��784��811��843��795��785��855��848��992��1,004��989��927��944��1,053��1,073��1,117��1,123��1,096��1,262�e]�(�151��'Backbone Transport Technology Migration�j�  j�  �410��6,652�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�152��Station Smart Device Upgrades�j�  j�  �704��363��741��351��694��378��750��438��886��436�j�  j�  j�  j�  j�  j�  j�  j�  e]�(�115533��<TTeelleeccoommmmu nniiccaattiioonnss RRiinngg CCllooss urree�j�  j�  j�  j�  j�  �228866��
33, 997799�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�154��6Total Telecom SCADA Protection and Control Sustainment��1,117��1,133��2,661��8,601��2,171��1,984��5,867��1,678��2,041��1,947��2,414��1,941��1,411��1,437��1,602��1,633��1,700��1,708��1,668��1,262�e]�(�155��3Total Telecom SCADA Protection and Control Projects��2,329��3,682��2,661��9,768��5,957��1,984��5,867��4,750��8,141��9,080��9,630��1,941��1,411��1,437��1,602��1,633��1,700��1,708��1,668��1,262�e]�(�156�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�157��General Plant�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�158��%Kootenay Long Term Facilties Strategy��6,020��10,477�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�159��Okanagan Long Term Solution��69��75��3,984�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�160��Central Warehousing��1,755�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�161��Trail Building Purchase�j�  �10,000�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�116622��>AAddvvaanncceedd MMeetteerriinngg IInnffrraassttrruuccttuurree��
44,,550011��2277,,993311��
66,,009999�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  e]�(�163��Information Systems�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�164��Infrastructure Sustainment��1,111��1,118��1,207��1,244��1,355��1,302��1,299��1,380��1,388��1,566��1,593��1,574��1,525��1,555��1,666��1,727��1,789��1,810��1,788��2,004�e]�(�165��"Desktop Infrastructure Sustainment��1,115��1,122��1,239��1,277��1,321��1,269��1,267��1,346��1,353��1,527��1,553��1,534��1,487��1,516��1,625��1,684��1,744��1,765��1,743��1,954�e]�(�166��Application Enhancements��1,235��1,242��1,296��1,336��1,382��1,328��1,325��1,408��1,415��1,598��1,625��1,605��1,555��1,586��1,700��1,762��1,825��1,847��1,824��2,044�e]�(�116677��-AApppplliiccaattiioonn SSuussttaaiinnmmeenntt��
11,,117799��
11,,221100��
11,,227766��
11,,334411��
11,,444411��
11,,446633��
11,,555599��
11,,993322��
22,,008822��
22,,335500��
22,,554499��
22,,551188��
22,,559922��
22,,664444��
22,,999999��
33,,110099��
33,,339999��
33,,444400��
33,,557766��
44,,000088�e]�(�168��!PowerSense DSM Reporting Software��1,032�j�  �131��135��140��67��671��71��72��81��82��81��79�j�  j�  j�  j�  j�  j�  j�  e]�(�169��Vehicles��2,541��2,574��2,699��2,796��2,906��2,805��2,803��2,970��2,989��3,350��3,408��3,374��3,281��3,346��3,573��3,700��3,828��3,876��3,836��4,273�e]�(�170��Metering Changes��403��406��212��222��234��229��233��252��259��298��308��621��613��637��695��735��775��800��805��919�e]�(�171��Telecommunications��121��183��191��196��203��195��195��207��208��235��239��236��229��233��250��259��268��272��268��301�e]�(�117722��BBuuiillddiinnggss��
11 ,336622��888833��660011��227788��228877��662299��227733��224477��553344��331133��336688��333377��335511��330022��552200��440011��669988��441133��888877��
11 ,000022�e]�(�173��Furniture and Fixtures��121��122��508��105��108��104��104��110��69��78��80��79��122��124��133��138��179��181��268��301�e]�(�174��Tools and Equipment��528��457��477��491��508��488��487��518��520��587��597��590��572��583��625��648��671��679��670�j�  e]�(�175��Total General Plant��23,093��57,800��19,920��9,423��9,885��9,881��10,217��10,442��10,889��11,984��12,403��12,548��12,405��12,527��13,786��14,164��15,175��15,082��15,665��16,806�e]�(�176�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  e]�(�117777��TTo tta ll GGenerattiion��1100, 113311��
22 ,994477��1111 ,669966��
44, 443333��
55, 001199��1133, 226699��2200, 556677��2233, 339977��2222, 003333��
55, 772200��
55, 227799��1188, 441188��1144, 335588��1155, 227700��3311, 997700��1122, 116633��1133, 000044��1122, 773366��1188, 221133��
77, 229933�e]�(�178��Total Transmission&Stations��35,254��32,854��32,024��38,385��30,220��61,195��76,936��34,056��39,510��13,125��15,197��21,812��49,617��45,622��21,647��33,826��20,866��37,161��67,393��12,751�e]�(�179��Total Distribution��29,249��25,889��29,351��27,537��32,878��28,489��30,453��30,733��30,476��35,115��35,580��35,267��33,140��33,826��35,558��36,314��37,590��37,580��44,505��42,300�e]�(�180��Total Telecom��2,329��3,682��2,661��9,768��5,957��1,984��5,867��4,750��8,141��9,080��9,630��1,941��1,411��1,437��1,602��1,633��1,700��1,708��1,668��1,262�e]�(�181��Total General Plant��23,093��57,800��19,920��9,423��9,885��9,881��10,217��10,442��10,889��11,984��12,403��12,548��12,405��12,527��13,786��14,164��15,175��15,082��15,665��16,806�e]�(�182��Grand Total��100,057��123,171��95,653��84,796��83,959��114,819��144,040��103,378��111,049��75,024��78,090��89,986��110,931��108,682��104,564��98,099��88,335��104,267��147,444��80,411�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �296�j�  Mj�  (G@p�@   G@K�Y�   G@�/�@   G@N\    t�ububh)��}�(h]�(j$	  j$	  j$	  eh]�((G@K
=`   G@Q@   G@�)G�   G@�8��   t�(G@Kn�fffG@Q@   G@�(Ĭ���G@���X   t�(G@K\   G@Q@   G@�(��   G@�4(�   t�(G@K\   G@Q@   G@�(��   G@�|(�   t�(G@K\   G@Q@   G@�(��   G@�[�   t�(G@K
=`   G@Q@   G@�)G�   G@j}p`   t�(G@X���   G@[\@   G@�38   G@wS38   t�eh?]�(�First Nations�j  j  ehA]�(]�(�First Nations�NNe]�(�Method of Contact��Organization��Position�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Penticton Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Osoyoos Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Lower Kootenay Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Upper Similkameen Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Lower Similkameen Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Westbank First Nation��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Okanagan Indian Band��Chief�e]�(�KJanuary 31, 2011 with offer to
present and invitation to public
open houses��Upper Nicola Indian Band��Chief�e]�(�OJanuary 31, 2011 with offer to
present and invitation to public
open houses
y ,��Shuswap Indian Band��Chief�e]�(�M2011 with invitation to public
open houses and reminder of
review process
y ,��Okanagan Nation Alliance��Grand Chief�e]�(�M2011 with invitation to public
open houses and reminder of
review process
y ,��Shuswap Nation Tribal Council��Chairman�e]�(�I2011 with invitation to public
open houses and reminder of
review process��Ktunaxa Nation��Tribal Chair�e]�(�`Email sent to Nation January 31,
2011 with offer to present and
invitation to public open houses��Sinixt Nation��Appointed Spokesperson�e]�(j  NNe]�(�Local Govenments�NNe]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��City of Castlegar��Mayor and Council�e]�(�Email sent January 31, 2011��Town of Creston��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Fruitvale��Mayor and Council�e]�(�Email sent January 31, 2011��City of Grand Forks��Mayor and Council�e]�(�Email sent January 31, 2011��City of Greenwood��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Kaslo��Mayor and Council�e]�(�Email sent January 31, 2011��City of Kelowna��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Keremeos��Mayor and Council�e]�(�Email sent January 31, 2011��District of Lake Country��Mayor and Council�e]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��District of Lillooet��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Midway��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Montrose��Mayor and Council�e]�(�Email sent January 31, 2011��City of Nelson��Mayor and Council�e]�(�Email sent January 31, 2011��Town of Oliver��Mayor and Council�e]�(�Email sent January 31, 2011��Town of Osoyoos��Mayor and Council�e]�(�Email sent January 31, 2011��City of Penticton��Mayor and Council�e]�(�Email sent January 31, 2011��Town of Princeton��Mayor and Council�e]�(�Email sent January 31, 2011��City of Rossland��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Salmo��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Slocan��Mayor and Council�e]�(�Email sent January 31, 2011��District of Summerland��Mayor and Council�e]�(�Email sent January 31, 2011��City of Trail��Mayor and Council�e]�(�Email sent January 31, 2011��Village of Warfield��Mayor and Council�e]�(�Email sent January 31, 2011��%Regional District of Central Kootenay��Chair�e]�(�Email sent January 31, 2011��%Regional District of Central Okanagan��Chair�e]�(�Email sent January 31, 2011��&Regional District of Kootenay-Boundary��Chair�e]�(�Email sent January 31, 2011��)Regional District of Okanagan-Similkameen��Chair�e]�(j  NNe]�(�=Members of Parliament and Members of the Legislative Assembly�NNe]�(�Method of Contact��Riding��Position�e]�(�Email sent January 31, 2011��Okanagan-Coquihalla��Member of Parliament�e]�(�Email sent January 31, 2011��Kelowna-Lake Country��Member of Parliament�e]�(�Email sent January 31, 2011��"British Columbia Southern Interior��Member of Parliament�e]�(�Email sent January 31, 2011��Kootenay Columbia��Member of Parliament�e]�(�Email sent January 31, 2011��	Penticton��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Boundary-Similkameen��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Kootenay West��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Nelson-Creston��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Westside-Kelowna��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Kelowna-Lake Country��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Kelowna-Mission��Member of Legislative Assembly�e]�(�Email sent January 31, 2011��Fraser Nicola��Member of Legislative Assembly�e]�(j  NNe]�(�;Chambers of Commerce and Economic Development Organizations�NNe]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��*Castlegar and District Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��(Creston and District Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��Grand Forks Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��Greenwood Board of Trade��Executive Director�e]�(�Email sent January 31, 2011��"Kaslo and Area Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011�� Lake Country Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��'Nelson and District Chamber of Commerce��Executive Director�e]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��,Penticton & Wine Country Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��Rossland Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��Summerland Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��&Trail and District Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��"Christina Lake Chamber of Commerce��Vice President�e]�(�Email sent January 31, 2011��Kelowna Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��Similkameen Country��Executive Director�e]�(�Email sent January 31, 2011��#Slocan District Chamber of Commerce��Executive Director�e]�(�Email sent January 31, 2011��0Central Okanagan Economic Development
Commission�j  e]�(�Email sent January 31, 2011��&Regional District of Kootenay Boundary��*Community Economic Development Coordinator�e]�(�Email sent January 31, 2011��'Nelson Economic Development Partnership��$General Manager of Community Futures�e]�(�Email sent January 31, 2011��Osoyoos Indian Band��Osoyoos Indian Band�e]�(j  NNe]�(�Large Customers�NNe]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��UBC Okanagan��0Assistant Vice President; Deputy Vice Chancellor�e]�(�Email sent January 31, 2011��District of Lake Country��Director of Engineering�e]�(�Email sent January 31, 2011��Al Stober Construction��Owner�e]�(�Email sent January 31, 2011��City of Kelowna��Director�e]�(�Email sent January 31, 2011��Interior Health Authority��Energy Manager�e]�(�Email sent January 31, 2011��Big White Ski Resort��EVice President Operations, Vice President Real Estate
and Development�e]�(�Email sent January 31, 2011��Rona��Manager�e]�(�Email sent January 31, 2011��Overwaitea Food Group��Energy Manager�e]�(�Email sent January 31, 2011��Orchard Park Shopping Centre��#General Manager, Operations Manager�e]�(�Email sent January 31, 2011��Sysco��'Vice President, Chief Financial Officer�e]�(�Email sent January 31, 2011��Bingo Kelowna��Owner�e]�(�Email sent January 31, 2011��McIntosh Properties�j  e]�(�Email sent January 31, 2011��Best Western Hotel��Owner�e]�(�Email sent January 31, 2011��Callahan Construction��Owner�e]�(�Email sent January 31, 2011��#Uptown Rutland Business Association��President, Executive Director�e]�(�Email sent January 31, 2011��Celgar��3Managing Director of Operations, Energy Coordinator�e]�(�Email sent January 31, 2011��Roxul��Factory Manager�e]�(�Email sent January 31, 2011��Interfor��Plant Manager�e]�(�Email sent January 31, 2011��Kalesnikoff�j  e]�(�Email sent January 31, 2011��Springer Creek�j  e]�(�Email sent January 31, 2011��Columbia Brewery��Director�e]�(�Email sent January 31, 2011��Porcupine Wood Products�j  e]�(�Email sent January 31, 2011��ATCO Wood Products�j  e]�(�Email sent January 31, 2011��Wyndel Box and Lumber�j  e]�(�Email sent January 31, 2011��Selkirk College�j  e]�(�Email sent January 31, 2011��%Regional District of Central Kootenay�j  e]�(�Email sent January 31, 2011��School District #20�j  e]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��School District #8�j  e]�(�Email sent January 31, 2011��School District #51�j  e]�(�Email sent January 31, 2011��School District # 23��Director of Operations�e]�(�Email sent January 31, 2011��School District #53��Manager of Operations�e]�(�Email sent January 31, 2011��City of Trail�j  e]�(�Email sent January 31, 2011��City of Castlegar�j  e]�(�Email sent January 31, 2011��&Regional District of Kootenay Boundary�j  e]�(�Email sent January 31, 2011��Town of Creston�j  e]�(�Email sent January 31, 2011��Red Mountain Resorts�j  e]�(�Email sent January 31, 2011��District of Summerland��Administrator�e]�(�Email sent January 31, 2011��Town of Oliver��Director of Operations�e]�(�Email sent January 31, 2011��Town of Osoyoos��Director of Public Works�e]�(�Email sent January 31, 2011��Town of Princeton�j  e]�(�Email sent January 31, 2011��&Regional District Okanagan Similkameen�j  e]�(�Email sent January 31, 2011��Weyerhaeuser Princeton��Mill Manager�e]�(�Email sent January 31, 2011��Greenwood Forest Products��Manager�e]�(�Email sent January 31, 2011��Princeton Wood Preserves��	President�e]�(�Email sent January 31, 2011��Agriculture Canada��Science Director�e]�(�Email sent January 31, 2011��0Okanagan-Kootenay Sterile Insect Release
Program�j  e]�(�Email sent January 31, 2011��(Okanagan Similkameen Cooperative Growers��General Manager�e]�(�Email sent January 31, 2011��Vincor��Manager�e]�(j  NNe]�(�%Associations, Builders and Developers�NNe]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��4Canadian Home Builders' Association Central
Interior��President, Executive Officers�e]�(�Email sent January 31, 2011��4Canadian Home Builders' Association Central
Okanagan��President, Executive Officers�e]�(�Email sent January 31, 2011��4Canadian Home Builders' Association Central
Okanagan��President, Executive Officer�e]�(�Email sent January 31, 2011��)Canadian Home Builders' Association of BC��Chief Executive Officer�e]�(�Email sent January 31, 2011��Fraser Basin Council��0Sustainability Facilitator,
BC Southern Interior�e]�(�Email sent January 31, 2011��#Urban Development Institute Kelowna��Chair, Executive Coordinator�e]�(�Email sent January 31, 2011��BC and Yukon Hotel Association��Purchasing Program Coordinator�e]�(�Email sent January 31, 2011��,BC Apartment Owners and Managers Association��Chief Executive Officer�e]�(�Email sent January 31, 2011��BC Chamber of Commerce��HPresident and Chief Executive Officer, Vice President
Policy Development�e]�(�Email sent January 31, 2011��!BC Greenhouse Growers Association��Executive Director�e]�(�Email sent January 31, 2011��(BC Restaurant & Foodservices Association��Membership Services Manager�e]�(�Email sent January 31, 2011��%Better Business Bureau of Mainland BC��$President, Communications Specialist�e]�(�Email sent January 31, 2011��(Building Owners and Managers Association��-Energy Conservation & Sustainability Programs�e]�(�Email sent January 31, 2011��(British Columbia Real Estate Association�j  e]�(�Email sent January 31, 2011��(Building Owners and Managers Association��Executive Vice President�e]�(�Email sent January 31, 2011��$Business Council of British Columbia��FPresident and Chief Executive Officer, Executive Vice
President Policy�e]�(�Email sent January 31, 2011��Community Energy Association��(Manager, Community Outreach and Strategy�e]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��Council of Forest Industries�j  e]�(�Email sent January 31, 2011��!Independent Power Producers of BC��-Executive Director, Vice President Operations�e]�(�Email sent January 31, 2011��Mining Association of BC��#President & Chief Executive Officer�e]�(�Email sent January 31, 2011��SolarBC��/Executive Director, Residential Project Manager�e]�(�Email sent January 31, 2011��)Thermal Environmental Comfort Association��Manager�e]�(�Email sent January 31, 2011��Urban Development Institute��-Executive Director, Deputy Executive Director�e]�(�Email sent January 31, 2011��Community Energy Association��#Development and Partnership Manager�e]�(�Email sent January 31, 2011��*Rental Owners & Managers Association of BC��Chief Executive Officer�e]�(�Email sent January 31, 2011��BC Health Services�j  e]�(�Email sent January 31, 2011��'GeoTility Geothermal Installation Corp.��General Manager�e]�(�Email sent January 31, 2011��Acorn Communities��	President�e]�(�Email sent January 31, 2011��Edgecombe Builders��	President�e]�(�Email sent January 31, 2011��Fenwick Developments��Chief Executive Officer�e]�(�Email sent January 31, 2011��G Group of Companies��	President�e]�(�Email sent January 31, 2011�j  �Site Supervisor�e]�(�Email sent January 31, 2011��Rohit Communities��Regional Manager�e]�(�Email sent January 31, 2011��Rykon Group��	President�e]�(�Email sent January 31, 2011��WestCorp Properties��Development Manager�e]�(j  NNe]�(�Regulatory Participants�NNe]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��Nova Independent Resources Ltd.��	President�e]�(�Email sent January 31, 2011��(Okanagan Environmental Industry Alliance��Executive Director�e]�(�Email sent January 31, 2011��MGM Management�j  e]�(�Email sent January 31, 2011��
Individual�j  e]�(�Email sent January 31, 2011��Horizon Technologies Inc.�j  e]�(�Email sent January 31, 2011��!BC Sustainable Energy Association�j  e]�(�Email sent January 31, 2011��
Individual�j  e]�(�Email sent January 31, 2011��"BC Public Interest Advocacy Centre�j  e]�(�Email sent January 31, 2011��-Commercial Energy Consumers Association of BC�j  e]�(�Email sent January 31, 2011��-Joint Industry Electricity Steering Committee��Executive Director�e]�(�By Telephone February 8, 2011��
Individual�j  e]�(�Email sent January 31, 2011��Natural Resource Industries�j  e]�(�Email sent January 31, 2011��
Individual�j  e]�(�Email sent January 31, 2011��
Individual�j  e]�(�Email sent January 31, 2011��4BC Ministry of Energy, Mines and Petroleum
Resources��Director - Energy Efficiency�e]�(�Email sent January 31, 2011��Mercer International��$Vice President Strategic Initiatives�e]�(�Email sent January 31, 2011��Big White Ski Resort��Managing Director�e]�(�Email sent January 31, 2011��Town of Oliver��Chief Fincancial Officer�e]�(�Email sent January 31, 2011��City of Rossland��City Manager�e]�(�Email sent January 31, 2011��BC Hydro��Chief Regulatory Officer�e]�(�Email sent January 31, 2011��Town of Osoyoos�j  e]�(�Email sent January 31, 2011��Town of Princeton�j  e]�(�Method of Contact��Organization��Position�e]�(�Email sent January 31, 2011��ATCO Wood Products�j  e]�(�Email sent January 31, 2011��Weyerhaeuser�j  e]�(�Email sent January 31, 2011��Red Mountain Ventures�j  e]�(�Email sent January 31, 2011��Irrigation Ratepayers Group�j  e]�(�Email sent January 31, 2011��!BC Municipal Electrical Utilities��City of Grand Forks�e]�(�Email sent January 31, 2011��!BC Municipal Electrical Utilities��City of Penticton�e]�(�Email sent January 31, 2011��!BC Municipal Electrical Utilities��City of Kelowna�e]�(�Email sent January 31, 2011��!BC Municipal Electrical Utilities��District of Summerland�e]�(�Email sent January 31, 2011��!BC Municipal Electrical Utilities��Nelson Hydro�e]�(�Outlet��City��Booking Dates (2011)�e]�(�Castlegar News��	Castlegar��Jan 27, Feb 3�e]�(�Grand Forks Gazette��Grand Forks��Jan 28, Feb 4�e]�(�Trail Daily Times and Weekender��Trail��Jan 28, Feb 4�e]�(�Rossland News��Trail��Jan 27, Feb 3�e]�(�Nelson Star��Nelson��Jan 28, Feb 4�e]�(�Creston Valley Advance��Creston��Jan 27, Feb 3�e]�(� Boundary Creek Times Mountaineer��	Greenwood��Jan 26, Feb 2�e]�(�	Pennywise��Kaslo��Feb 1, Feb 8�e]�(�Kelowna Capital News��Kelowna��Jan 28, Feb 4�e]�(�Kelowna Daily Courier��Kelowna��Jan 29, Feb 5�e]�(�#The Review in Keremeos and OK Falls��Keremeos��Jan 27, Feb 3�e]�(�Oliver Chronicle��Oliver��Jan 26, Feb 2�e]�(�Osoyoos Times��Osoyoos��Jan 26, Feb 2�e]�(�Penticton Herald/Okan. Sat.��	Penticton��Jan 29, Feb 5�e]�(�Similkameen News Leader��	Princeton��Jan 24, Jan 31�e]�(�Similkameen Spotlight��	Princeton��Jan 31�e]�(�Summerland Review��
Summerland��Jan 27, Feb 3�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  Nubh)��}�(h]�(j$	  j$	  j�  eh]�(K hhC   �>�B@���R�G@��     G@�      t�ah?]�(�Electric Tariff��Please visit any of the�K ehA]�(]�(j{\  j|\  K e]�(�
quirements��Location�K e]�(�
enditure &��Kelowna:�K e]�(�f Public��Osoyoos:�K e]�(�picalons��Creston:�K e]�(�	decisions��
Castlegar:�K e]�(�ts��Ifyou can't attend one��Kof the open houses but would Iike to participate, yOU are encouraged to re'�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M6j�  Nubh)��}�(h]�j$	  ah]�((G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�eh?]��\FortisBC Integrated System Plan PPuubblliicc OOppeenn HHoouussee - FFeebbrruuaarryy 22001111�ahA]�(]��\FortisBC Integrated System Plan
PPuubblliicc OOppeenn HHoouussee - FFeebbrruuaarryy 22001111�a]�j  a]�j  a]��;MMaarrkk WWaarrrreenn
FortisBC
Director of Customer Service�a]�j�  a]��Tonight�a]���FortisBC ISP overview 6:00 – 6:30
• High level goals and principles
Q&A at information stations 6:45 – 8:00
• Visit each station
• Get detailed answers to your questions
• Fill out forms and provide your input�a]�j�  a]�j  a]��BFortisBC
Providing electricity to southern interior for 100+ years�a]���• A regulated utility based in
KKeelloowwnnaa, BBCC, eessttaabblliisshheedd iinn
1897
• Serves approximately 161,000
customers
• Owns and operates four
generating plants
• Employs over 500 people
• Wholly owned subsidiary of
Fortis Inc.�a]�j�  a]��,The ISP:
A balanced framework for the future�a]���Energy
The ISP ensures we
Supply
wwiillll bbee aabbllee ttoo mmeeeett
customer needs for
decades to come.
Safety Energy
Delivery
Reliability Cost Energy
Conservation�a]�j�  a]�j  a]��5The FortisBC ISP:
A balanced framework for the future�a]�X1  1. The challenge: planning today to meet customer
needs for decades to come.
We must consider how we:
• Maintain existing infrastructure
• Meet growing demand for reliable electricity
• Meet dynamic public policy requirements
• Manage bill impacts
• Hear and act on customer input and priorities�a]�j�  a]��R1. Planning today for the future
…so we can maintain our existing infrastructure�a]��FortisBC
Infrastructure
GGeenneerraattiioonn
station 100 plus
year life
Transformer
40-60 year life
Wooden pole
40-70 year life�a]�j�  a]�j  a]��P2. Planning today for the future
…So we can meet growing demand from customers�a]���Customer #s
Customer
250,000
ggrroowwtthh…
200,000
150,000
Indirect
100,000 Direct
…is exppected to 50,000
continue in
-
coming years 1990 1995 2000 2005 2010 2015 2020 2025 2030 2035 2040
Year�a]�j�  a]��U3. Planning today for the future
…So we can meet dynamic public policy requirements�a]��PEElleeccttrriicc
vehicles
Smart grid
Renewable
AAeesstthheettiiccss generattiion�a]�j�  a]�j  a]��A4. Planning today for the future
…So we can manage bill impacts�a]�X/  We know that every ...because it will
iinnvveessttmmeenntt mmuusstt bbee iimmppaacctt tthhee bbiillll..
prudent...
Approx.
rates
5km of transmission line = 0.1% rate impact
New substation = 0.5% rate impact
50 MW generation plant = 2.5% rate impact
So before we build we consider the
impact to your bill�a]��10�a]��L5. Planning today for the future
…So we seek out your input and priorities�a]���Open
Houses
aanndd
focus Incorporate Anticipate
groups - input into Filing ISP BCUC
February ISP June 2011 approval
Feedback BCUC
until Feb 25 reggulatoryy
process
Public and stakeholder
Regulatory process
consultation�a]��11�a]�j  a]��5The FortisBC ISP:
A balanced framework for the future�a]�XJ  1. The challenge: planning today to meet your needs for
decades to come
2. The response: the ISP is a balanced framework for:
• Addressing growth
• Ensuring prudent and sustainable system maintenance
• IImmpprroovviinngg ccuussttoommeerr sseerrvviiccee lleevveellss
• Flexibility to meet dynamic public policy requirements�a]��12�a]��;1. The ISP is a balanced framework...
For addressing growth�a]���• Begin by predicting load
growtthh over nextt 2200 years
• Assessing existing capability
to meet demand
• Identifying potential solutions
to meet need in time�a]��13�a]�j  a]��;2. The ISP is a balanced framework...
For addressing growth�a]���We began by forecasting load growth over the next 20 years – and our ability to meet
it
Big picture:
We have
forecast overall
growth in
load ...
…and found
wee will neeeedd
more energy
to meet
growth�a]��14�a]��;3. The ISP is a balanced framework...
For addressing growth�a]���>10
New power supply
costs significantly
more than existing
power supply Present Energy Load )hWk
/
stnec(
ecirP
4.0
3.5
ygreenE
2.5
oorrddyyHH
CCBBssiittrrooFF tntnaailillilrirBB tekraM tekraM
CCBB
Energy (kWh)�a]��15�a]�j  a]��;4. The ISP is a balanced framework...
For addressing growth�a]���We began by forecasting load growth over the next 20 years – and our ability to meet
it
Local picture:
We looked at specific
communities and
customers...
...and found that we
will need new
investments to serve
them.�a]��16�a]��;5. The ISP is a balanced framework...
For addressing growth�a]�X�  • Big Picture:
– PPoowweerr ppuurrcchhaassee ffrroomm tthhee WWaanneettaa eexxppaannssiioonn wwiillll rreedduuccee
need for new generation infrastructure
– Determined that we need to have supply insurance (planning
reserve margin)
– Committed to continued expansion of our conservation
programs (e.g. PowerSense)
• Local Picture:
– Increase capacity at existing locations through station
upgrades
– Increase capacity with new station south of Penticton�a]��17�a]�j  a]��X6. The ISP is a balanced framework...
For ensuring our system serves customers for years�a]�X  Past: Transition Future
Time and
Time-Based Condition-
condition-
analysis based analysis
based analysis
Maintain based Maintain based
Maintain based
on on condition &
on age and/or
infrastructure consequence
condition
aaggee ooff ffaaiilluurree
Proposed approach under ISP�a]��18�a]��B7. The ISP is a balanced framework...
For improving service levels�a]���Advanced Metering Infrastructure
• Providing customers with better information about
their energy usage
• Helping customers manage their energy use�a]��19�a]�j  a]��B8. The ISP is a balanced framework...
For improving service levels�a]���By helping customers use information to manage their energy use
No Higher
cost cost
Spectrum of PowerSense programs help customers reduce use�a]��20�a]��LThe ISP is a balanced framework for the future
Forecast capital expenditures�a]���140
120 Facilities
AMI
100
$
Environmental
fo
80 Growth snoilliM
60 DSM
General Plant
40
Sustaining
2200
0
2012 2013 2014 2015 2016
Year�a]��21�a]�j  a]��GThe ISP is a balanced framework for the future
Estimated rate increases�a]���9
8
BBCC HHyyddrroo PPoowweerr PPuurrcchhaassee
7
Other Power Purchases )%(
6
*
tcapmI
5 Capital Expenditures
*
4
Operations & Maintenance etaR
3
2
1 *Assumes Waneta
Expansion will be phased in
0
2012 2013 2014 2015 2016 over multi-year period
Year�a]��22�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M9j�  Nubh)��}�(hNh]�(hhC   `�C@���R�hhC   @�a@���R�G@�      G@��     t�ah?]�(�!We Want To Hear Your Thoughts On:��Booth�ehA]�(]�(j3]  j4]  e]�(�6How we can make sure you are heard during this process��Overview�e]�(�,Appropriate spending on aesthetic mitigation��Growth�e]�(�'~Our infrastructure management approach��Sustainment�e]�(�.Whether FortisBC should fund in-house displays��Improving Service�e]�(�24��FORTISBC�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MDj�  j�  )��}�(j�  �#A balanced framework for the future�j�  MDj�  (G@a�3@   G@_1b    G@z V�   G@a�T�   t�ububh)��}�(hNh]�(G@_�z����G@X=q    G@~Z=j���G@���@   t�ah?]��5The FortisBC ISP: A balanced framework for the future�ahA]�(]��5The FortisBC ISP:
A balanced framework for the future�a]���1. The challenge: planning today to meet your needs for
decades to come
2. The Response: the ISP is a balanced framework
Safety
+ Public = ISP
Policy
Reliability Cost�a]��23�a]�j  a]��We want to hear from you!�a]�XD  We Want To Hear Your Thoughts On: Booth
Overview
- How we can make sure you are heard during this process - ISP
- Regulatory Process
Growth
- Appropriate spending on aesthetic mitigation
- Resource Planning
- Your priorities around site location
- Capital
Sustainment
-Our infrastructure management approach - Generation
(condition vs. time-based) - Infrastructure management
- SSuuppppoorrtt sseerrvviicceess ((IITT//fflleeeett//eettcc))
Improving Service
- Whether FortisBC should fund in-house displays
- Metering
- If there are future programs or rebates you want to see
- DSM�a]��24�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MDj�  Nubh)��}�(hNh]�(G@`���   G@~g�   G@~ z�   G@�#p�   t�ah?]�(�!We Want To Hear Your Thoughts On:��Booth�ehA]�(]�(�!We Want To Hear Your Thoughts On:��Booth�e]�(�8- How we can make sure you are heard during this process��#Overview
- ISP
- Regulatory Process�e]�(�U- Appropriate spending on aesthetic mitigation
- Your priorities around site location��$Growth
- Resource Planning
- Capital�e]�(�B-Our infrastructure management approach
(condition vs. time-based)��sSustainment
- Generation
- Infrastructure management
- SSuuppppoorrtt sseerrvviicceess ((IITT//fflleeeett//eettcc))�e]�(�j- Whether FortisBC should fund in-house displays
- If there are future programs or rebates you want to see��"Improving Service
- Metering
- DSM�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MDj�  j�  )��}�(j�  �+= ISP�j�  MDj�  (G@qٙ�   G@p�`   G@yyp    G@qD@   t�ububh)��}�(h]�j$	  ah]�((G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@_�z����G@X=q    G@~Z=j���G@���@   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�(G@IǍ@   G?�     G@���    G@��   t�eh?]��Your views are important�ahA]�(]��Your views are important�a]�X   • Give us your feedback form before you leave!
• Contact us at:
– Website: www.fortisbc.com or www.bcuc.com
– E-mail: FBCisp@fortisbc.com
– Phone:1-866-4 FORTIS (1-866-436-7847)
– Mail: Attn: Integgrated Syystem Plan,, Suite 100 – 1975
Springfield Road, Kelowna, BC, V1Y 7V7�a]��25�a]�j  a]�j  a]�j  a]��kThe ISP is a balanced framework for the future
FortisBC capacity resource stack – before Waneta Expansion�a]���1200
1100
1000
900
)WM(
800
700
tsaceroF
600
500
400
300
200
100
0
Year
FortisBC Brilliant Brilliant Upgrade
Other BCH PPA DSM
Capacity Block Forecast Gap Forecast Peak Demand�a]��27�a]�j  a]��iThe ISP is a balanced framework for the future
FortisBC capacity resource stack – with Waneta Expansion�a]���1200
1100
11000000
900
800 )WM(
700
600 tsaceroF
500
400
300
200
100
0
Year
FortisBC Brilliant Brilliant Upgrade
Other BCH PPA DSM
Capacity Block WAX CAPA Forecast Gap�a]��28�a]���2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Welcome to our open house
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Thanks for coming
Please sign in and help
yourself to refreshments
Energizing your community
www.fortisbc.com�a]�X�  Integrated System Plan (ISP)
Looks ahead 20 years to identify the energy and
infrastructure needs of our customers — then sets
out a five-year business plan to meet these needs.
r Identifeis what projects are needed to meet future growth
and sustain existing infrastructure
r Outlines energy efficiency and conservation measures
r Your input is important
Page 53 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Anticipated ISP timeline
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Consultation Regulatory process
JAN | FEB | MAR | APR | MAY JUN | JUL | AUG | SEP | OCT
1 2 3 4 5 6
1 Public input deadline February 25 4 File ISP with BCUC June 2011
2 Public, stakeholder and First Nations 5 BCUC review process Fall 2011
Consultation February – March 2011
6 BCUC decision late 2011
3 Finalize ISP May 2011
Energizing your community
www.fortisbc.com�a]�XW  Planning for future power supply
2011 Resource Plan:
r Looks at the next 30 years to identify shortfalls and
investigate future options
r Planning now is critical for the future – new generation
can take decades to plan and acquire
Resource Plan goals:
r Ensure long-term reliable power for customers through
acquisition of sufficient firm resources
r Reduce uncertainty and risks in current strategy which
includes purchases power from the market
r Balance cost efefctiveness with the directions and policy
actions of the Clean Energy Act
Page 54 of 138
Energizing your community
www.fortisbc.com�a]�X`  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Annual energy resource / Load gap (GWh)
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
6,000
5,500
5,000
)hWG(
4,500
4,000
ygrenE
3,500
3,000
2,500 launnA
2,000
1,500
1,000
500
0
1102 2102 3102 4102 5102 6102 7102 8102 9102 0202 1202 2202 3202 4202 5202 6202 7202 8202 9202 0302 1302 2302 3302 4302 5302 6302 7302 8302 9302 0402
FortisBC Brilliant (incl. upgrade)
Others BCH 3808 (Capped @ 2013)
BCH 3808 Forecast (High less 30% DSM)
Forecast (Expected less 50% DSM) Forecast (Low less 66% DSM)
Energizing your community
www.fortisbc.com�a]�X�  Power supply resources examples
Simple cycle gas turbine
Exhaust
Gas
H2 Rich Gas
Gas/Oil
Combustion Small hydro
Chamber
Air Electricity
Turbine Gas Turbine GT Generator
Simple cycle gas turbine schematic
Pumped storage hydro
Switchyard
Intake Reservoir
BC Hydro’s Aberfeldie
Elevator
Main Access Tunnel
Discharge Surge Chamber
Transformer Vault Breakers Powerplant Chamber
Senaca, PA
Page 55 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Power supply resources examples
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Wind
Rotor
Rotor Diameter
Blade
Gearbox
Generator
DiR amot eo tr er Nacelle HR eo igto hr t
Fixed
Pitch
Rotor
Blade
Tower
Gearbox Generator
Horizontal Axis Vertical Axis
Wind turbine confgiurations
Wind farm near Pincher Creek Alberta
Biogas
fWromoo Sd aWwamsitlels WooBdo iWlearste
Cooling Tower
HanSdolliindg F Suyeslstem SteGaemn eTruartboirne
Condenser
Water Treatment
Ash Land Fill
ECxohmaupsret sGsaosr eSst aFtrioonm
TCPL Gas Turbines CToCmPpLre Gssaosrs
ECxohmaupsret sGsaosr eSst aFtrioonm
Co-generation facility in Kimberley
Energizing your community
www.fortisbc.com�a]�X  Planning reserve margin (PRM)
2040 monthly capacity with expected load
1,300
1,200
1,100
A planning reserve margin is: 1,000 )WM(
r5IFQPXFSTVQQMZJOTVSBODFUIBUBMMPXTB 900
800 yticapaC
utility to reliably serve customers
700
600
Three primary drivers for PRM: 500 ylhtnoM
r6OBWBJMBCJMJUZPGTVQQMZEVFUPVOQMBOOFE 400
300
generating unit or transmission outages
200
r6OFYQFDUFEMZIJHIMPBET UZQJDBMMZEVFUP
100
extended extreme weather events 0
r"QFSJPEPGMPBEHSPXUIUIBUPVUQBDFTUIF
Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
possible installation of new power supply
resources High/Low Forecast Spread Others
Forecast Gap Brilliant (incl. upgrade)
WAX CAPA FortisBC
BCH 3808 Forecast (Expected + PRM less 50% DSM)
Page 56 of 138
Energizing your community
www.fortisbc.com�a]�X`  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Capital projects
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Sustaining capital:
r Shift from a period of capital investment in new
infrastructure to period of sustaining capital
r Shift from greenfeild construction to brownfeild
construction
r Focus on continuing reliability through maintaining and
upgrading current infrastructure
r Current projects and programs create a balance between
rates and reliability
r Continue to provide safe and reliable service at the lowest
reasonable costs
Energizing your community
www.fortisbc.com�a]�X�  Proposed Kootenay and Boundary region projects
Station Improvements:
r Beaver Park substation (Trail)
o Upgrade and expand existing Beaver Park substation to
accommodate larger transformer
o Convert transmission system in Montrose and Fruitvale
to 25kV
o Remove and salvage Fruitvale and Hearns substations
o Project solves capacity issues, removes two stations,
removes old equipment
r Grand Forks terminal station
o Upgrade station by adding terminal transformer
o Remove and salvage transmission lines (9 and 10 Lines)
between Christina Lake and Rossland
o Adding station ofsfets costs otherwise required to
maintain 9 and 10 Lines
o Provides continued reliability for area customers
Page 57 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Proposed Okanagan region projects
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Station improvements:
r DG Bell terminal station (Kelowna)
o Addition of Static VAR Compensator (SVC) which will support the
voltage of Kelowna’s transmission system needed by 2015 – 2017
r Lee terminal station (Kelowna)
o Addition of a third terminal transformer to maintain
reliable service to the Kelowna area as the load continues
to grow – needed by 2015
o Addition of distribution transformer possibly needed by 2018
Transmission improvements:
r Ellison to Sexsmith Road transmission
o New 138kV transmission line from Ellison substation along
Highway 97 to link with existing line at the corner of Highway 97
and Sexsmith Road
o Completes transmission loop for customers in north Kelowna
including airport and University of BC Okanagan
o Signifciantly improves reliability in the area
Energizing your community
www.fortisbc.com�a]�X"  Customer priorities
FortisBC must always balance safety, reliability and
cost when undertaking new projects, as well as other
considerations important to customers and communities.
These may include:
r Proximity to other buildings and amenities (homes, schools,
parks etc.)
r Environmental considerations (natural habitat, wildlife, “no
net loss”)
r Aesthetics
r Efefcts during construction
r Risk of project delay
r Flexibility for future growth
Please share your priorities with us.
Page 58 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Social and environmental consideration fund
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Every project in every community is a little bit difefrent.
r Our customers are telling us they want additional project
components
r Projects may need:
o Additional visual screening (vegetation, berming or
fencing)
o Special environmental treatment
o Other community specific amenities
Would you consider adding project funds for social
and environmental considerations? If so, how much is
reasonable? 1-3 per cent of project total ($1000 - $3000
for every $100,000)?
Energizing your community
www.fortisbc.com�a]�X(  FortisBC generation
Power generation:
r Water fol ws from the reservoir, down the
penstock and then spins a turbine connected
to a generator
r The generator converts mechanical energy into
electrical energy
Future generation projects:
r Moving from period of mechanical and electrical
unit upgrades to upgrading the infrastructure
required to support generation – like dams,
power house structures, spill gates and spill ways
r Work is scheduled based on engineering and
condition assessments
Page 59 of 138
Energizing your community
www.fortisbc.com�a]�X  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
FortisBC infrastructure
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
FortisBC manages over $1.2 billion in
infrastructure including:
r Four generating plants on Kootenay River
r 7,000 km of transmission and distribution lines
r 80 substations (including 13 for third parties)
r 102,000 poles
r 346 fleet vehicles
r 14 office buildings
r Over 670 desktop and laptop computers and
74 servers and storage devices
Energizing your community
www.fortisbc.com�a]�X�  Managing infrastructure
With over $1.2 billion
of infrastructure in
the FortisBC system,
prudent management of
infrastructure is essential.
r Proposes shift from time-
based management to
condition-based management
r Once new system is in place,
this approach ensures
maintenance dollars will
be used more efefctively
o Critical and high risk
equipment monitored
and dealt with first
Page 60 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
Automated Metering Infrastructure (AMI)
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
r Sometimes known as “smart meters”
r New meters provide customers with better energy
use information
r Provide better information to utility for outage
response
r Reduces need for vehicles and employees to read
each meter
r No bill estimates
Energizing your community
www.fortisbc.com�a]�X�  Manage your bill
View your energy use
KWH
14
12
r AMI provides more information
about when and how much energy 10
you use
r Customers can view energy use 8
on an in-home display or on a
6
secure website
r Provides daily, hourly and monthly 4
energy use information
2
0
00:0 00:1 00:2 00:3 00:4 00:5 00:6 00:7 00:8 00:9 00:01 00:11 00:21 00:31 00:41 00:51 00:61 00:71 00:81 00:91 00:02 00:12 00:22 00:32
Page 61 of 138
Energizing your community
www.fortisbc.com�a]�X�  2012 Long Term Capital Plan
Appendix K - ISP Consultation Report
What is DSM?
Attachment 8 - OPEN HOUSE
GRAPHIC INFORMATION PANELS
Demand side management or DSM is the planning and
implementation of programs designed to influence energy
consumption on the customer’s side of the electrical meter
by encouraging customers to improve energy efficiency,
reduce electricity use, change the time of use, or use a difefrent
energy source.
PowerSense is FortisBC’s demand side management program.
It provides programs and incentives encouraging energy
efficiency for FortisBC’s 161,000 direct and indirect customers.
Energizing your community
www.fortisbc.com�a]�X�  PowerSense history
r'PSUJT#$1PXFS4FOTFJTBOBXBSEXJOOJOHQSPHSBN
launched in 1989
r Cost-efefctive resource
r Mechanics:
o DSM is included in rate base
o Program must pass an economic test
o Includes both programs and incentives (rebates)
o Customer advisory committee
r 380 GWh of energy saved to date – enough to power
about 29,700 homes for a year
r Investment of $42.5 million has resulted in 60 megawatts
(MW) of capacity requirements avoided
r 2011 DSM initiatives currently meet 42 per cent of
FortisBC’s additional electrical needs
o BC Energy Plan calls for DSM to meet 50 per cent of
incremental resource needs by 2020
Page 62 of 138
Energizing your community
www.fortisbc.com�ae��     h�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MEj�  Nubh)��}�(hNh]�(hhC   @��A@���R�hhC   `��K@���R�G@�      G@��     t�ah?]�(K K ehA]�(]�(K K e]�(�ITEM #��	Questions�e]�(�1_�K e]�(NK e]�(�2.�K e]�(N�<You will be able to better monitor your energy use with more�e]�(�3_�K e]�(�4.��%reductions over the tollowing years:_�e]�(�5��EWhy don't you have district energy listed as a resource option on the�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  Nubh)��}�(h]�(j$	  j$	  eh]�(G@P����8�G@Yp�    G@�N~8�G@�.uUUUt�ah?]�(j  �!Integrated System Plan Open House�ehA]�(]�(�NO OF
ATTENDEES:�j�  e]�(�	LOCATION:��(Kelowna, B.C., Holiday Inn Express Hotel�e]�(�MEETING
DATE / TIME:���February 7, 2011
Doors open at 5:30 pm
Presentation 6:10 – 6:40 pm
Group questions 6:40 – 6:50
Information stations 6:50 – 7:40�e]�(j  Ne]�(�ITEM #��	Questions�e]�(j  j  e]�(j  �Group Questions�e]�(�1.��A Are you working with BC Hydro to make AMI systems compatible?�e]�(j  � Yes.�e]�(�2.��0 How will extra info from AMI meters help me?�e]�(j  X:   You will be able to better monitor your energy use with more
information. For instance you could monitor your use on a secure
website or in-home display that provides hourly information, rather than
waiting for a bill at the end of the month which only shows the block of
energy that you used during the month.�e]�(�3.��+ When is the payback for smart metering?�e]�(j  �o There are initial costs for the first three years of the program, then
reductions over the following years.�e]�(�4.��N What is the difference between time-based and condition-based
maintenance?�e]�(j  X�   Providing an example…time-based is similar to the regular change of
your furnace filter at home. You change it every one or two months
regardless of the condition of the filter. Condition-based would be if you
check the factors which affect the filter like how much the furnace has
been running and how dusty your home is. You would check the
condition of the filter and change it only as needed.�e]�(j  � Summary of Questions at Stations�e]�(�5.��[ Why don’t you have district energy listed as a resource option on the
display panels?�e]�(j  �q Not included as an option for FortisBC. Provided Terasen contact name
for district energy system information.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MXj�  j�  )��}�(j�  �'Integrated System Plan Open House �j�  MXj�  (G@i+�    G@R���   G@y�_    G@V0R    t�ububh)��}�(h]�(j^  j�  eh]�((hhC    �C@���R�hhC   �	G5@���R�G@�      hhC   ��@���R�t�(G@P�4�I$�G@R�    G@�F6�m�G@h>UUUUt�(hhC   ���?@���R�hhC   ��P@���R�G@�      G@��     t�eh?]�(�ITEM #��	Questions�ehA]�(]�(j<^  j=^  e]�(K �4Are AMI meters compatible with net metering program?�e]�(K �Yes.�e]�(�7�K e]�(N�7included in the handout of the PowerPoint presentation_�e]�(�ITEM #��	Questions�e]�(j  j  e]�(�6.��8 Are AMI meters compatible with net metering program?�e]�(j  � Yes.�e]�(�7.��- Can you provide map of service territory?�e]�(j  �@ Yes, included in the handout of the PowerPoint presentation.�e]�(�ITEM #��	Questions�e]�(K �Group Questions�e]�(�1,��)What is driving the cost in this project?�e]�(�2.��Ccenanienn nn  �e]�(K �Incluues Olisel Ny�e]�(K �UeeniCL�e]�(�4_��AHas there been any discussion about delaying any projects to meet�e]�(�5��DuUU�e]�(N�@We would likely have a capacity purchase from BC market to cover�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  MYj�  Nubh)��}�(h]�(j$	  j^  eh]�(hhC   �V@@���R�hhC   ���8@���R�G@�      G@��     t�ah?]�(�ITEM #�K ehA]�(]�(jz^  K e]�(�6_�K e]�(NK e]�(�7 _�K e]�(�8_�K e]�(�9_�K e]�(�J.�K e]�(�10_�K e]�(NK e]�(�11.�K e]�(�12.�K e]�(�13_�K e]�(�14_�K e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M[j�  Nubh)��}�(h]�(�number�j$	  eh]�((G@P����8�G@R�    G@�N8�G@�n�J���t�(hhC   @��:@���R�hhC   � ;@���R�G@�      G@��     t�eh?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(�6.��? Is the power purchase a rate increase each year (slide 22)?�e]�(j  �o No, once it is included, the cost is embedded and it doesn’t increase
again unless we must purchase more.�e]�(�7.��J What were the results of Demand Side Management open houses
last year?�e]�(j  X   Through our open houses in March last year, we found support for
an expanded program.
 FortisBC decided that the balanced approach was to go with the
middle of the road option we provided at open houses.
 Doubled our expenditure in 2011 as a result.�e]�(�8.��j Question on survey asks whether there is interest in joint programs
with Terasen. What does that mean?�e]�(j  �I Want to know if customers would like to see access to joint
programs.�e]�(�9.��' What is status of rate rebalancing?�e]�(j  �~ Will be coming into effect in April, 2011
 Residential will go up a little bit, commercial will come down a little
bit.�e]�(�10.��r Has BCUC set the ratios? Is there equality among residential rates
i.e. wholesale and non-wholesale residents?�e]�(j  XP   We can only look at customer classes which means we cannot deal
directly with wholesale residents, only the wholesale rate its self –
those wholesalers set their own residential rates.
 Increases capped at 2.5% each year.
 Lighting, wholesale and residential will see increases.
 Commercial and industrial will come down.�e]�(�11.��I What is the difference in cost between regular meters and AMI
meters?�e]�(j  � Only about $30�e]�(�12.��# What causes program costs then?�e]�(�13.��� Changing out the meters and installing the infrastructure to connect
to the meters.
 The up-front cost is about $40 million.
 We expect it to be a revenue neutral project – costs in first couple of
years and then decreases in following years.�e]�(�14.��$ What will in-home displays cost?�e]�(j  X   Very different costs depending on the level of information the
display provides and whether it is connect to other systems within
the house.
 What we are wondering is should we provide some basic level of
meter ($25) or should we let people buy their own?�e]�(�ITEM #��	Questions�e]�(K � Is goal to offset peak hour use?�e]�(N�<can be part of ultimate goal, but before that;, the BCUC has�e]�(K �%Are you looking at time of use rates?�e]�(K �L�e]�(K K e]�(K �-Are all wholesalers are paying the same rate?�e]�(K �Yes,�e]�(NK e]�(�19.��Wnat IS tne ditterence between�e]�(K �ExiSlIY�e]�(�21.��'Are you still considering gas turbines?�e]�(�22.��Yes,�e]�(N�	Uleleiice�e]�(�23_��(What kind of in-home displays are there?�e]�(�24.��'What new home programs are we offering?�e]�(N�Page 73 of 138�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M[j�  Nubh)��}�(hNh]�(hhC   ��1@@���R�hhC   `�9@���R�G@�      G@��     t�ah?]�hA]�(]�(�ITEM #��	Questions�e]�(�25_��=Could you present PowerSense information at a symposium about�e]�(N�Yes:�e]�(�26_��Can�e]�(N�8Provided copy of PowerPoint presentation; which includes�e]�(�27 .��:Will you be adding electronics (computer equipment) to DSM�e]�(N�:Possibly, have already started some rebates for EnergyStar�e]�(�28_��:Will you rebate customers to install their own generation?�e]�(N�CFortisBC is not providing rebates for self-generation at this time.�e]�(�29_��7Can you provide district energy contact at Terasen Gas?�e]�(N�Provided Terasen contact name:�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M]j�  Nubh)��}�(hNh]�(G@P��UUUG@R�    G@�(�UUUG@u�\    t�ah?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(�25.��] Could you present PowerSense information at a symposium about
energy efficiency programs?�e]�(j  � Yes.�e]�(�26.��3 Can I get more information about ISP elsewhere?�e]�(j  �� Provided copy of PowerPoint presentation, which includes
information about submitting comments and finding more
information.�e]�(�27.��e Will you be adding electronics (computer equipment) to DSM
program technology to save technology?�e]�(j  �Z Possibly, have already started some rebates for EnergyStar
appliances and electronics.�e]�(�28.��> Will you rebate customers to install their own generation?�e]�(j  �G FortisBC is not providing rebates for self-generation at this time.�e]�(�29.��; Can you provide district energy contact at Terasen Gas?�e]�(j  �" Provided Terasen contact name.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M]j�  Nubh)��}�(hNh]�(hhC   ��?A@���R�hhC   �oV@���R�G@�      G@��     t�ah?]�hA]�(]�(K K e]�(�ITEM #�K e]�(�1_��>What is growth rate used in your customer chart? Is growth the�e]�(�2.��>Will you split into Kootenay region vs. Okanagan? Has the BCUC�e]�(�3��
uuiluluugu�e]�(N�ACustomers pay for local costs, not always the upstream costs like�e]�(NK e]�(�5.�K e]�(N�6Was dealt with during Cost Of Service Analysis process�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M^j�  j�  )��}�(j�  �'Integrated System Plan Open House �j�  M^j�  (G@i+�    G@R���   G@y�_    G@V0R    t�ububh)��}�(h]�(j$	  j$	  eh]�(G@P�!���G@Yp�    G@�D---G@�UUUt�ah?]�(j  �!Integrated System Plan Open House�ehA]�(]�(�NO OF
ATTENDEES:��13�e]�(�	LOCATION:��5Creston, B.C., Creston and District Community Complex�e]�(�MEETING
DATE / TIME:���February 9, 2011
Doors open at 5:30 pm
Presentation 6:05 – 7:10 pm (group questions during presentation)
Information stations 7:10 – 8:20�e]�(j  Ne]�(�ITEM #��	Questions�e]�(j  �Group Questions�e]�(j  j  e]�(�1.��U What is growth rate used in your customer chart? Is growth the
same in all areas?�e]�(j  �� There are differences in growth rates across regions, between cities
and even within different areas of cities and towns.
 The growth shown is system-wide growth.�e]�(�2.��l Will you split into Kootenay region vs. Okanagan? Has the BCUC
been asked to change postage stamp rates?�e]�(j  �� No, we will not be splitting the Okanagan from the Kootenays.
 Yes, BCUC looked at it during recent rate design process and
concluded that postage stamp rates will remain.�e]�(�3.��X A new developer pays for poles and wires in a subdivision. Why do
rates still go up?�e]�(j  �� Customers pay for local costs, not always the upstream costs like
substations required or the cost of purchasing more energy to
supply new customers.�e]�(�4.��= How much is American demand influencing generation costs?�e]�(j  �� The graphs in the PowerPoint presentation show only the BC
market.
 FortisBC only sells a very small amount of power in the spring and
only when there is extra.
 Any FortisBC power sold, offsets power purchase costs.�e]�(�5.��5 Can you explain wholesale rates (i.e. to Nelson)?�e]�(j  �� Was dealt with during Cost Of Service Analysis process – the
process determined the costs on the system vs. what each
customer group was paying.
 Found commercial (6% reduction over the next few years) and�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M^j�  j�  )��}�(j�  �'Integrated System Plan Open House �j�  M^j�  (G@i+�    G@R���   G@y�_    G@V0R    t�ububh)��}�(h]�(j$	  j^  eh]�(hhC   ��9B@���R�hhC   @ۚ7@���R�G@�      G@��     t�ah?]�(�ITEM #�K ehA]�(]�(j�_  K e]�(NK e]�(�6�K e]�(NK e]�(�7�K e]�(�8_�K e]�(�9.�K e]�(�10_�K e]�(�1 U�K e]�(NK e]�(NK e]�(�12�K e]�(�13_�K e]�(NK e]�(�14.�K e]�(NK e]�(�15_�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M_j�  Nubh)��}�(h]�(j�^  j$	  eh]�((G@P��0�1G@R�    G@�g�y�G@�\*���t�(hhC   ��=@���R�hhC   `�<@���R�G@�      G@��     t�eh?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(j  ��industrial were overpaying.
 Residential, irrigation, wholesale were underpaying compared to
what they cost to the system as a whole.�e]�(�6.��? Shouldn’t commercial customers pay more than residential?�e]�(j  �� The adjustment was based only on what we charge and what it
costs to serve the customer group.
 If a customer group was paying more than they cost the system,
their rates will be decreased – this includes commercial.�e]�(�7.��C Are commercial users paying the same (kWh) rate as residential?�e]�(j  �� Each customer class has its own rate based on their costs to the
system for instance contact centre use, meter reading etc.
 Each customer group has its own rate (charged in kWhs).�e]�(�8.��u Is it more expensive to get energy to the Okanagan than the
Kootenays since you have generation in the Kootenays?�e]�(j  �� Some energy comes from the Kootenays but most of the energy
used in the Okanagan comes from interconnection points with BC
Hydro in the Okanagan.�e]�(�9.��; Will every meter be replaced for AMI or only new homes?�e]�(j  �X Every meter will be replaced over 2012 and 2013 if BCUC agrees
with the application.�e]�(�10.��� Do the rate increases compound each year? What is compound per
cent increase from the chart you show in the PowerPoint
presentation?�e]�(j  �? They do compound each year.
 Would be approximately 40%.�e]�(�11.�� What is smart grid?�e]�(j  �� One step further than smart meters - smart grid has monitors to talk
to rest of distribution system.
 Would allow some small local generation.
 Begins to allow control of all system below the substation level.�e]�(�12.��$ Will AMI cause a rate reduction?�e]�(�13.��� No, the cost for the program offsets the savings for a net sum zero
 We are doing this not just for economic reasons but also to provide
customers with more energy use information.�e]�(j  � Summary of Questions at Stations�e]�(�14.��0 Are you including any wind power generation?�e]�(j  �/ Would consider it as a clean energy option.�e]�(�15.��= Would you get involved in wood waste generation projects?�e]�(�ITEM #��	Questions�e]�(N�+Would consider it as a clean energy option.�e]�(�16��AHeard that in the community that FortisBC plans to raise the head�e]�(�17.��8Do you have any generation sources in front of BCUC now?�e]�(�18.��9Please explain the time-based vs. condition based system?�e]�(NK e]�(�19_��/Will you be including a standing offer program?�e]�(�20_��Comment�e]�(�21.��IOau�e]�(�21_��;Are you considering combined heat systems and other systems�e]�(�22.�K e]�(�0o�K e]�(�Cu�K e]�(�24.�K e]�(�25.��
LivGolalDU�e]�(�26_�K e]�(N�>Yes, you can access the information on the FortisBC website at�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M_j�  Nubh)��}�(hNh]�(hhC   @W�A@���R�hhC   �T]7@���R�G@�      G@��     t�ah?]�hA]�(]�(�ITEM #��	Questions�e]�(N�WWW.fortisbc com �e]�(�27 .��)When will AMI be implemented? We want it:�e]�(N�If�e]�(�28.��How much can we control on�e]�(N�>Can currently see transmission and to substation level but not�e]�(�29.��=How will the Kootenay Lake Water Stewardship planning process�e]�(N�DAt this point it is difficult to predict the outcome of the planning�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Maj�  Nubh)��}�(hNh]�(G@P���q�G@R�    G@�6�q�G@tz�@   t�ah?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(j  �www.fortisbc.com.�e]�(�27.��- When will AMI be implemented? We want it.�e]�(j  �O If accepted by the BCUC, implementation will take place over 2012
and 2013.�e]�(�28.��7 How much can we control on grid now vs. smart grid?�e]�(j  �� Can currently see transmission and to substation level but not
distribution.
 Smart grid has monitors to communicate with rest of distribution
system.
 Begins to allow control of all system below the substation level.�e]�(�29.��v How will the Kootenay Lake Water Stewardship planning process
influence the ISP – what provisions have you made?�e]�(j  �� At this point it is difficult to predict the outcome of the planning
process, however we have made mention of some potential
implications under the Generation section of the ISP.�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Maj�  Nubh)��}�(hNh]�(hhC   ��7;@���R�hhC   ��S@���R�G@�      G@��     t�ah?]�(K K ehA]�(]�(K K e]�(�ITEM #�K e]�(K K e]�(NK e]�(NK e]�(N�?but our needs are also different at different times of year and�e]�(�4_�K e]�(NK e]�(�5_�K e]�(NK e]�(�6_�K e]�(NK e]�(�7�K e]�(NK e]�(�8�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mbj�  j�  )��}�(j�  �I tt d StPlOH�j�  Mbj�  (G@i+�    G@R���   G@w�6�   G@V0R    t�ububh)��}�(h]�(j$	  j$	  eh]�(G@P�����G@Yp�    G@�n�]FG@�2fj���t�ah?]�(j  �!Integrated System Plan Open House�ehA]�(]�(�NO OF
ATTENDEES:��25�e]�(�	LOCATION:��Castlegar, B.C., Sandman Hotel�e]�(�MEETING
DATE / TIME:��|February 10, 2011
Doors open at 5:30 pm
Presentation and group questions 6:10 – 6:45 pm
Information stations 6:45 – 8:20�e]�(j  Ne]�(�ITEM #��	Questions�e]�(j  �Group Questions�e]�(j  j  e]�(�1.��D Can you offset planning reserve margin with operational savings?�e]�(j  �{ Depends on what happens in the market. Proposing to buy firm
“insurance policy” at one cost, the market fluctuates.�e]�(�2.��$ Is Waneta a freshet source only?�e]�(j  �� We are purchasing capacity from Waneta – it allows us to make
more efficient use of the energy we generate right now.
 Provides a type of storage under the Canal Plant Agreement.�e]�(�3.��< Are there seasonal differences in Waneta’s production?�e]�(j  �� Yes, but our needs are also different at different times of year and
the inclusion of the Waneta project in the Canal Plant Agreement
allows us to access it when needed.�e]�(�4.��3 Do you have targets for distributed generation?�e]�(j  �@ No, we have a net metering program available but no targets.�e]�(�5.��# You offer time of use metering?�e]�(j  � Yes.�e]�(�6.��' Does it match up with net metering?�e]�(j  �K It can but many of them don’t produce power when we need it the
most.�e]�(�7.��C Are smart meters up for debate still or has decision been made?�e]�(j  �} The Advanced Metering Infrastructure program will be going to the
BC Utilities Commission later this year for a decision.�e]�(�8.��6 Are you looking at smart meters in every location?�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mbj�  j�  )��}�(j�  �'Integrated System Plan Open House �j�  Mbj�  (G@i+�    G@R���   G@y�_    G@V0R    t�ububh)��}�(h]�(j�  j^  eh]�(hhC   ���<@���R�hhC   @�s6@���R�G@�      G@��     t�ah?]�(�ITEM #�K ehA]�(]�(j�`  K e]�(K K e]�(�J�K e]�(�10_�K e]�(�11.�K e]�(�12.�K e]�(K K e]�(K K e]�(NK e]�(K K e]�(�15�K e]�(NK e]�(�16�K e]�(�17.�K e]�(�18.�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mcj�  Nubh)��}�(h]�(j�^  j$	  eh]�(G@P�����G@R�    G@�WE�tG@�l�ʪ��t�ah?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(j  �` Yes, if the project is accepted by BCUC all meters in the FortisBC
system would be replaced.�e]�(�9.��5 Do you have an expected cost for the AMI project?�e]�(j  �� The up-front cost is about $40 million.
 We expect it to be a revenue neutral project – costs in first couple of
years and then decreases in following years.�e]�(�10.��_ With in-home displays, can you tell what in your house is causing
the change in energy use?�e]�(j  �� Most displays tell you how much your entire house is using at a
given time. You would have to do a little “sleuthing” to find which
fixtures or appliances in your home are high energy users.�e]�(�11.��k Comment – thanks for doing the open house. FortisBC is making a
big step coming out to the community.�e]�(�12.��? How does load growth compare in Kootenays vs. the Okanagan?�e]�(j  �� There are differences in growth rates across regions, between cities
and even within different areas of cities and towns.
 The growth shown on the slide is system-wide growth.�e]�(�13.��P What is the total amount of power generated in BC and what is the
total use?�e]�(j  �\ Don’t have the exact figure. Could perhaps get this kind of
information from BC Hydro.�e]�(�14.��: How much energy are you buying from the United States?�e]�(j  �o We don’t buy much from the US. We bought 9% from market last
year compared to about 45% that we generate.�e]�(j  � Summary of Questions at Stations�e]�(�15.��D Do you know what kind of power you are buying (i.e. coal fired)?�e]�(j  X   We do not monitor what is the source of the power that we buy. We
purchase energy from a marketer and don’t deal directly with the
generating company. We know that the marketer could be
purchasing the energy from any number of sources
(Wind/Hydro/Coal/Gas).�e]�(�16.��� Since FortisBC pays taxes and line charges to municipalities, why
shouldn’t local areas pay for infrastructure screening if they wish
instead of adding this to the project cost?�e]�(j  �� Lines and substations are paid for by all customers through rates.
We are proposing a consistent approach for all new projects.�e]�(�17.��h Is FortisBC interested in making distributed generation more
feasible for ratepayers to participate?�e]�(j  �) There are no incentives at this time.�e]�(�18.��D What kind of generation is FortisBC looking for to match up with�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mcj�  Nubh)��}�(h]�(j�  j^  eh]�(hhC   @��@@���R�hhC   `��6@���R�G@�      G@��     t�ah?]�(�ITEM #�K ehA]�(]�(jRa  K e]�(NK e]�(NK e]�(�19_�K e]�(K K e]�(K K e]�(�21_�K e]�(�22.�K e]�(�93�K e]�(�23_�K e]�(�C4_�K e]�(�25.�K e]�(�26�K e]�(NK e]�(�27 .�K e]�(�28_�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mdj�  Nubh)��}�(h]�(j�^  j$	  eh]�(G@P��y!dG@R�    G@�^��zG@�4Qꪪ�t�ah?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(j  �8wind generation? How much wind could our system sustain?�e]�(j  �� Wind is a non-firm source of generation so FortisBC would also
require a firm source as a simple cycle gas turbine peaking plant or
small hydro in addition to wind.�e]�(�19.��( Are you looking at Run of the River?�e]�(j  �V Not specifically, although we would consider small hydro as a clean
energy option.�e]�(�20.��5 What will be regulatory process be for AMI? When?�e]�(j  �� FortisBC will be making application to the BCUC later this year.
Once submitted, the BCUC will outline the regulatory process
required.�e]�(�21.��[ Have you heard about smart meter issues in other jurisdictions such
as privacy and EMF?�e]�(j  �g As part of the application process, FortisBC will provide a summary
of the experiences other areas.�e]�(�22.��M Have you decided what technology you will include in the AMI
application?�e]�(j  �, We are still investigating technologies.�e]�(�23.��A How aggressively will Terasen pursue district energy systems?�e]�(j  �I Provided contact name for Terasen district energy system
information.�e]�(�24.��5 What were the efficiency gains from ULE projects?�e]�(j  �� The efficiency gains from the ULE projects were recognized under
the Canal plant agreement. The total recognized gain was
approximately 10% additional capacity.�e]�(�25.��3 Is the FortisBC system ready for electric cars?�e]�(j  �w No, but part of the ISP planning is to look at future energy use
trends and ensure that we are when the time comes.�e]�(�26.��P What are you doing with South Slocan facilities? Why are we
changing things?�e]�(j  X   Some of the facilities in the South Slocan area are coming to the
end of their life span and are no longer appropriate for office use.
FortisBC is considering all facilities in the area including Terasen
Gas facilities to make sure we can meet our needs.�e]�(�27.��J How do you get condition based information back to our control
system?�e]�(j  �w The first part of the program would be information gathering to
record the current condition of our infrastructure.�e]�(�28.��9 Will AMI be able to accommodate time of use metering?�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mdj�  Nubh)��}�(hNh]�(hhC   `@=@���R�hhC    ��:@���R�G@�      hhC   �@"d@���R�t�ah?]�hA]�(]�(�ITfM #�K e]�(� TLi�K e]�(NK e]�(NK eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mej�  Nubh)��}�(hNh]�(G@P�G�UUUG@R�    G@���UUUG@[�G����t�ah?]�(�ITEM #��	Questions�ehA]�(]�(�ITEM #��	Questions�e]�(j  � Yes�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mej�  Nubh)��}�(hNh]�(G@O|(�   G@Z��    G@�(=p   G@pm�Y���t�ah?]�(�Organization��Date��Time��Location�ehA]�(]�(�Organization��Date��Time��Location�e]�(�MLA West Kootenay West��Feb 24, 2011��12:30 pm��MLA’s office, Castlegar�e]�(�District of Summerland
Council��Feb 28, 2011��8:30 am��	Town Hall�e]�(�City of Kelowna Council��Feb 28, 2011��1:30 pm��	City Hall�e]�(�Town of Princeton Council��March 7, 2011��7 pm��	Town Hall�e]�(�City of Trail Council��March 14, 2011��7 pm��	Town Hall�e]�(�/Regional District of
Okanagan Similkameen
Board��March 17, 2011��1:30 pm��Regional District office�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mgj�  j�  )��}�(j�  �;Integrated System Plan – Government Stakeholder Meetings �j�  Mgj�  (G@ZS3@   G@L�@   G@鯀   G@Q��    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report An Assessment of Public Reactions to the FortisBC Integrated System Plan March 15 2011 PN 7105�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
An Assessment of Public Reactions to the
FortisBC Integrated System Plan
March 15 2011
PN 7105�a]�X8  “Opinions are formed in a process of open
discussion and public debate, and where no
opportunity for the forming of opinions exists,
there may be moods—moods of the masses and
moods of individuals, the latter no less fickle and
unreliable than the former—but no opinion.”
- Hannah Arendt
2
Page 86 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mij�  Nubh)��}�(hNh]�(hhC    �yV@���R�hhC    �@J@���R�G@�      G@��     t�ah?]�(�Executive Summary��ge 4�ehA]�(]�(jb  jb  e]�(�Background and Methodology��ge 17�e]�(�Data Report��Ige�e]�(�Demographics��ge 24�e]�(�Price Sensitivity��ge 27�e]�(�Conservation��ge 29�e]�(�7Awareness Levels of the FortisBC Integrated System Plan��ge 33�e]�(�#Customer Perceptions and Priorities��ge 35�e]�(�Resource Planning��ge 51�e]�(�Capital Expenditures��ge 54�e]�(�Demand Side Management��ge 60�e]�(�&Advanced Metering Infrastructure (AMI)��ge 64�e]�(�Communication��ge 73�e]�(�1Overall Perceptions of the Integrated System Plan��ge 77�e]�(�"Appendix 1: How to Read 2X2 Charts��ge 80�e]�(�Appendix 2:�Ne]�(�SSummary of Super Group Question and Answer & Information Booth De-Brief Transcripts��SC�e]�(�'Appendix 3: Screener and Questionnaires��ige�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mjj�  Nubh)��}�(h]�j$	  ah]�((G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�eh?]�X,  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Table of Contents Executive Summary Page 4 Background and Methodology Page 17 Data Report Page 23 Demographics Page 24 Price Sensitivity Page 27 Conservation Page 29 Awareness Levels of the FortisBC Integrated System Plan Page 33 Customer Perceptions and Priorities Page 35 Resource Planning Page 51 Capital Expenditures Page 54 Demand Side Management Page 60 Advanced Metering Infrastructure (AMI) Page 64 Communication Page 73 Overall Perceptions of the Integrated System Plan Page 77 Appendix 1: How to Read 2X2 Charts Page 80 Appendix 2: Page 83 Summary of Super Group Question and Answer & Information Booth De-Brief Transcripts Appendix 3: Screener and Questionnaires Page 86 3�ahA]�(]�X,  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Table of Contents
Executive Summary Page 4
Background and Methodology Page 17
Data Report Page 23
Demographics Page 24
Price Sensitivity Page 27
Conservation Page 29
Awareness Levels of the FortisBC Integrated System Plan Page 33
Customer Perceptions and Priorities Page 35
Resource Planning Page 51
Capital Expenditures Page 54
Demand Side Management Page 60
Advanced Metering Infrastructure (AMI) Page 64
Communication Page 73
Overall Perceptions of the Integrated System Plan Page 77
Appendix 1: How to Read 2X2 Charts Page 80
Appendix 2:
Page 83
Summary of Super Group Question and Answer & Information Booth De-Brief Transcripts
Appendix 3: Screener and Questionnaires Page 86
3�a]��"Executive
Summary
4
Page 87 of 138�a]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
30,000 Foot View
Overall, customer perceptions of the Integrated System Plan (ISP) are positive.
• Customers identified moderate to high levels of support for implementing the proposed initiatives across each of the ISP
focus areas; however, there isn’t a strong willingness to pay for these initiatives.
Demand Side Resource Capital Revenue Integrated
AMI
Management Planning Expenditures Requirements System Plan
• Conservation • Planning Reserve Margin • Social and Environmental • AMI (Pgs. 13, 65- • Price Sensitivity (Pgs. • Overall ISP Rating
Attitudes (Pgs. 6, 7, (Pgs. 9, 52) Components (Pgs. 10, 72) 8, 28) (Pgs. 14, 78)
30, 31, 32) • Contractual Agreements 11, 55, 56, 57) • Willingness to pay • Understanding of
• Joint Conservation (Pgs. 9, 53) • Condition Based higher electricity prices ISP (Pgs. 74, 75)
Programs (Pg. 63) Management (Pgs. 12, (Pgs. 52, 57) • Balanced
58, 59 ) Perspective of
ISP (Pg. 76)
“I'm glad I came tonight because I gained a better understanding of where and how our daily electricity is generated, how it is provided and what
plans are being made for the future to meet expanding needs. While I don't look forward to increased costs in future, I must say I approved of
the methods being planned to keep costs down and to spread out costs over time. It seems like Fortis has their brains on straight.”
“I was somewhat surprised to know of a 40 to 41% increase in our rates over the next 5 years which I think is too excessive in my view.”
More Receptive Compared to Other Results in the Report: Average ratings of 85% or higher for strongly/somewhat agree or
definitely/probably should be considered.
Neutral Compared to Other Results in the Report: Average ratings of 61%-85% for strongly/somewhat agree or
definitely/probably should be considered.
Less Receptive Compared to Other Results in the Report: Average ratings of 60% or lower for strongly/somewhat agree or
definitely/probably should be considered, or average ratings of 40% or higher for strongly/somewhat disagree or definitely/probably
should NOT be considered. 5�a]�X@  Customer Priorities
Reliability of energy ranked most critical. Conserving energy and helping customers
manage consumption ranked higher (more critically important) after FortisBC’s
presentation, while keeping costs down and generating power in BC ranked lower.
Pre FortisBC Presentation Ranking of Planning Challenges Post FortisBC Presentation Ranking of Planning Challenges
Based on Total Respondents (n=115) Based on Total Respondents (n=115)
Ensuring reliable power 79% Ensuring reliable power 80%
Keeping down costs/ managing future costs 67% Conserving energy/ reducing energy consumption 73%
Generating power within BC 63% Helping customers manage consumption 72%
Conserving energy/ reducing energy consumption 54% Keeping down costs/ managing future costs 70%
Helping customers manage consumption 51% Generating power within BC 70%
Minimizing environmental impacts 50% Minimizing power outages 49%
Minimizing power outages 46% Minimizing environmental impacts 43%
Meeting the goals of the BC Clean Energy Act 44% Meeting the goals of the BC Clean Energy Act 41%
Meeting the goals of the BC Energy Plan 29% Meeting the goals of the BC Energy Plan 39%
Minimizing visual impacts of infrastructure/equipment 18% Minimizing visual impacts of infrastructure/equipment 25%
For further information please see pages 38 to 50.
6
Page 88 of 138�a]�Xu  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Conservation
Almost three quarters of customers identified conserving energy /reducing energy
consumption (73%) and helping customers manage consumption (72%) as critically
important challenges for planning for future energy and infrastructure needs.
• 83% of participants indicate that they feel conservation can have an impact on the regional supply of power (they feel they
can individually contribute to lowering overall levels of energy usage and believe that managing consumption can
positively contribute to energy supply). In contrast, only 6% of customers believe that conservation can play little or no
role in managing energy supply and that they personally cannot contribute to the regional energy supply.
Personal Contribution Conservation Impacts yletinifeD/ylekiL
but Limited Impact on Regional Power Supply
Overall Energy Supply 83%
4% NOITUBIRTNOC
LAUDIVIDNI
Total
toN
There are no regional ylekiL/yletinifeD
Support Conservation
Conservation is Not differences in these results.
but Unsure of
a Solution for Supply
Personal Contribution
6%
6% *Please note that results
may not round to 100% as
No/Minor Role Moderate/Major Role “no answers” are not
reported.
ROLE OF ENERGY CONSERVATION
For further information please see pages 30, 31 and 32.
7�a]�Xa  Revenue Requirements
Electrical rate increases are a concern across all potential ISP related initiatives.
Kootenay participants are more price sensitive; and consequently, they are less
willing to accept rate increases for ISP initiatives.
• Looking at current perceptions of electricity prices, considerably more Kootenay participants fall into the red zone (feel
prices are high and that rates noticeably impact household finances) and significantly fewer are in the green zone (price is
right and impact is low) compared to Okanagan participants.
• In general, Kootenay participants are less willing than Okanagan participants to pay more for initiatives. See pages 52 and
57.
SECNANIF SECNANIF
Yellow Zone Green Zone Yellow Zone Green Zone enoN enoN
14% 51% 17% 27%
/ /
DLOHESUOH llamS DLOHESUOH llamS
Okanagan Kootenays
n=56 n=59
elbaecitoN elbaecitoN
NO NO
Red Zone Yellow Zone Red Zone Yellow Zone
TCAPMI TCAPMI
21% 11% 37% 19%
High About Right / High About Right /
Low Low
PERCEIVED PRICE OF ELECTRICITY PERCEIVED PRICE OF ELECTRICITY
For further information please see pages 28, 52, and 57.
8
Page 89 of 138�a]�X<  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Resource Planning
Overall, 96% of customers support the Planning Reserve Margin and 75% support
the use of contractual agreements to fill small gaps in short term energy supply
rather than building new generation resources.
• Although 96% of participants support the Planning Reserve • 75% of participants strongly or somewhat agree with
Margin, only 60% are willing to pay higher electricity rates for it. using contractual agreements to fulfill small gaps in short
term energy supply rather than building additional
• Among those who state the planning reserve margin should
generation resources.
definitely be considered, 24% are not willing to pay more for it -
this jumps to 58% amongst those who state the planning • There are no differences in support for using contractual
reserve margin should probably be considered. agreements between Business and Residential
participants.
• Of those who wouldn’t pay higher prices to support the
Planning Reserve Margin, 69% are from the Kootenay region • 20% of Castlegar participants neither agree or disagree to
and 31% are from the Okanagan region. using contractual agreements to fill small gaps in short
term energy supply rather than building additional
96% Support for Planning Reserve Margin generation resources.
(Total Respondents, n=115)
75% Support for Contractual
Agreements
(Total Respondents, n=115)
Strongly Agree
24%
Somewhat Agree
51%
For further information please see pages 52 and 53.
9�a]�X*  Capital Expenditures - Social and Environmental Components
Customers identified impact on customer electricity rates as the most important
social and environmental consideration when determining future project and
equipment expenditures.
• 36% of participants state social and environmental components such as additional visual screening, special environmental
treatment, or other community specific amenities should definitely be considered in determining future project budgets,
while 53% state they should probably be considered. However, only half (50% ) of participants who state these
components should definitely/probably be considered are willing to pay higher electrical rates to include these components
in capital project budgets.
• In the context of different factors to consider for future project and capital expenditures, visual appearance ranks the
lowest. Only 14% of participants rate visual appearance as very important.
Very Important Important but Not
Critical
Impact on Customer Rates
Flexibility for Future Growth
Environmental Values
Distance from Buildings and Amenities
Effects on Community/Neighbourhood
During Construction
For further information
Timeliness of Construction Versus please see pages 55,
Need for Project 56, and 57.
Visual Appearance of Electrical
Equipment
10
Page 90 of 138�a]�X	  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Capital Expenditures - Social and Environmental Components
Support for including social and environmental components in future capital
budgets and willingness to accept higher rates to include these components
differed by region.
• Although participants from both regions support social and environmental components in future capital budgets,
participants from the Kootenay region are significantly less willing to pay for social and environmental components with
higher rates compared to participants from the Okanagan region.
• Overall, 1% was seen as a reasonable amount to add to the capital project budget by over half of participants.
Not Supportive, but Supportive and Willing Not Supportive, but Supportive and Willing
yletinifeD/ylekiL yletinifeD/ylekiL
Willing to Pay to Pay Willing to Pay to Pay
4% 59% 0% 42%
OT OT
SSENGNILLIW SSENGNILLIW
Okanagan Kootenays YAP YAP
n=56 n=59
toN toN
ylekiL/yletinifeD ylekiL/yletinifeD
Not Supportive and Supportive, but Not Not Supportive and Supportive, but Not
Not Willing to Pay Willing to Pay Not Willing to Pay Willing to Pay
4% 30% 10% 42%
Definitely/Probably Not Probably/ Definitely/Probably Not Probably/
Definitely Consider Definitely Consider
SOCIAL AND ENVIRONMENTAL COMPONENTS SOCIAL AND ENVIRONMENTAL COMPONENTS
Please note results may
not round to 100% as “no
For further information please see pages 55 , 56 and 57. answers” are not reported.
11�a]�X<  Capital Expenditures – Condition-Based Management
The majority of customers support the implementation of condition-based
management rather than time-based management.
• 92% of respondents support the change from time-based to condition-based management with just over one-third (37%)
state this process definitely should be considered. 55% state the change to condition-based management probably should
be considered.
• The top two reasons mentioned by participants for why condition-based management should be considered are:
• Condition-based management means lower costs overall, and
• Costs are lower because infrastructure is maintained/fixed when needed as opposed to on a fixed schedule.
• “Components should be replaced when needed, not because of time use policy.”
• “Cost effective in the long run.”
• “It makes sense as maintenance work is based on need not an arbitrary time span.”
• “I believe there will be a cost saving and reduced impact on the environment.”
• “Not enough info. "condition based" might have things till too late (i.e. timeliness vs. just-in-time).”
• “There is no definite relationship between condition based maintenance and cost savings. The investment
could provide little or no return.”
For further information please see pages 58 and 59.
12
Page 91 of 138�a]�XZ  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
AMI
While significantly more customers selected having in-home displays provided as
part of the AMI project, some hedged their selection with comments like only if
there is no additional cost or if it does not increase electricity rates.
Provide In-home displays Optional In-home Displays
59% 40%
• 30% say an in-home display will educate and make • 17% say they want the freedom to choose whether
them more aware of their energy usage. to have an in-home display and another 17% don’t
care or don’t see the value of an in-home display.
• Participants who would prefer an in-home display provided as part of the AMI project appear to have a stronger interest
in understanding their energy usage.
• Participants who prefer in-home displays to be optional indicate being disinterested in monitoring their usage or feel they
can manage their consumption on their own or via a secure website.
• 60% of respondents indicated that they strongly agree with offering an incentive program if in-home displays are an
optional component of AMI.
• If in-home displays are optional, most customers would pay up to 50 dollars for the technology.
• The most mentioned concerns by participants about AMI are about the impact of AMI on electricity rates and the
possibility of implementing time of use rates as well as the implications of time of use rates on electricity bills.
• Customers are looking for more information on AMI (9%) and are concerned about the cost implications (13%).
• 79% of customers indicate that they definitely or probably would use a secure FortisBC website to track energy usage.
However, access to a computer and computer literacy is an issue for 19% of customers.
For further information please see pages 65 to 72.
13�a]�X�  Overall Perceptions of the ISP
Overall, Super Group participants were supportive of the FortisBC Integrated
System Plan.
• 82% of participants did not find the FortisBC presentation confusing or difficult to understand.
• 94% strongly or somewhat agreed that FortisBC’s presentation helped them understand the ISP better.
• 83% strongly or somewhat agreed that FortisBC’s presentation provided a balanced perspective on the
ISP.
• 75% strongly or somewhat agreed that the ISP fulfills the objective of planning for the electrical needs of
the next 20 to 30 years.
• “Good presentation - appreciated the room full of experts. Excellent display boards. Come out feeling like I was
part of the team.”
• “It was interesting - it helped my understanding of hydro energy and happy to see your care.”
• “Very impressed. Lots of information. There was no discussion on alternative methods of hydro.”
• “After this presentation I strongly agree that everyone should have a smart meter installed in their home. Before
this presentation I would have been against this idea. Great presentation and this was time well spent. Thank
you! Keep up the great work!”
For further information please see pages 74 to 79.
14
Page 92 of 138�a]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Communications
Customers want to understand how different initiatives are going to benefit them
and what the bottom line cost implications are for them. Also important to
customers is information on how to better manage their energy consumption.
• Customers want FortisBC to provide additional information in several areas:
• How various initiatives will directly benefit them.
• The rate implications of implementing new programs. Specifically, they would like information on the size of
potential rate increases associated with new programs/processes and exactly how much it will increase their
electrical bill.
• How to better manage their energy consumption.
• Cost implications of AMI and in-home displays and how to use AMI to reduce energy consumption.
• How time of use rates work and how time of use rates would impact their lives and electricity bills (even
though FortisBC is not planning to implement time of use rates at this time).
• “How will this benefit me - I see how it benefits Fortis.”
• “I would like to see more information in $ signs as to how my FortisBC bill will be affected. Have customers
service representatives available to figure that out for individual customers.”
• “Concerned about cost of meter and possibility of charging for peak usage time.”
• “The cost and a breakdown of the advantages of the program (AMI).”
• “If FortisBC wants consumers to use less energy, it will force FortisBC to educate consumers of the value of in-
home displays and conserving energy.”
For further information please see pages 59, 61, 62, 71, 72, 74, 79, 84 and 85.
15�a]�Xv  Customer Class Differences
Both Residential and Business customers are looking for information on
conservation; however, the communications should be designed to meet their
different informational needs.
• The current price for household electricity is viewed as too high by Residential customers (53%) but about right by
Business customers (69%) (See page 28).
• Business customers are more likely to be aware of the ISP prior to attending the Super Groups (See page 34).
• Business customers are more likely than Residential to believe that FortisBC can supply as much energy as is needed in
the short term and long term (See pages 36 and 37).
• Residential customers provided more negative comments about the ISP (25% versus 6%) compared to Business customers
and appear to want a deeper level of understanding about FortisBC’s initiatives, particularly on how it impacts their
electrical bill. Additional information and education about future FortisBC initiatives may strengthen Residential support
of new initiatives like AMI (See page 79).
• The type of conservation programs desired differ between Residential (i.e., education on how to reduce energy
consumption) and Business customers (i.e., lighting rebates) (See page 62).
• The ISP presentation was found to be more difficult or confusing for Residential customers than Business customers (See
page 74).
16
Page 93 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Background and
Methodology
17�a]�X�  Project Overview
Background
FortisBC Inc. is an integrated regulated electric utility based in Kelowna, British Columbia. Focused on the safe delivery of
reliable and cost-effective electricity, FortisBC serves approximately 160,000 customers in the southern interior of B.C.
including residential, commercial (general service), industrial, lighting, irrigation and wholesale electricity customers. FortisBC
employs over 500 people in British Columbia and is an wholly owned subsidiary of Fortis Inc., the largest investor-owned
distribution utility in Canada.
Project Purpose
FortisBC is currently developing an Integrated System Plan. As part of its regulatory approval process, FortisBC is undertaking
consultation in the communities it services through open houses and direct dialogue with key stakeholders as well as general
communications and one-on-one discussions.
Illumina Research Partners has been asked to conduct research that will enable FortisBC to obtain detailed customer feedback
on the Integrated System Plan. The research will enable FortisBC to better understand the impacts of this plan on the
different regions and customer classes. The Super Group research process is used to provide a balanced representation of all
customer classes in both the Kootenay and Okanagan regions, providing feedback from customer classes which had been
under-represented during previous public open houses.
Overall Business Objective
Engage a representative cross-section of FortisBC customers in a meaningful dialogue and consultation on the ISP to
understand customer knowledge, beliefs, perceptions and concerns regarding the Integrated System Plan.
18
Page 94 of 138�a]�Xo  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Research Objectives
Overall Research Objectives
• Understand overall perception of ISP: positive, neutral, negative.
• Understand customer knowledge, beliefs, perceptions and concerns on the different components of the ISP: Resource
Plan, Capital Expenditures, Revenue Requirements, and Demand Side Management (as can be gathered based on the
questionnaire and information gathered during the super groups).
• Understand the impacts that changes due to the Integrated System Plan will have on different customer classes
(residential, commercial, industrial, wholesale and irrigation).
• Refine communications messages so that subsequent communications are able to explain the Integrated System Plan in a
way that resonates with each customer class.
Specific Research Questions to Address
• What is the customer context in regard to views on electrical prices, conservation, energy supply, and important
challenges for the region?
• How do customers feel about the Planning Reserve Margin, would customers pay higher prices to include this?
• What social and environmental factors are most important to customers when determining future energy expenditures?
Are customers willing to accept an increase in rates for social and environmental factors?
• Do customers agree with a condition-based infrastructure management approach?
• What is customer feedback on the AMI program? Should in-home meters be provided to all customers as part of the
AMI project or optional? If optional, is an incentive program needed?
• For each component, how willing are customers to pay higher prices for electricity for these changes in order to meeting
the planning objectives?
• What areas of the ISP are unclear to customers and need to be communicated differently?
19�a]�X1  Methodology – Overview
Session Agenda Super Group Method Details
1. Participant Registration • One Super Group was held in Kelowna on February 23,
2011 and a second one was held in Castlegar on
February 24, 2011.
2. Welcome by Illumina Research Partners
• In each Super Group, FortisBC gave a 30-minute
presentation on the Integrated System Plan. A formal
15 minute Question and Answer session was provided
3. Completion of Part 1 of the Integrated System Plan
Questionnaire following the presentation.
• FortisBC provided 5 information booths which
4. Introduction and Overview of the Evenings’ Purpose participants had 40 minutes to explore. FortisBC
representatives were available to answer questions at
each booth.
5. Presentation by FortisBC
• Participants were requested to complete surveys pre
and post session. The pre-survey was completed prior
to the presentation upon entry to the meeting, and
6. Question and Answer Period
the post-survey was completed following the
information booth session. Questionnaires were
developed using a format that allowed for scanning of
7. Exploration of FortisBC Information Booths the results into a database rather than data entry of
results. This method increases accuracy of results.
8. Completion of Part 2 of the Integrated System Plan
Questionnaire
20
Page 95 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mjj�  Nubh)��}�(hNh]�(hhC   `']P@���R�hhC    ��>@���R�G@�      hhC    �o@���R�t�ah?]�(NN�Customer €��lass Rep��resentatior�NNNNehA]�(]�(NNj�b  j�b  j�b  NNNNe]�(NN�Kelo��wna�NN�Castl��egar�Ne]�(�Customers By Class��Recri��it Quota�N�ctual��Recru��it Quota��A��Ictual�e]�(�Total�N�70�N�56�N�70�N�59�e]�(�Residential��49��(70%)��38��(68%)��49��(70%)��43��(73%�e]�(�General Service/Commercial��13��(19%)��11��(20%)��13��(19%)��9��(15%)�e]�(�Industrial Primary/Transmission��2��(3%)�N�(O%)��2��(3%)��2��(3%)�e]�(�Lighting��3��(4%)��3��(5%)��3��(4%)��2��(3%)�e]�(�
Irrigation��3��(4%)��2��(4%)��3��(4% ��3��(5%)�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Msj�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X	  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Methodology – Recruiting Strategy • Individuals were randomly selected by Illumina Research, from FortisBC’s customer database. These individuals were invited by telephone to attend a ‘focus group.’ This ‘focus group’ is referenced as a Super Group within this report as the size of the groups are much larger compared to a typical focus group. • The customer classes represented were: residential, general service (commercial), industrial Primary/Transmission, lighting and irrigation. A quota system was used to ensure that a minimum number of members of each of these customer classes was registered to attend the session. The table below provides these numbers. Please note that quotas are based on a recruitment goal, however, not all recruited will show for the event. • Screening questions did not specifically screen out respondents who may have attended a FortisBC Super Group in 2007 or 2009. Screening questions were in place to limit the number of people who have previously attended any focus groups. • Illumina Research Partners is a member of the The Marketing Research and Intelligence Association (MRIA), which is a Canadian not-for-profit association representing all aspects of the market intelligence and survey research industry. The MRIA code on conduct for members specifies that for research utilizing customer databases as the source for sample selection, the source of the list and identity of the Client must be revealed, if requested by the participant. The protocol for recruiters was that if requested, participants were to be told the groups were sponsored by FortisBC to discuss energy and infrastructure needs for the future. • Local participants received a $75 cash honorarium for attending. Individuals driving in excess of 1.0 hour were given a larger incentive of $100. Customer Class Representation Kelowna Castlegar Customers By Class Recruit Quota Actual Recruit Quota Actual Total 70 56 70 59 Residential 49 (70%) 38 (68%) 49 (70%) 43 (73%) General Service/Commercial 13 (19%) 11 (20%) 13 (19%) 9 (15%) Industrial Primary/Transmission 2 (3%) 0 (0%) 2 (3%) 2 (3%) Lighting 3 (4%) 3 (5%) 3 (4%) 2 (3%) Irrigation 3 (4%) 2 (4%) 3 (4%) 3 (5%) 21�ahA]�(]�X	  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Methodology – Recruiting Strategy
• Individuals were randomly selected by Illumina Research, from FortisBC’s customer database. These individuals were
invited by telephone to attend a ‘focus group.’ This ‘focus group’ is referenced as a Super Group within this report as the
size of the groups are much larger compared to a typical focus group.
• The customer classes represented were: residential, general service (commercial), industrial Primary/Transmission, lighting
and irrigation. A quota system was used to ensure that a minimum number of members of each of these customer classes
was registered to attend the session. The table below provides these numbers. Please note that quotas are based on a
recruitment goal, however, not all recruited will show for the event.
• Screening questions did not specifically screen out respondents who may have attended a FortisBC Super Group in 2007 or
2009. Screening questions were in place to limit the number of people who have previously attended any focus groups.
• Illumina Research Partners is a member of the The Marketing Research and Intelligence Association (MRIA), which is a
Canadian not-for-profit association representing all aspects of the market intelligence and survey research industry. The
MRIA code on conduct for members specifies that for research utilizing customer databases as the source for sample
selection, the source of the list and identity of the Client must be revealed, if requested by the participant. The protocol for
recruiters was that if requested, participants were to be told the groups were sponsored by FortisBC to discuss energy and
infrastructure needs for the future.
• Local participants received a $75 cash honorarium for attending. Individuals driving in excess of 1.0 hour were given a
larger incentive of $100.
Customer Class Representation
Kelowna Castlegar
Customers By Class Recruit Quota Actual Recruit Quota Actual
Total 70 56 70 59
Residential 49 (70%) 38 (68%) 49 (70%) 43 (73%)
General Service/Commercial 13 (19%) 11 (20%) 13 (19%) 9 (15%)
Industrial Primary/Transmission 2 (3%) 0 (0%) 2 (3%) 2 (3%)
Lighting 3 (4%) 3 (5%) 3 (4%) 2 (3%)
Irrigation 3 (4%) 2 (4%) 3 (4%) 3 (5%)
21�a]�X�  The Two Part Questionnaire Process
• After a brief welcome to the Super Group session participants were asked to complete Part 1 of a questionnaire. This
questionnaire gathered demographics and gained insight into customer views of current planning challenges before
FortisBC presented any information regarding the ISP. The Part 2 questionnaire was provided after the FortisBC content
presentation, question and answer period, and information booth exploration.
P art A Questionnaire Part B Questionnaire
– Demographic information on age, gender, employment – Post-test assessment of the importance of various challenges
status, home ownership, type of dwelling, number of to consider when planning for the future
residents in dwelling, and usage of heating and cooling – Support for contractual agreements
energy sources
– Support of, and likelihood to pay higher prices for a Planning
– Perceptual questions on the acceptability of current Reserve Margin
electricity prices and the impact on household finances
– Rating of the importance of various social and environmental
– Perception of power supply in the short term (2 to 5 years) components when determining future project and equipment
and long term (20 years from now) expenditures
– Attitudes about the role that conservation/managing – Support of, and likelihood to pay higher prices including social
consumption plays on energy supply (individually and overall) and environmental factors in budgets
– Awareness of the ISP prior to the Super Group sessions – Opinions on what a reasonable amount would be to add to
– Pre-test assessment of the importance of various challenges each project budget for social and environmental factors
to consider when planning for the future – Support for switching to condition-based management
– Ranking of the planning challenges attributes – Perceptions and support for AMI components (in-home
– Awareness of the PowerSense program displays, incentive programs for purchasing in-home displays,
price point for in-home displays, likelihood to use a website
for tracking energy usage
– Support for joint conservation programs between FortisBC
and Terasen Gas
– Overall assessment question regarding the ISP
– Overall feedback on the presentation
22
Page 96 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Msj�  Nubh)��}�(hNh]�(G@`.-    G@m��    G@N�   G@uFb@   t�ah?]�(j  �Kelowna�j  �	Castlegar�j  ehA]�(]�(j  �Kelowna�N�	Castlegar�Ne]�(�Customers By Class��Recruit Quota��Actual��Recruit Quota��Actual�e]�(�Total��70��56��70��59�e]�(�Residential��49 (70%)��38 (68%)��49 (70%)��43 (73%)�e]�(�General Service/Commercial��13 (19%)��11 (20%)��13 (19%)��9 (15%)�e]�(�Industrial Primary/Transmission��2 (3%)��0 (0%)��2 (3%)��2 (3%)�e]�(�Lighting��3 (4%)��3 (5%)��3 (4%)��2 (3%)�e]�(�
Irrigation��3 (4%)��2 (4%)��3 (4%)��3 (5%)�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Msj�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Customer Demographics Page 24 Price Sensitivity Page 27 Conservation Attitudes Page 29 Awareness of the Integrated System Plan Page 33 Customer Perceptions and Priorities Page 35 Resource Plan Data Report Page 51 Capital Expenditures Page 54 Demand Side Management Page 60 Advanced Metering Infrastructure (AMI) Page 64 Communications Page 73 Overall Perceptions of the Integrated System Plan Page 77 23�ahA]�(]�X  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Customer Demographics
Page 24
Price Sensitivity
Page 27
Conservation Attitudes
Page 29
Awareness of the Integrated System Plan
Page 33
Customer Perceptions and Priorities
Page 35
Resource Plan Data Report
Page 51
Capital Expenditures
Page 54
Demand Side Management
Page 60
Advanced Metering Infrastructure (AMI)
Page 64
Communications
Page 73
Overall Perceptions of the Integrated System Plan
Page 77
23�a]��'Customer
Demographics
24
Page 97 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mtj�  Nubh)��}�(h]�(j�  j$	  j$	  j$	  eh]�((hhC    �T@���R�hhC    <W`@���R�G@�      G@��     t�(hhC   @cQ@���R�hhC   `��E@���R�G@�      G@��     t�eh?]�(K �Total��Kelowna��	Castlegar�ehA]�(]�(K jCc  jDc  jEc  e]�(K NNNe]�(K �52%��55%��49%�e]�(�Oil��2%��4%��0%�e]�(�Propane��3%��4%��2%�e]�(�Electricity��50%��39%��59%�e]�(�Wood��22%��18%��25%�e]�(K �4%��7%��2%�e]�(�	No answer��1%��2%��0%�e]�(�Main Heating System�NNNe]�(�Central air��46%��57%��36%�e]�(�Electric baseboards��18%��18%��19%�e]�(�	Nol walei��0/0��Lio��0/0�e]�(�Heat pump (air or ground)��10%��11%��8%�e]�(�neat pump (air or grouna)��10/0��LL/o��6/0�e]�(�WOod, gas or electrictireplace��I1%��ro��15%�e]�(K �L0/o��4/0��1J/0�e]�(�Don t Know/No answer��3%��2%��3%�e]�(�central air��38%��57%��20%�e]�(K �23%��29%��19%�e]�(K �3r��13��61'�e]�(K �1/0��Lio��0/0�e]�(K �	nces at a��95% confidenc��e level�e]�(NNN�
Page 98 of�e]�(�Questionnaire Data��Total��Kelowna��	Castlegar�e]�(�Age�NNNe]�(�18 to 34��16%��13%��19%�e]�(�35to 54��31%��32%��31%�e]�(�
55and more��53%��55%��51%�e]�(�	No answer��0%��0%��0%�e]�(�Gender�NNNe]�(�Male��55%��54%��56%�e]�(�Female��45%��46%��44%�e]�(�Emplovment Status�NNNe]�(�Working full-time��38%��41%��36%�e]�(K �14%��16%��12%�e]�(K �8%��4%��12%�e]�(�at home full-time��5%��7%��3%�e]�(�Student��3%��2%��5%�e]�(�Retired��30%��30%��31%�e]�(�	No answer��1%��0%��2%�e]�(�Number of People_in Household�NNNe]�(�1��15%��16%��14%�e]�(�2��43%��46%��41%�e]�(�3��18%��13%��24%�e]�(�4or more��22%��21%��22%�e]�(�	No answer��2%��4%��0%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Muj�  j�  )��}�(j�  �&Air Conditioning in HomeSquare Footage�j�  Muj�  (G@p=    G@X��@   G@�R�   G@Ze��   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Price Sensitivity 27�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Price Sensitivity
27�a]�Xy  Super Group Respondent Profiles – Price Sensitivity
Half of customers find energy prices ‘about right’ and identify that prices have
only a small impact on their finances. Kootenay residents are comparatively
more price sensitive.
Attitude Toward Current Price of Household Electricity Impact of Household Electricity Bill on Household Finances
Total Kelowna (Okanagan) Castlegar (Kootenay) Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59 n=115 n=56 n=59
100% 100%
80% 80%
63%
54% 56%
60% 54% 60% 55%
50%
45% 46% 44% 44%
40% 36% 40%
32%
20% 20%
11%
5%
0% 0% 0% 1% 2% 0% 0% 1% 2% 0%
0% 0%
Too high About right Too low No answer Noticeable Small Impact No Impact No answer
Impact
Total Residential Business
n=115 n=81 n=32
About right 54% 47% 69%
Too high 45% 53% 28%
Refused 1% 0% 3%
Indicates significant regional differences at a 95% confidence level
28
Page 99 of 138�ae�-      h�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mvj�  Nubh)��}�(hNh]�(G@[J��   G@����   G@h�    G@���~��Ot�ah?]�(�54%��63%�ehA]�(]�(NNNNNNj  Ne]�(�54%
45%
36%�NNNNNj  Ne]�(NNNj  Nj  NNe]�(Nj  NNNNNj  e]�(j  j  j  j  j  j  j  j  e]�(NNj  NNNNNe]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mvj�  j�  )��}�(j�  �Price Sensitivity �j�  Mvj�  (G@a�t�   G@�5��   G@l�X�   G@���    t�ububh)��}�(hNh]�(G@t�j�   G@��   G@y��@   G@���~��Ot�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(NNNj  NNj  Ne]�(NNNNNj  NNe]�(Nj  NNNNNj  e]�(j  j  �32%�j  j  j  j  j  e]�(NNj  NNNNNe]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mvj�  j�  )��}�(j�  �Price Sensitivity �j�  Mvj�  (G@a�t�   G@�5��   G@l�X�   G@���    t�ububh)��}�(hNh]�(G@z���   G@��x�   G@|�    G@���~��Ot�ah?]�(j  j  ehA]�(]�(Nj  e]�(j  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mvj�  j�  )��}�(j�  �Price Sensitivity �j�  Mvj�  (G@a�t�   G@�5��   G@l�X�   G@���    t�ububh)��}�(hNh]�(G@bJ=`   G@�u �   G@r��@   G@�aE�   t�ah?]�(j  �Total n=115��Residential n=81��Business n=32�ehA]�(]�(j  �Total
n=115��Residential
n=81��Business
n=32�e]�(�About right��54%��47%��69%�e]�(�Too high��45%��53%��28%�e]�(�Refused��1%��0%��3%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mvj�  j�  )��}�(j�  �Price Sensitivity �j�  Mvj�  (G@a�t�   G@�5��   G@l�X�   G@���    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Conservation Attitudes 29�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Conservation
Attitudes
29�a]�X�  Super Group Respondent Profiles – Conservation Attitudes
FortisBC customers in both regions believe that energy conservation can play a
moderate to major role in power supply. Most people do feel they as individuals
can contribute to reducing overall energy usage in their regions.
The Role of Energy Conservation to Regional Power Supply Customer Contribution to Energy Conservation
Total Kelowna (Okanagan) Castlegar (Kootenay) Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59 n=115 n=56 n=59
100% 100%
80% 80%
60% 53% 54% 53% 60% 54%
47% 46%
39% 41% 41%
40% 37% 34% 40% 36%
20% 10% 13% 8% 20% 12% 11% 14%
0% 0% 0% 0% 0% 0%
0% 0%
Major Role Moderate Role Minor Role No Role at All Definitely could Likely could Likely not reduce Definitely not
reduce overall reduce overall overall energy reduce overall
energy use energy use use energy use
Indicates significant regional differences at a 95% confidence level
30
Page 100 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mwj�  Nubh)��}�(hNh]�(G@[J��   G@��Z    G@h"N    G@��d^���t�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(Nj  j  j  NNNNe]�(j  j  j  j  �34%�NNj  e]�(NNNNNj  j  Ne]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mwj�  j�  )��}�(j�  �Conservation Attitudes �j�  Mwj�  (G@b�V�   G@��r@   G@k�;�   G@���    t�ububh)��}�(hNh]�(G@t���   G@��Z    G@y�Z    G@��d^���t�ah?]�(�% 54%�j  j  j  j  j  ehA]�(]�(NNj  NNNNNe]�(Nj  NNNNNj  e]�(j  j  j  j  j  j  j  j  e]�(NNNNNNj  Ne]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mwj�  j�  )��}�(j�  �Conservation Attitudes �j�  Mwj�  (G@b�V�   G@��r@   G@k�;�   G@���    t�ububh)��}�(hNh]�(G@iڇ@   G@�i����G@l�=�   G@��d^���t�ah?]�(j  j  ehA]�]�(j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mwj�  j�  )��}�(j�  �Conservation Attitudes �j�  Mwj�  (G@b�V�   G@��r@   G@k�;�   G@���    t�ububh)��}�(hNh]�(G@z��@   G@�i����G@|�?�   G@��d^���t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mwj�  j�  )��}�(j�  �Conservation Attitudes �j�  Mwj�  (G@b�V�   G@��r@   G@k�;�   G@���    t�ububh)��}�(hNh]�(hhC   �h�G@���R�hhC    _�R@���R�G@�      G@��     t�ah?]�(�0Customer Awareness of FortisBC PowerSense Energy��	fficienci��Y�K ehA]�(]�(j�d  j�d  j�d  K e]�(�Total Mentions��Total��Kelowna�K e]�(�VLightbulbs/rebate for lightbulbs/lighting/rebate on more efficient lightbulbs/CF bulbs��47%��42%�K e]�(�JAppliances/energy-efficient appliances/replacement of old home appliances_��15%��15%�K e]�(�KWindows/rebate for newer windows/installing energy-efficient windows/window�NNK e]�(�upgrades/(doors)��13%��15%�K e]�(�LFurnaces/more efficient furnaceslfurnace changeout/upgradeslupdated furnaces��9%��6%�K e]�(�Heat pumps/geothermal heating_��9%��12%�K e]�(�re��9%��9%�K e]�(�IHot water heaterlreplacing hot water tankslenergy-efficient water heaters��7%��6%�K e]�(�?Insulation/home insulation rebate program for better insulation��7%��12%�K e]�(�KHome energy rebates/rebate on house repairs to keep energy consumption down��6%��3%�K e]�(�PMaking equipment more efficient/upgrading existing equipment/changing industrial�NNK e]�(�"equipment to more efficient models��6%��6%�K e]�(�3Solar panelskprogram for solar hot waterlsolar B.C:��6%��9%�K e]�(�6Home building with high R value/new home construction_��4%��6%�K e]�(�-PowerSmart/PowerSense (not specified further)��4%��3%�K e]�(�>VFDs/incentive to install VFD on motors/VFD for motor starting��4%��0%�K e]�(�JChristmas lights/exchanging older Christmas lights for rebates on LED ones��3%��6%�K e]�(�FLIP program for business��3%��6%�K e]�(�6Had a power audit completed/evaluation of energy usage��3%��3%�K e]�(�Clotheslines/free clotheslines��1%��3%�K e]�(�Miscellaneous single mentions��4%��3%�K e]�(�Don't know/not stated��6%��3%�K e]�(�Refused/No answer��3%��0%�K eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mxj�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Super Group Respondent Profiles – Conservation Awareness Almost 60% of customers identify awareness of FortisBC PowerSense rebates or programs. Almost half of those aware had heard of rebates for light bulbs/energy efficient lightbulbs.* Awareness of FortisBC PowerSense Energy Efficiency Rebates or Programs Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% *Please see the following page for a list of the rebates and 80% programs identified. 59% 59% 59% 60% 41% 41% 41% 40% 20% 0% Yes No 31�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Super Group Respondent Profiles – Conservation Awareness
Almost 60% of customers identify awareness of FortisBC PowerSense rebates or
programs. Almost half of those aware had heard of rebates for light
bulbs/energy efficient lightbulbs.*
Awareness of FortisBC PowerSense Energy Efficiency Rebates or
Programs
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100% *Please see the
following page for a list
of the rebates and
80% programs identified.
59% 59% 59%
60%
41% 41% 41%
40%
20%
0%
Yes No
31�a]�XB  Super Group Respondent Profiles – Conservation Awareness
Of the various PowerSense rebates and programs customers are most aware of
rebates on energy efficient light bulbs, appliances, and windows.
Customer Awareness of FortisBC PowerSense Energy Efficiency
Rebates or Programs
Kelowna Castlegar
Total (Okanagan) (Kootenay)
Total Mentions n=68 n=33 n=35
Lightbulbs/rebate for lightbulbs/lighting/rebate on more efficient lightbulbs/CF bulbs 47% 42% 51%
Appliances/energy-efficient appliances/replacement of old home appliances 15% 15% 14%
Windows/rebate for newer windows/installing energy-efficient windows/window
upgrades/(doors) 13% 15% 11%
Furnaces/more efficient furnaces/furnace changeout/upgrades/updated furnaces 9% 6% 11%
Heat pumps/geothermal heating 9% 12% 6%
Heating/rebates on energy-efficient heating/changing heating systems/rebates or
replacing heating and cooling equipment 9% 9% 9%
Hot water heater/replacing hot water tanks/energy-efficient water heaters 7% 6% 9%
Insulation/home insulation rebate/program for better insulation 7% 12% 3%
Home energy rebates/rebate on house repairs to keep energy consumption down 6% 3% 9%
Making equipment more efficient/upgrading existing equipment/changing industrial
equipment to more efficient models 6% 6% 6%
Solar panels/program for solar hot water/solar B.C. 6% 9% 3%
Home building with high R value/new home construction 4% 6% 3%
PowerSmart/PowerSense (not specified further) 4% 3% 6%
VFDs/incentive to install VFD on motors/VFD for motor starting 4% 0% 9%
Christmas lights/exchanging older Christmas lights for rebates on LED ones 3% 6% 0%
FLIP program for business 3% 6% 0%
Had a power audit completed/evaluation of energy usage 3% 3% 3%
Clotheslines/free clotheslines 1% 3% 0%
Miscellaneous single mentions 4% 3% 6%
Don't know/not stated 6% 3% 9%
Refused/No answer 3% 0% 6%
32
Page 101 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mxj�  Nubh)��}�(hNh]�(G@kݺ�   G@mS{@   G@xk   G@s��   t�ah?]�(j  �59%��59%��59%�j  j  j  j  ehA]�(]�(j  j  j  j  j  j  NNNNNNe]�(j  j  j  j  j  j  j  j  j  j  j  j  e]�(j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mxj�  Nubh)��}�(hNh]�(G@a}�p   G@�Š   G@}��Z���G@��l�   t�ah?]�(�Rebates or Programs�j  j  j  ehA]�(]�(�Total Mentions��
Total
n=68��Kelowna
(Okanagan)
n=33��Castlegar
(Kootenay)
n=35�e]�(�VLightbulbs/rebate for lightbulbs/lighting/rebate on more efficient lightbulbs/CF bulbs��47%��42%��51%�e]�(�IAppliances/energy-efficient appliances/replacement of old home appliances��15%��15%��14%�e]�(�\Windows/rebate for newer windows/installing energy-efficient windows/window
upgrades/(doors)��13%��15%��11%�e]�(�LFurnaces/more efficient furnaces/furnace changeout/upgrades/updated furnaces��9%��6%��11%�e]�(�Heat pumps/geothermal heating��9%��12%��6%�e]�(�wHeating/rebates on energy-efficient heating/changing heating systems/rebates or
replacing heating and cooling equipment��9%��9%��9%�e]�(�IHot water heater/replacing hot water tanks/energy-efficient water heaters��7%��6%��9%�e]�(�?Insulation/home insulation rebate/program for better insulation��7%��12%��3%�e]�(�KHome energy rebates/rebate on house repairs to keep energy consumption down��6%��3%��9%�e]�(�sMaking equipment more efficient/upgrading existing equipment/changing industrial
equipment to more efficient models��6%��6%��6%�e]�(�3Solar panels/program for solar hot water/solar B.C.��6%��9%��3%�e]�(�5Home building with high R value/new home construction��4%��6%��3%�e]�(�-PowerSmart/PowerSense (not specified further)��4%��3%��6%�e]�(�>VFDs/incentive to install VFD on motors/VFD for motor starting��4%��0%��9%�e]�(�JChristmas lights/exchanging older Christmas lights for rebates on LED ones��3%��6%��0%�e]�(�FLIP program for business��3%��6%��0%�e]�(�6Had a power audit completed/evaluation of energy usage��3%��3%��3%�e]�(�Clotheslines/free clotheslines��1%��3%��0%�e]�(�Miscellaneous single mentions��4%��3%��6%�e]�(�Don't know/not stated��6%��3%��9%�e]�(�Refused/No answer��3%��0%��6%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mxj�  j�  )��}�(j�  ��Super Group Respondent Profiles – Conservation Awareness Of the various PowerSense rebates and programs customers are most aware of rebates on energy efficient light bulbs, appliances, and windows. �j�  Mxj�  (G@T"z`   G@r�k�   G@�:    G@u�~�   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Awareness Levels of the FortisBC Integrated System Plan 33�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Awareness Levels of
the FortisBC
Integrated System
Plan
33�a]�X  Awareness of the Integrated System Plan
The majority of participants were unaware of the ISP prior to the FortisBC Super
Group Town Hall Forum. Awareness was higher at the Kelowna session.
Awareness of the Integrated System Plan Prior to the FortisBC Super Awareness of the Integrated System Plan Prior to
Group Session the FortisBC Super Group Session
(Comparison by Customer Type)
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
Total Residential Business
n=115 n=81 n=32
83%
80% 78% 73% Yes 15% 6% 34%
No 78% 86% 59%
60% Don’t 7% 7% 6%
know
40%
21% Business customers are more likely to be aware of
20% 15% the ISP prior to attending the Super Groups.
8% 7% 5% 8%
0%
Yes No Don't know
Indicates significant regional differences at a 95% confidence level
34
Page 102 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Myj�  Nubh)��}�(hNh]�(G@v�    G@���   G@�`   G@���@   t�ah?]�(j  �Total n=115��Residential n=81��Business n=32�ehA]�(]�(j  �Total
n=115��Residential
n=81��Business
n=32�e]�(�Yes��15%��6%��34%�e]�(�No��78%��86%��59%�e]�(�Don’t
know��7%��7%��6%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Myj�  j�  )��}�(j�  �Integrated System Plan �j�  Myj�  (G@a']�   G@�ʝ�   G@mR�`   G@���`   t�ububh)��}�(hNh]�(G@a�r�   G@��
@   G@o������G@�0㋢�t�ah?]�(j  j  j  �78%�j  �83%�ehA]�(]�(NNNNNNj  e]�(j  NNNj  �73%�j  e]�(NNNNNj  Ne]�(j  NNNj  j  j  e]�(�21%�NNNj  j  j  e]�(�15%�Nj  �8%�j  j  j  e]�(Nj  NNNNNe]�(NNNj  NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Myj�  j�  )��}�(j�  �3Awareness Levels of the FortisBC Integrated System �j�  Myj�  (G@`z�   G@�C��   G@n+�   G@���    t�ububh)��}�(hNh]�(G@p�@   G@�ܞ����G@s��`   G@�0㋢�t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Myj�  j�  )��}�(j�  �3Awareness Levels of the FortisBC Integrated System �j�  Myj�  (G@`z�   G@�C��   G@n+�   G@���    t�ububh)��}�(hNh]�(hhC    M=`@���R�hhC   �a�V@���R�G@�      G@��     t�ah?]�(K �	y and Den��and for Next Tv��JO to Five Years�ehA]�(]�(K jf  jf  jf  e]�(K �Total��Residential��Business�e]�(K �19%��20%��16%�e]�(K �47%��40%��66%�e]�(K �32%��40%��16%�e]�(K �2%��1%��3%�e]�(K �	5 custome��rS are more likel��y than�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mzj�  j�  )��}�(j�  �\Residential to believe that FortisBC can supply as much energy as needed in the short term. �j�  Mzj�  (G@Z�r�   G@R���   G@pc�    G@V��   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Customer Perceptions and Priorities 35�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Customer Perceptions
and Priorities
35�a]�X�  Awareness of Short-Term Supply and Demand
Customers in both the Okanagan and Kootenay regions perceive that energy
supply will meet demand in the short-term. The Okanagan region perceives a
greater deficit in short term supply.
Awareness of Supply and Demand for Next Two to Five Years
Awareness of Supply and Demand for Next Two to Five Years
Total Kelowna (Okanagan) Castlegar (Kootenay)
(Comparison by Customer Type)
n=115 n=56 n=59
100%
Total Residential Business
n=115 n=81 n=32
80%
More than enough 19% 20% 16%
60%
47% 45% 49% 46% As much as needed 47% 40% 66%
40%
32%
29%
Less than needed 32% 40% 16%
19% 19%
20%
9% Refused 2% 1% 3%
2% 0% 3%
0% Business customers are more likely than
More than As much as Less than needed No answer
Residential to believe that FortisBC can supply as
enough needed
much energy as needed in the short term.
Indicates significant regional differences at a 95% confidence level
36
Page 103 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mzj�  Nubh)��}�(hNh]�(G@s��    G@�]��   G@�FI�   G@�&p�   t�ah?]�(j  �Total n=115��Residential n=81��Business n=32�ehA]�(]�(j  �Total
n=115��Residential
n=81��Business
n=32�e]�(�More than enough��19%��20%��16%�e]�(�As much as needed��47%��40%��66%�e]�(�Less than needed��32%��40%��16%�e]�(�Refused��2%��1%��3%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mzj�  j�  )��}�(j�  ��Awareness of Short-Term Supply and Demand Customers in both the Okanagan and Kootenay regions perceive that energy supply will meet demand in the short-term.  The Okanagan region perceives a �j�  Mzj�  (G@T$j`   G@r�A@   G@�n�   G@u�~�   t�ububh)��}�(hNh]�(G@[J��   G@��4�   G@m�     G@�~sp   t�ah?]�(j�  �47%��45%��49%��%�j�  �46%�j  ehA]�(]�(NNNNNj  j  j  NNj  Ne]�(�29%
19%�NNNNj  j  j  �32%�Nj  Ne]�(NNNNNNNNNj  NNe]�(NNNj  NNNNNNNNe]�(j  j  �9%�j  j  j  j  j  j  j  j  j  e]�(NNj  NNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mzj�  j�  )��}�(j�  �$Customer Perceptions and Priorities �j�  Mzj�  (G@_�N    G@��r@   G@n�"�   G@���    t�ububh)��}�(hNh]�(hhC   @��@���R�hhC   �'P@���R�G@�      hhC   `��@���R�t�ah?]�(K �Residential��Busines=�ehA]�(]�(K j�f  j�f  e]�(K �11%��13%�e]�(N�25%��50%�e]�(N�63%��38%�e]�(K �1%��0%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M{j�  j�  )��}�(j�  ��Reliability was selected as critically important by more customers than any other challenge.  However, when asked to rank the challenges, cost became the one most important consideration.   See the next page. �j�  M{j�  (G@]Tz�   G@B��    G@|��    G@J�o�   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Awareness of Long-Term Supply and Demand Customers in both the Okanagan and Kootenay regions perceive that energy supply will not meet demand in the long term. The Okanagan region perceives a greater deficit in long term energy supply. Awareness of Supply and Demand for Next Twenty Years Awareness of Supply and Demand for Next Twenty Years (Comparison by Customer Type) Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 Total Residential Business 100% n=115 n=81 n=32 80% 73% More than enough 11% 11% 13% 60% 56% As much as needed 32% 25% 50% 39% 39% 40% 32% Less than needed 56% 63% 38% 25% 20% 20% No answer 1% 1% 0% 11% 2% 1% 0% 2% Business customers are more likely than 0% Residential to believe that FortisBC can supply as More than As much as Less than needed No answer much energy as needed in the long term. enough needed Indicates significant regional differences at a 95% confidence level 37�ahA]�(]�X  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Awareness of Long-Term Supply and Demand
Customers in both the Okanagan and Kootenay regions perceive that energy
supply will not meet demand in the long term. The Okanagan region perceives a
greater deficit in long term energy supply.
Awareness of Supply and Demand for Next Twenty Years Awareness of Supply and Demand for Next Twenty Years
(Comparison by Customer Type)
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
Total Residential Business
100%
n=115 n=81 n=32
80% 73%
More than enough 11% 11% 13%
60% 56%
As much as needed 32% 25% 50%
39% 39%
40% 32% Less than needed 56% 63% 38%
25%
20%
20% No answer 1% 1% 0%
11%
2% 1% 0% 2% Business customers are more likely than
0% Residential to believe that FortisBC can supply as
More than As much as Less than needed No answer much energy as needed in the long term.
enough needed
Indicates significant regional differences at a 95% confidence level
37�a]�X�  Perceptions on Planning Challenges
Customers in both regions identify reliability, managing cost, and local power
generation as top factors to consider when developing energy plans for the future.
Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs
“Critically Important Only” – Pre-Test Survey
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
79% 81%
80% 77%
67% 66% 68% 66%
63%
60% 50%5 5% 4 6% 59% 54% 55% 53% 44%5 0% 46%4 8% 4 4% 51%5 7% 4 6%
39%
40% 36%
29%
22%
18% 18% 19%
20%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
Reliability was selected as critically important by more customers than any other challenge. However, when
asked to rank the challenges, cost became the one most important consideration. See the next page.
38
Page 104 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M{j�  Nubh)��}�(hNh]�(G@s��    G@b�9`   G@�B�    G@q	
@   t�ah?]�(j  �Total n=115��Residential n=81��Business n=32�ehA]�(]�(j  �Total
n=115��Residential
n=81��Business
n=32�e]�(�More than enough��11%��11%��13%�e]�(�As much as needed��32%��25%��50%�e]�(�Less than needed��56%��63%��38%�e]�(�	No answer��1%��1%��0%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M{j�  j�  )��}�(j�  ��Reliability was selected as critically important by more customers than any other challenge.  However, when asked to rank the challenges, cost became the one most important consideration.   See the next page. �j�  M{j�  (G@]Tz�   G@B��    G@|��    G@J�o�   t�ububh)��}�(hNh]�(G@]1�UUUUG@h�`   G@oB�   G@rG�"   t�ah?]�(j  j  �73%�j  ehA]�(]�(NNNNNNNNj  Ne]�(�39%�NNNNNNNj  Ne]�(NNNNNNNj  NNe]�(�32%
25%
20%�NNNNj  j  j  j  j  e]�(NNNNj  NNNNNe]�(�11%
2%�j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M{j�  j�  )��}�(j�  ��Reliability was selected as critically important by more customers than any other challenge.  However, when asked to rank the challenges, cost became the one most important consideration.   See the next page. �j�  M{j�  (G@]Tz�   G@B��    G@|��    G@J�o�   t�ububh)��}�(hNh]�(G@Z�    G@�M��   G@�m-    G@�Y�    t�ah?]�(j  j  �79% 77% 81%�j  j  j  j  j  j  j  ehA]�(]�(N�67% 66% 68%
55%
50%�NNNNNNNNNj  j  j  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNj  j  j  NNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(Nj  j  j  �46%�Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  �44%
36%�NNNNNNNj  �46% 44%
39%�NNNNNNNNNNNNj  j  Ne]�(Nj  j  j  j  j  j  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  �29%
22%�NNNNNNj  j  j  j  Nj  j  j  �18% 18% 19%�NNNNNNj  j  j  e]�(NNNNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNe]�(Nj  j  j  j  j  j  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  j  Nj  j  j  e]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment�NNNN�&Keeping Down
Cost/Managing
Future Cost�NNNN�Ensuring
Reliable Power�NNNN�Generating
Power Within
BC�NNNN�'Conserving/
Reducing
Energy
Consumption�NNNN�'Meeting the
Goals of the BC
Energy Plan�NNNN�,Meeting the
Goals of the BC
Clean Energy
Act�NNNN�Minimizing
Power Outages�NNNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNNN�$Helping
Customers
Manage
Consumption�NNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M{j�  j�  )��}�(j�  ��Business customers are more likely than Residential to believe that FortisBC can supply as much energy as needed in the long term. �j�  M{j�  (G@Zګ�   G@}��`   G@pc��   G@�t@   t�ububh)��}�(hNh]�(hhC   ��eE@���R�hhC   ��V@���R�G@�      G@��     t�ah?]�(�6Customers Chose a Specific Challenge as Most Important�NehA]�(]�(jg  Ne]�(�Total Mentions��Total�e]�(�OMinimizing_the environmental impacts of electrical infrastructure and equipment��n-15�e]�(��We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the�Ne]�(�0environment is spoiled nothing else will mattert��67%�e]�(�KImportant for future generations/the legacy we leave for future generations��13%�e]�(�QCost-ettectivel cheaperin the long run/keep costs Iow/ saves money (non-specitic)��7%�e]�(�JLess energy usea/woula mean less energy consumption/to stop peing wastetui��ro�e]�(�Miscellaneous single mentions��7%�e]�(�30 It continuesisustainapiiity��ro�e]�(�1amunaiixeu iicumtieneu iicuitien ampuuicuiicemiui��1 /0�e]�(�Doi��1 /0�e]�(�Rneeping 4uwine cosh mallaging lne ruluie cusl ui eiecliicily Cnlaige4 O cuslummets��0-3s�e]�(�3��Sd�e]�(��Other specific cost mentions (ie. Cost of living is out of control, everyone has So much debt, if cost is unaffordable all other challenges are not�Ne]�(�Mgoing to matter,could Fortis instead cut back on some of their salary costs?)��18%�e]�(�DCnCTLS��9/0�e]�(�jImportant to sustain reasonable standard of living/high-priced power will result in lower living standards��6%�e]�(�gimporant IO sustain reasonapie Stanaara Or Iivingrnign-pricea power Wii resuit in ioweriiving Stanaaras��0�e]�(�:Support local economylrequired for growth of the region/to�Ne]�(��Support local economylrequired for growth of the region/to help support and maintain our province/helps our economylmoney should be��Cor�e]�(�spent in our province��6%�e]�(�47d�Ne]�(��Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable energy more�Ne]�(�YSupply is critical to meet demands/today's activities need power/growing demand for power��19%�e]�(�WsuppIy I> citicai lo meel uemanasiouay > aclivilies neeu poweingiowing uemana ioi powei��1J/0�e]�(�XSafetymentions/to ensure the safetyand security of our communities/for commercial safety��13%�e]�(�YSafety mentions/to ensure the safetyand security of our communitieslfor commercial safety��13%�e]�(�SConsistent supply keeps a community running smoothly/otherwise the whole city stops��6%�e]�(�jImportant to sustain reasonable standard of living/high-priced power will result in lower living standards��6%�e]�(�So it��6%�e]�(�So people stay happy��6%�e]�(�TSo we can avoid importing energylto continue to be self-sufficient/rely on ourselves��6%�e]�(��We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the��6%�e]�(�FBetter ability for Fortis to control their costs/keep their costs down��6%�e]�(�Miscellaneous single mentions��13%�e]�(�Don't know/not stated��6%�e]�(NNe]�(N�T aye�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Perceptions on Planning Challenges Managing cost is identified as the one most critically important planning challenge to address, followed by generating power within BC. The ONE “Critically Important” Challenge to Address (Ranking of Challenges) Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% 80% 60% 40% 30% 27%3 2% 27% 22% 20% 13%1 6% 15% 3%1 7% 16% 10%1 6% 1 0% 1 5% 8% 9% 7% 1% 2% 0% 1% 0% 2% 1% 2% 0% 0% 0% 0% 0% Minimizing Minimizing Environmental Conserving/ Meeting the Visual Impacts Helping Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption 39�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Perceptions on Planning Challenges
Managing cost is identified as the one most critically important planning
challenge to address, followed by generating power within BC.
The ONE “Critically Important” Challenge to Address
(Ranking of Challenges)
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
60%
40% 30% 27%3 2% 27%
22%
20% 13%1 6% 15% 3%1 7% 16% 10%1 6%
1 0% 1 5% 8% 9% 7%
1% 2% 0% 1% 0% 2% 1% 2% 0% 0% 0% 0%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
39�a]�X  Perceptions on Planning Challenges
Why Customers Chose a Specific Challenge as Most Important
Total Mentions Total
Minimizing the environmental impacts of electrical infrastructure and equipment n=15
We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the
environment is spoiled nothing else will matter/they care about the environment 67%
Important for future generations/the legacy we leave for future generations 13%
Cost-effective/cheaper in the long run/keep costs low/saves money (non-specific) 7%
Less energy used/would mean less energy consumption/to stop being wasteful 7%
Miscellaneous single mentions 7%
So it continues/sustainability 7%
I am on a fixed income/limited income/I am poor/concern for people on welfare and pensions/it is either electricity or food 7%
Don't know/not stated 7%
Keeping down the cost/managing the future cost of electricity charged to customers n=33
I am on a fixed income/limited income/I am poor/concern for people on welfare and pensions/it is either electricity or food 36%
Rates are too high/electricity is expensive/our electric bill is too high 21%
Other specific cost mentions (ie. Cost of living is out of control, everyone has so much debt, if cost is unaffordable all other challenges are not
going to matter, could Fortis instead cut back on some of their salary costs?) 18%
Benefits all/we all gain/critical to everyone 9%
Important to sustain reasonable standard of living/high-priced power will result in lower living standards 6%
Support local economy/required for growth of the region/to help support and maintain our province/helps our economy/money should be
spent in our province 6%
Ensuring a reliable source of power n=16
Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable energy more
important than low cost 38%
Supply is critical to meet demands/today's activities need power/growing demand for power 19%
Safety mentions/to ensure the safety and security of our communities/for commercial safety 13%
Consistent supply keeps a community running smoothly/otherwise the whole city stops 6%
Important to sustain reasonable standard of living/high-priced power will result in lower living standards 6%
So it continues/sustainability 6%
So people stay happy 6%
So we can avoid importing energy/to continue to be self-sufficient/rely on ourselves 6%
We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the
environment is spoiled nothing else will matter/they care about the environment 6%
Better ability for Fortis to control their costs/keep their costs down 6%
Miscellaneous single mentions 13%
Don't know/not stated 6%
40
Page 105 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  Nubh)��}�(hNh]�(G@Z�    G@o6��   G@p��    G@q��L   t�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(Nj  j  j  NNNj  e]�(�13%
10%�j  j  j  �15%
13%�j  j  j  e]�(NNNNNNj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �VKeeping down the cost/managing the future cost of electricity charged to customersn=33�j�  M|j�  (G@^5�    G@k&��   G@"�   G@k�    t�ububh)��}�(hNh]�(G@\*M�   G@p�7��N�G@a��    G@q��L   t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �VKeeping down the cost/managing the future cost of electricity charged to customersn=33�j�  M|j�  (G@^5�    G@k&��   G@"�   G@k�    t�ububh)��}�(hNh]�(G@hnz�   G@p�7��N�G@l5@   G@q��L   t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �VKeeping down the cost/managing the future cost of electricity charged to customersn=33�j�  M|j�  (G@^5�    G@k&��   G@"�   G@k�    t�ububh)��}�(hNh]�(G@qr�    G@p�7��N�G@r��P   G@q��L   t�ah?]�(j  j  ehA]�]�(j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �VKeeping down the cost/managing the future cost of electricity charged to customersn=33�j�  M|j�  (G@^5�    G@k&��   G@"�   G@k�    t�ububh)��}�(hNh]�(G@Z�    G@q��@   G@�m-    G@tA/    t�ah?]�(�=Minimizing Environmental Impacts of Infrastructure/ Equipment��&Keeping Down Cost/Managing Future Cost��Ensuring Reliable Power��Generating Power Within BC��'Conserving/ Reducing Energy Consumption��'Meeting the Goals of the BC Energy Plan��,Meeting the Goals of the BC Clean Energy Act��Minimizing Power Outages��6Minimizing Visual Impacts of Infrastructure/ Equipment��$Helping Customers Manage Consumption�ehA]�]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment��&Keeping Down
Cost/Managing
Future Cost��Ensuring
Reliable Power��Generating
Power Within
BC��'Conserving/
Reducing
Energy
Consumption��'Meeting the
Goals of the BC
Energy Plan��,Meeting the
Goals of the BC
Clean Energy
Act��Minimizing
Power Outages��6Minimizing
Visual Impacts
of
Infrastructure/
Equipment��$Helping
Customers
Manage
Consumption�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �VKeeping down the cost/managing the future cost of electricity charged to customersn=33�j�  M|j�  (G@^5�    G@k&��   G@"�   G@k�    t�ububh)��}�(h]�(j$	  j$	  eh]�(G@\*M�   G@}�cP   G@�#333G@����   t�ah?]�(�:Why Customers Chose a Specific Challenge as Most Important�j  ehA]�(]�(�Total Mentions��Total�e]�(�OMinimizing the environmental impacts of electrical infrastructure and equipment��n=15�e]�(��We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the
environment is spoiled nothing else will matter/they care about the environment��67%�e]�(�KImportant for future generations/the legacy we leave for future generations��13%�e]�(�PCost-effective/cheaper in the long run/keep costs low/saves money (non-specific)��7%�e]�(�JLess energy used/would mean less energy consumption/to stop being wasteful��7%�e]�(�Miscellaneous single mentions��7%�e]�(�So it continues/sustainability��7%�e]�(�{I am on a fixed income/limited income/I am poor/concern for people on welfare and pensions/it is either electricity or food��7%�e]�(�Don't know/not stated��7%�e]�(�RKeeping down the cost/managing the future cost of electricity charged to customers��n=33�e]�(�{I am on a fixed income/limited income/I am poor/concern for people on welfare and pensions/it is either electricity or food��36%�e]�(�IRates are too high/electricity is expensive/our electric bill is too high��21%�e]�(��Other specific cost mentions (ie. Cost of living is out of control, everyone has so much debt, if cost is unaffordable all other challenges are not
going to matter, could Fortis instead cut back on some of their salary costs?)��18%�e]�(�-Benefits all/we all gain/critical to everyone��9%�e]�(�jImportant to sustain reasonable standard of living/high-priced power will result in lower living standards��6%�e]�(��Support local economy/required for growth of the region/to help support and maintain our province/helps our economy/money should be
spent in our province��6%�e]�(�#Ensuring a reliable source of power��n=16�e]�(��Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable energy more
important than low cost��38%�e]�(�YSupply is critical to meet demands/today's activities need power/growing demand for power��19%�e]�(�ZSafety mentions/to ensure the safety and security of our communities/for commercial safety��13%�e]�(�SConsistent supply keeps a community running smoothly/otherwise the whole city stops��6%�e]�(�jImportant to sustain reasonable standard of living/high-priced power will result in lower living standards��6%�e]�(�So it continues/sustainability��6%�e]�(�So people stay happy��6%�e]�(�TSo we can avoid importing energy/to continue to be self-sufficient/rely on ourselves��6%�e]�(��We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the
environment is spoiled nothing else will matter/they care about the environment��6%�e]�(�FBetter ability for Fortis to control their costs/keep their costs down��6%�e]�(�Miscellaneous single mentions��13%�e]�(�Don't know/not stated��6%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M|j�  j�  )��}�(j�  �lPerceptions on Planning Challenges Why Customers Chose a Specific Challenge as Most Important Total Mentions�j�  M|j�  (G@T%3�   G@r�g�   G@x�    G@u�~�   t�ububh)��}�(h]�(j^  j$	  eh]�(hhC   �iE@���R�hhC    �>@���R�G@�      G@��     t�ah?]�(K �Total�ehA]�(]�(K j^h  e]�(K �n-25�e]�(K �20%�e]�(K �20%�e]�(K �16%�e]�(K �16%�e]�(K �12%�e]�(K �Ciadn�e]�(K Ne]�(K Ne]�(K �O/0�e]�(K �8%�e]�(K �8%�e]�(K �4%�e]�(K Ne]�(K �4%�e]�(K �4%�e]�(K �20%�e]�(K �20%�e]�(K Ne]�(K �Cez�e]�(K Ne]�(K Ne]�(K Ne]�(K Ne]�(K Ne]�(K Ne]�(K �*�e]�(K �*�e]�(K �*�e]�(K �*�e]�(K Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M}j�  Nubh)��}�(hNh]�(hhC   ��`C@���R�hhC   ��RW@���R�G@�      G@��     t�ah?]�(K NehA]�(]�(K Ne]�(K �Total�e]�(K �n=1�e]�(K �X�e]�(K �n=1�e]�(K �X�e]�(K �n=1�e]�(K �*�e]�(K �n=8�e]�(K �*�e]�(K �*�e]�(K �*�e]�(K �*�e]�(K �*�e]�(K Ne]�(K �*�e]�(K Ne]�(K Ne]�(K Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M}j�  j�  )��}�(j�  �A*Please note only themes with a base size of n=15 were provided. �j�  M}j�  (G@nh`   G@Mi�   G@v�0�   G@R&M    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�
  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Perceptions on Planning Challenges Why Customers Chose a Specific Challenge as Most Important Total Mentions Total Generating power within BC rather than importing it from outside B.C n=25 Local energy means local jobs/creates employment/keeps jobs in B.C. 20% So we can avoid importing energy/to continue to be self-sufficient/rely on ourselves 20% Lower consumer cost/keeping costs down to customers/lower cost to end-user 16% We have many/enough sources/we have more than enough power 16% Support local economy/required for growth of the region/to help support and maintain our province/helps our economy/money should be spent in our province 12% Cost-effective/cheaper in the long run/keep costs low/saves money (non-specific) 12% Better ability for Fortis to control their costs/keep their costs down 8% Important for future generations/the legacy we leave for future generations 8% Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable energy more important than low cost 8% So we can control our costs and supply in B.C./important to control our own energy 8% Benefits all/we all gain/critical to everyone 4% Supply is critical to meet demands/today's activities need power/growing demand for power 4% The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things are taken care of 4% Miscellaneous single mentions 20% Conserving/reducing our energy consumption n=11 Less energy used/would mean less energy consumption/to stop being wasteful * The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things are taken care of * Cost-effective/cheaper in the long run/keep costs low/saves money (non-specific) * Better ability for Fortis to control their costs/keep their costs down * Important for future generations/the legacy we leave for future generations * Education/information is powerful/if people are knowledgeable they are better able to make a difference * We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the Earth/if the environment is spoiled nothing else will matter/they care about the environment * Lower consumer cost/keeping costs down to customers/lower cost to end-user * Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable energy more important than low cost * Reduce building plants/reduced need to build infrastructure * *Please note only themes with a base size of n=15 were provided. 41�ahA]�(]�X�
  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Perceptions on Planning Challenges
Why Customers Chose a Specific Challenge as Most Important
Total Mentions Total
Generating power within BC rather than importing it from outside B.C n=25
Local energy means local jobs/creates employment/keeps jobs in B.C. 20%
So we can avoid importing energy/to continue to be self-sufficient/rely on ourselves 20%
Lower consumer cost/keeping costs down to customers/lower cost to end-user 16%
We have many/enough sources/we have more than enough power 16%
Support local economy/required for growth of the region/to help support and maintain our province/helps our economy/money
should be spent in our province 12%
Cost-effective/cheaper in the long run/keep costs low/saves money (non-specific) 12%
Better ability for Fortis to control their costs/keep their costs down 8%
Important for future generations/the legacy we leave for future generations 8%
Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable
energy more important than low cost 8%
So we can control our costs and supply in B.C./important to control our own energy 8%
Benefits all/we all gain/critical to everyone 4%
Supply is critical to meet demands/today's activities need power/growing demand for power 4%
The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things
are taken care of 4%
Miscellaneous single mentions 20%
Conserving/reducing our energy consumption n=11
Less energy used/would mean less energy consumption/to stop being wasteful *
The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things
are taken care of *
Cost-effective/cheaper in the long run/keep costs low/saves money (non-specific) *
Better ability for Fortis to control their costs/keep their costs down *
Important for future generations/the legacy we leave for future generations *
Education/information is powerful/if people are knowledgeable they are better able to make a difference *
We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the
Earth/if the environment is spoiled nothing else will matter/they care about the environment *
Lower consumer cost/keeping costs down to customers/lower cost to end-user *
Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable
energy more important than low cost *
Reduce building plants/reduced need to build infrastructure *
*Please note only themes with a base size of n=15 were provided.
41�a]�X�  Perceptions on Planning Challenges
Why Customers Chose a Specific Challenge as Most Important
Total Mentions Total
Meeting the goals of the BC Energy Plan n=1
Miscellaneous single mentions *
Meeting the goals of the BC Clean Energy Act n=1
Important for future generations/the legacy we leave for future generations *
Minimizing power outages n=1
Consistent supply keeps a community running smoothly/otherwise the whole city stops *
Helping customers understand how to manage their energy consumption n=8
Education/information is powerful/if people are knowledgeable they are better able to make a difference *
Few people understand the way it works and need constant reminding/people have a hard time finding ways to
reduce consumption and won't because they don't think it makes a difference *
Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable
power/reliable energy more important than low cost *
Less energy used/would mean less energy consumption/to stop being wasteful *
So it continues/sustainability *
Supply is critical to meet demands/today's activities need power/growing demand for power *
The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this
then the other things are taken care of *
Lower consumer cost/keeping costs down to customers/lower cost to end-user *
*Please note only themes with a base size of
n=15 were provided.
42
Page 106 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M}j�  Nubh)��}�(hNh]�(G@^A�   G@S(�   G@~��UUUG@v�H   t�ah?]�(�:Why Customers Chose a Specific Challenge as Most Important�j  ehA]�(]�(�Total Mentions��Total�e]�(�DGenerating power within BC rather than importing it from outside B.C��n=25�e]�(�CLocal energy means local jobs/creates employment/keeps jobs in B.C.��20%�e]�(�TSo we can avoid importing energy/to continue to be self-sufficient/rely on ourselves��20%�e]�(�JLower consumer cost/keeping costs down to customers/lower cost to end-user��16%�e]�(�:We have many/enough sources/we have more than enough power��16%�e]�(��Support local economy/required for growth of the region/to help support and maintain our province/helps our economy/money
should be spent in our province��12%�e]�(�PCost-effective/cheaper in the long run/keep costs low/saves money (non-specific)��12%�e]�(�FBetter ability for Fortis to control their costs/keep their costs down��8%�e]�(�KImportant for future generations/the legacy we leave for future generations��8%�e]�(��Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable
energy more important than low cost��8%�e]�(�RSo we can control our costs and supply in B.C./important to control our own energy��8%�e]�(�-Benefits all/we all gain/critical to everyone��4%�e]�(�YSupply is critical to meet demands/today's activities need power/growing demand for power��4%�e]�(��The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things
are taken care of��4%�e]�(�Miscellaneous single mentions��20%�e]�(�*Conserving/reducing our energy consumption��n=11�e]�(�JLess energy used/would mean less energy consumption/to stop being wasteful��*�e]�(��The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this then the other things
are taken care of�ji  e]�(�PCost-effective/cheaper in the long run/keep costs low/saves money (non-specific)�ji  e]�(�FBetter ability for Fortis to control their costs/keep their costs down�ji  e]�(�KImportant for future generations/the legacy we leave for future generations�ji  e]�(�gEducation/information is powerful/if people are knowledgeable they are better able to make a difference�ji  e]�(��We have to save the environment/we only have one Earth and we have to make it last/we must minimize the impact on the
Earth/if the environment is spoiled nothing else will matter/they care about the environment�ji  e]�(�JLower consumer cost/keeping costs down to customers/lower cost to end-user�ji  e]�(��Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable power/reliable
energy more important than low cost�ji  e]�(�;Reduce building plants/reduced need to build infrastructure�ji  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M}j�  j�  )��}�(j�  �A*Please note only themes with a base size of n=15 were provided. �j�  M}j�  (G@nh`   G@Mi�   G@v�0�   G@R&M    t�ububh)��}�(hNh]�(G@[m��   G@���   G@ZL
���G@�.   t�ah?]�(�:Why Customers Chose a Specific Challenge as Most Important�j  ehA]�(]�(�Total Mentions��Total�e]�(�'Meeting the goals of the BC Energy Plan��n=1�e]�(�Miscellaneous single mentions�ji  e]�(�,Meeting the goals of the BC Clean Energy Act��n=1�e]�(�KImportant for future generations/the legacy we leave for future generations�ji  e]�(�Minimizing power outages��n=1�e]�(�SConsistent supply keeps a community running smoothly/otherwise the whole city stops�ji  e]�(�CHelping customers understand how to manage their energy consumption��n=8�e]�(�gEducation/information is powerful/if people are knowledgeable they are better able to make a difference�ji  e]�(��Few people understand the way it works and need constant reminding/people have a hard time finding ways to
reduce consumption and won't because they don't think it makes a difference�ji  e]�(��Important to have a reliable supply/ensure a reliable source of power/people and businesses depend on reliable
power/reliable energy more important than low cost�ji  e]�(�JLess energy used/would mean less energy consumption/to stop being wasteful�ji  e]�(�So it continues/sustainability�ji  e]�(�YSupply is critical to meet demands/today's activities need power/growing demand for power�ji  e]�(��The other goals depend on this being achieved/it addresses many other things on the list/if you deal with this
then the other things are taken care of�ji  e]�(�JLower consumer cost/keeping costs down to customers/lower cost to end-user�ji  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M}j�  j�  )��}�(j�  �A*Please note only themes with a base size of n=15 were provided. �j�  M}j�  (G@f� �   G@z}��   G@v���   G@z�O`   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�Xa  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Perceptions on Planning Challenges The importance of various planning challenges was asked of customers in both pre and post surveys. Reliability remained the most important, but conservation and helping customers manage consumption became notably more important. Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs “Critically Important Only” (Based on Total Respondents) Pre-Survey Post-Survey (n=115) (n=115) 100% 1 1 2 3 2 79% 80% 3 80% 67% 70% 70% 73% 72% 63% 60% 50% 43% 54% 39% 44% 41% 46% 49% 51% 40% 29% 25% 18% 20% 0% Minimizing Minimizing Environmental Conserving/ Meeting the Visual Impacts Helping Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption Finding of interest. 43�ahA]�(]�Xa  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Perceptions on Planning Challenges
The importance of various planning challenges was asked of customers in both pre
and post surveys. Reliability remained the most important, but conservation and
helping customers manage consumption became notably more important.
Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs
“Critically Important Only”
(Based on Total Respondents)
Pre-Survey Post-Survey
(n=115) (n=115)
100% 1 1 2 3
2
79% 80% 3
80% 67% 70% 70% 73% 72%
63%
60% 50% 43% 54% 39% 44% 41% 46% 49% 51%
40%
29%
25%
18%
20%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
Finding of interest.
43�a]�XH  Perceptions on Planning Challenges
Overall, reliable power, managing cost, and local power generation were
critically important to address, while meeting the goals of the BC Clean Energy
Act and minimizing visual impacts were important but not critical.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Pre Survey
(Based on Total Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
Finding of interest.
79%
80%
67%
63%
60% 50% 54% 50% 50% 51%
43% 43% 44% 46% 45% 44%
40%
40% 32% 32%
29% 29%
20% 18%
20%
3% 1% 1% 0% 0% 0% 4% 0% 2% 0% 3% 0% 4% 2% 3% 0% 4% 3% 1%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
Minimizing the visual impacts of Infrastructure and Equipment was the only planning *Please note results
may not round to 100%
consideration or challenge that was considered by a substantial number of customers as
as “don’t know” is not
not very important.
reported.
44
Page 107 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  Nubh)��}�(hNh]�(G@h�#�UUUG@ed9`   G@ltb`   G@g.@*���t�ah?]�(ja
  ja
  ehA]�(]�(ja
  ja
  e]�(j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  j�  )��}�(j�  �*Please note results Minimizing the visual impacts of Infrastructure and Equipment was the only planning may not round to 100% �j�  M~j�  (G@W�%    G@G8��   G@|"�    G@N���   t�ububh)��}�(hNh]�(G@rb��   G@f	O�   G@s6�   G@g�@   t�ah?]�j  ahA]�(]�j  a]�j�  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  j�  )��}�(j�  �*Please note results Minimizing the visual impacts of Infrastructure and Equipment was the only planning may not round to 100% �j�  M~j�  (G@W�%    G@G8��   G@|"�    G@N���   t�ububh)��}�(hNh]�(G@hȸ   G@f	O�   G@��    G@g�@   t�ah?]�j  ahA]�(]�j  a]�j�  aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  j�  )��}�(j�  �*Please note results Minimizing the visual impacts of Infrastructure and Equipment was the only planning may not round to 100% �j�  M~j�  (G@W�%    G@G8��   G@|"�    G@N���   t�ububh)��}�(hNh]�(G@Z�     G@iM��UUUG@�k�@   G@t��    t�ah?]�(j  j�  j  j  �79% 1��80% 1�j  j�  j  j  �73% 2�j  j  j  j  j  j  j  j  j  j  j  j  j  �72% 3�ehA]�(]�(�67% 70%�NNNNNNNj  j  NNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNj  NNNNNNj  NNNj  NNNNNNNNNNNNNNNNNNj  e]�(�50%
43%�NNNj  j  j  Nj  j  j  j  j  �54%�NNj  �44% 46% 49% 51%
41%�NNNNNNNNNNNNNNNNNj  e]�(j  j  j  j  j  j  j  Nj  j  j  j  j  j  Nj  j  �39%
29%�NNNNj  j  NNj  j  �25%
18%�NNNNNj  j  e]�(j  j  j  j  j  j  j  Nj  j  j  j  j  j  Nj  j  j  j  j  j  Nj  j  j  Nj  j  j  Nj  j  j  Nj  j  e]�(�?Minimizing
Environmental
Impacts of
Infrastructure/ C
Equipment�NNN�%Keeping Down
ost/Managing
Future Cost�NN�Ensuring
Reliable Power�NNN�Generating
Power Within
BC�NN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  j�  )��}�(j�  �*Please note results Minimizing the visual impacts of Infrastructure and Equipment was the only planning may not round to 100% �j�  M~j�  (G@W�%    G@G8��   G@|"�    G@N���   t�ububh)��}�(hNh]�(G@Z�     G@�Dހ   G@�k�@   G@�n�   t�ah?]�(�67%�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�67%�NNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(�50%
43%�NNNj  j  NNj  j  Nj  NNNNNNNNNNNNNNNNNNNNNNNNe]�(Nj  NNNNNNNNNNNNNj  NNNj  NNNNNNNj  NNNNNNj  Ne]�(j  j  j  j  j  �32%�NNj  �20%�Nj  �32%�NNj  j  �29%�Nj  j  Nj  j  NNj  j  �18%�NN�29%�NNj  j  e]�(NNNNNj  NNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  �3% 1%�j  j  �1% 0%�Nj  j  �0% 0%�j  j  �4%
0%�Nj  j  �2% 0%�j  j  �3% 0%�Nj  j  �4% 2%�Nj  j  �3% 0%�Nj  j  �4%�Nj  j  e]�(j  NNNj  NNj  NNNj  NNj  NNNj  NNj  NNNj  NNNj  NNNj  NNe]�(�?Minimizing
Environmental
Impacts of
Infrastructure/ C
Equipment�NNN�%Keeping Down
ost/Managing
Future Cost�NN�Ensuring
Reliable Power�NNN�Generating
Power Within
BC�NN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M~j�  j�  )��}�(j�  �kPerceptions on Planning Challenges Overall, reliable power, managing cost, and local power generation were �j�  M~j�  (G@TJ�@   G@s��`   G@~���   G@u�<`   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Perceptions on Planning Challenges While the importance of most attributes remained similar before and after the FortisBC ISP presentation, the importance of conserving energy and helping customers manage consumption grew substantially. Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs – Post Survey (Based on Total Respondents) Critically Important Important but Not Critical Not Very Important Not at all Important 100% 80% 80% 70% 70% 73% 72% 60% 49% 49% 48% 43% 39%4 3% 41% 41% 44% 40% 29% 23% 23% 25% 24% 20% 18% 19% 3% 1% 0% 0% 0% 0% 4% 0% 1% 0% 3% 2% 6% 3% 4% 0% 5% 2% 0% 0% Minimizing Minimizing Environmental Conserving/ Meeting the Visual Impacts Helping Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption *Please note results may not round to 100% Finding of interest. as “don’t know” is not reported. 45�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Perceptions on Planning Challenges
While the importance of most attributes remained similar before and after the
FortisBC ISP presentation, the importance of conserving energy and helping
customers manage consumption grew substantially.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Post Survey
(Based on Total Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
80%
80% 70% 70% 73% 72%
60%
49% 49% 48%
43% 39%4 3% 41% 41% 44%
40%
29%
23% 23% 25% 24%
20% 18% 19%
3% 1% 0% 0% 0% 0% 4% 0% 1% 0% 3% 2% 6% 3% 4% 0% 5% 2% 0%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
*Please note results
may not round to 100%
Finding of interest. as “don’t know” is not
reported.
45�a]�X�  Pre Survey Perceptions on Planning Challenges - Okanagan
The top three critically important challenges in the Okanagan region are reliable
power, managing costs and generating power with BC. Minimizing visual
impacts of infrastructure and equipment rated lowest in critical importance.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Pre Survey
(Kelowna/Okanagan Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
80% 77%
66%
60% 55% 59% 55% 57%
48% 50% 48% 48% 46%
43%
41% 39%
40% 32% 36% 36% 38%
32%
23%
18%
20%
4% 0% 2% 0% 0% 0% 5% 0% 2% 0% 4% 0% 5% 0% 4% 0% 4% 4% 0%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
*Please note results
may not round to 100%
as “don’t know” is not
reported.
46
Page 108 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  Nubh)��}�(hNh]�(G@Z�<    G@g�1*���G@`   G@qe畟"�t�ah?]�(�70%�j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�70%�NNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNj  NNNNNj  NNj  NNNNNNNNNNNNNNNj  Ne]�(�49%
43%�NNNj  j  Nj  j  Nj  j  Nj  �49% 48%
39%4 3% 41% 41% 44%�NNNNNNNNNNNNNNj  Ne]�(NNj  NNNNNNNNNNNNNNNNNNNj  NNNj  NNNNe]�(j  j  j  j  j  �29%�Nj  �18%�Nj  �23%�Nj  �23%�Nj  j  j  j  j  j  j  j  �25%�Nj  �19%�Nj  Ne]�(NNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNj  NNj  NNNNNNNNNNj  NNNNj  e]�(j  j  j  �3% 1%�j  j  �0% 0%�j  j  �0% 0%�j  j  �4%
0%�j  j  �1% 0%�j  j  �3% 2%�j  j  �6%
3%�j  j  �4%
0%�j  j  j  �5%�j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �H*Please note results may not round to 100% as “don’t know” is not �j�  Mj�  (G@W�%    G@CA-�   G@d,Ġ   G@N���   t�ububh)��}�(hNh]�(G@Z�<    G@q��@   G@�j��   G@tA/    t�ah?]�(�=Minimizing Environmental Impacts of Infrastructure/ Equipment��&Keeping Down Cost/Managing Future Cost��Ensuring Reliable Power��Generating Power Within BC��'Conserving/ Reducing Energy Consumption��'Meeting the Goals of the BC Energy Plan��,Meeting the Goals of the BC Clean Energy Act��Minimizing Power Outages��6Minimizing Visual Impacts of Infrastructure/ Equipment��$Helping Customers Manage Consumption�ehA]�]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment��       �&Keeping Down
Cost/Managing
Future Cost��Ensuring
Reliable Power��Generating
Power Within
BC��'Conserving/
Reducing
Energy
Consumption��'Meeting the
Goals of the BC
Energy Plan��,Meeting the
Goals of the BC
Clean Energy
Act��Minimizing
Power Outages��6Minimizing
Visual Impacts
of
Infrastructure/
Equipment��$Helping
Customers
Manage
Consumption�eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  ��Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs – Pre Survey (Kelowna/Okanagan Respondents) �j�  Mj�  (G@kR``   G@o�x�   G@y<��   G@q}o@   t�ububh)��}�(hNh]�(G@Z�<    G@�_`   G@�j��   G@�i�   t�ah?]�(j  j  �77%�j  j  j  j  j  j  j  ehA]�(]�(NNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  �41%�NNj  j  NNj  j  NNj  �36%�NNj  �48% 50% 48% 48% 46%
43%
36% 38%�NNNNNNNNNNNNNNNNNNj  Ne]�(NNNNNNNNNNNNNNNNNNNNNj  NNj  NNNj  j  NNNj  NNNNNe]�(j  j  j  j  Nj  �32%�NNj  �23%�NNj  j  NNj  j  j  Nj  j  Nj  j  j  Nj  j  �18%�NNj  �32%�NNj  j  e]�(NNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNe]�(NNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  �4%
0%�Nj  j  �2% 0%�Nj  j  �0% 0%�Nj  j  �5%
0%�Nj  j  �2% 0%�j  j  �4%
0%�Nj  j  �5%
0%�Nj  j  �4%
0%�Nj  j  j  �4%�Nj  j  e]�(j  NNNj  NNNj  NNNj  NNNj  NNNj  NNj  NNNj  NNNj  NNNNj  NNe]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment�NNN�&Keeping Down
Cost/Managing
Future Cost�NNN�Ensuring
Reliable Power�NNN�Generating
Power Within
BC�NNN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  Mj�  j�  )��}�(j�  �H*Please note results may not round to 100% as “don’t know” is not �j�  Mj�  (G@W��`   G@{��   G@d,��   G@|j    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Post Survey Perceptions on Planning Challenges - Okanagan The importance of conservation and helping customers manage consumption increased significantly in critical importance among Okanagan participants after the FortisBC presentation. Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs – Post Survey (Kelowna/Okanagan Respondents) Critically Important Important but Not Critical Not Very Important Not at all Important 100% 82% 80% 75% 75% 63% 59% 60% 55% 50% 50% 46% 45% 43% 39% 39% 38% 38% 40% 29% 21% 21% 23% 20% 16% 20% 9% 2% 0% 0% 0% 0% 0% 5% 0% 2% 0% 4% 0% 0% 5% 0% 5% 0% 0% 0% Minimizing Minimizing Environmental Conserving/ Meeting the Visual Impacts Helping Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption *Please note results may not round to 100% as “don’t know” is not reported. 47�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Post Survey Perceptions on Planning Challenges - Okanagan
The importance of conservation and helping customers manage consumption
increased significantly in critical importance among Okanagan participants after
the FortisBC presentation.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Post Survey
(Kelowna/Okanagan Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
82%
80% 75% 75%
63%
59%
60% 55%
50% 50%
46% 45% 43%
39% 39% 38% 38%
40%
29%
21% 21% 23%
20% 16% 20%
9%
2% 0% 0% 0% 0% 0% 5% 0% 2% 0% 4% 0% 0% 5% 0% 5% 0% 0%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
*Please note results
may not round to 100%
as “don’t know” is not
reported.
47�a]�X�  Pre Survey Perceptions on Planning Challenges – Kootenay
Among Kootenay residents, the top three critically important planning
challenges are reliability, managing costs and generating power within BC.
Minimizing visual impacts rated lowest in critical importance.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Pre Survey
(Castlegar/Kootenay Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
81%
80%
68% 66%
60% 53% 51% 51% 49%
46% 4 4% 44% 42% 44% 44% 46%
39%
40% 32%
29%
25%
22%
20% 17% 19%
2% 2% 0% 0% 0% 0% 3% 0% 2% 0% 2% 0% 2% 3% 2% 0% 5% 2% 2%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
*Please note results
may not round to 100%
as “don’t know” is not
reported.
48
Page 109 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Z�<    G@g~c����G@�j��   G@t$5@   t�ah?]�(j  j  �82%�j  �75%�j  j  j  j  j�  ehA]�(]�(�59%
55%�NNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNj  Ne]�(�39%�NNNNj  �39%�NNj  j  Nj  j  NNj  �50% 50%
46% 45% 43%
38% 38%�NNNNNNNNNNNNNNNNNNj  Ne]�(NNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNj  NNNNNe]�(NNNNNNNNNNNNNNNNNNNNj  NNNj  NNNj  NNNNNNNNNe]�(j  j  j  j  Nj  j  j  Nj  �16%�Nj  �29%�NNj  �20%�Nj  j  j  Nj  j  j  Nj  j  �21%�NNj  �21%�NNj  Ne]�(NNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNe]�(j  j  j  �2% 0%�Nj  j  �0% 0%�Nj  �0% 0%�Nj  j  �5%
0%�Nj  j  �2% 0%�j  j  �4%
0%�Nj  j  �9%
0%�Nj  j  �5%
0%�Nj  j  j  �5%�Nj  j  e]�(NNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNe]�(NNNNNNNNNNNNNNj  NNNNNNj  NNNNNNNj  NNNNNNNNe]�(j  NNNj  NNNj  NNj  NNNj  NNNj  NNj  NNNj  NNNj  NNNNj  NNe]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment�NNN�&Keeping Down
Cost/Managing
Future Cost�NNN�Ensuring
Reliable Power�NN�Generating
Power Within
BC�NNN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �H*Please note results may not round to 100% as “don’t know” is not �j�  M�j�  (G@W�%    G@CA-�   G@d,Ġ   G@N���   t�ububh)��}�(hNh]�(G@Z�<    G@�>UUUG@�j��   G@�j3    t�ah?]�(j  j  �81%�j  j  j  j  j  j  j  ehA]�(]�(�68%�NNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNj  NNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNe]�(�46%
44%�NNNNj  j  NNj  j  NNj  NNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNj  NNNj  NNNNNNNj  NNNNNNNNj  e]�(j  j  j  j  Nj  �32%�NNj  �17%�NNj  �29%�NNj  j  �22%�Nj  j  NNj  j  Nj  j  �19%�NNj  �25%�NNj  j  e]�(NNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNe]�(j  j  j  �2% 2%�Nj  j  �0% 0%�Nj  j  �0% 0%�Nj  j  �3%
0%�Nj  j  �2% 0%�j  j  �2% 0%�Nj  j  �2% 3%�Nj  j  �2% 0%�Nj  j  j  �5%�Nj  j  e]�(j  NNNj  NNNj  NNNj  NNNj  NNNj  NNj  NNNj  NNNj  NNNNj  NNe]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment�NNN�&Keeping Down
Cost/Managing
Future Cost�NNN�Ensuring
Reliable Power�NNN�Generating
Power Within
BC�NNN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �H*Please note results may not round to 100% as “don’t know” is not �j�  M�j�  (G@W�#`   G@{ %`   G@d,��   G@|��    t�ububh)��}�(hNh]�(hhC   �wI@���R�hhC   ��#]@���R�G@�      G@��     t�ah?]�(�(Other Planning Challenges Not Identified�NNNehA]�(]�(j�j  NNNe]�(�Total Mentions��Total��Kelowna��	Castlegar�e]�(�BSolar energy/solar power incentives to the grid metering for solar��9%��13%��6%�e]�(�5Health issues/hazards of power lines and transmitters��6%��13%��0%�e]�(�\Alternate energy sources/renewable energy/combining alternative sources of energy production��6%��6%��6%�e]�(�5Wind as an energy source wind powerlmetering for wind��6%��9%��3%�e]�(�FEducating the public on energy consumption/ways to reduce consumption_��5%��3%��6%�e]�(�9Introduction of Smart Meters/Smart Metering program costs��5%��0%��9%�e]�(�Interconnection to the��5%��6%��3%�e]�(�Other cost mentions (ie_�NNNe]�(�dall rate groups,inflation has to be considered when budgeting ahead forpower that must be purchased)��5%��3%��6%�e]�(�mBuying power from residents and helping people to accomplish this/implementing a system to allow customers to�NNNe]�(�sell back to the grid��3%��6%��0%�e]�(�;Provide rebates/incentives to help people with energy costs��3%��6%��O%�e]�(�TCCp��U To��O 4o��U 4o�e]�(�8Individual vs. corporate usage/industrial energy savings��3%��0%��6%�e]�(�TAdv44ai��O40��/0��/0�e]�(�?Move to more electrical products/future impact of electric cars��3%��6%��O%�e]�(�AOVC��O40��Id/0��/0�e]�(�L4��U To��O 4o��A ( O�e]�(�generate power)��3%��3%��3%�e]�(�5Getting salmon back in our riversIrebuild salmon runs��3%��O%��6%�e]�(�mHydroelectric dams concerns/potential future of hydroelectric dams/ensuring stability of dams while replacing�NNNe]�(�*aging gas trunk lines and hydro facilities��3%��6%��0%�e]�(�QMoral and ethical responsibility Vs. profit/not thinking about our people and how��3%��3%��3%�e]�(�Miscellaneous single mentions��6%��3%��9%�e]�(�Nothing��28%��22%��33%�e]�(�Don't know/not stated��6%��0%��12%�e]�(NNNNe]�(NNN�Page�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Post Survey Perceptions on Planning Challenges - Kootenay Similar to the Okanagan region, the importance of conserving energy and helping customers manage consumption increased in critical importance after the FortisBC presentation. Most Important Challenges to Consider When Planning for Future Energy and Infrastructure Needs – Post Survey (Castlegar/Kootenay Respondents) Critically Important Important but Not Critical Not Very Important Not at all Important 100% 80% 80% 78% 76% 71% 69% 60% 47% 4 2% 44% 47%4 6% 46% 41% 41% 40% 37% 27% 29% 25% 20% 20% 19% 19% 17% 5% 2% 0% 0% 0% 0% 3% 0% 0% 0% 2% 3% 3% 5% 3% 0% 5% 3% 0% 0% Minimizing Minimizing Environmental Conserving/ Meeting the Visual Impacts Helping Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption *Please note results may not round to 100% as “don’t know” is not reported. 49�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Post Survey Perceptions on Planning Challenges - Kootenay
Similar to the Okanagan region, the importance of conserving energy and
helping customers manage consumption increased in critical importance after
the FortisBC presentation.
Most Important Challenges to Consider When Planning for
Future Energy and Infrastructure Needs – Post Survey
(Castlegar/Kootenay Respondents)
Critically Important Important but Not Critical Not Very Important Not at all Important
100%
80% 80% 78% 76%
71% 69%
60%
47% 4 2% 44% 47%4 6% 46%
41% 41%
40% 37%
27% 29%
25%
20%
20% 19% 19% 17%
5% 2% 0% 0% 0% 0% 3% 0% 0% 0% 2% 3% 3% 5% 3% 0% 5% 3% 0%
0%
Minimizing Minimizing
Environmental Conserving/ Meeting the Visual Impacts Helping
Impacts of Keeping Down Generating Reducing Meeting the Goals of the BC of Customers
Infrastructure/ Cost/Managing Ensuring Power Within Energy Goals of the BC Clean Energy Minimizing Infrastructure/ Manage
Equipment Future Cost Reliable Power BC Consumption Energy Plan Act Power Outages Equipment Consumption
*Please note results
may not round to 100%
as “don’t know” is not
reported.
49�a]�X�  Perceptions on Planning Challenges
Customers identified that alternate energy sources are an area to be considered
in future energy planning. The need to consider health hazards is also
identified.
Other Planning Challenges Not Identified
Kelowna Castlegar
Total (Okanagan) (Kootenay)
Total Mentions n=65 n=32 n=33
Solar energy/solar power incentives to the grid/metering for solar 9% 13% 6%
Health issues/hazards of power lines and transmitters 6% 13% 0%
Alternate energy sources/renewable energy/combining alternative sources of energy production 6% 6% 6%
Wind as an energy source/wind power/metering for wind 6% 9% 3%
Educating the public on energy consumption/ways to reduce consumption 5% 3% 6%
Introduction of Smart Meters/Smart Metering program costs 5% 0% 9%
Interconnection to the grid to ensure secure sources/minimal down time/uninterrupted service 5% 6% 3%
Other cost mentions (ie. Who is the competition to help keep costs down?, sharing the cost of service correctly to
all rate groups, inflation has to be considered when budgeting ahead for power that must be purchased) 5% 3% 6%
Buying power from residents and helping people to accomplish this/implementing a system to allow customers to
sell back to the grid 3% 6% 0%
Provide rebates/incentives to help people with energy costs 3% 6% 0%
Keep B.C.'s power in B.C./impact on B.C. and Canada of power being supplied to the U.S. 3% 6% 0%
Individual vs. corporate usage/industrial energy savings 3% 0% 6%
Move to more electrical products/future impact of electric cars 3% 6% 0%
Other infrastructure mentions/overhead transmission vs. underground/visual impact of infrastructure in tourism
area 3% 6% 0%
Other specific energy source mentions (ie. Role of nuclear energy, vast amounts of natural gas available to
generate power) 3% 3% 3%
Getting salmon back in our rivers/rebuild salmon runs 3% 0% 6%
Hydroelectric dams concerns/potential future of hydroelectric dams/ensuring stability of dams while replacing
aging gas trunk lines and hydro facilities 3% 6% 0%
Moral and ethical responsibility vs. profit/not thinking about our people and how they will cope with the impact 3% 3% 3%
Miscellaneous single mentions 6% 3% 9%
Nothing 28% 22% 33%
Don't know/not stated 6% 0% 12%
50
Page 110 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Z�<    G@gĲ�fffG@�j��   G@t$5@   t�ah?]�(j  �80%��78%��76%��71%�j  j  j  j  j  ehA]�(]�(j  NNNNj  j  NNj  j  NNj  NNNNNNNNNNNNNNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNNNNNNNj  Ne]�(�47%
42%�NNNNj  j  NNj  j  NNj  j  NNj  �44% 47%4 6% 46%
41% 41%�NNNNNNNNNNNNNNNNNNj  Ne]�(j  j  j  j  Nj  �19%�NNj  �20%�NNj  �19%�NNj  �27%�NNNNNj  �37%�NNj  j  �29%�NNj  �17%�NNj  Ne]�(NNNNNNNNNNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNj  NNNNNNNNNNNNNj  NNNNNj  e]�(j  j  j  �5%
2%�Nj  j  �0% 0%�Nj  j  �0% 0%�Nj  j  �3%
0%�Nj  j  �0% 0%�j  j  �2% 3%�Nj  j  �3% 5%�Nj  j  �3%
0%�Nj  j  j  �5%�Nj  j  e]�(j  NNNj  NNNj  NNNj  NNNj  NNNj  NNj  NNNj  NNNj  NNNNj  NNe]�(�=Minimizing
Environmental
Impacts of
Infrastructure/
Equipment�NNN�&Keeping Down
Cost/Managing
Future Cost�NNN�Ensuring
Reliable Power�NNN�Generating
Power Within
BC�NNN�'Conserving/
Reducing
Energy
Consumption�NNN�'Meeting the
Goals of the BC
Energy Plan�NN�,Meeting the
Goals of the BC
Clean Energy
Act�NNN�Minimizing
Power Outages�NNN�6Minimizing
Visual Impacts
of
Infrastructure/
Equipment�NNNN�$Helping
Customers
Manage
Consumption�NNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@\�V    G@�3)�   G@~���333G@�}x�   t�ah?]�(�(Other Planning Challenges Not Identified�j  j  j  ehA]�(]�(�Total Mentions��
Total
n=65��Kelowna
(Okanagan)
n=32��Castlegar
(Kootenay)
n=33�e]�(�BSolar energy/solar power incentives to the grid/metering for solar��9%��13%��6%�e]�(�5Health issues/hazards of power lines and transmitters��6%��13%��0%�e]�(�\Alternate energy sources/renewable energy/combining alternative sources of energy production��6%��6%��6%�e]�(�5Wind as an energy source/wind power/metering for wind��6%��9%��3%�e]�(�EEducating the public on energy consumption/ways to reduce consumption��5%��3%��6%�e]�(�9Introduction of Smart Meters/Smart Metering program costs��5%��0%��9%�e]�(�\Interconnection to the grid to ensure secure sources/minimal down time/uninterrupted service��5%��6%��3%�e]�(��Other cost mentions (ie. Who is the competition to help keep costs down?, sharing the cost of service correctly to
all rate groups, inflation has to be considered when budgeting ahead for power that must be purchased)��5%��3%��6%�e]�(��Buying power from residents and helping people to accomplish this/implementing a system to allow customers to
sell back to the grid��3%��6%��0%�e]�(�;Provide rebates/incentives to help people with energy costs��3%��6%��0%�e]�(�WKeep B.C.'s power in B.C./impact on B.C. and Canada of power being supplied to the U.S.��3%��6%��0%�e]�(�8Individual vs. corporate usage/industrial energy savings��3%��0%��6%�e]�(�?Move to more electrical products/future impact of electric cars��3%��6%��0%�e]�(�sOther infrastructure mentions/overhead transmission vs. underground/visual impact of infrastructure in tourism
area��3%��6%��0%�e]�(�{Other specific energy source mentions (ie. Role of nuclear energy, vast amounts of natural gas available to
generate power)��3%��3%��3%�e]�(�5Getting salmon back in our rivers/rebuild salmon runs��3%��0%��6%�e]�(��Hydroelectric dams concerns/potential future of hydroelectric dams/ensuring stability of dams while replacing
aging gas trunk lines and hydro facilities��3%��6%��0%�e]�(�pMoral and ethical responsibility vs. profit/not thinking about our people and how they will cope with the impact��3%��3%��3%�e]�(�Miscellaneous single mentions��6%��3%��9%�e]�(�Nothing��28%��22%��33%�e]�(�Don't know/not stated��6%��0%��12%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �H*Please note results may not round to 100% as “don’t know” is not �j�  M�j�  (G@W�#`   G@{ %`   G@d,��   G@|��    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Resource Planning The Resource Plan is a long range planning document used by electrical utilities to identify future power supply requirements. 51�ahA]�(]�X  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Resource Planning
The Resource Plan is a long
range planning document
used by electrical utilities
to identify future power
supply requirements.
51�a]�X(  Planning Reserve Margin
The Planning Reserve Margin is viewed quite positively with only a few
individual customers against the idea. However, over one-third are not willing
to pay increased electrical rates to support the Planning Reserve Margin.
Agreement that FortisBC Should Consider the Planning Reserve Willing to Pay a Higher Price to Support the Planning Reserve
Margin Margin
Total Kelowna (Okanagan) Castlegar (Kootenay) Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59 n=115 n=56 n=59
100%
100%
80%
80%
68%
63%
60% 57% 60% 53%
47% 49% 44%
39%
40% 40% 36%
29% 30%
23%
20% 20% 15%
7% 11% 9%
1% 0% 2% 1% 2% 0% 2% 2% 2% 3% 2%
0% 0%
Should Should Should Should No answer Definitely Likely Likely not Definitely not
definitely be probably be probably notdefinitely not
considered considered be consideredbe considered
Okanagan residents are more positive about the Planning Reserve Margin and are more willing
to pay a higher price to introduce this concept.
Indicates significant regional differences at a 95% confidence level
52
Page 111 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@[J��   G@�ۣ�   G@f	P    G@�l�ה6t�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(NNj  NNNNNe]�(j  Nj  NNNNNe]�(Nj  NNNNNNe]�(NNNj  NNNj  e]�(j  j  j  j  j  j  �29�jsf  e]�(NNNNNNj  Ne]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �.Resource Planning The Resource Plan is a long �j�  M�j�  (G@`]�   G@�f	`   G@ny�`   G@���    t�ububh)��}�(hNh]�(G@t�p�   G@���    G@|�K�   G@�l�ה6t�ah?]�(j  �63%�j  j  j  j  j  ehA]�(]�(NNj  NNNNNe]�(�53%�Nj  NNNNNe]�(Nj  NNNNNNe]�(NNNj  NNNNe]�(j  j  j  j  NNNNe]�(NNNNNNNj  e]�(NNNNNj  NNe]�(NNNNNNj  Ne]�(�	11%
7%
3%�j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �.Resource Planning The Resource Plan is a long �j�  M�j�  (G@`]�   G@�f	`   G@ny�`   G@���    t�ububh)��}�(hNh]�(G@u�@   G@��UUUG@vo30   G@�l�ה6t�ah?]�(j  j  ehA]�]�(j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �.Resource Planning The Resource Plan is a long �j�  M�j�  (G@`]�   G@�f	`   G@ny�`   G@���    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Resource Plan—Contractual Agreements Three quarters of customers strongly or somewhat agree with using contractual agreements to fulfill small gaps in short term energy supply rather than building additional generation resources. Agreement with the Use of Contractual Agreements Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% 80% 59% 60% 51% 44% 40% 25% 24% 24% 20% 20% 13% 5% 7% 9% 5% 5% 3% 0% 2% 2% 2% 0% Strongly agree Somewhat Neither agree Somewhat Strongly Refused agree or disagree disagree disagree Indicates significant regional differences at a 95% confidence level 53�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Resource Plan—Contractual Agreements
Three quarters of customers strongly or somewhat agree with using contractual
agreements to fulfill small gaps in short term energy supply rather than building
additional generation resources.
Agreement with the Use of Contractual Agreements
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
59%
60%
51%
44%
40%
25%
24% 24%
20%
20%
13%
5% 7% 9% 5% 5%
3% 0% 2% 2% 2%
0%
Strongly agree Somewhat Neither agree Somewhat Strongly Refused
agree or disagree disagree disagree
Indicates significant regional differences at a 95% confidence level
53�a]�X=  Capital Expenditures
The capital expenditures plan
outlines the capital expenditures
that FortisBC plans to make in
2012-2013, including sustaining
and growth capital requirements
for the generation, transmission
and distribution plant as well as
expenditures related to general
utility operations.
54
Page 112 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@hA��   G@kmv*���G@sz�`   G@s&��d,�t�ah?]�(�51%�j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(�51%�NNNNNj  NNNNNe]�(NNNNNj  NNNNNNe]�(NNNNNNNj  NNNNe]�(�25%
24% 24%�NNNNj  j  j  NNNNe]�(Nj  j  j  NNNNNNNNe]�(j  j  j  j  j  j  j  j  �13%
5%�NNj  e]�(NNNNNNNNNj  NNe]�(NNNNNNNNNNj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �UCapital Expenditures The capital expenditures plan outlines the capital expenditures �j�  M�j�  (G@_X��   G@c�    G@n׸    G@h�E    t�ububh)��}�(hNh]�(G@tCx�   G@r�Y����G@v�`   G@s&��d,�t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �UCapital Expenditures The capital expenditures plan outlines the capital expenditures �j�  M�j�  (G@_X��   G@c�    G@n׸    G@h�E    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Factors in the Capital Expenditures Decision Process When determining capital expenditures customers would like FortisBC to first consider the impact on customer rates. The visual appearance of equipment is not viewed as highly important or critical. Importance of Factors to Consider in the Capital Expenditures Decision Process (Total Respondents) Very Important Important but Not Critical Not Very Important Not at all Important 100% 80% 70% 60% 51% 52% 50% 54% 56% 55% 41% 41% 40% 37% 30% 27% 23% 20% 22% 20% 14% 17% 8% 4% 1% 1% 3% 0% 1% 2% 0% 1% 0% 0% Distance from Buildings and Environmental Values Visual Appearance of Effects on Timeliness of Construction Flexibility for Future Growth Impact on Customer Rates Amenties Electrical Equipment Community/Neighbourhood Versus Need for Project During Construction *Please note results may not round to 100% as “don’t know” and “no answer” are not reported. 55�ahA]�(]�X  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Factors in the Capital Expenditures Decision Process
When determining capital expenditures customers would like FortisBC to first
consider the impact on customer rates. The visual appearance of equipment is
not viewed as highly important or critical.
Importance of Factors to Consider in the Capital Expenditures
Decision Process
(Total Respondents)
Very Important Important but Not Critical Not Very Important Not at all Important
100%
80%
70%
60% 51% 52% 50% 54% 56% 55%
41% 41%
40% 37%
30%
27%
23% 20% 22%
20% 14% 17%
8%
4% 1% 1% 3% 0% 1% 2% 0% 1% 0%
0%
Distance from Buildings and Environmental Values Visual Appearance of Effects on Timeliness of Construction Flexibility for Future Growth Impact on Customer Rates
Amenties Electrical Equipment Community/Neighbourhood Versus Need for Project
During Construction
*Please note results may not
round to 100% as “don’t know”
and “no answer” are not
reported.
55�a]�X$  Social and Environmental Components
Customers support including social and environmental components when capital
projects are undertaken.
Agreement that FortisBC Should Consider Social and
Environmental Components when Capital Projects are
Undertaken
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
60% 56%
53%
50%
39%
40% 36%
32%
20%
9% 7% 10%
0% 0% 0% 3% 4% 2%
0%
Should definitely Should probably Should probably Should definitely No answer
be considered be considered not be considerednot be considered
56
Page 113 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Z�    G@j�    G@~�7@   G@rO3�=p�t�ah?]�(j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(NNNNNNNNNNNNNNNNNNNNNj  Ne]�(�51% 52% 50% 54%
41% 41%
37%�NNNNNNNNNNNNNNNNNNNNj  Ne]�(Nj  NNj  NNNj  NNNj  NNNj  Nj  NNNNe]�(j  j  j  j  j  j  NNj  �30%
23%�NNj  �20% 22%�NNj  �17%�j  j  j  j  Ne]�(NNNNNj  NNNNNNNNNNNNNNNNNe]�(NNNNNNNNNj  NNNNNNNNNNNNNe]�(NNNNNNNNNNNNNNNNNNNNNNj  e]�(j  j  j  �4%
1%�j  j  �	14%
8%
1%�Nj  j  �3%�j  j  j  �0%�j  j  �1%�j  j  �2% 0%�j  j  e]�(NNNNNNNj  NNNNNNNNNj  NNNNNe]�(NNNNNNj  NNNNNNNNNNNNNNNNe]�(NNNj  NNNNNNNNNNNNNNNNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@i.��   G@����   G@q�    G@�'?�KKKt�ah?]�(j  j  j  j  j  j  j  j  ehA]�(]�(NNNNNNNj  e]�(NNNNNj  NNe]�(NNNNNNj  Ne]�(j  Nj  �32%�Nj  j  j  e]�(Nj  NNNNNNe]�(NNNj  NNNNe]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �]*Please note results may not round to 100% as “don’t know” and “no answer” are not �j�  M�j�  (G@X�b�   G@{ %`   G@gU    G@|��    t�ububh)��}�(hNh]�(G@r�)�   G@��`   G@t�`   G@�'?�KKKt�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �{not viewed as highly important or critical. Importance of Factors to Consider in the Capital Expenditures Decision Process �j�  M�j�  (G@hM�    G@�\o�   G@y���   G@���`   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Social and Environmental Components Although 89% of customers state social and environmental components should definitely or probably be included in the project budget, only about half (53%) are willing pay a higher price for electricity to support it. A reasonable addition to the budget is 1%. Percentage that is Reasonable to add to Budget for Social and Willing to Pay a Higher Price to Include Social and Environmental Components Environmental Components Total Kelowna (Okanagan) Castlegar (Kootenay) Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 n=115 n=56 n=59 100% 100% 80% 80% 60% 55% 55% 60% 54% 52% 45% 39% 40% 40% 34% 32% 30% 22% 23% 20% 20% 14% 16% 20% 13% 12% 5% 8% 6%7 % 5% 6%5 %7 % 3% 4% 3% 8% 3% 4% 3% 2% 3% 0% 0% None 1% 2% 3% More than No answer Definitely Likely Likely not Definitely not No answer 3% Indicates significant regional differences at a 95% confidence level 57�ahA]�(]�X  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Social and Environmental Components
Although 89% of customers state social and environmental components should
definitely or probably be included in the project budget, only about half (53%) are
willing pay a higher price for electricity to support it. A reasonable addition to the
budget is 1%.
Percentage that is Reasonable to add to Budget for Social and Willing to Pay a Higher Price to Include Social and
Environmental Components Environmental Components
Total Kelowna (Okanagan) Castlegar (Kootenay)
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
n=115 n=56 n=59
100% 100%
80% 80%
60% 55% 55% 60%
54% 52%
45%
39%
40% 40% 34%
32% 30%
22% 23%
20%
20% 14% 16% 20% 13% 12%
5% 8% 6%7 % 5% 6%5 %7 % 3% 4% 3% 8% 3% 4% 3% 2% 3%
0% 0%
None 1% 2% 3% More than No answer Definitely Likely Likely not Definitely not No answer
3%
Indicates significant regional differences at a 95% confidence level
57�a]�X�  Condition-Based Infrastructure Management
Condition-based management is viewed as an approach that should be
considered. There are few people against the switch from time-based
management to condition-based management.
Agreement that FortisBC Should Consider Switching from Time-
Based Management to Condition-Based Management
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
61%
60% 55%
46% 48%
37%
40%
29%
20%
3% 5% 2% 2% 0% 3% 3% 0% 5%
0%
Should Should Should Should No answer
definitely be probably be probably notdefinitely not
considered considered be consideredbe considered
Indicates significant regional differences at a 95% confidence level
58
Page 114 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@\v��   G@n�`    G@h(V    G@s��9t�ah?]�(�55��54%�ehA]�(]�(NNNj  j  j  NNNe]�(�22%�NNj  j  j  NNNe]�(�14%
5%�j  j  j  j  j  �16%�Nj  e]�(NNNNNNNj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@u��UUUG@p�dS333G@}���   G@s��9t�ah?]�(j  j  j  �4%�ehA]�(]�(j  j  j  NNNNNNe]�(NNNNj  j  j  NNe]�(�	13%
8%
3%�j  j  j  j  j  j  �12%
4%�j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �jAgreement that FortisBC Should Consider Switching from TimeBased Management to Condition-Based Management �j�  M�j�  (G@i��`   G@oJ�@   G@y7�   G@p�c�   t�ububh)��}�(hNh]�(G@l^E�m��G@���@   G@rv�   G@�Ȱ]Ft�ah?]�(�55% 46% 48% 37%�j  j  j  j  j  j  j  ehA]�(]�(�55%
46% 48%
37%�NNNNNNj  e]�(NNNNNj  NNe]�(NNj  NNNj  Ne]�(j  Nj�  �9%�Nj  j  j  e]�(Nj  NNNNNNe]�(NNNj  NNNNe]�(j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Condition-Based Infrastructure Management Condition-based management is viewed as an approach that should be considered.  There are few people against the switch from time-based �j�  M�j�  (G@T#:�   G@r�k�   G@~�#�   G@u�~�   t�ububh)��}�(hNh]�(hhC   �pI@���R�hhC   �<�C@���R�G@�      G@��     t�ah?]�(�Total Mentions��Total��Kelowna��	Castlegar�ehA]�(]�(j!m  j"m  j#m  j$m  e]�(�NET: Should be Considered��n=87��n-48��n=39�e]�(�kLower cost/to keep costs down/controlled costs/could result in cost-savings/will cost less in the long term��37%��38%��36%�e]�(�%Work based on need/fix as needed/when��23%��25%��21%�e]�(� Common sense/it just makes sense��7%��6%��8%�e]�(�Will conserve��7%��8%��5%�e]�(�LAvoids wasting time/reduce unnecessary times/eliminates the problems of time��6%��6%��5%�e]�(�]extend uselmay get more use (longer life) out of equipment if keep using until no longer able��6%��6%��5%�e]�(�Miscellaneous single mentions��6%��4%��8%�e]�(�2Betterlincreased reliabilitylmore reliable measure��5%��6%��3%�e]�(�FGood/better control/management/more accurately addresses the situation��5%��4%��5%�e]�(�`Cost to consumers is all that matters/cost for such projects should not be passed on to consumer��3%��2%��5%�e]�(�0More efficient way of maintaining infrastructure��3%��0%��8%�e]�(�hMore predictable/would lessen the surprise factor/being proactive instead of reactive/knowing in advance�NNNe]�(�CeOdc��O4O��Ad/0��Id /0�e]�(�cThere would only be a cost-savings with condition-based maintenance if labour costs were reduced/no��3%��4%��3%�e]�(�_Will help educate customers about their energv use/be more conscious of theirenergv consumption��3%��2%��5%�e]�(�NET: Should Not be Considered��n=6��n=3��n=3�e]�(�YThe economv is not able to support the change at this time/poor timing in todav's economv�N�*��*�e]�(�2Better/increased reliability/more reliable measure�NNNe]�(�bwould not like to be ordered when to do what/should not be told when | can use drver or eat supper�N�*��*�e]�(�Miscellaneous single mentions�NNNe]�(�Don't know/not stated�NN�*�e]�(�+*Please note only mentions of 3% or greater��ase note��results for "N��JET: Should Not b�e]�(�Mae��sidered"��were not pron��ided as base size�e]�(�Sn��we��re smaller thz��an n-15_�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X
  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Condition-Based Infrastructure Management The switch to condition-based infrastructure management is supported by customers primarily because it is seen as a means to reducing costs. Reason for Why Condition-Based Infrastructure Management Should or Should Not be Considered Kelowna Castlegar Total Mentions Total (Okanagan) (Kootenay) NET: Should be Considered n=87 n=48 n=39 Lower cost/to keep costs down/controlled costs/could result in cost-savings/will cost less in the long term 37% 38% 36% Work based on need/fix as needed/when necessary/when the need arises action must be taken 23% 25% 21% Common sense/it just makes sense 7% 6% 8% Will conserve energy/less wasted energy/reduce peak usage/(reduced impact on environment) 7% 8% 5% Avoids wasting time/reduce unnecessary times/eliminates the problems of time 6% 6% 5% May extend use/may get more use (longer life) out of equipment if keep using until no longer able 6% 6% 5% Miscellaneous single mentions 6% 4% 8% Better/increased reliability/more reliable measure 5% 6% 3% Good/better control/management/more accurately addresses the situation 5% 4% 5% Cost to consumers is all that matters/cost for such projects should not be passed on to consumer 3% 2% 5% More efficient way of maintaining infrastructure 3% 0% 8% More predictable/would lessen the surprise factor/being proactive instead of reactive/knowing in advance about the condition of infrastructure 3% 6% 0% Not enough information/need more information to make a decision 3% 6% 0% There would only be a cost-savings with condition-based maintenance if labour costs were reduced/no definite relationship between condition-based maintenance and cost-savings/important to consider total cost for future maintenance at the outset 3% 4% 3% Will help educate customers about their energy use/be more conscious of their energy consumption 3% 2% 5% NET: Should Not be Considered n=6 n=3 n=3 The economy is not able to support the change at this time/poor timing in today's economy * * * Better/increased reliability/more reliable measure * * * I would not like to be ordered when to do what/should not be told when I can use dryer or eat supper * * * Miscellaneous single mentions * * * Don't know/not stated * * * *Please note only mentions of 3% or greater *Please note results for “NET: Should Not be are reported due to the wide variety of Considered” were not provided as base sizes comments provided. were smaller than n=15. 59�ahA]�(]�X
  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Condition-Based Infrastructure Management
The switch to condition-based infrastructure management is supported by
customers primarily because it is seen as a means to reducing costs.
Reason for Why Condition-Based Infrastructure Management
Should or Should Not be Considered
Kelowna Castlegar
Total Mentions Total (Okanagan) (Kootenay)
NET: Should be Considered n=87 n=48 n=39
Lower cost/to keep costs down/controlled costs/could result in cost-savings/will cost less in the long term 37% 38% 36%
Work based on need/fix as needed/when necessary/when the need arises action must be taken 23% 25% 21%
Common sense/it just makes sense 7% 6% 8%
Will conserve energy/less wasted energy/reduce peak usage/(reduced impact on environment) 7% 8% 5%
Avoids wasting time/reduce unnecessary times/eliminates the problems of time 6% 6% 5%
May extend use/may get more use (longer life) out of equipment if keep using until no longer able 6% 6% 5%
Miscellaneous single mentions 6% 4% 8%
Better/increased reliability/more reliable measure 5% 6% 3%
Good/better control/management/more accurately addresses the situation 5% 4% 5%
Cost to consumers is all that matters/cost for such projects should not be passed on to consumer 3% 2% 5%
More efficient way of maintaining infrastructure 3% 0% 8%
More predictable/would lessen the surprise factor/being proactive instead of reactive/knowing in advance
about the condition of infrastructure 3% 6% 0%
Not enough information/need more information to make a decision 3% 6% 0%
There would only be a cost-savings with condition-based maintenance if labour costs were reduced/no
definite relationship between condition-based maintenance and cost-savings/important to consider total cost
for future maintenance at the outset 3% 4% 3%
Will help educate customers about their energy use/be more conscious of their energy consumption 3% 2% 5%
NET: Should Not be Considered n=6 n=3 n=3
The economy is not able to support the change at this time/poor timing in today's economy * * *
Better/increased reliability/more reliable measure * * *
I would not like to be ordered when to do what/should not be told when I can use dryer or eat supper * * *
Miscellaneous single mentions * * *
Don't know/not stated * * *
*Please note only mentions of 3% or greater *Please note results for “NET: Should Not be
are reported due to the wide variety of Considered” were not provided as base sizes
comments provided. were smaller than n=15.
59�a]�X-  Demand Side
Management
Is the planning and
implementation of programs
designed to modify energy
consumption on the customer’s
side of the meter by encouraging
customers to improve energy
efficiency, reduce electricity use,
change the time of use, or use a
different energy source.
60
Page 115 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�(G@\��   G@\]    G@(�0   G@u)�eUUUt�ah?]�(�[Reason for Why Condition-Based Infrastructure Management Should or Should Not be Considered�j  j  j  ehA]�(]�(�Total Mentions��Total��Kelowna
(Okanagan)��Castlegar
(Kootenay)�e]�(�NET: Should be Considered��n=87��n=48��n=39�e]�(�kLower cost/to keep costs down/controlled costs/could result in cost-savings/will cost less in the long term��37%��38%��36%�e]�(�YWork based on need/fix as needed/when necessary/when the need arises action must be taken��23%��25%��21%�e]�(� Common sense/it just makes sense��7%��6%��8%�e]�(�YWill conserve energy/less wasted energy/reduce peak usage/(reduced impact on environment)��7%��8%��5%�e]�(�LAvoids wasting time/reduce unnecessary times/eliminates the problems of time��6%��6%��5%�e]�(�aMay extend use/may get more use (longer life) out of equipment if keep using until no longer able��6%��6%��5%�e]�(�Miscellaneous single mentions��6%��4%��8%�e]�(�2Better/increased reliability/more reliable measure��5%��6%��3%�e]�(�FGood/better control/management/more accurately addresses the situation��5%��4%��5%�e]�(�`Cost to consumers is all that matters/cost for such projects should not be passed on to consumer��3%��2%��5%�e]�(�0More efficient way of maintaining infrastructure��3%��0%��8%�e]�(��More predictable/would lessen the surprise factor/being proactive instead of reactive/knowing in advance
about the condition of infrastructure��3%��6%��0%�e]�(�?Not enough information/need more information to make a decision��3%��6%��0%�e]�(��There would only be a cost-savings with condition-based maintenance if labour costs were reduced/no
definite relationship between condition-based maintenance and cost-savings/important to consider total cost
for future maintenance at the outset��3%��4%��3%�e]�(�`Will help educate customers about their energy use/be more conscious of their energy consumption��3%��2%��5%�e]�(�NET: Should Not be Considered��n=6��n=3��n=3�e]�(�YThe economy is not able to support the change at this time/poor timing in today's economy�ji  ji  ji  e]�(�2Better/increased reliability/more reliable measure�ji  ji  ji  e]�(�dI would not like to be ordered when to do what/should not be told when I can use dryer or eat supper�ji  ji  ji  e]�(�Miscellaneous single mentions�ji  ji  ji  e]�(�Don't know/not stated�ji  ji  ji  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �aside of the meter by encouraging customers to improve energy efficiency, reduce electricity use, �j�  M�j�  (G@_:i    G@U/c    G@n���   G@\�e�   t�ububh)��}�(h]�(j�  j$	  j$	  j$	  eh]�(hhC   �g�H@���R�hhC   �?�=@���R�G@�      G@��     t�ah?]�(�Total Mentions��Total��Kelowna��	Castlegar�ehA]�(]�(j!n  j"n  j#n  j$n  e]�(�TEducation/information to consumers/educating the public on how to reduce consumption��13%��5%��20%�e]�(�.Rebates/incentive for customers (non-specific)��9%��8%��10%�e]�(�LSolar incentives/solar water heater rebates/more information on solar panels��8%��3%��13%�e]�(�JLighting/LED lights/incentives to change to more energv-efficient lighting��8%��10%��5%�e]�(�\Educating our children/energy conservation education programs in schools/information to kids��8%��10%��5%�e]�(�LUpgrading meters to better monitor usage/two-way metering/new metering/Smart�NNNe]�(�Meters/free new meters��6%��8%��5%�e]�(�SRebates to help out heating expenses/would like to know ways to cut down on heating��6%��8%��5%�e]�(K �4%��5%��3%�e]�(K �4%��3%��5%�e]�(K �4%��3%��5%�e]�(K �4%��5%�K e]�(�QNew building energy efficiencylmore on home renovations rebates on home upgrades_��4%��5%��3%�e]�(�eGet office buildings/large facilities to turn the lights out/get industrial and municipal lighting to�NNNe]�(�shut off when not needed��4%��5%��3%�e]�(�@Alternative energy generation/possible alternative power sources��4%��5%��3%�e]�(�5Would like one bill/one bill for Terason and FortisBC��3%��3%��3%�e]�(�[We do not have natural gas here and must use expensive propane/l use propane but would like�NNNe]�(�to be able to use natural gas��3%��3%��3%�e]�(�YOther devices to help reduce energy consumption/smart power bars that turn off/smart home�NNNe]�(�0units where You can operate by telephone control��3%��5%��0%�e]�(�LHelping families to replace oldhot water tanks/more efficienthot water tanks��3%��5%��0%�e]�(�Heat��3%��3%��3%�e]�(�#Education/information to businesses��3%��5%��0%�e]�(�Miscellaneous single mentions��20%��23%��18%�e]�(�Nothing��4%��3%��5%�e]�(�Don't know/not stated��9%��5%��13%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�(hhC   @$G@���R�hhC   `��[@���R�G@�      G@��     t�ah?]�(�:Desired Energy Efficiency and Conservation Programs (Total��	Mentions)�NNehA]�(]�(j�n  j�n  NNe]�(N�Total��Residential��Business�e]�(�TEducation/information to consumers/educating the public on how to reduce consumption��13%��17%��4%�e]�(�.Rebates/incentive for customers (non-specific)��9%��7%��13%�e]�(�JLighting/LED lights/incentives to change to more energy-efficient lighting��8%��2%��21%�e]�(�LSolar incentives/solar water heater rebates/more information on solar panels��8%��6%��13%�e]�(�\Educating our children/energy conservation education programs in schools/information to kids��8%��9%��4%�e]�(�\Rebates to help out heating expenses/would like to know ways to cut down on heating expenses��6%��4%��13%�e]�(�\Upgrading meters to better monitor usage/two-way metering/new metering/Smart Meters/free new��6%��7%��4%�e]�(�@Alternative energy generation/possible alternative power sources��4%��2%��4%�e]�(�jGet office buildings/large facilities to turn the lights out/get industrial and municipal lighting to shut��4%��2%��8%�e]�(�PNew building energy efficiency/more on home renovations/rebates on home upgrades��4%��6%�Ne]�(�_Programs that would assist customers to become more energv-efficientlenergv-efficiencv programs��4%��4%��4%�e]�(�4Rebates on appliance upgradeslenergv star appliances��4%��4%��4%�e]�(�DThe program thev have now is good/keep going/vou're doing a good job��4%�NNe]�(�Miscellaneous single mentions��20%�N�21%�e]�(�Nothing��4%��4%��4%�e]�(�Don't know/not stated��9%��7%��13%�e]�(�Imina�NN�6�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Energy Efficiency and Conservation Programs Customers would like FortisBC to provide general education or information about how to reduce energy consumption. Desired Energy Efficiency and Conservation Programs Kelowna Castlegar Total (Okanagan) (Kootenay) Total Mentions n=80 n=40 n=40 Education/information to consumers/educating the public on how to reduce consumption 13% 5% 20% Rebates/incentive for customers (non-specific) 9% 8% 10% Solar incentives/solar water heater rebates/more information on solar panels 8% 3% 13% Lighting/LED lights/incentives to change to more energy-efficient lighting 8% 10% 5% Educating our children/energy conservation education programs in schools/information to kids 8% 10% 5% Upgrading meters to better monitor usage/two-way metering/new metering/Smart Meters/free new meters 6% 8% 5% Rebates to help out heating expenses/would like to know ways to cut down on heating 6% 8% 5% Wind power/consider using wind for energy 4% 5% 3% The program they have now is good/keep going/you're doing a good job 4% 3% 5% Rebates on appliance upgrades/energy star appliances 4% 3% 5% Programs that would assist customers to become more energy-efficient/energy-efficiency 4% 5% 3% New building energy efficiency/more on home renovations/rebates on home upgrades 4% 5% 3% Get office buildings/large facilities to turn the lights out/get industrial and municipal lighting to shut off when not needed 4% 5% 3% Alternative energy generation/possible alternative power sources 4% 5% 3% Would like one bill/one bill for Terason and FortisBC 3% 3% 3% We do not have natural gas here and must use expensive propane/I use propane but would like to be able to use natural gas 3% 3% 3% Other devices to help reduce energy consumption/smart power bars that turn off/smart home units where you can operate by telephone control 3% 5% 0% Helping families to replace old hot water tanks/more efficient hot water tanks 3% 5% 0% Heat pumps/heat pump installations 3% 3% 3% Education/information to businesses 3% 5% 0% Miscellaneous single mentions 20% 23% 18% Nothing 4% 3% 5% Don't know/not stated 9% 5% 13% 61�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Energy Efficiency and Conservation Programs
Customers would like FortisBC to provide general education or information about
how to reduce energy consumption.
Desired Energy Efficiency and Conservation Programs
Kelowna Castlegar
Total (Okanagan) (Kootenay)
Total Mentions n=80 n=40 n=40
Education/information to consumers/educating the public on how to reduce consumption 13% 5% 20%
Rebates/incentive for customers (non-specific) 9% 8% 10%
Solar incentives/solar water heater rebates/more information on solar panels 8% 3% 13%
Lighting/LED lights/incentives to change to more energy-efficient lighting 8% 10% 5%
Educating our children/energy conservation education programs in schools/information to kids 8% 10% 5%
Upgrading meters to better monitor usage/two-way metering/new metering/Smart
Meters/free new meters 6% 8% 5%
Rebates to help out heating expenses/would like to know ways to cut down on heating 6% 8% 5%
Wind power/consider using wind for energy 4% 5% 3%
The program they have now is good/keep going/you're doing a good job 4% 3% 5%
Rebates on appliance upgrades/energy star appliances 4% 3% 5%
Programs that would assist customers to become more energy-efficient/energy-efficiency 4% 5% 3%
New building energy efficiency/more on home renovations/rebates on home upgrades 4% 5% 3%
Get office buildings/large facilities to turn the lights out/get industrial and municipal lighting to
shut off when not needed 4% 5% 3%
Alternative energy generation/possible alternative power sources 4% 5% 3%
Would like one bill/one bill for Terason and FortisBC 3% 3% 3%
We do not have natural gas here and must use expensive propane/I use propane but would like
to be able to use natural gas 3% 3% 3%
Other devices to help reduce energy consumption/smart power bars that turn off/smart home
units where you can operate by telephone control 3% 5% 0%
Helping families to replace old hot water tanks/more efficient hot water tanks 3% 5% 0%
Heat pumps/heat pump installations 3% 3% 3%
Education/information to businesses 3% 5% 0%
Miscellaneous single mentions 20% 23% 18%
Nothing 4% 3% 5%
Don't know/not stated 9% 5% 13%
61�a]�X�  Energy Efficiency and Conservation Programs
Residential customers would like information on how to reduce consumption while
businesses are significantly more likely to mention lighting rebates.
Desired Energy Efficiency and Conservation Programs (Total Mentions)
Total Residential Business
n=80 n=54 n=24
Education/information to consumers/educating the public on how to reduce consumption 13% 17% 4%
Rebates/incentive for customers (non-specific) 9% 7% 13%
Lighting/LED lights/incentives to change to more energy-efficient lighting 8% 2% 21%
Solar incentives/solar water heater rebates/more information on solar panels 8% 6% 13%
Educating our children/energy conservation education programs in schools/information to kids 8% 9% 4%
Rebates to help out heating expenses/would like to know ways to cut down on heating expenses 6% 4% 13%
Upgrading meters to better monitor usage/two-way metering/new metering/Smart Meters/free new
6% 7% 4%
meters
Alternative energy generation/possible alternative power sources 4% 2% 4%
Get office buildings/large facilities to turn the lights out/get industrial and municipal lighting to shut
4% 2% 8%
off when not needed
New building energy efficiency/more on home renovations/rebates on home upgrades 4% 6% 0%
Programs that would assist customers to become more energy-efficient/energy-efficiency programs 4% 4% 4%
Rebates on appliance upgrades/energy star appliances 4% 4% 4%
The program they have now is good/keep going/you're doing a good job 4% 6% 0%
Miscellaneous single mentions 20% 17% 21%
Nothing 4% 4% 4%
Don't know/not stated 9% 7% 13%
Indicates significant regional differences at a 95% confidence level
62
Page 116 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@`��P   G@[�1   G@}˱�   G@vuE�   t�ah?]�(�Total Mentions��
Total n=80��Kelowna (Okanagan) n=40��Castlegar (Kootenay) n=40�ehA]�(]�(�Total Mentions��
Total
n=80��Kelowna
(Okanagan)
n=40��Castlegar
(Kootenay)
n=40�e]�(�TEducation/information to consumers/educating the public on how to reduce consumption��13%��5%��20%�e]�(�.Rebates/incentive for customers (non-specific)��9%��8%��10%�e]�(�LSolar incentives/solar water heater rebates/more information on solar panels��8%��3%��13%�e]�(�JLighting/LED lights/incentives to change to more energy-efficient lighting��8%��10%��5%�e]�(�\Educating our children/energy conservation education programs in schools/information to kids��8%��10%��5%�e]�(�cUpgrading meters to better monitor usage/two-way metering/new metering/Smart
Meters/free new meters��6%��8%��5%�e]�(�SRebates to help out heating expenses/would like to know ways to cut down on heating��6%��8%��5%�e]�(�)Wind power/consider using wind for energy��4%��5%��3%�e]�(�DThe program they have now is good/keep going/you're doing a good job��4%��3%��5%�e]�(�4Rebates on appliance upgrades/energy star appliances��4%��3%��5%�e]�(�VPrograms that would assist customers to become more energy-efficient/energy-efficiency��4%��5%��3%�e]�(�PNew building energy efficiency/more on home renovations/rebates on home upgrades��4%��5%��3%�e]�(�~Get office buildings/large facilities to turn the lights out/get industrial and municipal lighting to
shut off when not needed��4%��5%��3%�e]�(�@Alternative energy generation/possible alternative power sources��4%��5%��3%�e]�(�5Would like one bill/one bill for Terason and FortisBC��3%��3%��3%�e]�(�yWe do not have natural gas here and must use expensive propane/I use propane but would like
to be able to use natural gas��3%��3%��3%�e]�(��Other devices to help reduce energy consumption/smart power bars that turn off/smart home
units where you can operate by telephone control��3%��5%��0%�e]�(�NHelping families to replace old hot water tanks/more efficient hot water tanks��3%��5%��0%�e]�(�"Heat pumps/heat pump installations��3%��3%��3%�e]�(�#Education/information to businesses��3%��5%��0%�e]�(�Miscellaneous single mentions��20%��23%��18%�e]�(�Nothing��4%��3%��5%�e]�(�Don't know/not stated��9%��5%��13%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@W��    G@��    G@�M��   G@��|�   t�ah?]�(j  �
Total n=80��Residential n=54��Business n=24�ehA]�(]�(j  �
Total
n=80��Residential
n=54��Business
n=24�e]�(�TEducation/information to consumers/educating the public on how to reduce consumption��13%��17%��4%�e]�(�.Rebates/incentive for customers (non-specific)��9%��7%��13%�e]�(�JLighting/LED lights/incentives to change to more energy-efficient lighting��8%��2%��21%�e]�(�LSolar incentives/solar water heater rebates/more information on solar panels��8%��6%��13%�e]�(�\Educating our children/energy conservation education programs in schools/information to kids��8%��9%��4%�e]�(�\Rebates to help out heating expenses/would like to know ways to cut down on heating expenses��6%��4%��13%�e]�(�cUpgrading meters to better monitor usage/two-way metering/new metering/Smart Meters/free new
meters��6%��7%��4%�e]�(�@Alternative energy generation/possible alternative power sources��4%��2%��4%�e]�(�~Get office buildings/large facilities to turn the lights out/get industrial and municipal lighting to shut
off when not needed��4%��2%��8%�e]�(�PNew building energy efficiency/more on home renovations/rebates on home upgrades��4%��6%��0%�e]�(�_Programs that would assist customers to become more energy-efficient/energy-efficiency programs��4%��4%��4%�e]�(�4Rebates on appliance upgrades/energy star appliances��4%��4%��4%�e]�(�DThe program they have now is good/keep going/you're doing a good job��4%��6%��0%�e]�(�Miscellaneous single mentions��20%��17%��21%�e]�(�Nothing��4%��4%��4%�e]�(�Don't know/not stated��9%��7%��13%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Energy Efficiency and Conservation Programs Residential customers would like information on how to reduce consumption while businesses are significantly more likely to mention lighting rebates. �j�  M�j�  (G@T&P�   G@r�k�   G@�=��   G@u�~�   t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Joint Conservation Programs Between FortisBC and Terasen Gas Support for joint conservation programs between FortisBC and Terasen Gas is good. The Okanagan region is more supportive. Support for FortisBC Considering Joint Energy Conservation Programs with Terasen Gas Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% 80% 66% 60% 47% 37% 40% 29% 27% 22% 20% 16% 17% 1 3% 2%0 %3 % 3% 0 %5 % 4%5 %3 % 0% Strongly Somewhat Neither Somewhat Strongly Refused agree agree agree or disagree disagree disagree Indicates significant regional differences at a 95% confidence level 63�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Joint Conservation Programs Between FortisBC and Terasen Gas
Support for joint conservation programs between FortisBC and Terasen Gas is
good. The Okanagan region is more supportive.
Support for FortisBC Considering Joint Energy Conservation
Programs with Terasen Gas
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
66%
60%
47%
37%
40%
29% 27%
22%
20% 16% 17% 1 3%
2%0 %3 % 3% 0 %5 % 4%5 %3 %
0%
Strongly Somewhat Neither Somewhat Strongly Refused
agree agree agree or disagree disagree
disagree
Indicates significant regional differences at a 95% confidence level
63�a]���Advanced Metering
Infrastructure (AMI)
The Advanced Metering
Infrastructure (AMI) project
involves replacing the current,
manually read meters with
advanced meters that can
transmit meter reading data
directly to FortisBC.
64
Page 117 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@lOl�   G@jD�@   G@s�r�   G@r��z�Ht�ah?]�(j  j  j  j  j  j  j  j  j  j  j  j  ehA]�(]�(NNj  NNNNNNNNNe]�(�47%�Nj  NNNNNNNNNe]�(Nj  NNNNNNNNNNe]�(j  j  j  NNNNNNNNNe]�(NNNNNNNjsf  NNNNe]�(NNNj  Nja
  NNNNNNe]�(j  j  j  j  j  j  j  j  �13�NNjsf  e]�(NNNNNNj  NNj  NNe]�(NNNNNNNNNNj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �'Advanced Metering Infrastructure (AMI) �j�  M�j�  (G@`w�    G@e��`   G@m�j�   G@h�E    t�ububh)��}�(hNh]�(hhC   �%I@���R�hhC   ��X@���R�G@�      G@��     t�ah?]�(�Reason for Preferred Option��e Provic��Jed�NehA]�(]�(j p  j!p  j"p  Ne]�(�Total Mentions��Total��Kelowna��	Castlegar�e]�(�OSmart Meters help to educatelinform customerslincreases awareness of energy use��37%��37%��38%�e]�(�Customers will be more��21%��32%��7%�e]�(�#Provide it free of charge/having to��18%��13%��24%�e]�(�WEveryone should participate/would be more effective if everyone had one/if not everyone��16%��26%��3%�e]�(�8Excellent idea/reallv like it/beneficial/l would want it�NNNe]�(�XHelps Fortis keep costs downloverall cost for Fortis would be cheaperlit benefits Fortis�N�5%��10%�e]�(�YOther cost mentions (ie. Some cost increase can be justified, if everyone is included the�NNNe]�(�(cost per customer is lower, less cost if�NNNe]�(�raciiily ui��0/0��11/o��0/0�e]�(�!ie Wioie Tamy couiu panicipatelne��0/0��11/0��0/0�e]�(�
Tvo alCCss��4/0��3/0��1/0�e]�(�cncapei iui cuslumniensimnuie��3/0��9/0��3/0�e]�(�4o��L40��LO��O40�e]�(�Ldcaila��Lio��Lo��Li u�e]�(�Tcc_��I7o��CEO��O40�e]�(�P_ Dni��Li (��Li��t�e]�(N�CO��Li��t�e]�(N�4�NNe]�(NNN�Pag�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Advanced Metering Infrastructure (AMI) Two-thirds of Okanagan residents would like in-house displays provided as part of the AMI project, while Kootenay residents are equally split between having in-house displays provided as part of the project or as an optional purchase. Support for In-House Displays as Part of the AMI Project Versus an Optional Purchase Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% 80% 68% 59% 60% 51% 47% 40% 40% 32% 20% 0% Basic In-Home Displays Should be In-House Meters Should be *Please note results Provided Optional may not round to 100% as “no answer” is not reported. 65�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Advanced Metering Infrastructure (AMI)
Two-thirds of Okanagan residents would like in-house displays provided as part
of the AMI project, while Kootenay residents are equally split between having
in-house displays provided as part of the project or as an optional purchase.
Support for In-House Displays as Part of the AMI Project Versus
an Optional Purchase
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
68%
59%
60%
51%
47%
40%
40%
32%
20%
0%
Basic In-Home Displays Should be In-House Meters Should be *Please note results
Provided Optional may not round to 100%
as “no answer” is not
reported.
65�a]����      X%  Advanced Metering Infrastructure (AMI)
Support for implementing in-house displays across the entire customer base
relates to customer interest in education, information, and awareness of energy
usage. 18% of people mention cost implications.
Reason for Preferred Option – Basic In-House Displays Should be Provided
Kelowna Castlegar
Total (Okanagan) (Kootenay)
Total Mentions n=67 n=38 n=29
Smart Meters help to educate/inform customers/increases awareness of energy use 37% 37% 38%
Customers will be more responsible/mindful of their energy consumption/good for energy
conservation 21% 32% 7%
Provide it free of charge/having to buy them would be a disincentive/a lot of people would
not want to pay out of pocket for it 18% 13% 24%
Everyone should participate/would be more effective if everyone had one/if not everyone
participates there would be no benefit 16% 26% 3%
Excellent idea/really like it/beneficial/I would want it 7% 5% 10%
Helps Fortis keep costs down/overall cost for Fortis would be cheaper/it benefits Fortis 7% 5% 10%
Other cost mentions (ie. Some cost increase can be justified, if everyone is included the
cost per customer is lower, less cost if they are provided without a 3rd party, if I have to
have it I would want to get the most for my rate increase) 6% 8% 3%
Facility of implementation/use 6% 11% 0%
The whole family could participate/the kids too/will also teach the kids 6% 11% 0%
No access to computer/not a big user of internet/some cannot operate a computer 4% 3% 7%
Cheaper for customers/more cost-effective for consumers/will save money on electric bill 3% 3% 3%
It is up to FortisBC to educate people about the value of the meters/people need to be 3% 5% 0%
Should be given the option/freedom of choice/people should have the option to refuse 3% 3% 3%
Accessing through website would be more cost-effective 1% 3% 0%
If people have it they will use it 1% 3% 0%
Internet is better/preferred/easily accessible 1% 3% 0%
Miscellaneous single mentions 15% 11% 21%
Indicates significant regional differences at a 95% confidence level
66
Page 118 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@lOl�   G@lI�   G@x���   G@s}�   t�ah?]�(j  j  �68%�j  j  j  j  j  ehA]�(]�(NNNj  NNNNNNNNe]�(j  j  j  j  NNNNNNNNe]�(NNNNNj  NNNNNNe]�(NNNNNNNNNNNj  e]�(j  j  j  j  j  j  j  j  �32%�NNj  e]�(NNNNNNNNNj  NNe]�(j  j  j  j  j  j  j  j  j  j  j  j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�((G@_镀   G@�<�`   G@~E��UUUG@�|�   t�(hhC   ��K@���R�hhC   ���@���R�G@�      G@��     t�eh?]�(�Total Mentions��
Total n=67��Kelowna (Okanagan) n=38��Castlegar (Kootenay) n=29�ehA]�(]�(�Total Mentions��
Total
n=67��Kelowna
(Okanagan)
n=38��Castlegar
(Kootenay)
n=29�e]�(�OSmart Meters help to educate/inform customers/increases awareness of energy use��37%��37%��38%�e]�(�cCustomers will be more responsible/mindful of their energy consumption/good for energy
conservation��21%��32%��7%�e]�(�Provide it free of charge/having to buy them would be a disincentive/a lot of people would
not want to pay out of pocket for it��18%��13%��24%�e]�(�~Everyone should participate/would be more effective if everyone had one/if not everyone
participates there would be no benefit��16%��26%��3%�e]�(�8Excellent idea/really like it/beneficial/I would want it��7%��5%��10%�e]�(�XHelps Fortis keep costs down/overall cost for Fortis would be cheaper/it benefits Fortis��7%��5%��10%�e]�(��Other cost mentions (ie. Some cost increase can be justified, if everyone is included the
cost per customer is lower, less cost if they are provided without a 3rd party, if I have to
have it I would want to get the most for my rate increase)��6%��8%��3%�e]�(�Facility of implementation/use��6%��11%��0%�e]�(�HThe whole family could participate/the kids too/will also teach the kids��6%��11%��0%�e]�(�ONo access to computer/not a big user of internet/some cannot operate a computer��4%��3%��7%�e]�(�XCheaper for customers/more cost-effective for consumers/will save money on electric bill��3%��3%��3%�e]�(�VIt is up to FortisBC to educate people about the value of the meters/people need to be��3%��5%��0%�e]�(�TShould be given the option/freedom of choice/people should have the option to refuse��3%��3%��3%�e]�(�6Accessing through website would be more cost-effective��1%��3%��0%�e]�(�"If people have it they will use it��1%��3%��0%�e]�(�.Internet is better/preferred/easily accessible��1%��3%��0%�e]�(�Miscellaneous single mentions��15%��11%��21%�e]�(�Total Mentions��Total��Kelowna��	Castlegar�e]�(�TShould be given the option/freedom of choicelpeople should have the option to refuse��27%��27%��27%�e]�(�#Provide it free of charge/having to��20%��27%��15%�e]�(�aNot a critical issue/doesn't matter to me/wouldn't change my energy consumption/l can regulate my��17%��0%��27%�e]�(�.Internet is betterlpreferred/easily accessible��15%��27%��8%�e]�(�jPeople will get tired of looking at it/would just be a novelty at the beginning/would look at it initially��10%��20%��4%�e]�(�XCheaper for customers/more cost-effective for consumers/will save money on electric bill��7%��0%��12%�e]�(�No access to computer/not a��7%��20%��0%�e]�(�#Those who want it should pav for it��%��13%��4%�e]�(�!Benefits don't outweigh the costs��5%��7%��4%�e]�(� Benetits dont outweigh the costs��5%��r%��4%�e]�(�Don't want it/against them��5%��0%��8%�e]�(�8Excellent idea/really like it/beneficial/| would want it��5%��0%��8%�e]�(�DExpensive venture ifno one usesit/itis money Fortis could have saved��5%��0%��8%�e]�(�CInternetis more comprehensive/better more usable information online��5%��7%��4%�e]�(�6Accessing through website would be more cost-effective��2%��7%��0%�e]�(�XHelps Fortis keep costs down/overall cost for Fortis would be cheaperlit benefits Fortis��2%��7%��0%�e]�(�Ifpeople_haveit��2%��0%��4%�e]�(�eIt is up to FortisBC to educate people about the value of the meters/people need to be educated about��2%��0%��4%�e]�(�OSmart Meters help to educatelinform customerslincreases awareness of energy use��2%��0%��4%�e]�(�Miscellaneous single mentions��2%��7%��0%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �E*Please note results may not round to 100% as “no answer” is not �j�  M�j�  (G@Z���   G@{�`   G@e���   G@}]c�   t�ububh)��}�(hNh]�(hhC   �tG@���R�hhC   ��"`@���R�G@�      G@��     t�ah?]�(�2Reasons Why a Secure Website Would or Would not be��Jsed�NNNehA]�(]�(jzq  j{q  NNNe]�(�Total Mentions��Total��Kelowna��	Castlegar�Ne]�(�"Net: Would use (definitely/likely)��n=89��n-48��n-41�Ne]�(�Ham interested in my usage/tracking my usage spotting my high usage times��30%��29%��32%�Ne]�(�\Would help with energy conservation/would use less/would help me make smart choices on power�NNNNe]�(�use/change the times of��26%��33%��17%�Ne]�(�Interested in Where��16%��19%��12%�Ne]�(�4To become more intormed/get more intormation/details��8%��10%��5%�Ne]�(�Awould only Iook at It periodicallylnot Ottennot ona regular basis��8%��8%��r�Ne]�(�(Just t0 seerout OT curiositylinteresting��ro��67��ro�Ne]�(�Vwe use the Internet extensivelylalways on the computer anywaylaiready pay pills online��6%��8%��Z%�Ne]�(�?Preterto use in-home displayinome display meter would be enough��6%��4��r�Ne]�(�Uwantt0 Know lne eneigy-araw Or cenain applanceslwnat ItiS cosing me Tor eacn applance��470��470��3/0�Ne]�(�Gooa/convenient access��47��47��57�Ne]�(K �4%��8%��0%�Ne]�(�LWonder how much this would cost/as long as there are no new charges for this��4%��2%��7%�Ne]�(�Jwvonaer now Mucn nis wouia cost/as iong as tnere are no new cnaiges tornis��470��Lyo��1 /0�Ne]�(�Miscellaneous single mentions��8%��6%��10%�Ne]�(�iviiscenaneous singie Mentions��6/0��0/0��10/0�Ne]�(K �nzZI��n=s��nz1o�Ne]�(�Dont nave access to a computer��19%��40%��13%�Ne]�(�
DOt Cnav C��19/0��Lto/0��10/0�Ne]�(�DNot computer literate/don't know how to operate a computer very well��19%��0%��25%�Ne]�(�won t attect my power usage��14%��U��19�Ne]�(�)Don t Iike my personal information online��10%��0%��13%��#Please�e]�(�5Dont use tne computer mucn/aontilke usingtne computer��JU��U��137��mentior�e]�(�\Would help with energy conservation/would use less/would help me make smart choices on power��10%��20%��6%��grea�e]�(�KWonderhow much this would cost/as long as there are no new charges for this��5%��20%��0%��the wic�e]�(�%Don't have a lot of time to go online��5%��0%��6%��of Co"�e]�(�@Prefer to use in-home display/home display meter would be enough��5%��20%��0%��pro�e]�(K �10%��0%��13%�Ne]�(NNNN�Pace�e]�(NNNN�4J~�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �3Net: Not use (likely not/definitely not)n=21n=5n=16�j�  M�j�  (G@b���   G@]XS�   G@~;|�   G@^Ѕ    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X�  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Advanced Metering Infrastructure (AMI) When AMI is introduced, the majority of customers will use the FortisBC website provided for tracking their energy usage. Likelihood that Customers Will Use a Secure Website to Track Energy Usage Total Kelowna (Okanagan) Castlegar (Kootenay) n=115 n=56 n=59 100% 80% 60% 54% 43% 40% 34% 36% 34% 37% 20% 20% 14% 7% 6% 5% 7% 1% 0% 2% 0% Definitely use Likely use Likely not use Definitely not use No answer Indicates significant regional differences at a 95% confidence level 69�ahA]�(]�X�  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Advanced Metering Infrastructure (AMI)
When AMI is introduced, the majority of customers will use the FortisBC website
provided for tracking their energy usage.
Likelihood that Customers Will Use a Secure Website to Track
Energy Usage
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
60% 54%
43%
40% 34% 36% 34% 37%
20%
20% 14%
7% 6% 5% 7%
1% 0% 2%
0%
Definitely use Likely use Likely not use Definitely not use No answer
Indicates significant regional differences at a 95% confidence level
69�a]�XU	  Advanced Metering Infrastructure (AMI)
The FortisBC website is a popular idea for customers so they can easily track
usage or spot times of high usage. Issues with a website only approach is that
some customers don’t have access to a computer or are not computer literate.
Reasons Why a Secure Website Would or Would not be Used
Kelowna Castlegar
Total Mentions Total (Okanagan)(Kootenay)
Net: Would use (definitely/likely) n=89 n=48 n=41
I am interested in my usage/tracking my usage/spotting my high usage times 30% 29% 32%
Would help with energy conservation/would use less/would help me make smart choices on power
use/change the times of day I would use it 26% 33% 17%
Interested in where I can save money/helps me to manage the bill/it will keep the cost lower 16% 19% 12%
To become more informed/get more information/details 8% 10% 5%
Would only look at it periodically/not often/not on a regular basis 8% 8% 7%
Just to see/out of curiosity/interesting 7% 6% 7%
We use the internet extensively/always on the computer anyway/already pay bills online 6% 8% 2%
Prefer to use in-home display/home display meter would be enough 6% 4% 7%
Want to know the energy-draw of certain appliances/what it is costing me for each appliance 4% 4% 5%
Good/convenient access 4% 4% 5%
Other specific convenience mentions (ie. Can view information at my leisure, easy way, most efficient
way, more flexible) 4% 8% 0%
Wonder how much this would cost/as long as there are no new charges for this 4% 2% 7%
Miscellaneous single mentions 8% 6% 10%
Net: Not use (likely not/definitely not) n=21 n=5 n=16
Don't have access to a computer 19% 40% 13%
Not computer literate/don't know how to operate a computer very well 19% 0% 25%
Won't affect my power usage 14% 0% 19%
Don't like my personal information online 10% 0% 13% *Please note only
Don't use the computer much/don't like using the computer 10% 0% 13% mentions of 4% or
Would help with energy conservation/would use less/would help me make smart choices on power greater are
use/change the times of day I would use it 10% 20% 6% reported due to
Wonder how much this would cost/as long as there are no new charges for this 5% 20% 0% the wide variety
Don't have a lot of time to go online 5% 0% 6% of comments
Prefer to use in-home display/home display meter would be enough 5% 20% 0% provided.
Miscellaneous single mentions 10% 0% 13%
70
Page 120 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@hR�    G@l�-    G@t��`   G@r�����t�ah?]�(j  �54%�j  j  j  j  j  j  j  ehA]�(]�(NNj  NNNNNNNNNe]�(Nj  NNNNNNNNNNe]�(j  j  j  NNNNNNNNNe]�(NNNj  Nj  j  j  NNNNe]�(j  j  j  j  j  j  j  j  �14%
7%�NNj  e]�(NNNNNNNNNj  NNe]�(NNNNNNNNNNj  Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �3Net: Not use (likely not/definitely not)n=21n=5n=16�j�  M�j�  (G@b���   G@]XS�   G@~;|�   G@^Ѕ    t�ububh)��}�(hNh]�(G@u��   G@rBG�   G@x"K����G@r�����t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �BppReasons Why a Secure Website Would or Would not be Used Kelowna �j�  M�j�  (G@e %    G@o�S    G@y�    G@r���   t�ububh)��}�(h]�(j$	  j$	  j$	  j$	  eh]�((G@_�p�   G@�. �   G@}�����G@���   t�(hhC   �=K@���R�hhC   �|�L@���R�G@�      G@��     t�eh?]�(�Total Mentions��Total��Kelowna (Okanagan)��Castlegar (Kootenay)�ehA]�(]�(�Total Mentions��Total��Kelowna
(Okanagan)��Castlegar
(Kootenay)�e]�(�"Net: Would use (definitely/likely)��n=89��n=48��n=41�e]�(�JI am interested in my usage/tracking my usage/spotting my high usage times��30%��29%��32%�e]�(��Would help with energy conservation/would use less/would help me make smart choices on power
use/change the times of day I would use it��26%��33%��17%�e]�(�\Interested in where I can save money/helps me to manage the bill/it will keep the cost lower��16%��19%��12%�e]�(�4To become more informed/get more information/details��8%��10%��5%�e]�(�CWould only look at it periodically/not often/not on a regular basis��8%��8%��7%�e]�(�(Just to see/out of curiosity/interesting��7%��6%��7%�e]�(�VWe use the internet extensively/always on the computer anyway/already pay bills online��6%��8%��2%�e]�(�@Prefer to use in-home display/home display meter would be enough��6%��4%��7%�e]�(�[Want to know the energy-draw of certain appliances/what it is costing me for each appliance��4%��4%��5%�e]�(�Good/convenient access��4%��4%��5%�e]�(�yOther specific convenience mentions (ie. Can view information at my leisure, easy way, most efficient
way, more flexible)��4%��8%��0%�e]�(�LWonder how much this would cost/as long as there are no new charges for this��4%��2%��7%�e]�(�Miscellaneous single mentions��8%��6%��10%�e]�(�(Net: Not use (likely not/definitely not)��n=21��n=5��n=16�e]�(�Don't have access to a computer��19%��40%��13%�e]�(�DNot computer literate/don't know how to operate a computer very well��19%��0%��25%�e]�(�Won't affect my power usage��14%��0%��19%�e]�(�)Don't like my personal information online��10%��0%��13%�e]�(�9Don't use the computer much/don't like using the computer��10%��0%��13%�e]�(��Would help with energy conservation/would use less/would help me make smart choices on power
use/change the times of day I would use it��10%��20%��6%�e]�(�LWonder how much this would cost/as long as there are no new charges for this��5%��20%��0%�e]�(�%Don't have a lot of time to go online��5%��0%��6%�e]�(�@Prefer to use in-home display/home display meter would be enough��5%��20%��0%�e]�(�Miscellaneous single mentions��10%��0%��13%�e]�(�Total Mentions��Total��Kelowna��	Castlegar�e]�(�Net: Positive��46%��53%��39%�e]�(�LSounds good/fine/positive/l support it/the right direction/excellent project��33%��41%��25%�e]�(�TWill help people understand their usage/tool to keep people informed of their energy��9%��8%��9%�e]�(�:It is smart/smart investmentlsmart people use Smart Meters��3%��6%��0%�e]�(�Will have a��3%��2%��5%�e]�(�More control over my power bill��2%��0%��5%�e]�(�!Would mean savings/savings in the��2%��2%��2%�e]�(�UGood idea to change the price depending on the peak period/there should be two prices��3%��6%��0%�e]�(�Net: Neutral��27%��31%��23%�e]�(�Never heard of this before��11%��12%��9�e]�(�IWould like more information/customers need to be enlightened and informed��9%��8%��9%�e]�(�-Good as long as it doesn't cost more/too much��3%��2%��5%�e]�(�(Puts the responsibility on the consumers��3%��6%��0%�e]�(�A��2%��2%��2%�e]�(�Net: Negative��15%��2%��30%�e]�(�BMakes billing more expensive/concerned about increased costs/rates��8%��2%��14%�e]�(�SNot needed/should be optional/our usage wouldn't change/l am smarter than the meter��4%��0%��9%�e]�(�Mwill charge uS more during peak periods/worry about variable time-based rates��5%��0%��11%�e]�(�&Totally against it/they should be axed��2%��0%��5%�e]�(�Miscellaneous single mentions��15%��18%��11%�e]�(�Nothing��10%��10%��9%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Advanced Metering Infrastructure (AMI) The FortisBC website is a popular idea for customers so they can easily track usage or spot times of high usage.  Issues with a website only approach is that �j�  M�j�  (G@T%�    G@r���   G@��@   G@u�~�   t�ububh)��}�(hNh]�(hhC   �	iP@���R�hhC    ��V@���R�G@�      G@��     t�ah?]�(K K ehA]�(]�(K K e]�(K K e]�(K K e]�(K K e]�(K �13%�e]�(K K e]�(K K e]�(K K e]�(K �13%�e]�(K �t�e]�(K �15_�e]�(�e level�Neeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �Z*Please note regional differences were not provided as base sizes were smaller than n=15. �j�  M�j�  (G@`7�    G@Mi��   G@pC�@   G@RZ�    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Communications 73�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Communications
73�a]�X�  Communications/Information on the ISP
Super Group participants in both locations identified that the ISP presentation
was neither confusing or difficult to understand. A few customers would like to
better understand the business model of FortisBC (i.e., where profits are used).
Customer Opinions on Whether the Informational Presentation What Customers Found Confusing or Difficult to Understand
on the ISP Had Confusing or Difficult Parts to Understand about the ISP Presentation
Total Kelowna (Okanagan) Castlegar (Kootenay)
Total
n=115 n=56 n=59
n=15
100% The idea that it is not for profit/that
82% 86% Fortis doesn't make money by selling
78%
80% power?/then where do all the profits go? 20%
Was not sure what this was about/not
60% familiar with these things so it was
confusing 13%
40% Understanding the purchasing of
power/complex nature of power
17%
20% 13% 9% purchasing and storing 13%
5% 5% 5%
Future of the Smart Meters/Smart
0% Metering vs. advanced metering 13%
Yes No Refused
Current and future rate increases 7%
Total Residential Business Sound system was bad/poor mic
n=115 n=81 n=32 setup/couldn't hear 7%
Miscellaneous single mentions 13%
Yes 13% 17% 3%
Refused/No answer 13%
No 82% 81% 81%
*Please note regional differences were not
No answer 5% 1% 16% provided as base sizes were smaller than n=15.
Indicates significant regional differences at a 95% confidence level
74
Page 122 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@u��P   G@��9�   G@(d`   G@�{X   t�ah?]�(�ACustomers Found Confusing or Difficult about the ISP Presentation��	to Unders�ehA]�(]�(j  �
Total
n=15�e]�(�uThe idea that it is not for profit/that
Fortis doesn't make money by selling
power?/then where do all the profits go?��20%�e]�(�SWas not sure what this was about/not
familiar with these things so it was
confusing��13%�e]�(�TUnderstanding the purchasing of
power/complex nature of power
purchasing and storing��13%�e]�(�?Future of the Smart Meters/Smart
Metering vs. advanced metering��13%�e]�(�!Current and future rate increases��7%�e]�(�1Sound system was bad/poor mic
setup/couldn't hear��7%�e]�(�Miscellaneous single mentions��13%�e]�(�Refused/No answer��13%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Communications/Information on the ISP Super Group participants in both locations identified that the ISP presentation was neither confusing or difficult to understand.  A few customers would like to �j�  M�j�  (G@T$3�   G@r�k�   G@��   G@u�~�   t�ububh)��}�(hNh]�(G@[�
�   G@��    G@kބj���G@�hU�t]t�ah?]�(j  j  j  �82%��86%��78%�ehA]�(]�(NNNNNNj  Ne]�(j  NNNNj  j  j  e]�(j  NNNNj  j  j  e]�(�17%
13%�NNNNj  j  j  e]�(�9%�NNj  j  j  j  j  e]�(Nj  NNNNNNe]�(NNj  NNNNNeeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Communications/Information on the ISP Super Group participants in both locations identified that the ISP presentation was neither confusing or difficult to understand.  A few customers would like to �j�  M�j�  (G@T$3�   G@r�k�   G@��   G@u�~�   t�ububh)��}�(hNh]�(G@n��   G@�A�UUUUG@q�"�   G@�hU�t]t�ah?]�(j  j  j  ehA]�]�(j  j  j  eah�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �Communications �j�  M�j�  (G@a���   G@�5��   G@l��   G@���    t�ububh)��}�(hNh]�(G@]-q    G@��6EUUUG@q"9`   G@��h`   t�ah?]�(j  �Total n=115��Residential n=81��Business n=32�ehA]�(]�(j  �Total
n=115��Residential
n=81��Business
n=32�e]�(�Yes��13%��17%��3%�e]�(�No��82%��81%��81%�e]�(�	No answer��5%��1%��16%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �Communications �j�  M�j�  (G@a���   G@�5��   G@l��   G@���    t�ububh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Overall Perception of the Integrated System Plan 77�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Overall Perception of the
Integrated System Plan
77�a]�X  Overall Perceptions of the ISP
Most customers are agree that the Integrated System Plan is fulfilling the
objective of setting out a five year plan towards meeting the energy needs of the
next 20 to 30 years. Kootenay residents are less positive overall about the ISP
compared to Okanagan residents.
Customer Opinion on Whether or Not the Integrated System Plan
Fulfills FortisBC’s Objective of Planning for the Next 20 to 30 Years
Total Kelowna (Okanagan) Castlegar (Kootenay)
n=115 n=56 n=59
100%
80%
60%
46% 43% 46%
41%
40%
32%
24%
19%
20% 15%
5% 4% 2 %7 % 1% 2% 4% 5% 3%
0%
0%
Strongly Somewhat Neither Somewhat Strongly No answer
agree agree agree or disagree disagree
disagree
Indicates significant regional differences at a 95% confidence level
78
Page 124 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@kݺ�   G@�4��   G@sQ��   G@����I$�t�ah?]�(j  j  j  ehA]�(]�(�32%�Nj  �19%�Nj  j  j  NNe]�(Nj  NNNNNNNNe]�(NNNNNNNNNj  e]�(j  j  j  j  j  j  j  j  �5%�j  eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  �1Overall Perception of the Integrated System Plan �j�  M�j�  (G@[��   G@��    G@n��@   G@��u    t�ububh)��}�(hNh]�(hhC   ���G@���R�hhC    ��N@���R�G@�      G@��     t�ah?]�(K �Total��Kelowna��	Castlegar�ehA]�(]�(K jt  jt  jt  e]�(K �37%��53%��15%�e]�(� Excellent presentation/well done��15%��25%��0%�e]�(K �3%��6%��0%�e]�(K �8%��14%��0%�e]�(K �18%��28%��4%�e]�(K �20/��20/�Ne]�(�part Of the team)��5%��3%��4%�e]�(K �13%��14%��12%�e]�(K �31%��36%��23%�e]�(K �8%��6%��12%�e]�(K �6%��6%�K e]�(K �6%��11%��0%�e]�(K �5%��6%��4%�e]�(�_sliungy eiicuuiase cunseivaliunipiomiule eneisy-Saving piouucls alu piaclicesiiieiei> aiulmiute��5%��6%��4%�e]�(�<Provide incentiveslrebates to help customers conserve energy��3%��6%��0%�e]�(�Net:Negative��24%��11%��42%�e]�(K �10%��6%��15%�e]�(�lOther cost mentions (ie. Altogether costs too much, only above a certain basic amount should be at a cost to��8%��3%��15%�e]�(�/Don't want to see any excess energy sold out of��5%��3%��8%�e]�(�vNegative presentation mentions (ie. Give Us actual dollar figures so it makes more sense,rates should have been noted_��3%��3%��4%�e]�(K �18%��22%��12%�e]�(�Nothing��6%��0%��15%�e]�(�LResidential customers were more likely than business customers to provide ne��gative��comment��S about�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X"
  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Overall Perceptions of the ISP Okanagan participants enjoyed the presentation while Kootenay participants have concerns about costs and rate increases. Additional Thoughts and Comments Kelowna Castlegar Total (Okanagan)(Kootenay) Total Mentions n=62 n=36 n=26 Net: Positive 37% 53% 15% Excellent presentation/well done 15% 25% 0% Fortis is Canadian-owned and operated/good to know that Fortis and Terasen are Canadian-owned 3% 6% 0% Fortis seems to know what they're doing/doing a good job/making great strides to ensure the advancement of 8% 14% 0% Informative presentation/learned a lot 18% 28% 4% Other specific positive presentation mentions (ie. Good display boards, honest and composed presenter, felt like I was part of the team) 3% 3% 4% Thank you/thanks for the opportunity 13% 14% 12% Net:Neutral 31% 36% 23% Would like a discussion on alternate energy methods/nuclear energy/promote solar energy/need new ways to produce 8% 6% 12% Hold more customer workshops/seminars on energy conservation 6% 6% 8% Keep educating kids/getting kids involved 6% 11% 0% Need more public awareness/send out information to people 5% 6% 4% Strongly encourage conservation/promote energy-saving products and practices/there is a lot more customers could do to conserve energy 5% 6% 4% Provide incentives/rebates to help customers conserve energy 3% 6% 0% Net:Negative 24% 11% 42% Too many rate increases/too excessive/where will all the increases leave us? 10% 6% 15% Other cost mentions (ie. Altogether costs too much, only above a certain basic amount should be at a cost to consumers, worry about cost increase in relation to Waneta expansion, people should expect energy costs to go up and not complain about it, approve of the methods planned to keep costs down) 8% 3% 15% Don't want to see any excess energy sold out of country/want to know why power was sold to U.S./control where the power goes 5% 3% 8% Negative presentation mentions (ie. Give us actual dollar figures so it makes more sense, rates should have been noted 3% 3% 4% Miscellaneous single mentions 18% 22% 12% Nothing 6% 0% 15% Residential customers were more likely than business customers to provide negative comments about the ISP (25% versus 6% respectively). While both groups were concerned with possible rate increases/costs, only Residential were concerned with selling energy outside BC or the content of the presentation. Indicates significant regional differences at a 95% confidence level 79�ahA]�(]�X"
  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Overall Perceptions of the ISP
Okanagan participants enjoyed the presentation while Kootenay participants
have concerns about costs and rate increases.
Additional Thoughts and Comments
Kelowna Castlegar
Total (Okanagan)(Kootenay)
Total Mentions n=62 n=36 n=26
Net: Positive 37% 53% 15%
Excellent presentation/well done 15% 25% 0%
Fortis is Canadian-owned and operated/good to know that Fortis and Terasen are Canadian-owned 3% 6% 0%
Fortis seems to know what they're doing/doing a good job/making great strides to ensure the advancement of 8% 14% 0%
Informative presentation/learned a lot 18% 28% 4%
Other specific positive presentation mentions (ie. Good display boards, honest and composed presenter, felt like I was
part of the team) 3% 3% 4%
Thank you/thanks for the opportunity 13% 14% 12%
Net:Neutral 31% 36% 23%
Would like a discussion on alternate energy methods/nuclear energy/promote solar energy/need new ways to produce 8% 6% 12%
Hold more customer workshops/seminars on energy conservation 6% 6% 8%
Keep educating kids/getting kids involved 6% 11% 0%
Need more public awareness/send out information to people 5% 6% 4%
Strongly encourage conservation/promote energy-saving products and practices/there is a lot more customers could do
to conserve energy 5% 6% 4%
Provide incentives/rebates to help customers conserve energy 3% 6% 0%
Net:Negative 24% 11% 42%
Too many rate increases/too excessive/where will all the increases leave us? 10% 6% 15%
Other cost mentions (ie. Altogether costs too much, only above a certain basic amount should be at a cost to
consumers, worry about cost increase in relation to Waneta expansion, people should expect energy costs to go up and
not complain about it, approve of the methods planned to keep costs down) 8% 3% 15%
Don't want to see any excess energy sold out of country/want to know why power was sold to U.S./control where the
power goes 5% 3% 8%
Negative presentation mentions (ie. Give us actual dollar figures so it makes more sense, rates should have been noted 3% 3% 4%
Miscellaneous single mentions 18% 22% 12%
Nothing 6% 0% 15%
Residential customers were more likely than business customers to provide negative comments about the ISP
(25% versus 6% respectively). While both groups were concerned with possible rate increases/costs, only
Residential were concerned with selling energy outside BC or the content of the presentation.
Indicates significant regional differences at a 95% confidence level
79�a]��6Appendix 1:
How to Read 2X2
Charts.
80
Page 125 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@^�z�   G@ZO�   G@~�UUUG@t"��   t�ah?]�(� Additional Thoughts and Comments�j  j  j  ehA]�(]�(�Total Mentions��
Total
n=62��Kelowna
(Okanagan)
n=36��Castlegar
(Kootenay)
n=26�e]�(�Net: Positive��37%��53%��15%�e]�(� Excellent presentation/well done��15%��25%��0%�e]�(�]Fortis is Canadian-owned and operated/good to know that Fortis and Terasen are Canadian-owned��3%��6%��0%�e]�(�jFortis seems to know what they're doing/doing a good job/making great strides to ensure the advancement of��8%��14%��0%�e]�(�&Informative presentation/learned a lot��18%��28%��4%�e]�(��Other specific positive presentation mentions (ie. Good display boards, honest and composed presenter, felt like I was
part of the team)��3%��3%��4%�e]�(�$Thank you/thanks for the opportunity��13%��14%��12%�e]�(�Net:Neutral��31%��36%��23%�e]�(�pWould like a discussion on alternate energy methods/nuclear energy/promote solar energy/need new ways to produce��8%��6%��12%�e]�(�<Hold more customer workshops/seminars on energy conservation��6%��6%��8%�e]�(�)Keep educating kids/getting kids involved��6%��11%��0%�e]�(�9Need more public awareness/send out information to people��5%��6%��4%�e]�(��Strongly encourage conservation/promote energy-saving products and practices/there is a lot more customers could do
to conserve energy��5%��6%��4%�e]�(�<Provide incentives/rebates to help customers conserve energy��3%��6%��0%�e]�(�Net:Negative��24%��11%��42%�e]�(�LToo many rate increases/too excessive/where will all the increases leave us?��10%��6%��15%�e]�(X+  Other cost mentions (ie. Altogether costs too much, only above a certain basic amount should be at a cost to
consumers, worry about cost increase in relation to Waneta expansion, people should expect energy costs to go up and
not complain about it, approve of the methods planned to keep costs down)��8%��3%��15%�e]�(�|Don't want to see any excess energy sold out of country/want to know why power was sold to U.S./control where the
power goes��5%��3%��8%�e]�(�vNegative presentation mentions (ie. Give us actual dollar figures so it makes more sense, rates should have been noted��3%��3%��4%�e]�(�Miscellaneous single mentions��18%��22%��12%�e]�(�Nothing��6%��0%��15%�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]�X*  2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report How to Read 2X2 Charts Questions X-Axis: Do you feel the price you currently pay for your household electricity service is: Too low, About right, Too high AND Y-Axis: Does the current size of your household electricity bill make a noticeable, small or no impact on your household finances each month? Noticeable impact, Small impact, No impact Questions X-Axis: Customers are telling FortisBC that they want to see additional social and environmental components added when capital projects are undertaken… Please indicate whether you think visual screening, special environmental treatments or other community specific amenities should: definitely be considered, probably be considered, probably not be considered or definitely not be considered. AND Y-Axis: Would you definitely, likely, likely not, or definitely not pay a higher price for the electricity you buy at home in order to include social and environmental components… 81�ahA]�(]�X*  2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
How to Read 2X2 Charts
Questions
X-Axis: Do you feel the price you currently pay for your
household electricity service is:
Too low, About right, Too high
AND
Y-Axis: Does the current size of your household
electricity bill make a noticeable, small or no impact on
your household finances each month?
Noticeable impact, Small impact, No impact
Questions
X-Axis: Customers are telling FortisBC that they want to
see additional social and environmental components
added when capital projects are undertaken… Please
indicate whether you think visual screening, special
environmental treatments or other community specific
amenities should:
definitely be considered, probably be considered,
probably not be considered or definitely not be
considered.
AND
Y-Axis: Would you definitely, likely, likely not, or
definitely not pay a higher price for the electricity you
buy at home in order to include social and
environmental components…
81�a]�X�  How to Read 2X2 Charts
Questions
X-Axis: Do you think energy conservation can play a
major role, a moderate role, a minor role, or no role at
all in ensuring the region has the supply of power it will
need.
Major role, Moderate role, Minor role, No role at all.
AND
Y-Axis: Do you think that individual consumers such as
yourself can definitely, likely, likely not or definitely not
make an important contribution to reducing the overall
amount of electrical energy used in the region?
Definitely could reduce overall energy use,
Likely could reduce overall energy use,
Likely not reduce overall energy use,
Definitely not reduce overall energy use.
82
Page 126 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(hhC   ���C@���R�hhC    �`@���R�G@�      G@��     t�ah?]�(K K �Transcript Summary�ehA]�(]�(K K ju  e]�(K K �
gy Sources�e]�(K K �&agement and Condition-Based Management�e]�(K K �ansion (Costs, Ownership)�e]�(K K �Iying from External Sources/Selling Power to External Sources (outside BC)�e]�(K K �
Generation�e]�(K K �ner�e]�(K K �/ucation/Education on Energy Efficient Products_�e]�(K K �egrams (General Information/Costs/Rates/Costs of In-House Meters/Introduction in Municipalities/lmpact�e]�(K K �0echnology/Billing Process/Content of Meter Data)�e]�(K K �ms/Rebates/Buy Back Programs�e]�(K K �I�e]�(K K �gge Pricing/ Time of Use Rates/ Benefit to FortisBC of Time of Use Rate System/Information on Peak Usage�e]�(K K �
ng Program�e]�(K K �Istomer Generated power suppiy�e]�(K K �2�e]�(K K �0History/ Rate Increases/ Cause Of Rate Increases�e]�(K K �Financial Model�e]�(K �ciod _��Lciot #i�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@Iǎ    G?��   G@���@   G@��=�   t�ah?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Appendix 2: Summary of Super Group Question and Answer & Information Booth De-Brief Transcripts 83�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Appendix 2:
Summary of Super
Group Question and
Answer &
Information Booth
De-Brief Transcripts
83�a]�X|  Summary of Question and Answer Transcripts
The Question and Answer session primarily touched on key topics of the ISP
presentation. A number of questions were due to the desire for greater
information on the topic, especially relating to costs and rate implications.
Transcript Summary – General Topics
Alternative Energy Sources
Time-Based Management and Condition-Based Management
The Waneta Expansion (Costs, Ownership)
Discussion on Buying from External Sources/Selling Power to External Sources (outside BC)
Building of New Generation
Trends in Consumer Conservation/Energy Conservation Incentives/Education about Conservation for Youth/FortisBC
Outreach and Education/Education on Energy Efficient Products)
Smart Meter Programs (General Information/Costs/Rates/Costs of In-House Meters/Introduction in Municipalities/Impact
on Jobs/Meter Technology/Billing Process/Content of Meter Data)
Incentive Programs/Rebates/Buy Back Programs
Use of Independent Power and Renewables
Peak Power Usage Pricing/Time of Use Rates/Benefit to FortisBC of Time of Use Rate System/Information on Peak Usage
Profiles
Herbicide Spraying Program
Net Metering/Customer Generated Power Supply
Usage of Gas Fire Generating Plants
Residential Rate History/Rate Increases/Cause of Rate Increases
FortisBC Profits/Financial Model
FortisBC Contribution or Involvement in the Government Regulation Process
84
Page 127 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(hNh]�(G@`ٙ�   G@�YƠ   G@~I?�   G@����   t�ah?]��Alternative Energy Sources�ahA]�(]��Alternative Energy Sources�a]��4Time-Based Management and Condition-Based Management�a]��'The Waneta Expansion (Costs, Ownership)�a]��YDiscussion on Buying from External Sources/Selling Power to External Sources (outside BC)�a]��Building of New Generation�a]���Trends in Consumer Conservation/Energy Conservation Incentives/Education about Conservation for Youth/FortisBC
Outreach and Education/Education on Energy Efficient Products)�a]���Smart Meter Programs (General Information/Costs/Rates/Costs of In-House Meters/Introduction in Municipalities/Impact
on Jobs/Meter Technology/Billing Process/Content of Meter Data)�a]��,Incentive Programs/Rebates/Buy Back Programs�a]��'Use of Independent Power and Renewables�a]��|Peak Power Usage Pricing/Time of Use Rates/Benefit to FortisBC of Time of Use Rate System/Information on Peak Usage
Profiles�a]��Herbicide Spraying Program�a]��,Net Metering/Customer Generated Power Supply�a]��#Usage of Gas Fire Generating Plants�a]��?Residential Rate History/Rate Increases/Cause of Rate Increases�a]�� FortisBC Profits/Financial Model�a]��IFortisBC Contribution or Involvement in the Government Regulation Process�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  j�  )��}�(j�  ��Summary of Question and Answer Transcripts The Question and Answer session primarily touched on key topics of the ISP presentation.  A number of questions were due to the desire for greater �j�  M�j�  (G@T'8`   G@r�k�   G@M"�   G@u�~�   t�ububh)��}�(hNh]�((hhC   �(E@���R�hhC   @��K@���R�G@�      G@��     t�(K hhC    �J@���R�G@�      G@��     t�(hhC   ��G@���R�hhC    z�H@���R�hhC   `�@���R�hhC   �4�@���R�t�eh?]�hA]�(]�(�&Brownfield and Greenfield construction��!field and Greenfield construction�e]�(�;Resource Plan (power purchase agreements/Canal Plant Agreen��:ce Plan (power purchase agreements/Canal Plant Agreements)�e]�(� EMF (Electric & Magnetic Fields)��lectric & Magnetic Fields)�e]�(�"Asset Value Definition/Description��Ialue Definition/Description�e]�(�.Substations/Transmission of Distribution Lines��(tions/Transmission of Distribution Lines�e]�(�+Delivery Charge for Gas/Cost of Electricity��$Y Charge for Gas/Cost of Electricity�e]�(�Growth of Customer Base��1 of Customer Base�e]�(�Penticton Substation��on Substation�e]�(�'FortisBC' s Susceptibility to Take-Over�� C' s Susceptibility to Take-Over�e]�(�"3808 negotiations and the PPA Rate��egotiations and the PPA Rate�e]�(�Load Forecasting��
orecasting�e]�(�"BC Hydro IPPs/Clean Call for Power��Iro IPPs/Clean Call for Power�e]�(�'Differences Between Energy and Capacity�� nces Between Energy and Capacity�e]�(�9Gas Turbines/Wind Turbines and PowerSense/Pump Storage H)��6rbines/Wind Turbines and PowerSense/Pump Storage Hydro�e]�(�#Power Supply Contractual Agreements��Supply Contractual Agreements�e]�(�Price Upon Renewal��Ipon Renewal�e]�(�Demand Side Management��Id Side Management�e]�(N� Advanced Metering/Smart Metering�e]�(�ents��IEnergy Options or Combinations�e]�(�ino��6IConservation/Energy Efficiency Education or Solutions�e]�(�L��)Peak Period Reduction/Peak Period Pricing�e]�(N�Live Smart Program�e]�(NK e]�(NK e]�(NK e]�(N�IPlanning Reserve Margin�e]�(N�"IBuilding Versus Buying for Supply�e]�(N�$Bill Impacts/Residential Basic Rates�e]�(N�!PowerSense Education/iInitiatives�e]�(N�&Condition Versus Time-Based Management�e]�(N�9Alternative Power Subsidies/Incentives in Other Countries�e]�(�dro��Rate of Return�e]�(N�Distributed Generation�e]�(N�IISP Benefits�e]�(K �ruiters please note: Ifthe res}��ondent mentions it will tal�e]�(K �elplease offer them S100.00!��lists�e]�(N�Customer Class��Customer Count�e]�(N�Residential��(497�e]�(N�General��(13)�e]�(N�
Industrial�Ne]�(N�	wnoiesaie��Mui�e]�(N�Lignting�Ne]�(N�	rrigation�Ne]�(�IND��ICATE:��1 % PER GROUF�e]�(N�Female��2 % PER GROUF�e]�(�1.��Are you or is any member €��fyour household�eeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubh)��}�(h]�j$	  ah]�((G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�(G@Iǎ    G?��   G@���@   G@��=�   t�eh?]���2012 Long Term Capital Plan Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report Screener 89�ahA]�(]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Screener
89�a]��Screener
90
Page 130 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Screener
91�a]��Screener
92
Page 131 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Screener
93�a]��Screener
94
Page 132 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Questionnaire – Part 1
95�a]��+Questionnaire – Part 1
96
Page 133 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Questionnaire – Part 1
97�a]��+Questionnaire – Part 1
98
Page 134 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Questionnaire – Part 2
99�a]��,Questionnaire – Part 2
100
Page 135 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Questionnaire – Part 2
101�a]��,Questionnaire – Part 2
102
Page 136 of 138�a]���2012 Long Term Capital Plan
Atttachment 12 - ILLUMINA RESEARCH PARTNERS SUPER GROUP SUMMARY Appendix K - ISP Consultation Report
Questionnaire – Part 2
103�a]��,Questionnaire – Part 2
104
Page 137 of 138�aeh�<fortisbc-2012-integrated-system-plan---volume-1---30june2011�j�  M�j�  Nubeub.