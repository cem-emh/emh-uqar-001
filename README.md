# EMH-UQAR-001


## Visuals
https://miro.com/app/board/uXjVNTcISuw=/
https://docs.google.com/presentation/d/1LGjt4TOmbYypdMt-Wzf6-DupKTWYaDiEDpJzgiAEOPc/edit?usp=drive_link
https://zoom.us/rec/share/60zXlum_AEzCP3ENTc6XK59FJZqW7U4iYtdPQAmyXIpqXQPmy3ooyA3flgPO2rK2.3MUnyQ_GUvc4PLpc


## Extract Pipeline Ledger: 
https://docs.google.com/spreadsheets/d/1dhyOJdIExzQgckSiFMbHqbEKymMF-VWL/edit?usp=drive_link&ouid=107737350542366886588&rtpof=true&sd=true


## CODERS Data Guide: 
https://docs.google.com/document/d/1eCOZEtNEgmRBPmAv2CYY2Ss1c3BmTJWB/edit?usp=sharing&ouid=107737350542366886588&rtpof=true&sd=true


## Project Folder: 
https://drive.google.com/drive/folders/10tR2AheOH4E3mRqarM8_ZZwi2XzJY10q?usp=drive_link

## Useful links
https://framework.frictionlessdata.io/index.html

## Data Source: HFED
https://energy-information.canada.ca/en/resources/high-frequency-electricity-data?utm_source=lnkn&utm_medium=smo&utm_campaign=%20statcan_ccei-ccie_2024



