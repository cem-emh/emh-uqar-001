from datetime import datetime
import logging
from typing import Dict, Generator, Union
import os
import pandas as pd

from difflib import SequenceMatcher
from helpers.table_toolbox import CustomOutputDocument, CustomBaseDocument, CustomOutputTable, TableTitle
from helpers.pdf_toolbox import CustomTable
from helpers.docling_toolbox import CustomDocLingTable
from helpers.helper import Helper

class CompareTable():
    id_counter = 0
    #TODO : Suggestion Passer en paramètre le répertoire du log
    #TODO : Suggestion Passer en paramètre confidence_threshold
    def merge_extractors_tables(self, documents: list[CustomBaseDocument], confidence_threshold: float = 0.8) -> CustomOutputDocument:
        """
        This public method merges the tables found by different extractors on a PDF.

        Args:
        documents (list[CustomBaseDocument]): A list of documents to merge.
        confidence_threshold (float): The threshold of confidence to consider two tables as identical.

        Return:
        Output_document: CustomOutputDocument 
        """
        #Group the documents by name

        for doc in documents:

          if doc.tables is None or len(doc.tables) == 0:
              continue

          #PATCH
          #TODO : À traiter dans la classe
          if not hasattr(doc, "extractor_name"):
              doc.extractor_name = self.__infer_extractor__(doc.tables[0])
             
        doc = self.__compare_tables_from_document__(documents, confidence_threshold)
        doc.update_metadata(self.__compare_metadata__(documents))
        return doc

    def __compare_tables_from_document__(self, docs: list[CustomBaseDocument], confidence_threshold: float) -> CustomOutputDocument:
        """
        This public method compares the output of two or more extractors on a PDF to log the differences found and possibly
        merge identical tables.

        Args:
        docs (list[CustomBaseDocument]): A list of documents extracted by different extractors.
        confidence_threshold (float): The threshold of confidence to consider two tables as identical.

        Return:
        CustomTable: Clean and complete CustomTable objects ready to be exported.
        """
        #Initialize the logger
        if len(docs) == 0:
            raise Exception("No documents to compare")
        logger = ComparisonLogger(docs[0])

        #Create the output document
        output_document = CustomOutputDocument(docs[0].document_name)
        output_document.extractor_list = [doc.extractor_name for doc in docs]

        pages = {}
        #Group the tables by page
        for doc in docs:
            for table in doc.tables:
                output_table = table.to_output_table(self.__give_merge_id__())                
                pages.setdefault(table.page_number, []).append((doc.extractor_name, output_table))

        for page_number, page_tables in pages.items():

            logger.log_page(page_number)
            if len(page_tables) == 1:
                extractor_name, table = page_tables[0]
                logger.log_lone_table(table)

            #TODO: Add the comparison functions
            #TODO: Add better logging messages
            merged_tables_id = []
            for i, (extractor_a, table_a) in enumerate(page_tables):
                if table_a.merge_id in merged_tables_id:
                    continue
                current_merge_list = []
                logger.log_table(table_a)

                for extractor_b, table_b in page_tables[i + 1:]:
                    if table_b.merge_id in merged_tables_id or extractor_a == extractor_b:
                        continue

                    bbox_similarity = self.__compare_bbox__(table_a.bbox_list, table_b.bbox_list)
                    if bbox_similarity < confidence_threshold:
                        logger.log_bbox_failure(table_a, table_b, bbox_similarity)
                        continue
                    #TODO Ludovic Log bbox si c'est similaire

                    dimensions_identical = self.__compare_dimensions__(table_a, table_b)
                    if not dimensions_identical:
                        logger.log_dimension_failure(table_a, table_b)
                        continue
                    #TODO Ludovic Log dim si c'est similaire

                    header_similarity = self.__compare_strings__(table_a.header, table_b.header)
                    row_similarity = self.__compare_rows__(table_a.to_pandas(), table_b.to_pandas())
                    
                    #TODO, idéalement il faudrait toujours avoir un title, ex : créer une classe "sans titre"
                    if table_a.title and table_b.title :
                      title_similarity = self.__compare_strings__([table_a.title.title], [table_b.title.title])

                    comparison_result = ComparisonResult(bbox_similarity, dimensions_identical, header_similarity, row_similarity, title_similarity)
                    comparison_result.add_origin(table_b)
                    table_a.comparisons.append(comparison_result)
                    current_merge_list.append(table_b)
                    merged_tables_id.append(table_b.merge_id)
                    #TODO : Ludovic il faut un log_table_match et log_table à la fin car on peut merger plusieurs tables
                    #Il faut ajouter les extractors dans extractors_lists et modifier le extractor_type='output'
                    logger.log_table_match(table_a, table_b, comparison_result)

                if len(current_merge_list) > 0:
                    merged_table = self.__merge_tables__(table_a, current_merge_list)
                    output_document.append_table(merged_table)
                else:
                    output_document.append_table(table_a)
                    logger.log_no_match(table_a)
                logger.log_end_of_table_comparison()
                    
        return output_document

    def __give_merge_id__(self) -> int:
        """
        This private method returns a unique identifier for the table.

        Returns:
            int: A unique identifier for the table.
        """
        self.id_counter += 1
        return self.id_counter

    def __compare_bbox__(self, bbox_list_a: list[tuple[float, float, float, float]], bbox_list_b: list[tuple[float, float, float, float]]) -> float:
        """
        This private method compares two bounding box lists to determine their similarity.

        Args:
            bbox_list_a (list[tuple[float, float, float, float]]): A list of bounding boxes for the table A.
            bbox_list_b (list[tuple[float, float, float, float]]): A list of bounding boxes for the table B.

        Returns:
            float: A value between 0.0 and 1.0 representing the similarity between the two bounding box lists.
        """
        if len(bbox_list_a) != len(bbox_list_b) or len(bbox_list_a) == 0 or len(bbox_list_b) == 0:
            return 0.0
        
        #Explore the two lists wtih the same index
        sum_similarity = 0.0
        for i in range(len(bbox_list_a)):
            x0_a, y0_a, x1_a, y1_a = bbox_list_a[i]
            x0_b, y0_b, x1_b, y1_b = bbox_list_b[i]

            min_x = min(x0_a, x0_b, x1_a, x1_b) 
            min_y = min(y0_a, y0_b, y1_a, y1_b)
            if min_x < 0:
                x0_a += -min_x
                x0_b += -min_x
                x1_a += -min_x
                x1_b += -min_x

            if min_y < 0:
                y0_a += -min_y
                y0_b += -min_y
                y1_a += -min_y
                y1_b += -min_y
            
            x0 = max(x0_a, x0_b)
            y0 = max(y0_a, y0_b)
            x1 = min(x1_a, x1_b)
            y1 = min(y1_a, y1_b)

            # Compute intersection area
            intersection_width = max(0, x1 - x0)
            intersection_height = max(0, y1 - y0)
            intersection_area = intersection_width * intersection_height

            # Compute the area of both boxes
            box1_area = max(0, x1_a - x0_a) * max(0, y1_a - y0_a)
            box2_area = max(0, x1_b - x0_b) * max(0, y1_b - y0_b)

            # Compute union area
            union_area = box1_area + box2_area - intersection_area

            intersection_ratio = intersection_area / union_area if union_area > 0 else 0
            sum_similarity += intersection_ratio
        
        return sum_similarity / len(bbox_list_a)
    
    def __compare_dimensions__(self, table_a: CustomOutputTable, table_b: CustomOutputTable) -> bool:
        """
        This private method compares two tables to determine if their dimensions are identical.

        Args:
            table_a (CustomOutputTable): A table to compare.
            table_b (CustomOutputTable): A table to compare.

        Returns:
            bool: True if the tables are identical in dimension, False otherwise.
        """
        return table_a.to_pandas().shape == table_b.to_pandas().shape
        
    def __compare_strings__(self, string_list_a: Union[list[str],None], string_list_b: Union[list[str],None]) -> float:
        """
        This private method compares two strings to determine their similarity.

        Args:
            string_a (str): A string to compare.
            string_b (str): A string to compare.

        Returns:
            float: A value between 0.0 and 1.0 representing the similarity between the two bounding box lists.
        """
        result=0.0 #By default no similarity

        if string_list_a is None or string_list_b is None:
            pass
        elif len(string_list_a) != len(string_list_b) or len(string_list_a) == 0 or len(string_list_b) == 0:
            pass
        else:
          sum_similarity = 0.0
          for i in range(len(string_list_a)):
              sum_similarity += SequenceMatcher(None, f'{string_list_a[i]}', f'{string_list_b[i]}').ratio()
          result=sum_similarity / len(string_list_a)

        return result

    def __compare_rows__(self, pd_dataframe_a: pd.DataFrame, pd_dataframe_b: pd.DataFrame) -> float:
        """
        This private method compares two rows to determine their similarity.

        Args:
            pd_dataframe_a (pd.DataFrame): A pandas DataFrame to compare.
            pd_dataframe_b (pd.DataFrame): A pandas DataFrame to compare.

        Returns:
            float: A value between 0.0 and 1.0 representing the similarity between the two bounding box lists.
        """
        #Save the dataframes columns
        columns_a = pd_dataframe_a.columns
        columns_b = pd_dataframe_b.columns

        #Normalize the columns names
        pd_dataframe_a.columns = range(len(columns_a))
        pd_dataframe_b.columns = range(len(columns_b))

        #Compare the two dataframes
        #TODO: Itterate over the rows and compare them with a better method since this one is too sensitive to negligeable differences
        pd_comparison = pd_dataframe_a.compare(pd_dataframe_b)
        #comparison_array = pd_comparison.to_numpy()

        #Iterate over the comparison array and count the number of differences with

        number_of_differences = pd_comparison.size / 2
        total_number_of_cells = pd_dataframe_a.size
        similarity = 1 - (number_of_differences / total_number_of_cells)

        #TODO: See if restoring the columns names is necessary
        #Restore the columns names
        pd_dataframe_a.columns = columns_a
        pd_dataframe_b.columns = columns_b
        return similarity
    
    def __merge_tables__(self, table_a: CustomOutputTable, list_tables: list[CustomOutputTable]) -> CustomOutputTable:
        """
        This private method merges two tables into a single table.

        Args:
            table_a (CustomOutputTable): The first table to merge.
            list_tables (CustomOutputTable): The list of tables to merge with the first one.

        Returns:
            CustomOutputTable: The merged table.
        """
        merged_table = CustomOutputTable()
        merged_table.title = self.__merge_titles__(table_a, list_tables)
        merged_table.header = self.__merge_headers__(table_a, list_tables)
        merged_table._df_table = self.__merge_df_table__(table_a, list_tables)

        merged_table.extractors_list = table_a.extractors_list + [table.extractors_list for table in list_tables]
        merged_table._extractor_type="output"
        merged_table.merge_id = table_a.merge_id
        merged_table.page_number = table_a.page_number
        merged_table.bbox_list = table_a.bbox_list
        merged_table.comparisons = table_a.comparisons

        return merged_table

    def __merge_headers__(self, table_a: CustomOutputTable, list_tables: list[CustomOutputTable]) -> list[str]:
        """
        This private method merges the headers of the tables.
        
        Args:
        table_a (CustomOutputTable): The first table to merge.
        list[CustomOutputTable]: The list of tables to merge with the first one.

        Returns:
        list[str]: The merged headers.
        """
        list_tables_to_merge = list_tables.copy()
        list_tables_to_merge.insert(0, table_a)

        if table_a.header is None:
            table_a.header = table_a.to_pandas().columns.to_list()
        list_headers = [[] for _ in range(len(table_a.header))]

        #Check if the headers are present in the other tables, if not, add them
        for table in list_tables_to_merge:
            if table.header is None:
                table.header = table.to_pandas().columns.to_list()
            for i, column_header in enumerate(table.header):
                column_header = str(column_header)
                if column_header == "" or column_header == "None":
                    column_header = "nan"
                if column_header not in list_headers[i]:
                    list_headers[i].append(column_header)
            
        #Remove nan if there are other headers
        for headers in list_headers:
            if len(headers) > 1 and "nan" in headers:
                headers.remove("nan")

        #Concatenate the headers with the separation symbol and apply them to the table_a
        merged_headers = ["!&!".join(map(str, headers)) for headers in list_headers]
        table_a.header = merged_headers
        table_a._df_table.columns = merged_headers

        #Apply the same headers to the other tables
        for table_b in list_tables:
            table_b._df_table.columns = merged_headers

        return merged_headers

    def __merge_titles__(self, table_a:CustomOutputTable, list_tables: list[CustomOutputTable]) -> TableTitle:
        """
        This private method merges the titles of the tables.
        
        Args:
        table_a (CustomOutputTable): The first table to merge.
        list[CustomOutputTable]: The list of tables to merge with the first one.

        Returns:
        TableTitle: The merged title.
        """
        #TODO: The current implementation of title does not allow for multiple bbox and page number, so the logic is not that great
        #Create a list of titles
        list_titles = []
        if table_a.title:
            list_titles.append(table_a.title.title)

        for table_b in list_tables:
            if table_b.title.title not in list_titles:
                list_titles.append(table_b.title.title)

        #Concatenate the titles
        merged_title = "!&!".join(list_titles)
        merged_page_number = table_a.title.page_number

        #Calculate the bbox average
        #HACK: This is a naive way to find the bbox
        x0 = (sum([table.title.bbox[0] for table in list_tables]) + table_a.title.bbox[0]) / len(list_titles) + 1
        y0 = (sum([table.title.bbox[1] for table in list_tables]) + table_a.title.bbox[1]) / len(list_titles) + 1
        x1 = (sum([table.title.bbox[2] for table in list_tables]) + table_a.title.bbox[2]) / len(list_titles) + 1
        y1 = (sum([table.title.bbox[3] for table in list_tables]) + table_a.title.bbox[3]) / len(list_titles) + 1
        merged_bbox = (x0, y0, x1, y1)

        return TableTitle(merged_page_number, merged_title, merged_bbox)
    
    def __merge_df_table__(self, table_a: CustomOutputTable, list_tables: list[CustomOutputTable]) -> pd.DataFrame:
        # Start with the first DataFrame
        merged_df = table_a.to_pandas().copy().astype(str)
        list_of_df = [table.to_pandas().astype(str) for table in list_tables]

        # Iterate and merge using the custom function
        for df in list_of_df:
            for column in merged_df.columns:
                merged_df[column] = merged_df[column].combine(df[column], self.__combine_cells__)

        return merged_df
    
    def __combine_cells__(self, x, y, separator='!&!'):

        if y == "nan":
            return x
        if y not in x.split(separator):
            if x == 'nan':
                return y
            return f"{x}{separator}{y}"
        else:
            return x

    def __infer_extractor__(self, table: CustomOutputTable) -> str:
        """
        This private method infers the extractor that found the table. It is only used for non updated pickles.

        Args:
            table (CustomOutputTable): The table to infer the extractor from.

        Returns:
            str: The name of the extractor that found the table.
        """
        if type(table) == CustomDocLingTable:
            return "Docling"
        elif type(table) == CustomTable:
            return "PyMu"
        else:
            raise Exception("Table type not supported")

    def __compare_metadata__(self,documents):
        
        merge_metadata={}
        for doc in documents:
          if doc.metadata:
            for k,v in doc.metadata.items():
              if k in merge_metadata:
                  if v!= merge_metadata[k]:
                    merge_metadata[k]= f'{merge_metadata[k]}!&!{v}'   
              else:
                merge_metadata[k]=v

        return merge_metadata
        
class ComparisonResult():
    bbox_similarity: float
    dimensions_identical: bool
    header_similarity: float
    row_similarity: float
    title_similarity: float
    global_similarity: float

    origin_extractor: str
    origin_table_id: int

    def __init__(self, bbox_similarity: float, dimensions_identical: bool, header_similarity: float, row_similarity: float, title_similarity: float):
        self.bbox_similarity = bbox_similarity
        self.dimensions_identical = dimensions_identical
        self.header_similarity = header_similarity
        self.row_similarity = row_similarity
        self.title_similarity = title_similarity
        self.global_similarity = sum([bbox_similarity, header_similarity, row_similarity, title_similarity]) / 4

    def add_origin(self, table: CustomOutputTable):
        """
        This public method adds the origin of the table.
        """
        self.origin_extractor = table.extractors_list[0]
        self.origin_table_id = table.merge_id

class ComparisonLogger():
    def __init__(self, document: CustomBaseDocument):
        self.__initialize_logging__(document)

    def __initialize_logging__(self, document: CustomBaseDocument):
        """
        This private method initializes the logging system.
        """
        helper = Helper()
        target_folder = os.environ.get("CSV_PATH","")
        if not target_folder:
            raise Exception("No CSV_PATH environment variable found")
        
        target_folder = helper.add_path_from_the_workspace(target_folder)
        
        first_sub_folder = "output"
        if not helper.is_folder_exist_in_directory(first_sub_folder, target_folder):
            raise Exception(f"Folder {first_sub_folder} not found in {target_folder}")
        
        target_folder = os.path.join(target_folder, first_sub_folder)
        second_sub_folder = f"{document.document_name}"
        
        Helper.add_folder_to_an_existing_path(second_sub_folder, target_folder)
        if not helper.is_folder_exist_in_directory(second_sub_folder, target_folder):
            raise Exception(f"Folder {second_sub_folder} not found in {target_folder}")
        
        target_folder = os.path.join(target_folder, second_sub_folder)
        third_sub_folder = "logs"

        log_filepath = helper.add_folder_to_an_existing_path(third_sub_folder, target_folder)

        date_time = datetime.now().strftime("%Y-%m-%d %H-%M-%S")
        filename = f"{document.document_name} - {date_time}"

        log_filepath = helper.add_file_to_an_existing_path(filename, log_filepath, ".log")
        
        #HACK: Might conflict with other loggers
        logging.shutdown()
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

        logging.basicConfig(filename=log_filepath, level=logging.INFO, format='%(message)s')
        self.__present_context__(document)

    def __present_context__(self, document: CustomBaseDocument):
        """
        This private method presents the context of the document.
        """
        logging.info(f"Log of the document: {document.document_name}")
        logging.info(f"Date: {datetime.now()}")

    def log(self, message: str):
        logging.info(message)

    def log_page(self, page_number: int):
        """
        This public method logs the page number.
        """
        # Standardise the page number to three digits
        page_number = str(page_number).zfill(3)
        logging.info(f"--- Page {page_number} ---")
        self.__insert_lines__(1)

    def log_table(self, table: CustomOutputTable):
        """
        This method presents a table.
        """
        identifiers = f"Table {str(table.merge_id).zfill(3)} - [{table.title.title}]"
        shape = f"Shape: {table.to_pandas().shape}"
        bbox = f"Bbox: {table.bbox_list}"
        extractors = f"Extractors: {', '.join(table.extractors_list)}"

        resulting_string = '\n'.join([identifiers, shape, bbox, extractors])

        logging.info(resulting_string)

    def log_lone_table(self, table: CustomOutputTable):
        """
        This public method logs a table found by only one extractor.
        """
        logging.info(f"Table {table.merge_id} found on page {table.page_number} by only the extractor {table.extractors_list[0]}")

    def log_dimension_failure(self, table_a: CustomOutputTable, table_b: CustomOutputTable):
        """
        This public method logs a dimension failure.
        """
        logging.info(f"Dimensions differ for tables {table_a.merge_id} and {table_b.merge_id} on page {table_a.page_number} between {table_a.extractors_list[0]} and {table_b.extractors_list[0]} ({table_a.to_pandas().shape} != {table_b.to_pandas().shape})")

    def log_bbox_failure(self, table_a: CustomOutputTable, table_b: CustomOutputTable, bbox_similarity: float):
        """
        This public method logs a bbox failure.
        """
        logging.info(f"Bounding boxes differ for tables {table_a.merge_id} and {table_b.merge_id} on page {table_a.page_number} between {table_a.extractors_list[0]} and {table_b.extractors_list[0]} ({bbox_similarity * 100:.2f}% similarity)")

    def log_comparison_result(self, comparison_result: ComparisonResult):
        """
        This public method logs a comparison result.
        """
        global_similarity = f"--- Global similarity: {comparison_result.global_similarity * 100:.2f}% ---"
        bbox_similarity = f"Bbox similarity: {comparison_result.bbox_similarity * 100:.2f}%"
        dimensions_identical = f"Dimensions identical: {comparison_result.dimensions_identical}"
        header_similarity = f"Header similarity: {comparison_result.header_similarity * 100:.2f}%"
        row_similarity = f"Row similarity: {comparison_result.row_similarity * 100:.2f}%"
        title_similarity = f"Title similarity: {comparison_result.title_similarity * 100:.2f}%"
        message = '\n'.join([global_similarity, bbox_similarity, dimensions_identical, header_similarity, row_similarity, title_similarity])

        logging.info(message)

    def log_table_match(self, table_a: CustomOutputTable, table_b: CustomOutputTable, comparison_result: ComparisonResult):
        """
        This public method logs a table match.
        """
        #TODO Ludovic Ici moi je ferais l'inverse. merge with table_b  et log_table(table_a)
        logging.info(f"Compatible for merge with {table_a.merge_id} :")
        self.log_comparison_result(comparison_result)
        self.__insert_lines__(1)
        self.log_table(table_b)

    def log_no_match(self, table_a: CustomOutputTable):
        """
        This public method logs a table with no match.
        """
        logging.info(f"No compatible table found for merge with {table_a.merge_id}")

    def log_end_of_table_comparison(self):
        """
        This public method logs the end of the table comparison.
        """
        logging.info("End of table comparison")
        self.__insert_lines__(2)

    def __insert_lines__(self, number_of_lines: int):
        for _ in range(number_of_lines):
            logging.info("")



