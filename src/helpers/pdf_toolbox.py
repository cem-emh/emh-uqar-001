
from typing import Dict, Iterator, List, Literal, Optional, Tuple

import fitz as PyMuPDF
from fitz import Document, TEXTFLAGS_TEXT, find_tables
from operator import itemgetter
from frictionless import Resource, validate
import pandas as pd
import json
from enum import Enum

from transformers import TableTransformerForObjectDetection, DetrImageProcessor
from PIL import Image
import torch
import numpy as np
import easyocr
from io import BytesIO
import math
from PIL import Image
import matplotlib.pyplot as plt

from helpers.helper import Helper #
from helpers.table_toolbox import CustomBaseDocument,CustomBaseTable,TableTitle,  BaseExtractor


"""
Parameters to adjust the padding around the table  (model : microsoft/table-transformer-detection) 
and the padding around the the last row of the table (model : microsoft/table-transformer-structure-recognition-v1.1-all).
"""
is_microsoft_table_transformer_models_loaded = False
image_processor = None

"""
Parameters used for the microsoft/table-transformer-detection model and to adjust the padding around the table.
"""
MTTD_MODEL_NAME = "microsoft/table-transformer-detection"
MTTD_LOADED_MODEL_NAME = "table-transformer-detection"
MTTD_THRESHOLD = 0.9
MTTD_TABLE_LEFT_PADDING = 0.10
MTTD_TABLE_TOP_PADDING = 0.09
MTTD_TABLE_RIGHT_PADDING = 0.03
MTTD_TABLE_BOTTOM_PADDING = 0.07
model_table = None

"""
Parameters used for the microsoft/table-transformer-structure-recognition-v1.1-all model and to adjust the padding around the the last row of the table.
"""
MTTSR_MODEL_NAME = "microsoft/table-transformer-structure-recognition-v1.1-all"
MTTSR_LOADED_MODEL_NAME = "table-transformer-structure-recognition-v1.1-all"
MTTSR_MODEL_2_NAME = "microsoft/table-transformer-structure-recognition"
MTTSR_THRESHOLD = 0.6
MTTSR_LAST_ROW_LEFT_PADDING = 0.0
MTTSR_LAST_ROW_TOP_PADDING = 0.0
MTTSR_LAST_ROW_RIGHT_PADDING = 0.0
MTTSR_LAST_ROW_BOTTOM_PADDING = 0.0
model_structure = None

"""
The first one is a dictionary used to store efficiency results.

The following are parameters used to dictate the maximum admissible tolerances that can separate the location coordinates 
of an item (table, title) from its model, depending on the axis. Both parameters use the unit of measurement 
"Point". The relationship is that there are 72 points in an inches, so a standard U.S. Letter-sized page of 
8.5 * 11 inches is equivalent to 612 * 792 points.
"""
efficiency_results_dict = {}
HORIZONTAL_TOLERANCE = 31 
VERTICAL_TOLERANCE =  40

"""
Colors for visualization and used in the "plot_results" and "visualize_data" functions.
"""
COLORS = [[0.000, 0.447, 0.741], [0.850, 0.325, 0.098], [0.929, 0.694, 0.125],
          [0.494, 0.184, 0.556], [0.466, 0.674, 0.188], [0.301, 0.745, 0.933]]

class CustomTable(CustomBaseTable):
    
    """
    This is a class for merging any table reprensentation into a single class representation.
    It is based on pymupdf Table. You will need to adapt your table representation into this one.
    
    Attributes:

        rows (list[list[str]]): List of rows and row is a list contaning the cell string.
        __columns_type : Attributes that contain the detected type of the table columns.
        _extractor_type (str) : define which extractor find the table (pymu)
    """
        
    rows: list[list[str]]
    __columns_type: list    
    _extractor_type : Literal['pymu','detr'] 
    
    def __init__(self,extractor_type :Literal['pymu',] ) -> None:
        """
        The constructor for CustomTable class. Please directly set attributes values.
        """
        self._extractor_type=extractor_type
        self.__columns_type = None
        pass
    
    @property
    def columns_type(self):
        """
        Attributes that contain the detected type of the table columns.
        """
        if not self.__columns_type:
            self.__get_columns_type__()
        return self.__columns_type
    

    def extend_table(self, second_table):
        """
        Method to merge a table into its parent table.
        
        Parameters:
            second_table (CustomTable): CustomTable object to merge into self.
        """
        
        self.extend_bbox(second_table.bbox_list)
        self.rows.extend(second_table.rows)
        pass
        
    def __get_columns_type__(self):
        """
        Private method that get the table columns types.
        """
        
        self.__columns_type = []
        with Resource(self.rows) as resource:
            for field in resource.schema.fields:
                self.__columns_type.append(field.type)

    def to_pandas(self)-> pd.DataFrame:
        """
        Method to convert CustomTable into a pandas DataFrame.

        Returns:
            pd.DataFrame: Pandas DataFrame Object of the CustomTable.
        """
        if self._df is None:
           
          try:
              #If the fisrt row equals the header, skip this row as data
              tmp_rows = self.rows[1:] if self.header==self.rows[0] else self.rows
              self._df = pd.DataFrame.from_records(tmp_rows, columns=self.header)
          except:
              self._df = pd.DataFrame.from_records(self.rows)
          #Empty string is convert to N/A
          #https://www.statology.org/pandas-replace-empty-string-with-nan/
          with pd.option_context('future.no_silent_downcasting', True):
            self._df = self._df.replace(r'^\s*$', np.nan, regex=True)
        
        return self._df
    
    @property
    def extractor_type(self)->str:  
      #HACK
      #TODO : Refaire un dump et éliminer le None comme valeur
      if hasattr(self, "_extractor_type") and self._extractor_type :
        return self._extractor_type
      
      return 'pymu'
    
class PymuDocument(CustomBaseDocument):
    
    extractor_name:str #TODO : À supprimer, attribut existe dans CustomBaseDocument
    __tables: list[CustomTable]

    def __init__(self,tables: list[CustomTable],doc_name:str):
        super().__init__(doc_name,"pymu")
        self.__tables=tables
    
    @property
    def tables(self):
        return self.__tables


class PyMuExtractor(BaseExtractor):

    """
    This is a class for converting one pdf file into fitz Document 
    extract all tables .

    """

    #TODO : comme docling, séparer l'extraction des objets et la conversion vers les custom tables dans une 2e option
    #       Objectif : Faire un dump des objets natifs de PYMU et DETR

    #TODO : Metadata extraction

    def __init__(self,dump_path :Optional[str]=None, to_dump:bool=False,to_pickle:bool=False) -> None:
        super().__init__(dump_path,to_dump,to_pickle)
        self.extractor_name='pymu'
        pass
    
    def __convert_aux__(self,file_name:str) -> PymuDocument:
        
        doc=Document(file_name) 
        tables:list[CustomTable]=[t for t in self.__extract_tables_from_doc__(doc)]
        #HACK: extractor_name is not in the constructor
        pymu_doc=PymuDocument(tables,file_name)
        pymu_doc.update_metadata(self.__extract_metadata(doc))
        pymu_doc.extractor_name=self.extractor_name
        return pymu_doc
    
    def __custom_doc__(self,doc:PymuDocument,doc_name:str)->PymuDocument:
        
        #Return the same object, the document is already a custom document

        #Patch : some dump object has no document_name        
        for t in doc.tables:
          try: 
            doc_name_table=t.document_name
          except AttributeError: 
            try:
              doc_name_table=self.documents_name
            except AttributeError: 
              doc_name_table=doc_name
          
          t.document_name=doc_name_table
        
        new_doc=PymuDocument(doc.tables,doc_name)
        new_doc.update_metadata(doc.metadata)
        return new_doc
    
    def __extract_metadata(self,pymu_doc:Document)-> dict:
        """
        This private method extract the metadata from a pymu document

        Args:
            doc (Document): The PyMuPDF.Document
        
        Return
          dict
        """
        response:dict =pymu_doc.metadata
        response['page_count']=pymu_doc.page_count
        response['pagelayout']=pymu_doc.pagelayout
        return response
    
    def __extract_tables_from_doc__(self, doc: Document):
        """
        This private method extract tables from a document.

        Args:
            doc (Document): The PyMuPDF.Document to extract tables from.

        Yields:
            PymuTable: PymuTable object that contain a table that is ready to be exported. This table should already be clean and complete.
        """
        
        current_table: CustomTable = None       # Memory of the last table.
        for page in doc:
            for found_table in find_table(page):
                found_table.document_name, _ =Helper.get_file_name(doc.name)
                found_table.page_number = page.number
                if not is_table_valid(found_table):
                    continue

                title = find_title(page,found_table)
                found_table.title = title
                
                if not current_table:
                    current_table = found_table
                    continue
                
                #TODO: This part should not be loacated here. The decision process should be in the pdf_toolbox.
                if title:
                    yield current_table
                    
                    # Update the current table with the new one.
                    current_table = found_table
                
                else:
                    # Check if same column etc.
                    if current_table.header == found_table.header:
                        current_table.extend_table(found_table)
                        break

                    if current_table.nb_col == found_table.nb_col and current_table.columns_type == found_table.columns_type:
                        current_table.extend_table(found_table)
                        break
                    
                    yield current_table
                    current_table = found_table
                    
        #Yield the last table of the document.
        if current_table:
            yield current_table
    


class EfficiencyStatus(Enum):
    """
    Enumeration of elements representing results of a location data comparison between an extracted table and its equivalent from a model (Tabula).

    FALSE_MATCH : There is an extracted table, but no equivalence from the model.
    NO_MATCH : The extracted table does not respects the tolerances with its equivalent from the model.
    NOT_DETECTED : There is no extracted table, but one detected table from the model.
    NOT_PROCESSED : The extracted table has not been compared.
    STRONG_MATCH : The extracted table respects the tolerances with its equivalent from the model and has a global_efficiency_indicator equal to or greater than 50.
    WEAK_MATCH : The extracted table respects the tolerances with its equivalent from the model and has a global_efficiency_indicator less than 50.
    """
    
    FALSE_MATCH = 1
    NO_MATCH = 2
    NOT_DETECTED = 3
    NOT_PROCESSED = 4
    STRONG_MATCH = 5
    WEAK_MATCH = 6
    

class EfficiencyItem:
    """
    Class that contains the result of an efficiency evaluation between a detected item (a table or a title location) and its equivalent from a model (Tabula).
    """
    def __init__(self, type:str, page:int, horixontal_tolerance:int, vertical_tolerance:int):
        """
        The constructor for EfficiencyItem class.

        Args:
            type (str): The type of the evaluated item (table or title).
            page (int): The number of the PDF document page, starting from 1.
            horixontal_tolerance (int): Maximum admissible tolerances that can separate the location coordinates of the item from its model, on the horizontal axis. 
                                        The unit of measurement is the "Point".
            vertical_tolerance (int): Maximum admissible tolerances that can separate the location coordinates of the item from its model, on the vertical axis. 
                                        The unit of measurement is the "Point".
        """
        
        self.type = type
        self.page = page
        self.efficiency_status = EfficiencyStatus.NOT_PROCESSED
        self.horixontal_tolerance = horixontal_tolerance        
        self.vertical_tolerance = vertical_tolerance             
        self.global_efficiency_indicator = 0
        self.left_difference_indicator = 0
        self.right_difference_indicator = 0
        self.top_difference_indicator = 0
        self.bottom_difference_indicator = 0
        
    def calculate_efficiency_with_tabula_model(self, model_item:Dict, method_item:Dict)->None:
        """
        Method which calculates the system item identification efficiency by comparing its location coordinates with those of an equivalent item, 
        identified by the Tabula model.

        Args:
            model_item (Dict): Item detected by the system.
            method_item (Dict): Equivalent item detected by the model.
        """
      
        if (model_item == None or method_item == None):
            self.efficiency_status = EfficiencyStatus.NO_MATCH
            return
            
        self.left_difference_indicator = model_item["x1"] - method_item["x0"]
        self.top_difference_indicator = (model_item["y1"] - method_item["y0"])*(-1)
        self.right_difference_indicator = (model_item["x2"] - method_item["x1"])*(-1)
        self.bottom_difference_indicator = model_item["y2"] - method_item["y1"]
        
        if (abs(self.left_difference_indicator) > self.horixontal_tolerance or abs(self.right_difference_indicator) > self.horixontal_tolerance ):
            self.efficiency_status = EfficiencyStatus.NO_MATCH
            return
            
        if (abs(self.top_difference_indicator) > self.vertical_tolerance or abs(self.bottom_difference_indicator) > self.vertical_tolerance ):
            self.efficiency_status = EfficiencyStatus.NO_MATCH
            return
            
        global_difference = abs(self.left_difference_indicator) + abs(self.right_difference_indicator) + abs(self.top_difference_indicator) + abs(self.bottom_difference_indicator)
        global_tolerance = 2*(self.horixontal_tolerance + self.vertical_tolerance)
        self.global_efficiency_indicator = 100 - ((global_difference * 100) / global_tolerance)
        
        if self.global_efficiency_indicator > 50:
            self.efficiency_status = EfficiencyStatus.STRONG_MATCH
            return
            
        self.efficiency_status = EfficiencyStatus.WEAK_MATCH
    
    def serialize_to_json(self)->Dict:
        """
        Method that applies a serialization process to a EfficiencyItem object.

        Returns:
            Dict: Dictionary that contains the key-value data representation of the EfficiencyItem object.
        """
        
        json_item = {
                "type": self.type,
                "page": self.page,
                "efficiency_status": self.efficiency_status.name,
                "horixontal_tolerance": self.horixontal_tolerance,
                "vertical_tolerance" : self.vertical_tolerance,
                "global_efficiency_indicator" : self.global_efficiency_indicator,
                "left_difference_indicator": self.left_difference_indicator,
                "right_difference_indicator": self.right_difference_indicator,
                "top_difference_indicator": self.top_difference_indicator,
                "bottom_difference_indicator": self.bottom_difference_indicator
            }
        return json_item
    
    def set_efficiency_status(self, efficiency_status:EfficiencyStatus)->bool:
        """
        Methode that set the set_efficiency_status attribute.

        Args:
            efficiency_status (EfficiencyStatus): Efficiency status of the efficiency evaluation.

        Returns:
            bool: True if proper functioning. Always for now.
        """
        
        self.efficiency_status = efficiency_status 
        return True


class ModelItem:
    """
    Class that contains the detected item (a table or a title location) from a model (tabula) and that will be used in measuring the efficiency of an EfficiencyItem.
    """
    
    def __init__(self, page:int, extraction_method:str, selection_id:str, x1:int, x2:int, y1:int, y2:int, width:int, height:int, spec_index:str):
        """
        The constructor for ModelItem class. Based on the tabula location data model.

        Args:
            page (int): Page number where the item is is located.
            extraction_method (str): Model Extraction method used.
            selection_id (str): Identifier of the model item.
            x1 (int): Item xmin coordinate.
            x2 (int): Item xmax coordinate.
            y1 (int): Item ymin coordinate.
            y2 (int): Item ymax coordinate.
            width (int): PDF page width, in "Point" unit.
            height (int): PDF page height, in "Point" unit.
            spec_index (str): Item special index.       
        """
        
        self.page = page
        self.extraction_method = extraction_method
        self.selection_id = selection_id
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.width = width
        self.height = height
        self.spec_index = spec_index       
    
    @staticmethod
    def translate_dict_to_object(dict:Dict):
        """
        Static method that transfer a dictionary representation into an object of type ModelItem.

        Args:
            dict (Dict): Dictionary representation of ModelItem.

        Returns:
            ModelItem: Resulting ModelItem object.
        """
        
        page = dict["page"]
        extraction_method = dict["extraction_method"]
        selection_id = dict["selection_id"]
        x1 = dict["x1"]
        x2 = dict["x2"]
        y1 = dict["y1"]
        y2 = dict["y2"]
        width = dict["width"]
        height = dict["height"]
        spec_index = dict["spec_index"]
        obj = ModelItem(page, extraction_method, selection_id, x1, x2, y1, y2, width, height, spec_index)
        return obj
    
    def convert_to_string(self)->str:
        """
        Method that onvert a ModelItem object into string representation.

        Returns:
            str: String representation of a ModelItem.
        """
        
        json_item = {
            "page" : self.page,                            
            "extraction_method" : self.extraction_method,
            "selection_id" : self.selection_id,
            "x1" : self.x1,
            "x2" : self.x2,
            "y1" : self.y1,
            "y2" : self.y2,
            "width" : self.width,
            "height" : self.height,
            "spec_index" : self.spec_index
            }
        string_item = json.dumps(json_item)
        return string_item
    

def add_a_key_to_the_dictionary(dictionary:Dict, key:str)->None:
    """
    Function that add a key to a dictionary.

    Args:
        dictionary (Dict): Dictionary to add a key to.
        key (str): Key to add.
    """
    
    try:
        dictionary[key] = []

    except :
        return


def add_the_key_content_to_the_dictionary(dictionary:Dict, key:str, content_path:str)->bool:
    """
    Function that add the content of an object to a dictionary key.

    Args:
        dictionary (Dict): Dictionary to add a content to.
        key (str): Key of the dictionary.
        content_path (str): Absolute complete path of the json file where is save the information who needs to be add to the dictionary.

    Returns:
        bool: True if the function worked, False otherwise.
    """
    
    content = Helper.deserialize_a_json_file(content_path)
    if is_key_in_dictionary(dictionary, key):
        dictionary[key] = content
        return True
    return False  


def adjust_table_structure(structure_array:list, left_padding:float = 0.0, top_padding:float = 0.0, right_padding:float = 0.0, bottom_padding:float = 0.0, element_position:int = -1)->None:
    """
    Function that add padding for one element of the table. When element_position = -1, the element is the last row.

    Args:
        structure_array (list): List of table rows.
        left_padding (float, optional): Left padding to add to the element, from 0.0 (0%) to 1.0 (100%). Defaults to 0.0.
        top_padding (float, optional): Top padding to add to the element, from 0.0 (0%) to 1.0 (100%). Defaults to 0.0.
        right_padding (float, optional): Right padding to add to the element, from 0.0 (0%) to 1.0 (100%). Defaults to 0.0.
        bottom_padding (float, optional): Bottom padding to add to the element, from 0.0 (0%) to 1.0 (100%). Defaults to 0.0.
        element_position (int, optional): Element (or row) position in the table to apply the padding to. Defaults to -1.
    """
    
    structure_element = structure_array[element_position]
    padding_adjustment = torch.tensor([left_padding * structure_element[0] * (-1), top_padding * structure_element[1] * (-1), right_padding * structure_element[2], bottom_padding * structure_element[3]])
    structure_array[element_position] = torch.add(structure_element, padding_adjustment)
 
 
def bold_in_page(page: PyMuPDF.Page)->bool:
    """
    Function that look for fontname that contain the word Bold in them. this is used to quickly find a page contain bold text in it.

    Args:
        page (PyMuPDF.Page): Page of the PDF document where words in bold are searched. 

    Returns:
        bool: True if there is at least a word in bold, False otherwise.
    """
    
    return any("Bold" in font_name[-3] for font_name in page.get_fonts())


def check_equivalance_of_data(data1:float, data2:float, adjustment_factor:float=0)->bool:
    """
    Function that indicates if two coordinates are equivalent following rounding and/or the addition of an adjustment factor.
    This is used for intersecting columns and rows of a table to determine the resulting coordinates of data cells.

    Args:
        data1 (float): Coordinates of a row or column.
        data2 (float): Coordinates of an equivalent row or column.
        adjustment_factor (float, optional): Tolerated variation accepted. Defaults to 0.

    Returns:
        bool: True if consider as equivalent, False otherwise.
    """
    
    data1_floor = math.floor(data1)
    data1_ceil = math.ceil(data1)
    data2_floor = math.floor(data2)
    data2_ceil = math.ceil(data2)
    
    if(data1_floor == data2_floor or data1_floor == data2_ceil):
        return True
    if(data1_ceil == data2_floor or data1_ceil == data2_ceil):
        return True
    
    if(data1_floor - adjustment_factor == data2_floor or data1_floor - adjustment_factor == data2_ceil):
        return True
    if(data1_floor + adjustment_factor == data2_floor or data1_floor + adjustment_factor == data2_ceil):
        return True
    
    if(data1_ceil - adjustment_factor == data2_floor or data1_ceil - adjustment_factor == data2_ceil):
        return True
    if(data1_ceil + adjustment_factor == data2_floor or data1_ceil + adjustment_factor == data2_ceil):
        return True
    
    return False


def determine_table_perimeter(results_table:Dict, threshold:float = 0.8, left_padding:float = 0.0, top_padding:float = 0.0, right_padding:float = 0.0, bottom_padding:float = 0.0)->None:
    """
    Function that adjusts the perimeter of a table's coordinates by adding padding and rejects predictions below the threshold.

    Args:
        results_table (Dict): Table data predicted by the model.
        threshold (float, optional): Minimum threshold to accept a prediction data. Defaults to 0.8.
        left_padding (float, optional): Padding to add to the left side of the table boder. Defaults to 0.0.
        top_padding (float, optional): Padding to add to the top side of the table boder. Defaults to 0.0.
        right_padding (float, optional): Padding to add to the right side of the table boder. Defaults to 0.0.
        bottom_padding (float, optional): Padding to add to the bottom side of the table boder. Defaults to 0.0.
    """
    
    boxes = results_table['boxes']
    scores = results_table['scores']
    
    for index, (boxe, score)  in enumerate(zip(boxes, scores)):
        if (score.item() < threshold):
            del scores[index]
            del boxes[index]
            continue
    
        padding_adjustment = torch.tensor([left_padding * boxe[0] * (-1), top_padding * boxe[1] * (-1), right_padding * boxe[2], bottom_padding * boxe[3]])
        boxes[index] = torch.add(boxe, padding_adjustment)


def determine_the_efficiency_of_a_surface_item_search_method(system_data_path:str, system_dict:Dict, model_data_paths:list[str], model_dict:Dict)->Dict|bool:
    """
    Function that calculates efficiency of data areas that have been identified by the system compared to the equivalent ones identified by a model (tabula).

    Args:
        system_data_path (str): Directory path where the location data of the items identified by the system (following an extraction) is located.
        system_dict (Dict): Dictionary which will contain the location data of the items identified by the system and will be used by the calculating efficiency.
        model_data_paths (list[str]): JSON files paths where the location data of the items identified by the Tabula model (used as reference) are located.
        model_dict (Dict): Dictionary which will contain the location data of the items identified by the Tabula model and will be used by the calculating efficiency.

    Returns:
        Dict|bool: Dictionary containing efficiency results if the function worked, False otherwise.
    """
    
    global efficiency_results_dict
    is_the_system_loaded = load_pdf_data_into_a_dictionary("SYSTEM", system_data_path, system_dict)
    is_the_model_loaded = load_pdf_data_into_a_dictionary("MODEL", model_data_paths, model_dict)
    
    if (is_the_system_loaded and is_the_model_loaded):
        
        # Sorting dictionary to get increasing data based on page number.
        sort_dictionary_by_page_attribute(system_dict)
        sort_dictionary_by_page_attribute(model_dict)
        
        keys_of_method_dict = set(system_dict.keys())
        keys_of_model_dict = set(model_dict.keys())
        common_keys_of_both_dict = keys_of_method_dict.intersection(keys_of_model_dict)
        
        for pdf_key in common_keys_of_both_dict: 
            efficiency_results_dict[pdf_key] = []
            hashed_items_from_model_dict = {}
            
            for method_item in system_dict[pdf_key]:
                model_common_items = []
                
                for model_item in model_dict[pdf_key]:
                    
                    if model_item["page"] < method_item["page"]:
                        continue
                    
                    elif model_item["page"] == method_item["page"]:
                        model_common_items.append(model_item)
                        continue
            
                    elif model_item["page"] > method_item["page"]:
                        efficiency_item = EfficiencyItem(method_item["type"], method_item["page"], HORIZONTAL_TOLERANCE, VERTICAL_TOLERANCE)
                        common_items_number = len(model_common_items)
                        
                        match common_items_number:
                            # 0 table from model - 1 table from tested method (ONLY FALSE_MATCH)
                            case 0:
                                efficiency_item.set_efficiency_status(EfficiencyStatus.FALSE_MATCH)
                                json_efficiency_item = efficiency_item.serialize_to_json()

                            # 1 table from model - 1 table from tested method (can be STRONG_MATCH, WEAK_MATCH, NO_MATCH)
                            case 1:
                                efficiency_item.calculate_efficiency_with_tabula_model(model_common_items[0], method_item)
                                json_efficiency_item = efficiency_item.serialize_to_json()
                                
                                dict_item = model_common_items[0]
                                obj_item = ModelItem.translate_dict_to_object(dict_item)
                                string_item = obj_item.convert_to_string()
                                hash_item = hash(string_item)       # If an model item was used for a comparison, it is assigned a hashed value.
                                
                                if hashed_items_from_model_dict.get('{}'.format(hash_item)) is None:
                                    hashed_items_from_model_dict['{}'.format(hash_item)] = {obj_item} 
                                
                            # * tables from model - 1 table from tested method (can be STRONG_MATCH, WEAK_MATCH, NO_MATCH)
                            case _ if common_items_number > 1:
                                item_index = find_nearest_neighbor_of_a_surface_item_with_tabula_model(model_common_items, method_item)
                                efficiency_item.calculate_efficiency_with_tabula_model(model_common_items[item_index], method_item)
                                json_efficiency_item = efficiency_item.serialize_to_json()
                                
                                dict_item = model_common_items[item_index]
                                obj_item = ModelItem.translate_dict_to_object(dict_item)
                                string_item = obj_item.convert_to_string()
                                hash_item = hash(string_item)       # If an model item was used for a comparison, it is assigned a hashed value.
                                
                                if hashed_items_from_model_dict.get('{}'.format(hash_item)) is None:
                                    hashed_items_from_model_dict['{}'.format(hash_item)] = {obj_item} 
                                
                        efficiency_results_dict[pdf_key].append(json_efficiency_item)
                        break  
        
            did_determine_the_not_detected_cases_work = determine_the_not_detected_cases_of_a_surface_item_search_method(pdf_key, hashed_items_from_model_dict, model_dict)
            
            if did_determine_the_not_detected_cases_work:
                return efficiency_results_dict
            
    return False


def determine_the_not_detected_cases_of_a_surface_item_search_method(pdf_key_of_model:str, items_from_model:Dict, model_dict:Dict)->bool:
    """
    Function that calculates the efficiency of data areas that have been identified for the NOT_DETECTED cases. 
    NOT_DETECTED case is when the model (Tabula) detects an area, but the system did not.
    
    It does this by comparing the model items which were used for the efficiency measurement to all the model items, 
    in order to find those which correspond to the cases NOT_DETECTED. It uses hashing for that.

    Args:
        pdf_key_of_model (str): Key of the dictionary that represent a PDF document source and where the efficiency is measured.
        items_from_model (Dict): Dictionary of the model items (data area) which were used for the efficiency measurement.
        model_dict (Dict): Dictionary of all the items (data area) that have been identified by the model.

    Returns:
        bool: True if the function has worked, False otherwise.
    """
    
    global efficiency_results_dict
    
    #(ONLY NOT_DETECTED)
    if model_dict.get('{}'.format(pdf_key_of_model)) is not None:
        for item in model_dict[pdf_key_of_model]:
            obj_item = ModelItem.translate_dict_to_object(item)
            string_item = obj_item.convert_to_string()
            hash_item = hash(string_item)
            
            # If, for a pdf document, a model item has not been used for the efficiency measurement, it's because we are in a case of NOT_DETECTED.
            if items_from_model.get('{}'.format(hash_item)) is None:
                efficiency_item = EfficiencyItem(item["extraction_method"], item["page"], HORIZONTAL_TOLERANCE, VERTICAL_TOLERANCE)
                efficiency_item.set_efficiency_status(EfficiencyStatus.NOT_DETECTED)
                json_efficiency_item = efficiency_item.serialize_to_json()
                efficiency_results_dict[pdf_key_of_model].append(json_efficiency_item)       
        return True
    return False


def does_bbox_contain_a_negative_value(bbox:Tuple)->bool:
    """
    Function that indicates if a bbox contains a negative value.

    Args:
        bbox (Tuple): _description_

    Returns:
        bool: True if the bbox has a negative value, False otherwise.
    """

    return any(value < 0 for value in bbox)


def extract_values_from_cells_table(image: Image, sorted_cells_matrix: np.ndarray)->np.ndarray :
    """
    Function that extract the values (text contained in each cell) from an image of of a table.

    Args:
        image (Image): Image of the table.
        sorted_cells_matrix (np.ndarray): Array of the data extracted from the table.

    Returns:
        np.ndarray: Array of the data extracted from the table (sorted_cells_matrix).
    """
    
    reader = easyocr.Reader(['en'])
    dimensions = sorted_cells_matrix.shape
    num_rows, num_cols = dimensions
    values_matrix = np.zeros((num_rows, num_cols), dtype=object)
    PERCENTAGE_ADJUSTMENT_MARGIN = 5/100
    
    for row_idx in range(sorted_cells_matrix.shape[0]):
        for col_idx in range(sorted_cells_matrix.shape[1]):
            element = sorted_cells_matrix[row_idx, col_idx]
            if isinstance(element, torch.Tensor):
                png_buffer = BytesIO()
                # adjusted_element = (element[0], element[1], element[2], element[3])
                adjusted_element = element.tolist()
                cell_image = image.crop(adjusted_element)
                resized_cell_image = cell_image.resize((cell_image.width * 2, cell_image.height * 2))
                # resized_cell_image.show()
                resized_cell_image.save(png_buffer, format="PNG")
                png_buffer_bytes = png_buffer.getvalue()
                cell_value = reader.readtext(png_buffer_bytes)
                if cell_value != []:
                    data = cell_value[0][1]
                else:
                    data = ""
                values_matrix[row_idx, col_idx] = data
                
    return values_matrix


def find_table(page: PyMuPDF.Page)->Iterator[CustomTable]:
    """
    Function that will try to find table(s) in a page. Depending on the content of the page, it will use a diffrent method to find the tables.

    Args:
        page (PyMuPDF.Page): A PymuPDF representation of a PDF page.

    Yields:
        Iterator[CustomTable]: CustomTable object that represent a table that was found.
    """
    
    try:
        # if someting1:
        # if someting2:
        # if someting3:

        images = page.get_image_info(hashes=False, xrefs=False)

        if len(images)>0:
            
            #TODO (OR NOT, I DON'T KNOW)
            #create a new 
            #for image in images:
                
            #    image_bbox = image["bbox"]
                
            #    if does_bbox_contain_a_negative_value(image_bbox):
            #        continue
            #    print(f'---find_table_of_microsoft_table_transformer--- {image_bbox}' )
            #    #image_bbox is tuple, the function needs an int
            #
            #    for table in find_table_of_microsoft_table_transformer(page, image_bbox): 
            #        yield table
 
            for table in find_table_of_microsoft_table_transformer(page):
                yield table
                
        # We always do this one.
        for table in find_table_pymupdf(page, strategy="lines_strict"):
             yield table
            
    except Exception as e:
        print(e)
    
    
def find_table_of_microsoft_table_transformer(page: PyMuPDF.Page, image_resolution_in_dpi:int=300)->Iterator[CustomTable]:
    """
    This function will isolate table(s) in the page, using the microsoft/table-transformer-detection model
    and will pass image(s) of these table(s) to find_table_structure_of_microsoft_table_transformer function.

    Args:
        page (PyMuPDF.Page): A PymuPDF representation of a PDF page.
        image_resolution_in_dpi (int, optional): Number of squared pixels of the image (resolution). Defaults to 300. 

    Yields:
        Iterator[CustomTable]: CustomTable object that represent a table.
    """
    
    global is_microsoft_table_transformer_models_loaded
    if not is_microsoft_table_transformer_models_loaded:
        loading_microsoft_table_transformer_models()
    
    global image_processor
    global model_table
    global model_structure
    
    # Transform PDF page in image format.
    pix = page.get_pixmap(alpha=False, dpi=image_resolution_in_dpi)
    # pix = page.get_pixmap(alpha=False, clip=bbox_of_image, dpi=image_resolution_in_dpi)
    image_data = pix.tobytes()
    stream = BytesIO()
    stream.write(image_data)
    stream.seek(0)
    image = Image.open(stream)
    width, height = image.size
    
    # Identification of the table with the microsoft/table-transformer-detection model.

    encoding_table = image_processor(image, return_tensors="pt")
    with torch.no_grad():
        outputs_table = model_table(**encoding_table)

    results_table = image_processor.post_process_object_detection(outputs_table, threshold=MTTD_THRESHOLD, target_sizes=[(height, width)])[0]
    
    # plot_results(model_table, 0, image, results_table['scores'], results_table['labels'], results_table['boxes'])       # To visualize the table found by the microsoft/table-transformer-detection model.
    
    determine_table_perimeter(results_table, MTTD_THRESHOLD, MTTD_TABLE_LEFT_PADDING, MTTD_TABLE_TOP_PADDING, MTTD_TABLE_RIGHT_PADDING, MTTD_TABLE_BOTTOM_PADDING)
    identified_tables = results_table['boxes'].tolist()
    
    if (identified_tables is not None):
        for table in identified_tables :
            table_image = image.crop(table)
            # table_image.show()         # To visualize the image of the table found by the microsoft/table-transformer-detection model.
            yield find_table_structure_of_microsoft_table_transformer(table_image,page)


def find_table_pymupdf(page:PyMuPDF.Page, strategy:str="lines_strict")->Iterator[CustomTable]:
    """
    Function that try to find tables in a page by using PyMuPDF.find_tables function.

    Args:
        page (PyMuPDF.Page): A PymuPDF representation of a PDF page.
        strategy (str, optional): PyMuPDF documentation : Strategy “lines_strict” ignores borderless rectangle vector graphics. 
                                  Sometimes single text pieces have background colors which may lead to false columns or lines. 
                                  This strategy ignores them and can thus increase detection precision. 
                                  Defaults to "lines_strict".

    Yields:
        Iterator[CustomTable]: CustomTable object that represent a table.
    """
    
    for table in PyMuPDF.find_tables(page,strategy=strategy):
        item = CustomTable('pymu')
        item.append_bbox(table.bbox)
        item.header = [("" if not col else col.replace("\n", " ")) for col in table.header.names]
        item.rows = table.extract()
        yield item


def find_table_structure_of_microsoft_table_transformer(table_img:Image,page: PyMuPDF.Page)->CustomTable:
    """
    Function that finds the structure of a table based on its image.

    Args:
        table_img (Image): Image of a table. 
        page (PyMuPDF.Page ) : Page of the image
    Returns:
        CustomTable: CustomTable object that represent a table.
    """
    
    structure_image = table_img.convert("RGB")

    encoding_structure = image_processor(structure_image, return_tensors="pt")

    with torch.no_grad():
        outputs_structure = model_structure(**encoding_structure)
    width, height = structure_image.size

    results_structure = image_processor.post_process_object_detection(outputs_structure, threshold=MTTSR_THRESHOLD, target_sizes=[(height, width)])[0]
    
    # To visualize the table parameters in their respective order (table rows, columns, header row, ..., ...), found by the microsoft/table-transformer-detection model.
    # plot_results(model_structure, 2, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       
    # plot_results(model_structure, 1, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       
    # plot_results(model_structure, 3, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       
    # plot_results(model_structure, 4, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       
    # plot_results(model_structure, 5, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       
    # plot_results(model_structure, 0, structure_image, results_structure['scores'], results_structure['labels'], results_structure['boxes'])       

    item = CustomTable('detr')
    
    boxes=[]
    for (boxe, label)  in zip(results_structure['boxes'], results_structure['labels']):
        if (label.item() == 1):
          boxes.append(boxe.tolist())
    #TODO les dimensions des boxe ne correspondent pas aux dimensions de la page
    item.append_bbox(merge_bbox(boxes,page.rect.width,page.rect.height))  

    unsorted_columns_array, unsorted_rows_array, unsorted_header_columns_array  = isolate_table_rows_and_columns(results_structure, MTTSR_THRESHOLD)

    sorted_columns_array = sorted(unsorted_columns_array, key=lambda c: c[0])       # Sort columns in function of x0 (xmin) coordinate.
    sorted_rows_array = sorted(unsorted_rows_array, key=lambda c: c[1])         # Sort rows in function of y0 (ymin) coordinate.
    
    # Add padding for the last row for better text extraction.  
    adjust_table_structure(sorted_rows_array, MTTSR_LAST_ROW_LEFT_PADDING, MTTSR_LAST_ROW_TOP_PADDING, MTTSR_LAST_ROW_RIGHT_PADDING, MTTSR_LAST_ROW_BOTTOM_PADDING)

    sorted_cells_matrix = isolate_table_cells(sorted_columns_array, sorted_rows_array)
    sorted_values_matrix = extract_values_from_cells_table(structure_image, sorted_cells_matrix)
    
    # TODO : For improvement, make it work for cases where column headers are found on multiple lines !
    item.header = []
    if unsorted_header_columns_array != []:
        
        sorted_header_columns_array = sorted(unsorted_header_columns_array, key=lambda c: c[1])
        first_header_column_array = []
        first_header_column_array.append(sorted_header_columns_array[0])
        
        sorted_header_cells_matrix = isolate_table_cells(sorted_columns_array, first_header_column_array)
        sorted_header_values_matrix = extract_values_from_cells_table(structure_image, sorted_header_cells_matrix) 
    
        row_index = 0
        # print("\n\nDetected header of table :")       # To visualize the table header columns (visual formatting). 
        for col_index in range(sorted_header_values_matrix.shape[1]):
            # print(f" {sorted_values_matrix[row_index, col_index]} ", end="")        # To visualize the table header columns.
            temp_cell_str = sorted_values_matrix[row_index, col_index]
            if (temp_cell_str != ""):
                item.header.append(temp_cell_str)
            else:
                item.header.append(None)
        print("")       # To visualize the table header columns (visual formatting).
    
    temp_row_list = []
    # print("\n\nDetected data of table :")       # To visualize the table data (visual formatting). 
    for row_idx in range(sorted_values_matrix.shape[0]):
        # print()         # To visualize the table data (visual formatting). 
        temp_str_list = []
        for col_idx in range(sorted_values_matrix.shape[1]):
            # print(f" {sorted_values_matrix[row_idx, col_idx]} ", end="")        # To visualize the table data. 
            temp_cell_str = sorted_values_matrix[row_idx, col_idx]
            if (temp_cell_str != ""):
                temp_str_list.append(temp_cell_str)
            else:
                temp_str_list.append(None)
                
        temp_row_list.append(temp_str_list)
    
    item.rows = temp_row_list
    return item


def find_nearest_neighbor_of_a_surface_item_with_tabula_model(model_items:list[Dict], method_item:Dict)->int:
    """
    Function that compares coordinate data between a target element and its neighbors to determine which one is closest.
    It implements the k-nearest neighbors algorithm.

    Args:
        model_items (list[Dict]): All neighbors of the element.
        method_item (Dict): Target element to find its nearest neighbor.

    Returns:
        int: The resulting index of the list element that represents the nearest neighbor.
    """
    
    index = -1
    min_value = 0
    for model_item in model_items:
        index += 1
        result = math.sqrt( ((method_item["x0"]-model_item["x1"])**2) + ((method_item["y0"]-model_item["y1"])**2) + ((method_item["x1"]-model_item["x2"])**2) + ((method_item["y1"]-model_item["y2"])**2) )
        
        if (index == 0):
            min_value = result
            min_value_index = index
        
        if (result < min_value):
            min_value = result
            min_value_index = index
            
    return min_value_index

def merge_bbox(bbox_list, page_width :float, page_height:float):
    """
    Function to merge a list of bbox to create one bbox who represents the aera of bbox list

    Args :
      bbox_list (list) : List of bbox to merge
      page_width (float)
      page_height (float)
    Returns :
      Tuple(float,float,float,float) : one bbox
    """
    if len(bbox_list)==1:
        return bbox_list[0]
    elif len(bbox_list)==0:
        return None
    else:

      #the result bbox contains all the bbox      
      #top_left_origin
      #____________________
      #|x0,y0             |
      #|                  |
      #|                  |
      #|                  |
      #|             x1,y1|

      arr=np.array(bbox_list)
      #By default, we set the page size
      x0=max(0,np.min(arr[:,0])) 
      y0=max(0,np.min(arr[:,1]))
      x1=min(page_width,np.max(arr[:,2]))
      y1=min(page_height,np.max(arr[:,3]))

      return tuple((x0,y0,x1,y1))


def isolate_table_cells(columns:list, rows:list)->np.ndarray|None:
    """
    Function that joins the columns and rows of a table to isolate its cells.

    Args:
        columns (list): All the columns of the table.
        rows (list): All the rows of the table.

    Returns:
        np.ndarray|None: Cell array if the fonction has worked, None otherwise. np = Numpy.
    """
    
    # Hypothesis that inputs are sorted from left to right and top to bottom.
    if rows != []:
        cols_nbr = len(columns)
        row_nbr = len(rows)
        cells_matrix = np.zeros((row_nbr, cols_nbr), dtype=object)
        
        for col_idx, column in enumerate(columns) :
            for row_idx, row in enumerate(rows) :
                if ( (column[0] >= row[0] or check_equivalance_of_data(column[0], row[0])) and (column[2] <= row[2] or check_equivalance_of_data(column[2], row[2])) ) :      
                    cell_coordinates = torch.tensor([column[0], row[1], column[2], row[3]])
                    cells_matrix[row_idx, col_idx] = cell_coordinates
                    
        return cells_matrix


def isolate_table_rows_and_columns(results_structure:Dict, threshold:float=0.6)->Tuple[list[torch.tensor], list[torch.tensor], list[torch.tensor]]:
    """
    Function who classifies the different data in the structure of a table according to their categories (column, row, header column) and using a confidence threshold.

    Args:
        results_structure (Dict): Prediction data of the structure elements of a table. Each set of data is compose of a predicted coordinate, 
        associated with a confidence rate of the prediction and associated with a type (column, row, header)
        threshold (float, optional): Minimum threshold to accept a prediction data. Defaults to 0.6.

    Returns:
        Tuple[list[torch.tensor], list[torch.tensor], list[torch.tensor]]: Three list of tensors representing the different elements of the table structure, 
                                                                           separated according to their type (columns, rows, header).
    """
    
    boxes = results_structure['boxes']
    labels = results_structure['labels']
    scores = results_structure['scores']
    
    columns_tensors = []
    rows_tensors = []
    header_columns_tensors = []
    
    for (boxe, score, label)  in zip(boxes, scores, labels):
        if (score.item() > threshold):
            match label:
                case 1:
                    columns_tensors.append(boxe)
                case 2:
                    rows_tensors.append(boxe)
                case 3:
                    header_columns_tensors.append(boxe)
                case 4:
                    pass
                case _:
                    pass
            
    return columns_tensors, rows_tensors, header_columns_tensors


def is_key_in_dictionary(dictionary:Dict, key:str)->bool:
    """
    Function that indicates if the key is present in a dictionary.

    Args:
        dictionary (Dict): Dictionary where we want to search for the key.
        key (str): key searched.

    Returns:
        bool: True if the key has been found, False otherwise.
    """
    
    try:
        if dictionary.get(key) is not None:
            return True
        return False
    except :
        return False
        

def is_table_valid(table:CustomTable)-> bool:
    """
    #TODO: This function has not been coded yet. We could implement a LLM model to validate the structure of that tables.
    
    Function that contains the code to validate if the extracted table is really a table. It should cleen or remove any element that was dected as table, but is not.
    Example: Tables with a lot of empty rows or columns should be discarded.

    Args:
        table (CustomTable): A CustomTable object that need to be checked for its structural integrity.

    Returns:
        bool: True means the structural integrity of the table is ok, False means it needs to be discarded.
    """
    
    #For now we skip that validation. I want to use a LLM model to validate the data structure.
    return True


def loading_microsoft_table_transformer_models()->None:
    """
    Function that loads models for table and table structure detection.
    """
    
    global image_processor
    image_processor = DetrImageProcessor()
    
    # working_directory_path = Helper.get_working_directory_path()
    
    global model_table
    model_table = TableTransformerForObjectDetection.from_pretrained(MTTD_MODEL_NAME)
    
    # if (Helper.is_folder_exist_in_directory(MTTD_LOADED_MODEL_NAME, working_directory_path)):
    #     model_table = TableTransformerForObjectDetection.from_pretrained(MTTD_LOADED_MODEL_NAME)
    # else:
    #     model_table = TableTransformerForObjectDetection.from_pretrained(MTTD_MODEL_NAME)
    #     model_table.save_pretrained(MTTD_LOADED_MODEL_NAME)
    # print("\n"+f"{model_table.config.id2label}"+"\n")
    
    global model_structure
    model_structure = TableTransformerForObjectDetection.from_pretrained(MTTSR_MODEL_NAME)
    
    # if (Helper.is_folder_exist_in_directory(MTTSR_LOADED_MODEL_NAME, working_directory_path)):
    #     model_structure = TableTransformerForObjectDetection.from_pretrained(MTTSR_LOADED_MODEL_NAME)
    # else:
    #     model_structure = TableTransformerForObjectDetection.from_pretrained(MTTSR_MODEL_NAME)
    #     model_structure.save_pretrained(MTTSR_LOADED_MODEL_NAME)
    # print("\n"+f"{model_structure.config.id2label}"+"\n")
    
    global is_microsoft_table_transformer_models_loaded
    is_microsoft_table_transformer_models_loaded = True


def load_pdf_data_into_a_dictionary(dictionary_category:str, data_file_s_path_s:list[str]|str, data_dict:Dict)->bool:
    """
    Function that load json data into a dictionary.

    Args:
        dictionary_category (str): Dictionary category, because there is a variation between the steps to be applied for the system and the model.
        data_file_s_path_s (list[str] | str): Path(s) of json file(s), one path for the system and a list of paths for the model.
        data_dict (Dict): Dictionary which will receive the data from the json file(s).

    Returns:
        bool: True if the function has worked, False otherwise.
    """
    
    TABULA_KEYWORD = "tabula-"
    JSON_KEYWORD = ".json"
    
    if dictionary_category == "MODEL":     
        for data_file_path in data_file_s_path_s :
            file_name = Helper.get_file_name_from_path(data_file_path)
            if TABULA_KEYWORD in file_name :
                key_name = Helper.substract_a_substring_from_a_string(file_name, TABULA_KEYWORD) 
            if JSON_KEYWORD in data_file_path :
                key_name = Helper.substract_a_substring_from_a_string(key_name, JSON_KEYWORD)  
        
            add_a_key_to_the_dictionary(data_dict, key_name)
            add_the_key_content_to_the_dictionary(data_dict, key_name, data_file_path)
            return True
    
    elif dictionary_category == "SYSTEM":
        if JSON_KEYWORD in data_file_s_path_s :
            with open(data_file_s_path_s, 'r') as json_file: 
                temp_dict = json.load(json_file)
                data_dict.update(temp_dict)
        return True
    
    return False         
        

def plot_results(model:TableTransformerForObjectDetection, config_number_to_show:int, pil_img:Image, scores:np.ndarray, labels:np.ndarray, boxes:np.ndarray)->None:
    """
    Function of visualization of predicted table or table structure. It is suitable for data of the "microsoft/table-transformer-detection" and
    "microsoft/table-transformer-structure-recognition-v1.1-all" models. The "config_number_to_show" input indicates the category of the predicted data to display.

    Args:
        model (TableTransformerForObjectDetection): "microsoft/table-transformer-detection" or "microsoft/table-transformer-structure-recognition-v1.1-all" model.
        config_number_to_show (int): Number that indicates the category of the predicted data to display. See "model_used.config.id2label" for the legend of the categories.
        pil_img (Image): Image of the PDF page the predicted data will appear on.
        scores (np.ndarray): Model confidence rates of the predicted data.
        labels (np.ndarray): Labels (like row, column) associated to the predicted data.
        boxes (np.ndarray): Coordinates (xmin, ymin, xmax, ymax) of the predicted data location on the page.
    """
    
    # To create a new figure object (width, height).
    plt.figure(figsize=(16,10))
    
    # To display an image represented as a PIL Image object.
    plt.imshow(pil_img)
    
    # Get the current Axes instance.
    ax = plt.gca()
    colors = COLORS * 100
    
    for score, label, (xmin, ymin, xmax, ymax),c  in zip(scores.tolist(), labels.tolist(), boxes.tolist(), colors):
      if label != config_number_to_show:
        continue
      
      ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                  fill=False, color=c, linewidth=3))
      text = f'{model.config.id2label[label]}: {score:0.2f}'
      ax.text(xmin, ymin, text, fontsize=15,
              bbox=dict(facecolor='yellow', alpha=0.5))
    plt.axis('off')
    plt.show()


def sort_dictionary_by_page_attribute(dictionary:Dict)->None:
    """
    Sorting dictionary key values ​​based on a page attribute.

    Args:
        dictionary (Dict): Dictionary to sort.
    """
    keys = set(dictionary.keys())
    
    for key in keys:
        dictionary[key].sort(key=lambda objet: objet["page"])
    
    
def visualize_data(pil_img:Image, labels:list[str], boxes)->None:
    """
    Function for the visualization of image predicted table or table structure data visualization. This is the equivalent of the "plot_results" function, 
    but it displays all data categories and it is more generic.

    Args:
        pil_img (_type_): Image of the PDF page the predicted data will appear on.
        labels (_type_): Labels (like row, column) associated to the predicted data.
        boxes (_type_): Coordinates (xmin, ymin, xmax, ymax) of the predicted data location on the page.
    """
    
    plt.figure(figsize=(16,10))
    
    plt.imshow(pil_img)
    
    ax = plt.gca()
    colors = COLORS * 100
    legend = ["Titre", "Tableau", "Métadonnées"]
    hor_adjustment_factor = pil_img.width/612.0
    ver_adjustment_factor = pil_img.height/792.0
    
    for label, (xmin, ymin, xmax, ymax),c  in zip(labels, boxes, colors):
      
      xmin = xmin * hor_adjustment_factor
      ymin = ymin * ver_adjustment_factor
      xmax = xmax * hor_adjustment_factor
      ymax = ymax * ver_adjustment_factor
      
      # if label != config_number_to_show:
      #   continue
      
      ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                  fill=False, color=c, linewidth=3))
      text = f'{legend[label]}'
      ax.text(xmin, ymin, text, fontsize=15,
              bbox=dict(facecolor='yellow', alpha=0.5))
    plt.axis('off')
    plt.show()
    print()
    

def find_title(page, table: CustomTable)-> TableTitle|None:
    """
    Function that try to find a title in a page that contain a table. It will use diffrent logic to find the title.

    Args:
        page (PyMuPDF.Page): A PymuPDF representation of a page.
        table (CustomTable): CustomTable object that represent a table.

    Returns:
        TableTitle|None: either return a TableTitle object with the title, or None when no Title are found.
    """
    
    # TODO: Add other custom logic to identify title. For now we only have one. We could implement something along the lines for trying 
    #       to find specific keyword in the text above the table. For example: "Table {float}:" could be one of the keyword to look for !
    found_title = find_title_in_bold(page, table)
    
    return found_title


def find_title_in_bold(page, table: CustomTable, starting_y_location=0)-> TableTitle|None:
    """
    Function that looks above the table to find bold text and determine if its a title or not.

    Args:
        page (PyMuPDF.Page): A PymuPDF representation of a page.
        table (CustomTable): CustomTable object that represent a table.
        starting_y_location (int, optional): the y value for where to stop looking for a title. Defaults to 0.

    Returns:
        TableTitle|None: either return a TableTitle object with the title, or None when no Title are found.
    """
    
    above_table_rectangle = PyMuPDF.Rect(
        x0=0,
        x1=1000,
        y0=starting_y_location,
        y1=table.bbox_list[0][1]
    )
    
    if not bold_in_page(page):
        return
    
    rectangle_content = []
    
    blocks = page.get_text("dict", clip=above_table_rectangle, flags=PyMuPDF.TEXTFLAGS_TEXT)["blocks"]
    for block in blocks:
        for line in block["lines"]:
            for span in line["spans"]:
                if (not span["flags"] & 1 and span["text"].strip()):  # ignore superscripts and empty text
                        rectangle_content.append(span)
    
    rectangle_content.sort(key=lambda s: s["bbox"][3], reverse=True) #from bottom -> up
    
    titles = []
    title_Y_location = set()
    
    for i in range(len(rectangle_content)):
        s = rectangle_content[i]
        bold = s["flags"] & 16
        
        if bold == 16:
            if titles:
                height = titles[0]["size"]
                max_title_distance = height * 3
                actual_distance = titles[0]["origin"][1] - s["origin"][1]
                if actual_distance > max_title_distance or len(title_Y_location) == 3:
                    break
            title_Y_location.add(s["origin"][1])
            titles.append(s)

    title = ""
    if titles != []:
        title = ''.join(title_part for title_part in map(itemgetter("text"), titles))
        
        bbox_values = map(itemgetter("bbox"), titles)
        
        x0_values, y0_values, x1_values, y1_values = zip(*bbox_values)
        min_x, min_y, max_x, max_y = min(x0_values), min(y0_values), max(x1_values), max(y1_values)
        
           
        return TableTitle(page.number, title, (min_x, min_y, max_x, max_y))