from datetime import datetime
import logging
import re
from typing import Any, Dict, Generator, Optional, Union
import os
import pandas as pd
from frictionless import Resource, validate
from difflib import SequenceMatcher
from helpers.helper import Helper

from dotenv import find_dotenv, load_dotenv
import os


load_dotenv(find_dotenv())

NAME_PATTERN = r"([^-a-zA-Z0-9._ ])+"

BASE_VISUALISATION_JSON_FILE = os.environ.get("BASE_VISUALISATION_JSON_FILE","")

class BaseExtractor:
    """
        Base class to extract tables from a pdf file via a converter 

    Attributes:
        dump_path Optional[str] : path to dump the document  
        to_pickle :bool : load the file instead of the function convert   
        pickle_path:Optional[str] : path to find the .p file to load
        extractor_name : Name given to the extractor (using to create sub folder, etc...)
      """
    
    def __init__(self,base_dump_path :Optional[str]=None, to_dump:bool=False,to_pickle:bool=False) -> None:
        self.base_dump_path=base_dump_path
        self.to_pickle=to_pickle and os.path.exists(base_dump_path)
        self.to_dump=to_dump and os.path.exists(base_dump_path)
      
        pass
    
    @property
    def dump_path(self):
        """
          Add the sub_dir to the base path
          Create the directory if not exists
        """
        return Helper.add_folder_to_an_existing_path(self.extractor_name,self.base_dump_path,True)
    

      
    def __load_result__(self,doc_name:str):
        #Pickle the object the file_name
        #If not found , return None
        result:any=None
        if self.to_pickle :   
            result=Helper.load_from_file(doc_name,self.dump_path)     
        return result
    
    def convert(self,file_name:str):
        """
        Method to convert the pdf file into a document
        If the dump file document exists, load it
        If not, convert with the algo of class call
        
        Args:
            file_name str : path and file name of the pdf 

        Return:
            CustomBaseDocument
        """
        doc_name,_=Helper.get_file_name(file_name)
                
        result=self.__load_result__(doc_name)
    
        if result is None:
            result=self.__convert_aux__(file_name)
            self.__dump_result__(result,doc_name)

        return self.__custom_doc__(result,doc_name)    
        
    def __dump_result__(self,result,doc_name:str)->None:
        
        if self.to_dump : Helper.dump_to_file(result,doc_name,self.dump_path)        
        pass
    
class TableTitle(object):
    """
    This is a class that hold the title of a table.
    
    Attributes:
        title (str): The title string.  
        page_number (int): The page number starting from 0.
        bbox (tuple[float,float,float,float]): Coordinates of the border box around the title which represents respectively min(x), min(y), max(x), max(y).
    """
    
    title: str
    page_number: int
    bbox: tuple[float,float,float,float]
    
    def __init__(self, page_number:int, title:str, bbox: tuple[float,float,float,float]) -> None:
        """
        The constructor for TableTitle class.
 
        Parameters:
            page_number (int): The page number starting from 0.
            title (str): The title string. 
            bbox (tuple[float,float,float,float]): Coordinates of the border box around the title which represents respectively min(x), min(y), max(x), max(y).
        """
        
        self.title = title
        self.page_number = page_number
        self.bbox = bbox
        
    def to_visualisation(self)-> Dict:
        """
        Transfer TableTitle objet to a dictionary type for visualisation.

        Returns:
            Dict: TableTitle in dictionary type for visualisation.
        """
        
        x0,y0,x1,y1 = self.bbox
        return {
            "type": "title",
            "page": self.page_number,
            "x0": x0,
            "y0": y0,
            "x1": x1,
            "y1": y1,
            "name":self.title
        }
    @property
    def file_name(self)->str:
        #replace any non-alphanumeric character by
        
        name:str=re.sub(NAME_PATTERN, '', self.title)
        name=name.replace(" ","_")
        name=name.replace(".","_")
        return name[:50] #Max 50 characters

class CustomBaseDocument(object):
    """
    document_name (str) Name of the pdf file without the extension
    sub_dir (str) direction to save export file (ex: csv)
    extractor_name (str)
    """
    #TODO : Ajouter un attribut pour les metadatas du document (title, authors, references & language)
    #TODO : Ajouter une référence à la licence https://github.com/DS4SD/docling/blob/main/README.md
    def __init__(self,document_name,sub_dir):
        self.document_name=document_name
        self.sub_dir=sub_dir
        self.extractor_name=sub_dir
        self._metadata={}
    
    def update_metadata(self,new_meta:dict):
        """
        update the metadata
        """
        if new_meta:
          if self._metadata:
            self._metadata.update(new_meta)
          else:
            self._metadata=new_meta.copy()    

    @property
    def metadata(self)->dict:
        
        #PATCH
        #If not exist or None, we create the attributs
        if hasattr(self, "_metadata"):
          if self._metadata is None : self._metadata={}
        else :
          self._metadata={}
    
        return self._metadata
    
    def get_visualisation(self)->list:
        """
        compute the visualisation of the tables with their title

        return list :  list of visualisation_item
        
        """        
        items=[item for i,t in enumerate(self.tables) for item in t.to_visualisation(i)]    

        return items
    
    def tables_to_visualise(self,base_path)->None:
        """
        save the dictonnary of visualisation
        """
        path_file_name=Helper.add_file_to_an_existing_path(BASE_VISUALISATION_JSON_FILE,base_path,'json',self.sub_dir)        
        Helper.save_json_visualisation({self.document_name:self.get_visualisation()},path_file_name)
    
    def tables_to_csv(self, base_path: str)->None: 
        """
        Export each table data in a cvs file
        Save all the csv files in a specific folder (base_path\[extractor_type]\[document_name]\*.csv)
        args:
        base_path (str)
        """ 
        path=self.make_dir_to_csv(base_path)
        Helper.delete_all_files_from_folder(path)
        for i,t in enumerate(self.tables):
            t.to_csv(path,i)        
    
    def metadata_to_csv(self,base_path: str)->None:   
        
        """
        Export metadata in a one cvs file
        Save the metadata.csv files in a specific folder (base_path\[extractor_type]\[document_name]\_metadata.csv)
        args:
        base_path (str)
        """ 

        if self.metadata:
          path=self.make_dir_to_csv(base_path)
          file_path_name=Helper.add_file_to_an_existing_path('_metadata',path,"csv")
          rows=[[key,value] for key,value in self.metadata.items()] #map Key , value [[key1, value1],[key2,value2],...]
          
          #use CSV to export in UTF-8
          df_tmp=pd.DataFrame.from_records(rows)
          df_tmp.to_csv(file_path_name, index=False)
          
          #with open(file_path_name, 'w') as csvfile:
          #  writer = csv.writer(csvfile)
          #  writer.writerows(rows)

        pass
    
    def make_dir_to_csv(self,base_path: str)->str:
        """
          If not exist, create the directory to save the csv files

          args :
          base_path (str) : output base path

          return :
          str : the complete path created
        """
        folder=os.path.join(rf'{self.sub_dir}', self.document_name)
        path=Helper.add_folder_to_an_existing_path(folder,base_path)

        return path

class CustomBaseTable(object):
    """
    This is a class for merging any table reprensentation into a single class representation.
    It is based on pymupdf Table. You will need to adapt your table representation into this one.
    
    Attributes:
        title (TableTitle): The title object containining.  
        bbox_list (list[tuple[float,float,float,float]]): This is a list that containt the border box of every table.
        rows (list[list[str]]): List of rows and row is a list contaning the cell string.
        header (list[str]): Table header. This is the names of every columns. Usualy the first row of a table.
        page_number (int): The page number starting from 0.
        document_name (str): The name of the PDF file. Without any path or extention.  
        _df (Union[pd.DataFrame,None]) : private attr to store the data in pandas dataframe, None if not call, see method to_pandas
        _index (int) : Index in the table list of the document, #TODO To set whe you create the table 
        merge_id (int) unique numbre from the merge list
    """
    
    title: TableTitle
    bbox_list: list[tuple[float,float,float,float]]=None
    header: list[str]=[]
    page_number: int
    document_name: str
    _df: Union[pd.DataFrame,None]=None
    _index:int=None
    merge_id: int=None
    
    def __init__(self) -> None:
        """
        The constructor for CustomTable class. Please directly set attributes values.
        """

        pass
    
    @property
    def nb_col(self):
        """
        Return the number of columns in that table.
        """
        
        return len(self.header)
    
    @property 
    def doc_name(self) :
        
        #TODO: Fix the PyMuPDF dump documents_name to document_name
        #
        #PATCH
        #PATH : some dump files have no document_name attribut
        try: 
            doc_name=self.document_name
        except AttributeError: 
            try:
              doc_name=self.documents_name
            except AttributeError: 
              doc_name="not found"

        return doc_name
    
    def append_bbox(self,bbox:tuple[float,float,float,float]):
        if self.bbox_list is None:
            self.bbox_list=[bbox]
        else:
            self.bbox_list.append(bbox)

    def extend_bbox(self,bbox_list:list[tuple[float,float,float,float]]):
        if self.bbox_list is None:
            self.bbox_list=bbox_list
        else:
            self.bbox_list.extend(bbox_list)
    @property  
    def file_name(self)->str:

        """
          return a string for tje name of the csv file
        """

        if self.merge_id : return f'page_{self.page_number:03}_{self.merge_id:02}_{self.title_name}'
        if self._index : return f'page_{self.page_number:03}_{self._index:02}_{self.title_name}'
        
        return f'page_{self.page_number:03}_{self.title_name}'

    @property 
    def title_name(self) -> str:
     return self.title.file_name if self.title else 'NO_TITLE'

    @property
    def errors(self):
        """
        Attributes that contain the detected errors of the table.
        """
        
        with Resource(self.to_pandas()) as resource:
            report = validate(resource) 
            errors = report.task.errors
            return errors
        
    def to_csv(self, path: str, table_index: int, file_name:Optional[str] = None)->None:
        """
        Method to save the CustomTable object into a CSV file.

        Args:
            path (str): The path of where to put the file.
            table_index (int) : index of the table in the list
            filename (str, optional): The name of the CSV file. If None, use the attribut file_name of the object.
        """
        #We set the index
        #TODO Set the index when we create the object
        self._index=table_index

        if file_name is None: file_name=self.file_name

        file_path_name=Helper.add_file_to_an_existing_path(file_name,path,"csv")
        self.to_pandas().to_csv(file_path_name, index=False)   
        

    def to_visualisation(self, table_index: int)-> list[Dict]:
        """
        Method that return the type, page nummber and bbox of its CustomTable object.

        Returns:
            list[Dict]: List of dictionaries with the following keys. Structure : type, page, x0, y0, x1, y1.
        """
        self._index=table_index #Patch #TODO Should be set when we create the object

        item_list = []
        if self.bbox_list: 
          bbox_total=len(self.bbox_list)                   
          for i, bbox in enumerate(self.bbox_list):
              #i start at 0, for a list > 1, we have one bbox par page
              x0,y0,x1,y1 = bbox
              temp = {
                  "type": "table",
                  "page": self.page_number + i, #We assume one bbox by page
                  "x0": x0,
                  "y0": y0,
                  "x1": x1,
                  "y1": y1,
                  "class":str(self.__class__),
                  "extractor":self.extractor_type,
                  "comment":'' if bbox_total==1 else f'{i} of {bbox_total}',
                  "csv_file":self.file_name
              }
              item_list.append(temp)
            
        if self.title:
            item_list.append(self.title.to_visualisation())
            
        return item_list
    
    def to_output_table(self,merge_id: int)->Any:
        """
        Method to convert CustomBaseTable into a OutputTable.
        Args :
          merge_id(int) : id from the merge list, give a unique id to the table
        Returns:
            Any: OutputTable Object.
        """
        self.merge_id=merge_id
        
        output_table = CustomOutputTable()
        output_table.merge_id=merge_id
        if self.title is None:
          output_table.title = TableTitle(self.page_number, "Sans Titre", [0, 0, 0, 0])
        else:
          output_table.title = self.title

        output_table.header = self.header
        output_table._df_table = self.to_pandas()
        output_table.page_number = self.page_number
        output_table.bbox_list = self.bbox_list
        output_table.document_name = self.doc_name
        output_table._extractor_type = self.extractor_type
        output_table.extractors_list = [self.extractor_type]
        return output_table

class CustomOutputDocument(CustomBaseDocument):
    """
    This class represents a document from an output from merge by the extractors.

    Attributes:
    tables (list[CustomBaseTable]): A list of tables extracted from the document.
    extractor_list (str): A list of the extractors that found the tables in the document.
    """

    __tables: list[CustomBaseTable]
    extractor_list: str

    def __init__(self, doc_name: str) -> None:
        """
        The constructor for CustomOutputDocument class.

        Args:
        document_name (str): The name of the document.
        tables (list[CustomOutputTable]): A list of tables extracted from the document.
        """
        super().__init__(doc_name,"output")
        self.__tables = list()

    def append_table(self,table:CustomBaseTable):
        self.__tables.append(table)    
    
    @property
    def tables(self)-> list[CustomBaseTable]:
        return self.__tables

class CustomOutputTable(CustomBaseTable):
    """
    This is a class for merging any table reprensentation into a single class representation for export.
    It is based on pymupdf Table. You will need to adapt your table representation into this one.

    Attributes:
        merge_id (int): The unique identifier of the table for all tables in the merge.
        _df_table (pd.DataFrame): The pandas dataframe representation of the table.
        extractors_list
        _extractor_type : Name of the extractor or output when merge from extractors_list 
    """	
    
    _df_table: pd.DataFrame
    extractors_list: list[str] = list()
    _extractor_type : str
    comparisons: list[object] = list()

    def __init__(self) -> None:
        """
        The constructor for CustomOutputTable class. Please directly set attributes values.
        """
        pass
    
    def to_pandas(self)-> pd.DataFrame:
        """
        Method to convert the CustomOutputTable object into a pandas DataFrame.

        Returns:
            pd.DataFrame: The pandas DataFrame representation of the table.
        """
        return self._df_table 
        
    @property
    def extractor_type(self)->str:
        return self._extractor_type 



