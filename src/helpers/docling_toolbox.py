from functools import cached_property
import json
import os
from pathlib import Path
from typing import Union,Any, Generator, Optional, List

import pandas as pd
import numpy as np

from helpers.table_toolbox import CustomBaseDocument,CustomBaseTable,TableTitle, BaseExtractor
from helpers.helper import Helper
from docling.datamodel.base_models import InputFormat
from docling.datamodel.document import TableItem,DoclingDocument

from docling.document_converter import DocumentConverter,PdfFormatOption,ConversionResult

from docling.datamodel.pipeline_options import PdfPipelineOptions, TableFormerMode
from docling.backend.pypdfium2_backend import PyPdfiumDocumentBackend

from docling_core.types.doc import DocItemLabel,ProvenanceItem,DocItem


class CustomDocLingTable(CustomBaseTable):
    
    docling_table_list:List[TableItem]=[]

    def extend_table(self, second_table):
        """
        extend : 
        Append the table_item to the table list.
        Append the bbox
        Parameters:
            second_table (CustomTable): CustomTable object to merge into self.
        """
        self.docling_table_list.extend(second_table.docling_table_list)
        self.extend_bbox(second_table.bbox_list)
        pass
    
    def to_pandas(self)-> pd.DataFrame:
        """
        Method to convert docling_table into a pandas DataFrame.

        Returns:
            pd.DataFrame: Pandas DataFrame Object of the docling_table.
        """
        if self._df is None:
           
          if len(self.docling_table_list)==1:
              self._df=self.docling_table_list[0].export_to_dataframe()
          else:
              self._df=pd.concat([t.export_to_dataframe() for t in self.docling_table_list ])
          #Empty string is convert to N/A
          #https://www.statology.org/pandas-replace-empty-string-with-nan/
          with pd.option_context('future.no_silent_downcasting', True):
            self._df = self._df.replace(r'^\s*$', np.nan, regex=True)
        
        return self._df

    @property
    def extractor_type(self)->str:  
      return 'docling'
    
class CustomDocLingDocument(CustomBaseDocument):
    
    dl_doc:DoclingDocument
    __tables: List[CustomDocLingTable] = None
    
    def __init__(self,dl_doc:DoclingDocument,doc_name:str)->None:
        super().__init__(doc_name,"docling")
        self.dl_doc=dl_doc

    @property
    def tables(self)-> List[CustomDocLingTable]:
        if self.__tables is None:
            self.__tables=[t for t in extract_tables_from_docling(self) ]
        
        return self.__tables
    
class DoclingExtractor(BaseExtractor):

    """
    This is a class to extract tables one pdf file with docling .

    """
    #TODO : Metadata extraction
    
    def __init__(self,dump_path :Optional[str]=None, to_pickle:bool=False,pickle_path:Optional[str]=None) -> None:
        super().__init__(dump_path,to_pickle,pickle_path)
        self.extractor_name='docling'
        pass
    
    def __convert_aux__(self,file_path:str) -> ConversionResult:
      
        pipeline_options = PdfPipelineOptions()
        pipeline_options.do_ocr = True
        pipeline_options.do_table_structure = True
        pipeline_options.table_structure_options.mode = TableFormerMode.ACCURATE

        converter = DocumentConverter(allowed_formats=[InputFormat.PDF],
                                        format_options={InputFormat.PDF: PdfFormatOption(pipeline_options=pipeline_options, # pipeline options go here.
                                                        backend=PyPdfiumDocumentBackend # optional: pick an alternative backend
                                    )},)
        result = converter.convert(file_path)
        
        return result
    
    def __load_result__(self,doc_name:str):
        
        result:Any=None
        if self.to_pickle :
          #First Try to load a json file
          try :
            
            json_file_path=Helper.add_file_to_an_existing_path(doc_name,self.dump_path,"json",None)
            if os.path.exists(json_file_path):
              with Path(json_file_path).open("r") as fp:
                doc_dict = json.loads(fp.read())
                result = DoclingDocument.model_validate(doc_dict)
  
          except :
            print(f'ERROR DoclingDocument json.load {doc_name} !')

          #Second, try to load a pickle file    
          if result is None:
            result=Helper.load_from_file(doc_name,self.dump_path)

        return result

    def __dump_result__(self,result:Union[DoclingDocument,ConversionResult],doc_name:str)->None:
        
        """
          Save the DoclingDocument as JSON File
          Dump the result as .p file
        """

        if self.to_dump and os.path.exists(self.dump_path):
          
          if isinstance(result,DoclingDocument):
            doc=result
          elif isinstance(result,ConversionResult):
            doc=result.document
          else:
           raise NotImplemented

          try:
            #First Dump as json  
            file_path_name=Helper.add_file_to_an_existing_path(doc_name,self.dump_path,"json",None)
            
            with Path(rf'{file_path_name}').open("w") as fp:
              fp.write(json.dumps(doc.export_to_dict(),ensure_ascii=False)) # use `export_to_dict` to ensure consistency
          except Exception as ex:
              print(f'docling dump :{ex}')

          #Second dump as pickle (for debug purpose, we never know if we need it)
          Helper.dump_to_file(result,doc_name,self.dump_path)        
        
        pass

    def __custom_doc__(self,result:Union[DoclingDocument,ConversionResult],doc_name) -> CustomDocLingDocument:
        extractor_name="Docling"
        #HACK : extractor_name is not in the constructor
        if isinstance(result,DoclingDocument):
            custom_docling_doc=CustomDocLingDocument(result,doc_name)
            custom_docling_doc.extractor_name=extractor_name
            return custom_docling_doc
        elif  isinstance(result,ConversionResult):
            custom_docling_doc=CustomDocLingDocument(result.document,doc_name)
            custom_docling_doc.extractor_name=extractor_name
            return custom_docling_doc  
        else:
           raise NotImplemented
   
def check_to_extend_table(current_table:TableItem,previous_table : TableItem, dl_doc:DoclingDocument) -> bool :
  """
  Function that return true or false if the table is extended to a previous table .

  Args:
    current_table (TableItem) : Table to verify if it is an extension of the previous_table
    previous_table (TableItem) : Previous table found in the previous page
    dl_doc (DoclingDocument): Docling Document of the 2 tables
    .

  Returns:
      bool: to extend or not
  """
  to_extend=False

  previous_page_no=previous_table.prov[0].page_no
  current_page_no=current_table.prov[0].page_no

  if (current_page_no - previous_page_no > 1) or (current_page_no - previous_page_no < 0) :
    #The gap between the tables is too big
    to_extend=False
  elif extract_table_header(current_table) != previous_table :  
    #The header is different
    to_extend=False    
  else:
    #check if we have different items between the 2 tables   
    current_top=item_top_from_bottom_by_doc(dl_doc,current_table)
    previous_top=item_top_from_bottom_by_doc(dl_doc,previous_table)

    for item, _level in dl_doc.iterate_items(): 
      item_page=item.prov[0].page_no 
      if item_page==current_page_no:    
        
        if item.label in [DocItemLabel.CAPTION, DocItemLabel.TABLE, DocItemLabel.TEXT, DocItemLabel.TITLE] and item !=current_table and item !=previous_table:
          item_top=item_top_from_bottom_by_doc(dl_doc,item)
          if previous_page_no==current_page_no and item_top>previous_top :
             #Item is before previous table
             continue
             
          if item_top>current_top:
            #The item is between both tables, we don't extend
            to_extend=False
            break      
            
      elif item_page>current_page_no: #We assume that page are sorted in order
        #No need to continue
        break

  return to_extend

def item_top_from_bottom_by_doc(dl_doc:DoclingDocument,item:any) -> float:
  """
  Function that return the the top of the item in the page to bottom left origin .

  Args:
    dl_doc (DoclingDocument): Docling Document to get the page of the item
    item (any ItemNode): Item to fin the top position.

  Returns:
      float: top of the item in the page
  """
  page_no=item.prov[0].page_no
  page_w, page_h = dl_doc.pages[page_no].size.as_tuple()
  top=item.prov[0].bbox.to_bottom_left_origin(page_h).t

  return top

def extract_table_header(table:TableItem):
  """
  Function that return the header of the table .
  Reference : method export_to_dataframe from class TableItem
  
  Args:
    table (TableItem) : Table to get the header

  Returns:
    Optional[List[str]] : List of the colomun names
  """
  # Count how many rows are column headers
  num_headers = 0
  for i, row in enumerate(table.data.grid):
    if len(row) == 0:
        raise RuntimeError(
          f"Invalid table. {len(row)=} but {table.data.num_cols=}."
        )

    any_header = False
    for cell in row:
        if cell.column_header:
            any_header = True
            break

    if any_header:
        num_headers += 1
    else:
        break

  # Create the column names from all col_headers
  columns: Optional[List[str]] = None
  if num_headers > 0:
      columns = ["" for _ in range(table.data.num_cols)]
      for i in range(num_headers):
          for j, cell in enumerate(table.data.grid[i]):
              col_name = cell.text
              if columns[j] != "":
                  col_name = f".{col_name}"
              columns[j] += col_name
  return columns 

def extract_tables_from_docling(doc: CustomDocLingDocument):
    """
    This method extract tables from a Docling Document.

    Args:
        doc (CustomDocLingDocument): The CustomDocLingDocument contain the docling to extract tables from.

    Yields:
        CustomDocLingTable: CustomDocLingTable object that contain a table that is ready to be exported. This table should already be clean and complete.
    """
    
    current_table: CustomDocLingTable = None       # Memory of the last table.
    for table_idx, table in enumerate(doc.dl_doc.tables):
        
        to_extend=False
        captions=table.captions
        if len(captions)==0 and current_table is not None:
            #No title : check if we need to extend to the previous
            to_extend=check_to_extend_table(table,current_table.docling_table_list[-1],doc.dl_doc)
            
        if to_extend:
            second_table=create_custom_docling_table(table,doc)
            current_table.extend_table(second_table)
            continue  
        else : 
            if current_table is not None:
                yield current_table

            #Create a new table custom
            current_table=create_custom_docling_table(table,doc)
            continue
        
    #Yield the last table of the document.
    if current_table:
        yield current_table
    
    pass

def create_custom_docling_table(table_item:TableItem,doc:CustomDocLingDocument)->CustomDocLingTable:
    
    """
    Create a CustomDocLingTable object from a TableItem extract with docling
    
    Args:
        table_item (TableItem): table object from docling
        doc (CustomDocLingDocument) : CustomDocLingDocument object who contains the table
    Return
      CustomDocLingTable
    """

    custom_table = CustomDocLingTable()
    custom_table.docling_table_list=[table_item]   
    custom_table.title=create_title_from_TableItem(table_item,doc.dl_doc) 
    custom_table.page_number=extract_page_no(table_item)
    custom_table.header=extract_table_header(table_item)
      
    custom_table.append_bbox(extract_bbox_from_prov(table_item.prov[0],doc.dl_doc))
    custom_table.document_name=doc.document_name

    return custom_table

def create_title_from_TableItem(table_item:TableItem,dl_doc:DoclingDocument )->TableTitle:
  """
  Create a TableTitle object associate with the captions of the TableItem

  Args:
    table_item(TableItem) : TableItem Object  to find the title
    dl_doc(DoclingDocument) : DoclingDocument Object to get the references

  return: TableTitle object
  """
  title_found=False
  bbox=[0,0,0,0]
  page_no=-1
  crefs= [i.cref for i in table_item.captions]
  if len(crefs)==0:
    #No caption found to create the title
    pass #TODO We should have a class TableTitleNone instead
  else:
    for item, _level in dl_doc.iterate_items():
      if item.self_ref in crefs:
        bbox=extract_bbox_from_prov(item.prov[0],dl_doc)
        page_no=extract_page_no(item)
        title_found=True
        break

  return TableTitle(page_no, table_item.caption_text(dl_doc), bbox) if title_found else None
   
def extract_bbox_from_prov(provenance:ProvenanceItem,dl_doc:DoclingDocument)->tuple[float, float, float, float] :
  """
  Create the bbox of the item

  Args: 

  provenance (ProvenanceItem) : provenance of the item
  dl_doc (DoclingDocument) : docling parent of the item
  
  """
  page_w, page_h = dl_doc.pages[provenance.page_no].size.as_tuple()
  bbox = provenance.bbox.to_top_left_origin(page_h)  #To be comptabile with pymupdf and tabula

  return [bbox.l, bbox.t, bbox.r, bbox.b]

def extract_page_no(item:DocItem)->int:
  """
  Extract the page_no from the attribute prov

  Args : item(DocItem) : DocItem object to extract the page no

  Return int
  """
  #To be compatible with pymupdf, the 1st page starts at 0
  return item.prov[0].page_no-1

"""
def __extract_tables_from_doc_with_docling__(self, doc: Document):

    #This private method extract tables form a document with docling.

    Args:
        doc (Document): The PyMuPDF.Document to extract tables from.

    Yields:

    # Get the document converter
    pipeline_options = PdfPipelineOptions()
    pipeline_options.do_ocr = True
    pipeline_options.do_table_structure = True
    pipeline_options.table_structure_options.mode = TableFormerMode.ACCURATE
    file_path = doc.name

    converter = DocumentConverter(allowed_formats=[InputFormat.PDF],
                            format_options={InputFormat.PDF: PdfFormatOption(pipeline_options=pipeline_options, # pipeline options go here.
                                            backend=PyPdfiumDocumentBackend # optional: pick an alternative backend
                            )},)
    
    # Do the extraction with docling
    result = converter.convert(file_path)
    previous_table = None
    # Convert the tables to CustomTable objects
    for table in result.document.tables:
        found_table = CustomTable()
        df_table=table.export_to_dataframe()
        found_table.header = df_table.columns.tolist()
        found_table.rows = df_table.values.tolist()

        if not table_toolbox.is_table_valid(found_table):
            continue

        provenance = table.prov[0]
        found_table.page_number = provenance.page_no -1
        found_table.document_name, _ =Helper.get_file_name(file_path)
        bbox = provenance.bbox.to_top_left_origin(doc[found_table.page_number].rect[3])
        found_table.bbox_list = [[bbox.l, bbox.t, bbox.r, bbox.b]]
        
        page = doc[found_table.page_number]
        title = pdf_toolbox.find_title(page,found_table)
        found_table.title = title

        if not previous_table:
            previous_table = found_table
            continue

        if not previous_table.page_number == found_table.page_number or previous_table.page_number == found_table.page_number - 1:
            yield previous_table
            previous_table = found_table
            continue

        if not ((previous_table.title != None) and (found_table.title != None) and (previous_table.title != found_table.title)):
            if previous_table.header == found_table.header:
                previous_table.extend_table(found_table)
                continue

            if previous_table.nb_col == found_table.nb_col and previous_table.columns_type == found_table.columns_type:
                previous_table.extend_table(found_table)
                continue
            

        yield previous_table
        previous_table = found_table
"""