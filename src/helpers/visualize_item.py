import fitz as PyMuPDF 
from PIL import Image
import matplotlib.pyplot as plt
from io import BytesIO
from pdf2image import convert_from_bytes


class Visualizer(object):
    """
    Class used to visualize data location areas from a PDF.
    """
    
    pdf_sources: list[str] = []
    image_dpi = 300
    pdf_keyword = ".pdf"
    visualisation_json: str
    colors =    [[0.000, 0.447, 0.741], [0.929, 0.694, 0.125], [0.494, 0.184, 0.556],
                [0.466, 0.674, 0.188], [0.850, 0.325, 0.098], [0.301, 0.745, 0.933]]

    def __init__(self, pdf_sources:list[str], image_dpi = 300) -> None:
        """
        The constructor for Visualizer class.

        Args:
            pdf_sources (list[str]): List of the PDF absolute paths which will be visualize sequentially.
            image_dpi (int, optional): Number of squared pixels of the image (resolution). Defaults to 300.
        """
        
        self.pdf_sources = pdf_sources
        self.image_dpi = image_dpi
        
    def visualize_item(self,pdf_doc_name:str, page_number:int, boxes:list[tuple], labels:list[str], pdf_page_width:float=612.0, pdf_page_height:float=792.0):
        """
        Method that display a perimeter corresponding to a data area (item) of a PDF document. It corresponds to a visual representation functionality.

        Args:
            pdf_doc_name (str): Name of the pdf file (without its file extension, so no ".pdf"), where the item(s) coordinates are located.
            page_number (int): Page index of the pdf file where the item(s) coordinates are located. 
                               WARNING : The index starting at 0, a subtraction of one unit from the page numbers will be necessary if the first page of the pdf starts at 1.
            boxes (list[tuple]): The item(s) coordinates that will be displayed.
            labels (list[str]): The item(s) label(s) or type(s) that will be displayed.
            pdf_page_width (float, optional): The PDF page width in "Point" units. Defaults to 612.0.
            pdf_page_height (float, optional): The PDF page height in "Point" units. Defaults to 792.0.
        """
        
        path_list = [path for path in self.pdf_sources if pdf_doc_name in path]
        if len(path_list) < 1:
            raise Exception(f"Unknown file {pdf_doc_name}.pdf. Please add it to the pdf sources list.")
        
        pdf_document = PyMuPDF.open(path_list[0])  
        page = pdf_document.load_page(page_number)
        
        pix = page.get_pixmap(alpha=False, dpi=self.image_dpi)
        image_data = pix.tobytes()
        stream = BytesIO()
        stream.write(image_data)
        stream.seek(0)
        image = Image.open(stream)
        width, height = image.size
        
        # To create a new figure object (width, height).
        plt.figure(figsize=(16,10))
        
        # To display an image represented as a PIL Image object.
        plt.imshow(image)
        
        # Get the current Axes instance.
        ax = plt.gca()
        colors = self.colors * 100
        hor_adjustment_factor = image.width/pdf_page_width
        ver_adjustment_factor = image.height/pdf_page_height
        
        for label, (xmin, ymin, xmax, ymax)  in zip(labels, boxes):
        
            xmin = xmin * hor_adjustment_factor
            ymin = ymin * ver_adjustment_factor
            xmax = xmax * hor_adjustment_factor
            ymax = ymax * ver_adjustment_factor
        
            match label:
                case "title":
                    selected_color = colors[0]
                case "table":
                    selected_color = colors[1]
                case "metadata":
                    selected_color = colors[2]
                case _:
                    selected_color = colors[3]
        
            ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                        fill=False, color=selected_color, linewidth=3))
        
            text = label
            # text = f'{label}'
            ax.text(xmin, ymin, text, fontsize=15,
                    bbox=dict(facecolor='gray', alpha=0.5))
        
        plt.axis('off')
        plt.show()




# TODO : Fred, eliminate dead code after testing !
# root = get_project_root()
# PDF_SOURCES = f"{root}\\inputs_outputs\\pdf_sources"
# #DEFAULT_VISUALISATION_JSON = "inputs_outputs\\pdf_data\\localisation_data\\visualisation.json"
# PDF_KEYWORD = ".pdf"
# IMAGE_DPI = 300
# list_of_pdf_files_paths = []
# pdf_sources_path = ""
# # colors for visualization Blue - Yellow - Brown - Purple - Red - Green
# COLORS = [[0.000, 0.447, 0.741], [0.929, 0.694, 0.125], [0.494, 0.184, 0.556],
#           [0.466, 0.674, 0.188], [0.850, 0.325, 0.098], [0.301, 0.745, 0.933]]

# def init():
#     global pdf_sources_path
#     pdf_sources_path = Helper.add_path_from_the_workspace(PDF_SOURCES)
#     global list_of_pdf_files_paths
#     list_of_pdf_files_paths = Helper.get_files_paths_of_a_folder(pdf_sources_path)

# init()

# def visualize_item(pdf_doc_name:str, page_number:int, boxes:list[tuple], labels:list[str], pdf_page_width=612.0, pdf_page_height=792.0):
#     """
#     This function allows the display of a perimeter corresponding to a data area (item) in a PDF document. 
#     It corresponds to a visual representation functionality.

#     Args:
#         pdf_doc_name (str): Name of the pdf file (without its file extension, so no ".pdf") where the item(s) coordinates are located.
#         page_number (int): Page index of the pdf file where the item(s) coordinates are located. 
#                            WARNING : The index starting at 0, a subtraction of one unit from the page numbers will be necessary if the first page of the pdf starts at 1.
#         boxes (list[tuple]): The item(s) coordinates that will be displayed.
#         labels (list[str]): The item(s) label(s) or type(s) that will be displayed.
#         pdf_page_width (float, optional): _description_. Defaults to 612.0.
#         pdf_page_height (float, optional): _description_. Defaults to 792.0.
#     """
    
#     for pdf_file_path in list_of_pdf_files_paths:
#         if (pdf_doc_name in pdf_file_path):
#             pdf_document_path = pdf_file_path
    
#     pdf_document = PyMuPDF.open(pdf_document_path)  
#     page = pdf_document.load_page(page_number)
    
#     pix = page.get_pixmap(alpha=False, dpi=IMAGE_DPI)
#     image_data = pix.tobytes()
#     stream = BytesIO()
#     stream.write(image_data)
#     stream.seek(0)
#     image = Image.open(stream)
#     width, height = image.size
    
#     # To create a new figure object (width, height).
#     plt.figure(figsize=(16,10))
    
#     # To display an image represented as a PIL Image object.
#     plt.imshow(image)
    
#     # Get the current Axes instance.
#     ax = plt.gca()
#     colors = COLORS * 100
#     hor_adjustment_factor = image.width/pdf_page_width
#     ver_adjustment_factor = image.height/pdf_page_height
    
#     for label, (xmin, ymin, xmax, ymax)  in zip(labels, boxes):
      
#         xmin = xmin * hor_adjustment_factor
#         ymin = ymin * ver_adjustment_factor
#         xmax = xmax * hor_adjustment_factor
#         ymax = ymax * ver_adjustment_factor
      
#         match label:
#             case "title":
#                 selected_color = colors[0]
#             case "table":
#                 selected_color = colors[1]
#             case "metadata":
#                 selected_color = colors[2]
#             case _:
#                 selected_color = colors[3]
      
#         ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
#                                     fill=False, color=selected_color, linewidth=3))
     
#         text = label
#         # text = f'{label}'
#         ax.text(xmin, ymin, text, fontsize=15,
#                 bbox=dict(facecolor='gray', alpha=0.5))
     
#     plt.axis('off')
#     plt.show()
    

#     # TODO : Add the case where there is labels
#     # for label, (xmin, ymin, xmax, ymax),c  in zip(labels, boxes, colors):
      
#     #   xmin = xmin * hor_adjustment_factor
#     #   ymin = ymin * ver_adjustment_factor
#     #   xmax = xmax * hor_adjustment_factor
#     #   ymax = ymax * ver_adjustment_factor
      
#     #   ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
#     #                               fill=False, color=c, linewidth=3))
#     #   if labels != None :
#     #     text = f'{legend[label]}'
#     #   ax.text(xmin, ymin, text, fontsize=15,
#     #           bbox=dict(facecolor='yellow', alpha=0.5))
#     # plt.axis('off')
#     # plt.show()
#     # print()

       
# def test_of_visualize_item_func():
#         # {
#         #     "type": "table",
#         #     "page": 2,
#         #     "x0": 84.48025131225586,
#         #     "y0": 242.60702514648438,
#         #     "x1": 545.5035574776786,
#         #     "y1": 514.14990234375
#         # },
#         # ,
#         # {
#         #     "type": "table",
#         #     "page": 2,
#         #     "x0": 90.20241012573243,
#         #     "y0": 625.6101150512695,
#         #     "x1": 557.283805847168,
#         #     "y1": 731.508544921875
#         # }
        
#     pdf = "206.3-UCAP"
    
#     page_nbr = 2
    
#     list_of_items_coordinates = []
#     item_coordinates_1 = (84.48025131225586, 242.60702514648438, 545.5035574776786, 514.14990234375)
#     item_coordinates_2 = (90.20241012573243, 625.6101150512695, 557.283805847168, 731.508544921875)
#     list_of_items_coordinates.append(item_coordinates_1)
#     list_of_items_coordinates.append(item_coordinates_2)
    
#     list_of_items_types=[]
#     item_type_1 = "title"
#     item_type_2 = "table"
#     list_of_items_types.append(item_type_1)
#     list_of_items_types.append(item_type_2)
    
#     visualize_item(pdf, page_nbr, list_of_items_coordinates, list_of_items_types)
 

# # import json
# # from pprint import pprint
# # with open(DEFAULT_VISUALISATION_JSON,"r")as f:
# #     json_data:dict = json.load(f)

# # for key in json_data:
# #     item_list:list = json_data[key]
# #     item_list.sort(key=lambda x: x['page'])
# #     page = item_list[0]["page"]
# #     bbox = item_list[0]["x0"],item_list[0]["y0"],item_list[0]["x1"],item_list[0]["y1"]
# #     label_list = [item_list[0]["type"]]
# #     bbox_list = [bbox]
# #     for item in item_list[1:]:
# #         if item["page"] == page:
# #             bbox_list.append((item["x0"],item["y0"],item["x1"],item["y1"]))
# #             label_list.append(item["type"])
# #         else:
# #             visualize_item(key,page,bbox_list,label_list)
# #             page = item["page"]
# #             label_list = [item["type"]]
# #             bbox_list = [(item["x0"],item["y0"],item["x1"],item["y1"])]