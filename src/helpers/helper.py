import glob
import pickle
from typing import Any, Dict, List, Optional, Tuple, Iterator, Union
import os
import json
from pathlib import Path


def get_project_root() -> Path:
    """
    Function that get the project root path.

    Returns:
        Path: The absolute root path of the project.
    """
    
    return Path(__file__).parent.parent.parent


class Helper:
    """
    Class of utility methods.
    """
    @staticmethod
    def check_path_format(path:Union[str,None])->str:
        """
        Static method to check the format of the path, must end by "\"

        Args:
            path (str): Path to check

        Returns:
            str: Resulting path.
        """    
        format_path=""
        if path and len(path)>0:
          format_path = rf'{path}'+ "\\" if (path[-1]!='/' or path[-1]!='\\') else rf'{path}'
    
        return format_path
    
    @staticmethod
    def add_path_from_the_workspace(path:str,check_if_exist:bool=False)-> str:
        """
        Static method that add a new absolute path which originates from the workspace project.

        Args:
            path (str): Path to add to the workspace project.
            check_if_exist : check if the path exists

        Returns:
            str: Resulting path.
        """        
        relative_path = Helper.format_relative_path(path)
        absolute_path = os.path.abspath(relative_path)

        if check_if_exist :
            if not os.path.isdir(absolute_path):
              raise Exception(f"the path {path} does not exist for the absolute path {absolute_path}")
        return absolute_path
    
    @staticmethod
    def add_file_to_an_existing_path(file:str, path:str, extension : Optional[str]=None,prefix : Optional[str]=None)-> str:
        """
        Static method that add a new file to an existing absolute path.

        Args:
            file (str): file name, including its file extension or not (see extension args).
            path (str): Existing absolute path to add the file.
            prefix_file (Optional[str]) : add to the file name
            extension  Optional[str] :  add to the file name

        Returns:
            str: Resulting absolute path that include the file to.
        """
        file_name=rf'{prefix}_{file}' if prefix else file
        file_name=rf'{file_name}.{extension}' if extension else file_name

        relative_path = os.path.join(rf'{path}', file_name)
        absolute_path = os.path.abspath(relative_path)
        return absolute_path
    
    @staticmethod
    def delete_all_files_from_folder(path:str):
      """
        Static method that all files from an directory absolute path.

        Args:
            path (str): Existing absolute path to delete the files.

        Returns:
            pass
        """
        
      for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
      
        # Check if it is a file (not a subdirectory)
        if os.path.isfile(file_path):
            os.remove(file_path)  # Remove the file
      
      pass
          
    @staticmethod
    def add_folder_to_an_existing_path(folder:str, path:str,create:bool=True)-> str:
        """
        Static method that add a new folder to an existing absolute path.

        Args:
            folder (str): Full folder name, including its file extension.
            path (str): Existing absolute path to add the folder to.
            create (bool) : Create the fold if not exists
        Returns:
            str: Resulting absolute path that include the folder to.
        """
        
        relative_path = os.path.join(rf'{path}', folder)
        absolute_path = os.path.abspath(relative_path)
        if create:
          os.makedirs(absolute_path, exist_ok=True)
          
        return absolute_path
    
    # TODO : Replace with the "is_file_exist_in_directory" function !
    @staticmethod
    def check_if_file_exist_in_directory(file_name:str, directory_path:str)-> bool:
        """
        Static method that searches for the existence of a file in a directory.

        Args:
            file_name (str): Name of the file to research.
            directory_path (str): Full path of the directory where to search.

        Returns:
            bool: True if the file exist, False if it does not exist.
        """
        
        file_absolute_path = Helper.add_file_to_an_existing_path(file_name, directory_path)
        
        if os.path.exists(file_absolute_path):
            return True
        return False
    
    @staticmethod
    def deserialize_a_json_file(json_file_path:str)-> dict|None:
        """
        Static method that applies a deserialization process to a json file.

        Args:
            json_file_path (str): Absolute path of the json file to deserialize.

        Returns:
            dict|None: Resulting dictionary of the deserialization process, None if the process did not work.
        """
        
        data = None
        try:
            if os.path.exists(json_file_path):
                with open(json_file_path, 'r') as json_file: 
                    data = json.load(json_file)
                            
        except Exception as exc :
            # TODO : Add code to handle exception.
            print(f'ERROR : {exc} !')
        
        return data
    
    @staticmethod
    def get_working_directory_path()->str:
        """
        Static method that identify the path of the current working directory.

        Returns:
            str: Path of the current working directory.
        """
        
        current_working_directory = os.getcwd()
        return current_working_directory
        
    @staticmethod
    def get_files_paths_of_a_folder(folder_path:str)->list[str]:
        """
        Static method that get the list of all the files absolute paths associated to a folder.

        Args:
            folder_path (str): Absolute path of a folder.

        Returns:
            list[str]: Resulting list of files absolute paths.
        """
        
        files = [os.path.join(folder_path, f) for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
        return files
    
    @staticmethod
    def get_file_name_from_path (path:str)->str:
        """
        Static method that isolate file name from its path.

        Args:
            path (str): Absolute path of a file.

        Returns:
            str: Resulting complete file name, including its file extension.
        """
        
        filename = os.path.basename(path)
        return filename

    @staticmethod
    def get_file_name(path:str)->Tuple[str, str]:
        """
        Static method that isolate file name and file extension from its file extension.

        Args:
            path (str): Absolute path of a file.

        Returns:
            Tuple[str, str]: Resulting file name and its file extension separated.
        """
        
        filename, extension =os.path.splitext(os.path.basename(path))
        return filename, extension
    
    @staticmethod
    def is_file_exist_in_directory(file_name:str, directory_path:str)->bool:
        """
        Static method that searches for the existence of a file in a directory.

        Args:
            file_name (str): Name of the file to research.
            directory_path (str): Full path of the directory where to search.

        Returns:
            bool: True if the file exist, False if it does not exist.
        """
        
        file_absolute_path = Helper.add_file_to_an_existing_path(file_name, directory_path)
        
        if os.path.exists(file_absolute_path):
            return True
        return False
    
    @staticmethod
    def is_folder_exist_in_directory(folder_name:str, directory_path:str)->bool:
        """
        Static method that searches for the existence of a folder in a directory.

        Args:
            folder_name (str): Name of the folder to research.
            directory_path (str): Full path of the directory where to search.

        Returns:
            bool: True if the folder exist, False if it does not exist.
        """
        
        folder_absolute_path = Helper.add_folder_to_an_existing_path(folder_name, directory_path,False)
        if os.path.isdir(folder_absolute_path):
            return True
        return False
    
    @staticmethod
    def serialize_in_json(data_structure:dict, json_file_path:str)->bool:
        """
        Static method that applies a serialization process to a data structure.

        Args:
            data_structure (dict): Dictionary that contains the key-value data.
            json_file_path (str): Complete absolute path of the json file to serialize the data in (including its file extension).

        Returns:
            bool: True if the serialization process worked, False if the process did not work.
        """
        
        try:
            with open(json_file_path, 'w') as json_file: 
                json.dump(data_structure, json_file)
                return True

        except :
            # TODO : Add code to handle exception.
            print("ERROR !")  
            return False
    
    @staticmethod
    def substract_a_substring_from_a_string(string:str, substring:str)->str:
        """
        Subtract a substring from a string.

        Args:
            string (str): Chain to apply subtraction to.
            substring (str): Substring that we want to subtract.

        Returns:
            str: Resulting string.
        """
        
        final_string = string
        
        if substring is None :
            return final_string
            
        sub_start_index = string.find(substring)
        sub_end_index = sub_start_index + len(substring)
        
        match sub_start_index :
            case -1:
                pass
            case 0 :
                final_string = string[sub_end_index:len(string)]
            case _:
                if sub_end_index == len(string)-1:
                    final_string = string[0:sub_start_index]
                else :
                    final_string_st = string[0:sub_start_index]
                    final_string_nd = string[sub_end_index:len(string)]
                    final_string = final_string_st + final_string_nd
                      
        return final_string  
    
    @staticmethod        
    def union_of_visualisation_obj(current_items: dict[str,list], items_to_add: dict[str,list]):
        """
        this function makes a union on two diffrent dict.
        the goal here is to make remove any possible duplicate when merging.
        
        Args:
            current_items (dict[str,list]): _description_
            items_to_add (dict[str,list]): _description_
        Returns:
            merge_items(dict[str,list]): Resulting of the union on two diffrent dict.
        """
        result_items=current_items.copy()
        for key in items_to_add:
            if key in result_items:
                #merge with union of the element
                set1 = set(tuple(obj.items()) for obj in result_items.get(key, []))
                set2 = set(tuple(obj.items()) for obj in items_to_add.get(key, []))
                
                union_set = set1.union(set2)
                union_objects = [dict(item) for item in union_set]
                result_items[key] = union_objects
            else:
                #add key
                result_items[key] = items_to_add[key]

        return result_items
    
    @staticmethod            
    def get_files_from_directory(path_to_directory:str, extention:str = "pdf"):
        """
        Method that generates the PDF files absolute paths associated to a folder.

        Args:
            path_to_directory (str): The absolute path of the directory where to search for PDF files.
            extention (str, optional): The extension file to search for. Defaults to ".pdf".

        Yields:
            Iterator[str]: Generator of PDF files paths found.
        """
        path = Helper.check_path_format(path_to_directory)
        return glob.glob(rf'{path}*.{extention}')
        
        for file in Helper.get_files_paths_of_a_folder(path_to_directory):
            _,found_extention = Helper.get_file_name(file)
            if found_extention == extention:
                yield file

    @staticmethod  
    def save_json_visualisation(visualisation_dict: Dict, json_file_path:str)->None:
        
        """
        This method export the visualisation for a PDF document.

        Args:
            visualisation_dict (Dict): Dictionary containing the visualisation for a pdf file.
            json_file_path (str): Complete absolute path of the json file to serialize the data in (including its file extension).
        """
        
        data = Helper.deserialize_a_json_file(json_file_path)
        if data:
            #data = Helper.union_of_visualisation_obj(data, visualisation_dict)
            for key,value in visualisation_dict.items():
              data[key]=value
        else:
            data = visualisation_dict
        
        Helper.serialize_in_json(data, json_file_path)

    @staticmethod
    def format_relative_path(path:str)->str:
        """
        This static method ensures that the path is correctly formatted.
        """
        path = Helper.check_path_format(path)
        if path and len(path)>0:
            if path[0].isalpha() and path[1]==":":
                return rf".{path[2:]}"
        return path

    def dump_to_file(object: Any, file_name :str, dump_path: Union[str,None])->None:
        """
        This method dump an object into to a file. (using load function to load back the file)

        Args:
            object (any): object to save into a dump file (.p)
            dump_path :  Union[str,None]: Complete absolute path to dump the file. If not exist, no dump (without error)
            file_name (str): name of the object to dump            
        """
    
        if dump_path is not None and os.path.exists(dump_path) :
            file_path_name=Helper.add_file_to_an_existing_path(file_name,dump_path,"p",None)
            with open(file_path_name, 'wb') as f:
                pickle.dump(object,f)
    
    def load_from_file(file_name :str, dump_path: Union[str,None])->Any:
        """
        This method load an object from a dump file. (previous dump by dump_to_file )

        Args:
            object (any): object to save into a dump file (.p)
            dump_path :  Union[str,None]: Complete absolute path to dump the file. If not exist, no dump (without error)
            file_name (str): name of the object to dump   
        return :
          object (Union[any,None])  : if the file does not exist or error during the load, return None       
        """
    
        object=None
        pickle_file=Helper.add_file_to_an_existing_path(file_name,dump_path,"p",None)
        try :          
          if os.path.exists(pickle_file):
              with open(pickle_file, 'rb') as f:
                  object=pickle.load(f)            
        except :
                print(f'ERROR pickle.load {pickle_file} !')

        return object