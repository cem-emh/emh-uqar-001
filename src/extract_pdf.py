from engine import Engine
from dotenv import find_dotenv, load_dotenv
from helpers.helper import Helper

import os

dotenv_path = find_dotenv()
print(dotenv_path)
#Override is necessary to update the environment variables
load_dotenv(dotenv_path, override=True)

DEFAULT_PATH        = os.environ.get("DEFAULT_PATH","")
CSV_PATH            = os.environ.get("CSV_PATH","")
LOCALISATION_PATH   = os.environ.get("LOCALISATION_PATH","")
ANNOTATION_PDF_PATH =  os.environ.get("ANNOTATION_PDF_PATH","")
TABULA_DATA_PATH =  os.environ.get("MODEL_DATA_PATH","")
DUMP_PATH = os.environ.get("DUMP_PATH","")

document_source = (DEFAULT_PATH if os.path.exists(DEFAULT_PATH) else Helper.add_path_from_the_workspace(DEFAULT_PATH,True))
table_destination = (CSV_PATH if os.path.exists(CSV_PATH) else Helper.add_path_from_the_workspace(CSV_PATH,True))
localisation_data = (LOCALISATION_PATH if os.path.exists(LOCALISATION_PATH) else Helper.add_path_from_the_workspace(LOCALISATION_PATH,True))
annotation_pdf = (ANNOTATION_PDF_PATH if os.path.exists(ANNOTATION_PDF_PATH) else Helper.add_path_from_the_workspace(ANNOTATION_PDF_PATH,True))
tabula_source=(TABULA_DATA_PATH if os.path.exists(TABULA_DATA_PATH) else Helper.add_path_from_the_workspace(TABULA_DATA_PATH,True))
dump_destination=(DUMP_PATH if os.path.exists(DUMP_PATH) else Helper.add_path_from_the_workspace(DUMP_PATH,False))

engine = Engine(document_source,table_destination,localisation_data,annotation_pdf,tabula_source,dump_destination)

extract_docling:bool=True
extract_pymu:bool=True
engine.start(extract_docling,extract_pymu)