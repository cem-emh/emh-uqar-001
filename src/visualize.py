import json
from helpers.visualize_item import Visualizer
from helpers.helper import get_project_root, Helper

"""
Python file that activates the visualization of all the PDF extracted location data when running. 
This implies having previously runned the "engine.py" on PDF documents
"""

root = get_project_root()
DEFAULT_PDF_SOURCES_PATH = f"{root}\\inputs_outputs\\pdf_sources"
DEFAULT_VISUALISATION_JSON = f"{root}\\inputs_outputs\\pdf_data\\localisation_data\\visualisation.json"

pdf_list= list(Helper.get_pdf_from_directory(DEFAULT_PDF_SOURCES_PATH))
visualizer = Visualizer(pdf_list)
        
with open(DEFAULT_VISUALISATION_JSON,"r")as f:
    json_data:dict = json.load(f)

for key in json_data:
    item_list:list = json_data[key]
    item_list.sort(key=lambda x: x['page'])
    page = item_list[0]["page"]
    bbox = item_list[0]["x0"],item_list[0]["y0"],item_list[0]["x1"],item_list[0]["y1"]
    label_list = [item_list[0]["type"]]
    bbox_list = [bbox]
    for item in item_list[1:]:
        if item["page"] == page:
            bbox_list.append((item["x0"],item["y0"],item["x1"],item["y1"]))
            label_list.append(item["type"])
        else:
            visualizer.visualize_item(key,page,bbox_list,label_list)
            page = item["page"]
            label_list = [item["type"]]
            bbox_list = [(item["x0"],item["y0"],item["x1"],item["y1"])]