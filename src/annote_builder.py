"""
This Python file include a class to annote the pdf with to identify the different visulisations of tables, titles and metadata from the visualization of all extractor method found in location data. 
This implies having previously runned the "engine.py" on PDF documents
https://pymupdf.readthedocs.io/en/latest/recipes-annotations.html#
"""

import json
from typing import Any, Dict

from helpers.helper import  Helper

import os
from dotenv import find_dotenv, load_dotenv

import fitz as PyMuPDF 
from fitz.utils import getColor

color_set = {'docling':{'table':[0.000, 0.447, 0.741],'title': [0.929, 0.694, 0.125],'metadata': [0.494, 0.184, 0.556]},
          'pymu':{'table':[0.466, 0.674, 0.188],'title': [0.850, 0.325, 0.098],'metadata': [0.301, 0.745, 0.933]},        
          'output':{'table':getColor('DARKBLUE'),'title': getColor('TURQUOISE4'),'metadata': getColor('CYAN4')}}

color_set_other={'table':getColor('CHOCOLATE'),'title': getColor('BROWN'),'metadata': getColor('COFFEE')}
color_other= [0.301, 0.745, 0.933]
color_err=getColor('RED')
color_tabula= getColor('CYAN4')
color_text=getColor('BLACK')

load_dotenv(find_dotenv())


class AnnotationBuilder(object):
  
  """
    Class to annote a pdf with highlight the table title and draw a rect for table position
  """
  tabula:bool  #Load the tabula vizualisation
  tabula_source_path:str 
  annotation_pdf_path:str
  
  
  def __init__(self, tabula:bool=True, tabula_source_path:str = None, annotation_pdf_path:str = None) -> None:
    
    self.tabula=tabula
    self.tabula_source_path=tabula_source_path
    self.annotation_pdf_path=annotation_pdf_path
    self.tabula_keyword=os.environ.get("TABULA_KEYWORD","") 

  def annote(self,pdf_path_file:str,doc_visualisations:Dict[str,list]):
    """
    Annote a pdf with item found in doc_visualisations

    args :
    pdf_path_file (str) : absolute path and file name of the pdf source file
    doc_visualisations (Dict[str:Any]) : list of item to annote in the pdf by extractor
    
    """
    pdf_name,extension=Helper.get_file_name(pdf_path_file)
    pymu_doc = PyMuPDF.open(pdf_path_file)
    
    #TODO: Fix erro [Errno 2] No such file or directory: '...emh-uqar-001\\inputs_outputs\\pdf_data\\model_data\\tabula_stream\\tabula-table.json' !
    tabula_vis=self.load_tabula(pdf_name)
    
    pages_to_delete=[p for p in range(1,pymu_doc.page_count)]  #We set all pages : remove when we found a item, start 1 to always keep the 1st page
    
    tables_vis :dict[str,list]={} 
    for default_extractor, item_list in doc_visualisations.items():
      
      for item in item_list:
        try:
          page_no = item["page"]
          pdf_page = pymu_doc.load_page(page_no)
          if page_no in pages_to_delete : pages_to_delete.remove(page_no)
          try:
            bbox=item["x0"],item["y0"],item["x1"],item["y1"]
          
            if item["type"]=="table":
              
              extractor=item.get('extractor','extractor_unknown')
              colors=color_set.get(extractor,color_set_other)
              color=colors.get(item["type"],color_other)

              is_tabula=False
              tabula_txt=None
              if tabula_vis:
                is_tabula=find_bbox(bbox,tabula_vis.get(page_no,[]),5,5)
                tabula_txt="TABULA_EXIST" if is_tabula else "TABULA_NOT_FOUND"

              a=pdf_page.add_rect_annot(PyMuPDF.Rect(bbox))

              info = a.info
              info["title"] = item.get('csv_file',None)            
              info["content"] = f"{extractor} {item.get('comment','')} {tabula_txt}"            
              a.set_info(info)
              
              a.set_colors({"stroke":color})
              if is_tabula : a.set_border(width=2, dashes=[3])
              a.update()

              #create a dict by page            
              if tables_vis.get(page_no) is None:
                tables_vis[page_no]=[bbox]
              else:
                tables_vis[page_no].append(bbox) 
            else:
              h = pdf_page.add_highlight_annot(PyMuPDF.Rect(bbox))
              info = h.info
              if name:=item.get('name',None): info["title"] = name
              h.set_info(info)
              h.update()

          except Exception as exc :
            
            e=pdf_page.add_freetext_annot(PyMuPDF.Rect(5,5,50,15), "ERROR", text_color=color_err, border_color=color_err)
            info=e.info
            info["content"]=f'Error for {item}'
            info["title"]=default_extractor
            e.set_info(info)
            #print(f'ERROR annot: {exc} ! {prefixe_extractor} {pdf_name} page # {page_no} {bbox} ')
        
        except Exception as exc :
          print(f'ERROR Page not found: {exc} ! {default_extractor} {pdf_name} page # {page_no} ')

    #Add tabula visualisation for table not found
    
    if tabula_vis:
      for page_tabula,bbox_list_tabula in tabula_vis.items():
        pdf_page = pymu_doc.load_page(page_tabula)
        bbox_list_tables=tables_vis.get(page_tabula,[])
        if page_tabula in pages_to_delete : pages_to_delete.remove(page_tabula)
        
        for bbox_tab in bbox_list_tabula:
          found=find_bbox(bbox_tab,bbox_list_tables,5,5)

          if not found:
            #Add the tabula bbox
            annot=pdf_page.add_rect_annot(PyMuPDF.Rect(bbox_tab))
            annot.set_info(title='tabula')
            annot.set_colors({"stroke":color_tabula})
            annot.update()
    else :
      pdf_page = pymu_doc.load_page(0)
      annot=pdf_page.add_rect_annot(PyMuPDF.Rect(5,20,50,35))
      annot.set_info(title='No Tabula Location')
      annot.set_colors({"stroke":color_tabula})
      annot.update()

    annotated_file=Helper.add_file_to_an_existing_path(pdf_name,self.annotation_pdf_path,'pdf',"annotation")
    
    pymu_doc.delete_pages(pages_to_delete)
    if pymu_doc.page_count==0:
      pymu_doc.new_page()
    
    pymu_doc.save(annotated_file)

  def load_tabula(self,pdf_name)-> dict:
    
    """Load Visualisation from Tabula

    args:
    pdf_name (str) : Name of the pdf file

    return:
      dict (str, list) : list of table bbox by page
    """
    d_tabula={}
    if self.tabula:
      try:
        tabula_file=Helper.add_file_to_an_existing_path(rf'{self.tabula_keyword}{pdf_name}',self.tabula_source_path,'json',None)
        with open(tabula_file,"r") as f:
          
          tabula_list= json.load(f)

          for item in tabula_list:
            bbox = item["x1"],item["y1"],item["x2"],item["y2"]
            page_no=item['page']-1
            if d_tabula.get(page_no) is None:
              d_tabula[page_no]=[bbox]
            else:
              d_tabula[page_no].append(bbox)
        
      except Exception as exc :
        # Continue without annotation from tabula
        print(f'ERROR : {exc} !')
        d_tabula={}
    
    return d_tabula

def find_bbox(bbox_to_find,bbox_list,tolerance_in,tolerance_out)->bool:
  found=False
  for bbox in bbox_list:
    found=compare_bbox(bbox_to_find,bbox,tolerance_in,tolerance_out)  
    if found : break
  return found

def compare_bbox(bbox_a,bbox_b,tolerance_in,tolerance_out)->bool:
  x0_a, y0_a, x1_a, y1_a = bbox_a
  x0_b, y0_b, x1_b, y1_b = bbox_b
  
  is_equal = (x0_a-tolerance_out<=x0_b) and (x0_a+tolerance_in >=x0_b)
  is_equal = is_equal and (y0_a-tolerance_out <= y0_b) and (y0_a+tolerance_in >= y0_b)
  is_equal = is_equal and (x1_a+tolerance_out >= x0_b) and (x1_a-tolerance_in <= x1_b)
  is_equal = is_equal and (y1_a+tolerance_out >= y0_b) and (y1_a-tolerance_in <= y1_b)

  return is_equal
