import glob
import os
import json
from helpers.helper import  Helper
from annote_builder import AnnotationBuilder
from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())
PDF_SOURCES_PATH  = os.environ.get("DEFAULT_PATH","")
LOCALISATION_PATH = os.environ.get("LOCALISATION_PATH","")
TABULA_DATA_PATH =  os.environ.get("MODEL_DATA_PATH","")
ANNOTATION_PDF_PATH =  os.environ.get("ANNOTATION_PDF_PATH","")
TABULA_KEYWORD = os.environ.get("TABULA_KEYWORD","") 


tabula_source_path=Helper.add_path_from_the_workspace(TABULA_DATA_PATH,True)
annotation_pdf_abs_path=Helper.add_path_from_the_workspace(ANNOTATION_PDF_PATH,True) #TODO Enregistrer dans un sous répertoire (output, pymu, docling)    

annot_builder=AnnotationBuilder(True,tabula_source_path,annotation_pdf_abs_path)

pdf_sources_abs_path=Helper.add_path_from_the_workspace(PDF_SOURCES_PATH,True)
localisation_abs_path=Helper.add_path_from_the_workspace(LOCALISATION_PATH,True)

pdf_sources_abs_path=Helper.check_path_format(pdf_sources_abs_path)
pdf_list= glob.glob(rf'{pdf_sources_abs_path}*.pdf')

localisation_abs_path=Helper.check_path_format(localisation_abs_path)
visualisation_list= glob.glob(rf'{localisation_abs_path}*.json')

prefixe_extractor='output' #TODO Créer une fonction pour passer en paramètre
visualisations={}
for v in visualisation_list:
  json_file,_=Helper.get_file_name(v)
  if json_file.startswith(prefixe_extractor): 
    with open(v,"r") as f:
        visualisations = json.load(f)
        break

for pdf in pdf_list:
  pdf_name,_=Helper.get_file_name(pdf)
  items_vis=visualisations.get(pdf_name)
  if items_vis :
    annot_builder.annote(pdf,{prefixe_extractor:items_vis})
  else:
    print (f"No visualisation list for {pdf} in {json_file}")