from helpers.pdf_toolbox import determine_the_efficiency_of_a_surface_item_search_method
from helpers.helper import Helper, get_project_root
from datetime import datetime

import os

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())
DEFAULT_PATH        = os.environ.get("DEFAULT_PATH","")
CSV_PATH            = os.environ.get("CSV_PATH","")
LOCALISATION_PATH   = os.environ.get("LOCALISATION_PATH","")

MODEL_DATA_PATH =  os.environ.get("MODEL_DATA_PATH","")
PDF_EFFICIENCY_RESULTS = os.environ.get("PDF_EFFICIENCY_RESULTS","")
RESULTS_SUMMARY = os.environ.get("RESULTS_SUMMARY","")
JSON_KEYWORD = ".json"

system_localization_data_dict = {}
model_localization_data_dict = {}

"""
Python file that, when running, activates the data efficiency measurement process between the extraction data identified by the "engine.py" 
and those identified manually with a model as a reference (tabula).

This implies having previously runned the "engine.py" on a few PDF documents and to have identified manually the correct data location areas 
of these same PDF, using the tabula application.
"""

root_path = get_project_root()
localization_data_path = Helper.add_folder_to_an_existing_path(LOCALISATION_PATH, root_path)
model_data_path = Helper.add_folder_to_an_existing_path(MODEL_DATA_PATH, root_path)
model_data_files_paths = Helper.get_files_paths_of_a_folder(model_data_path)
pdf_efficiency_results_path = Helper.add_folder_to_an_existing_path(PDF_EFFICIENCY_RESULTS, root_path)

# Loop that goes through the system location results files.
localization_data_files_paths = Helper.get_files_paths_of_a_folder(localization_data_path)
for file_path in localization_data_files_paths : 
    if (JSON_KEYWORD in file_path):
        efficiency_results_dict = determine_the_efficiency_of_a_surface_item_search_method(file_path, system_localization_data_dict, model_data_files_paths, model_localization_data_dict)
        if type(efficiency_results_dict)==dict:
            file_name, file_extension = Helper.get_file_name(file_path)
            
            # Save the json file into the efficiency results repository.
            efficiency_file_path = Helper.add_file_to_an_existing_path("efficiency_of_"+file_name+file_extension,pdf_efficiency_results_path)
            did_efficiency_file_save_work = Helper.serialize_in_json(efficiency_results_dict,efficiency_file_path)

temp_dict = {}
pdf_efficiency_results_files_paths = Helper.get_files_paths_of_a_folder(pdf_efficiency_results_path)
results_summary_path = Helper.add_file_to_an_existing_path(RESULTS_SUMMARY,pdf_efficiency_results_path)
with open (results_summary_path, "w") as file:
    current_datetime = datetime.now()
    current_formatted_datetime = current_datetime.strftime("%d/%m/%Y %H:%M:%S")
    file.write(f"Summary of the efficiency results as of {current_formatted_datetime} :\n")

# Counts the efficiency results for each json file and writes it to a text file.
for efficiency_file_path in pdf_efficiency_results_files_paths :
    if (JSON_KEYWORD in efficiency_file_path):
        efficiency_file_name, efficiency_file_extension = Helper.get_file_name(efficiency_file_path)
        with open (results_summary_path, "a") as file:
            
            file.write(f"\n\n-- For {efficiency_file_name} file :\n")
            
            temp_dict = Helper.deserialize_a_json_file(efficiency_file_path)
            if(type(temp_dict)==dict):
                keys_of_dict = temp_dict.keys()
                for key in keys_of_dict:
                    
                    file.write(f"\n---- For {key} PDF document :\n")
                    
                    list_of_values = temp_dict[f"{key}"]
                    
                    false_match_counter = 0
                    no_match_counter = 0
                    not_detected_counter = 0
                    not_processed_counter = 0
                    strong_match_counter = 0
                    weak_match_counter = 0
                    comparison_number = 0
    
                    for value in list_of_values:
                        result=value["efficiency_status"]
                        match result:
                            case "FALSE_MATCH":
                                false_match_counter = false_match_counter+1
                                comparison_number = comparison_number+1
                            case "NO_MATCH":
                                no_match_counter = no_match_counter+1
                                comparison_number = comparison_number+1
                            case "NOT_DETECTED":
                                not_detected_counter = not_detected_counter+1
                                comparison_number = comparison_number+1
                            case "NOT_PROCESSED":
                                not_processed_counter = not_processed_counter+1
                                comparison_number = comparison_number+1
                            case "STRONG_MATCH":
                                strong_match_counter = strong_match_counter+1
                                comparison_number = comparison_number+1
                            case "WEAK_MATCH":
                                weak_match_counter = weak_match_counter+1
                                comparison_number = comparison_number+1
                            case _:
                                comparison_number = comparison_number+1
                                
                    file.write(f"------ results of FALSE_MATCH := {false_match_counter}\n")
                    file.write(f"------ results of NO_MATCH := {no_match_counter}\n")
                    file.write(f"------ results of NOT_DETECTED := {not_detected_counter}\n")
                    file.write(f"------ results of NOT_PROCESSED := {not_processed_counter}\n")
                    file.write(f"------ results of STRONG_MATCH := {strong_match_counter}\n")
                    file.write(f"------ results of WEAK_MATCH := {weak_match_counter}\n")
                    file.write(f"------ number of comparisons made := {comparison_number}\n")
                        

      
                    
                    
        
    
