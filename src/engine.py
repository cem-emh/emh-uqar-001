from typing import Dict, Iterator, Optional
from pprint import pprint

from helpers.pdf_toolbox import PyMuExtractor
from helpers.docling_toolbox import DoclingExtractor
from helpers.table_comparison import CompareTable
from helpers.table_toolbox import BaseExtractor
from annote_builder import AnnotationBuilder
import os
import glob




class Engine(object):
    """
    This class is the center point of the PDF extraction process.
    
    Attributes:
        document_source (str): Path to the PDF directory.  
        table_destination (str): Path where to put extracted table.  
        localisation_data (str): Path to the visualisation data.  
        dump_path : Path to save the extractor object. Debug purpose
        pdf_path_files (list[str]): A list with all PDF path.  
        annotation (bool) : If True, Annote a PDF with AnnotationBuilder
        annotation_builder (AnnotationBuilder) : class to build a annotated pdf
    """
    
    document_source: str
    table_destination: str
    localisation_data: str
    dump_path: str
    pdf_path_files: list[str]
    
    def __init__(self, document_source_path:str, table_destination:str, localisation_data:Optional[str]=None ,annotation_pdf_path:Optional[str]=None,tabula_path:Optional[str]=None,dump_path:Optional[str]=None) -> None:
        """
        The constructor for Engine class.
        
        Args:
            document_source_path (str): Path to pdf files directory.
            table_destination (str): Path where to put extracted table.
            localisation_data (str, optional): Path to the visualisation data.
            annotation_pdf_path (str, optional): Path to save the annotated PDF.
            tabula_path (str, optional): Path to read the tabula visualisation.
            dump_path (str, optional): Path to dump the extractor object
        """
        
        self.document_source =document_source_path
        self.table_destination = table_destination
        self.localisation_data = localisation_data
        self.dump_path=dump_path

        self.annotation_builder=None
        self.annotation=False
        if annotation_pdf_path:
          self.annotation=True
          self.annotation_builder=AnnotationBuilder(True,tabula_path,annotation_pdf_path)
        
        pass
    
    def __build_list__(self):
        """
        This private method is building a list of the path to PDF files.

        Raises:
            Exception: Bad path. This function require a path to a directory.
        """
        
        if not os.path.isdir(self.document_source):
            raise Exception("the document source should be a directory not a file. make sure it ends with '/'.")
        
        self.pdf_path_files = glob.glob(self.document_source + "/*.pdf")
        print("Files to extract from : ")
        pprint(self.pdf_path_files)
    
    def __get_doc__(self,extractors:list)->Iterator[tuple[list,str]]:
        """
        This private method is going to load the yield document only when needed.

        args :
            extractors : list of different extractors to convert document 

        return:
            tuple(list,str) : list of custom documents and source_path_file 
        """
        for path_file in self.pdf_path_files:
            print(f"Starting Extraction : {path_file} ")
            results=[extractor.convert(path_file) for extractor in extractors]
            yield tuple([results,path_file])
            
    def start(self, extract_docling:bool=True,extract_pymu:bool=True,filters = None):
        """
        This method will build the list of pdf and extract table for every page of every documents.

        Args:
            filters (list[str], optional): A list of filters to apply on titles and headers of tables. Defaults to None. IT IS NOT IMPLEMENTED YET.
        """
        
        extractors=[]

        if extract_pymu: 
          to_pickle=self.__str_to_bool__(os.environ.get("PICKLE_PYMU",False))
          to_dump=self.__str_to_bool__(os.environ.get("DUMP_PYMU",False))
          pymu_extractor=PyMuExtractor(self.dump_path,to_dump,to_pickle)
          extractors.append(pymu_extractor)

        if extract_docling:
          to_pickle=self.__str_to_bool__(os.environ.get("PICKLE_DOCLING",False))
          to_dump=self.__str_to_bool__(os.environ.get("DUMP_DOCLING",False))
          dl_extractor=DoclingExtractor(self.dump_path,to_dump,to_pickle)
          extractors.append(dl_extractor)

        compare_table = CompareTable()
                    
        self.__build_list__()
        #TODO : Remplacer les print par un log
        #
        for docs_path in self.__get_doc__(extractors):
          docs=docs_path[0]
          source_path_file=docs_path[1]
          for doc in docs:
              print(f'{doc.extractor_name} : {len(doc.tables)} tables found')
              doc.tables_to_csv(self.table_destination)
              doc.metadata_to_csv(self.table_destination) #HACK Must be call after tables_to_csv
              doc.tables_to_visualise(self.localisation_data)

          merge_doc=compare_table.merge_extractors_tables(docs)
          merge_doc.tables_to_csv(self.table_destination)  
          merge_doc.metadata_to_csv(self.table_destination) #HACK Must be call after tables_to_csv
          merge_doc.tables_to_visualise(self.localisation_data)
          print(f"Output completed : {len(doc.tables)} tables")

          if self.annotation:
            self.annotation_builder.annote(source_path_file,{merge_doc.extractor_name:merge_doc.get_visualisation()})    

    #TODO : Créer une classe dans Helper pour caster les variables d'environnement
    #Ex : https://discuss.python.org/t/add-environment-variable-type-casting-to-the-os-module/17408/6

    def __str_to_bool__(self,string) -> bool:
        """
        This method is converting a string to a boolean.

        Args:
            string (str): The string to convert.

        Returns:
            bool: The boolean value.
        """
        if string is None or False:
            return False
        return string.lower() in ("true", "1")


