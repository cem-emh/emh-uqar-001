import os
import tkinter as tk
from io import TextIOWrapper

from dotenv import load_dotenv, find_dotenv
from tkinter import filedialog
from typing import Dict

class PathBuilder():
    """
    Class in charge to build all path for the extraction process
    """

    main_path: str

    def __init__(self, main_path: str) -> None:
        """
        The constructor for PathBuilder class.
        
        Args:
            main_path (str): Path to the main directory.
        """
        
        self.main_path = main_path
        self.output_paths = {}
        pass

    def build_paths(self, path_list: list[str]) -> None:
        """
        Build all the paths for the extraction process.
        
        Args:
            path_list (List[str]): A list with the paths to build.
        """
        for path in path_list:
            os.makedirs(path, exist_ok=True)
        
        return

    def verify_paths(self, path_list: list[str]) -> bool:
        """
        Verify the existence of the paths.
        
        Args:
            path_list (List[str]): A list with the paths to verify.
        """
        for path in path_list:
            if not os.path.exists(os.path.join(self.main_path, path)):
                return False
            
        return True
    
    def build_default_paths(self) -> None:
        """
        Build the default paths for the extraction process.
        """

        path_list = []
        working_dir = os.getcwd()
        inputs_outputs = "inputs_outputs"
        pdf_data = "pdf_data"

        path_list.append(os.path.join(working_dir, inputs_outputs, "pdf_sources"))
        path_list.append(os.path.join(working_dir, inputs_outputs, pdf_data, "annotation_pdf"))
        path_list.append(os.path.join(working_dir, inputs_outputs, pdf_data, "table_csv"))
        path_list.append(os.path.join(working_dir, inputs_outputs, pdf_data, "localisation_data"))
        path_list.append(os.path.join(working_dir, inputs_outputs, pdf_data, "model_data", "tabula_stream"))
        path_list.append(os.path.join(working_dir, inputs_outputs, pdf_data, "dump"))
        path_list.append(os.path.join(working_dir, inputs_outputs, "pdf_efficiency_results"))


        self.build_paths(path_list)
        return

class EnvBuilder():
    """
    Class in charge to build the .env file and modify it
    """
    env : Dict[str, object]
    default_env : Dict[str, object]
    env_pattern: list[bool]
    default_env_pattern: list[bool]

    def __init__(self) -> None:
        """
        The constructor for EnvBuilder class.
        """
        
        self.env = {}
        self.default_env = {}
        self.env_pattern = []
        self.default_env_pattern = []
        pass

    @property
    def env_path(self) -> str:
        """
        Get the path to the .env file
        """
        current_path = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(current_path, ".env")
    
    @property
    def default_env_path(self) -> str:
        """
        Get the path to the .envsrc file
        """
        current_path = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(current_path, ".envsrc")
    
    @property
    def env_variables(self) -> Dict[str, object]:
        """
        Get the variables in the .env file
        """
        if not self.env:
            self.__get_env__()

        return self.env
    
    @property
    def default_env_variables(self) -> Dict[str, object]:
        """
        Get the variables in the .envsrc file
        """
        if not self.default_env:
            self.__get_default_env__()

        return self.default_env

    def restore_env(self) -> None:
        """
        Restore the .env file to its original state with the .envsrc file as a default
        """
        try:
            self.__get_default_env__()
        except FileNotFoundError:
            raise FileNotFoundError("The .envsrc file is missing. Please make sure it is in the root directory of the project.")

        with open(self.env_path, "w", encoding="utf-8") as f:
            self.__dict_to_file__(self.default_env, f)

    def __remove_unused_variables__(self) -> None:
        """
        Remove unused variables not present in .envsrc from the .env file
        """
        remove_keys = []
        for key in self.env.keys():
            if key not in self.default_env.keys():
                remove_keys.append(key)

        for key in remove_keys:
            del self.env[key]
        return

    def __add_missing_variables__(self) -> None:
        """
        Add missing variables from the .envsrc file to the .env file
        """
        for key, value in self.default_env.items():
            if key not in self.env.keys():
                self.env[key] = value

    def clean_env(self) -> None:
        """
        Clean the .env file by removing unused variables and adding missing variables
        """
        try:
            self.__get_env__()
        except FileNotFoundError:
            raise FileNotFoundError("The .env file is missing. Please make sure it is in the root directory of the project.")
        
        try:
            self.__get_default_env__()
        except FileNotFoundError:
            raise FileNotFoundError("The .envsrc file is missing. Please make sure it is in the root directory of the project.")
        
        self.__remove_unused_variables__()
        self.__add_missing_variables__()
        self.__dict_to_file__(self.env, open(self.env_path, "w", encoding="utf-8"))

    def __get_env__(self) -> None:
        """
        Get the .env file as a dictionary
        """

        with open(self.env_path, "r", encoding="utf-8") as f:
            self.env = self.__file_to_dict__(f, "env")

    def __get_default_env__(self) -> None:
        """
        Get the .envsrc file as a dictionary
        """
        with open(self.default_env_path, "r", encoding="utf-8") as f:
            self.default_env = self.__file_to_dict__(f, "default_env")

    def __file_to_dict__(self, file: TextIOWrapper, target_env: str) -> Dict[str, object]:
        """
        Convert a file to a dictionary.
        
        Args:
            file (str): The file to convert.
        """
        dict = {}
        for line in file:
                result = line.strip().replace(" ", "").split("=")

                # Skip lines that are not key=value
                if len(result) != 2:
                    (self.env_pattern if target_env == "env" else self.default_env_pattern).append(False)
                    continue

                key, value = result
                if value == "1":
                    value = True
                elif value == "0":
                    value = False
                dict[key] = value
                
                (self.env_pattern if target_env == "env" else self.default_env_pattern).append(True)

        return dict
    
    def __dict_to_file__(self, dict: Dict[str, object], file: TextIOWrapper) -> None:
        """
        Convert a dictionary to a file.
        
        Args:
            dict (Dict[str, object]): The dictionary to convert.
            file (str): The file to write to.
        """
        # Clear the file
        file.truncate(0)

        j = 0
        for key, value in dict.items():

            while self.default_env_pattern[j] == False:
                file.write("\n")
                j += 1

            if value == True:
                value = "1"
            elif value == False:
                value = "0"
            file.write(f"{key} = {value}\n")
            j += 1
    
    def apply_modifications(self, modifications: Dict[str, object]) -> None:
        """
        Apply modifications to the .env file.
        
        Args:
            modifications (Dict[str, object]): The modifications to apply.
        """
        for key, value in modifications.items():

            if key not in self.env.keys():
                continue
            if type(value) == str:
                value = f"\"{value}\""
            self.env[key] = value

        self.__dict_to_file__(self.env, open(self.env_path, "w", encoding="utf-8"))
            
class TkinterApp():
    """
    Class in charge of the user interface to select the paths and manage the .env file
    """
    env_builder: EnvBuilder
    path_builder: PathBuilder
    window: tk.Tk
    #The association between the widgets and the boolean variables depends on the order of the lists, do not change it
    procedurally_generated_widgets: list[tk.Widget]
    procedurally_generated_bool_vars: list[tk.BooleanVar]

    document_source_path: str = None
    table_destination_path: str = None
    localisation_data_path: str = None
    dump_path: str = None
    annotation_pdf_path: str = None
    tabula_path: str = None

    def __init__(self) -> None:
        """
        The constructor for TkinterApp class.
        """
        self.procedurally_generated_widgets = []
        self.procedurally_generated_bool_vars = []

        self.window = tk.Tk()
        self.window.title("Configuration editor")
        self.window.geometry("700x400")
        self.window.resizable(False, False)

        self.path_builder = PathBuilder(os.path.dirname(os.path.realpath(__file__)))
        self.path_builder.build_default_paths()

        self.env_builder = EnvBuilder()
        self.env_builder.clean_env()

    def start(self) -> None:
        """
        Start the user interface.
        """
        self.__create_widgets__()
        self.window.mainloop()
        

    def __create_widgets__(self) -> None:
        """
        Create the widgets for the user interface.
        """
        self.label = tk.Label(self.window, text="Choose the paths for the extraction process")
        self.label.pack()

        self.document_source_button = tk.Button(self.window, text="Document source path", command=self.__select_document_source__)
        self.document_source_button.pack()

        self.table_destination_button = tk.Button(self.window, text="CSV output path", command=self.__select_table_destination__)
        self.table_destination_button.pack()

        self.localisation_data_button = tk.Button(self.window, text="Localisation data path", command=self.__select_localisation_data__)
        self.localisation_data_button.pack()

        self.dump_path_button = tk.Button(self.window, text="Dump path", command=self.__select_dump_path__)
        self.dump_path_button.pack()

        self.annotation_pdf_path_button = tk.Button(self.window, text="PDF annotation path", command=self.__select_annotation_pdf_path__)
        self.annotation_pdf_path_button.pack()

        self.tabula_path_button = tk.Button(self.window, text="Tabula path", command=self.__select_tabula_path__)
        self.tabula_path_button.pack()

        for key, value in self.env_builder.env_variables.items():
            if type(value) != bool:
                continue

            var = tk.BooleanVar(value=value)
            self.procedurally_generated_widgets.append(tk.Checkbutton(self.window, text=key, variable=var))
            self.procedurally_generated_bool_vars.append(var)
        
        for widget in self.procedurally_generated_widgets:
            widget.pack()

        self.save_button = tk.Button(self.window, text="Save parameters", command=self.__save__)
        self.save_button.pack()

        self.reset_button = tk.Button(self.window, text="Reset to default", command=self.__reset__)
        self.reset_button.pack()

    def __select_document_source__(self) -> None:
        """
        Select the document source path.
        """
        selected_path = filedialog.askdirectory()
        if selected_path and selected_path != "":
            self.document_source_path = selected_path
        return
    
    def __select_table_destination__(self) -> None:
        """
        Select the table destination path.
        """
        selected_path = filedialog.askdirectory()
        if selected_path and selected_path != "":
            self.table_destination_path = selected_path
        return
    
    def __select_localisation_data__(self) -> None:
        """
        Select the localisation data path.
        """
        selected_path = filedialog.askdirectory()
        if selected_path and selected_path != "":
            self.localisation_data_path = selected_path
        return
    
    def __select_dump_path__(self) -> None:
        """
        Select the dump path.
        """
        selected_path = filedialog.askdirectory()

        if selected_path and selected_path != "":
            self.dump_path = selected_path
        return
    
    def __select_annotation_pdf_path__(self) -> None:
        """
        Select the annotation pdf path.
        """
        selected_path = filedialog.askdirectory()
        if selected_path and selected_path != "":
            self.annotation_pdf_path = selected_path
        return
        
    def __select_tabula_path__(self) -> None:
        """
        Select the tabula path.
        """
        selected_path = filedialog.askdirectory()
        if selected_path and selected_path != "":
            self.tabula_path = selected_path
        return

    def __save__(self) -> None:
        """
        Save the paths and configuration to the .env file.
        """
        modifications = {}
        for i in range(len(self.procedurally_generated_bool_vars)):
            modifications[self.procedurally_generated_widgets[i].cget("text")] = self.procedurally_generated_bool_vars[i].get()

        if self.document_source_path and self.document_source_path != "":
            modifications["DEFAULT_PATH"] = self.document_source_path.replace("/", "\\\\")
        if self.table_destination_path and self.table_destination_path != "":
            modifications["CSV_PATH"] = self.table_destination_path.replace("/", "\\\\")
        if self.localisation_data_path and self.localisation_data_path != "":
            modifications["LOCALISATION_DATA"] = self.localisation_data_path.replace("/", "\\\\")
        if self.dump_path and self.dump_path != "":
            modifications["DUMP_PATH"] = self.dump_path.replace("/", "\\\\")
        if self.annotation_pdf_path and self.annotation_pdf_path != "":
            modifications["ANNOTATION_PDF_PATH"] = self.annotation_pdf_path.replace("/", "\\\\")
        if self.tabula_path and self.tabula_path != "":
            modifications["MODEL_DATA_PATH"] = self.tabula_path.replace("/", "\\\\")

        self.env_builder.apply_modifications(modifications)
        self.window.quit()
        return

    def __reset__(self) -> None:
        """
        Reset the .env file to its original state.
        """
        self.env_builder.restore_env()
        i = 0
        for key, value in self.env_builder.default_env_variables.items():
            if type(value) != bool:
                continue

            self.procedurally_generated_bool_vars[i].set(value)
            i += 1

        return
    
app = TkinterApp()
app.start()


"""
PathBuilder tasks:
- Build the path for the document source
- Build the path for the table destination
- Build the path for the localisation data
- Build the path for the dump path
- Build the path for the annotation pdf path
- Build the path for the tabula path
- Validate the existence of the paths

EnvBuilder tasks:
- Build the .env file
- Modify the .env file
- Validate the .env file
- Add missing variables from the .envsrc file to the .env file
- Remove unused variables not present in .envsrc from the .env file
- Restore the .env file to its original state with the .envsrc file as a default
- Inform any collaborators of the variables, their values, and their types in the .env file

TkinterApp tasks:
- Create the main window
- Ask for the document source path
# TODO: Possibly add more options for the output paths so that they are not all in the same directory
- Ask for the outputs path
- Ask if the user wants pickle and dump files
- Allow the user to reset the .env file to its original state
- Allow to save the path and configuration changes to the .env file
"""